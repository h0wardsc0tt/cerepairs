<!---

	Copyright 2009, Code Craftings
 
Licensed under the Apache License, Version 2.0 (the "License"); 
you may not use this file except in compliance with the License.
You may obtain a copy of the License at 
	
	http://www.apache.org/licenses/LICENSE-2.0 
	
Unless required by applicable law or agreed to in writing, software 
distributed under the License is distributed on an "AS IS" BASIS, 
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
See the License for the specific language governing permissions and 
limitations under the License.

--->
<cfcomponent displayname="Handler" extends="BasePlugin">
	
	<cfset variables.name = "Blog Subscriber" />
	<cfset variables.id = "com.codecraftings.plugins.BlogSubscriber" />
	<cfset variables.package = "com/codecraftings/plugins/BlogSubscriber" />

<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->	
	<cffunction name="init" access="public" output="false" returntype="any">
		<cfargument name="mainManager" type="any" required="true" />
		<cfargument name="preferences" type="any" required="true" />
		
		<cfset var blogID = arguments.mainManager.getBlog().getId() />
		<cfset var newLine = "#chr(13)##chr(10)#" />
		<cfset var path = blogID & "/" & variables.package />
		
		<cfset variables.preferencesManager = arguments.preferences />
		<cfset variables.manager = arguments.mainManager />
		<cfset variables.blogSubscriberConfirm = variables.preferencesManager.get(path,"blogSubscriberConfirm","") />
		<cfset variables.blogSubscriberContent = variables.preferencesManager.get(path,"blogSubscriberContent","") />
		<cfset variables.blogSubscriberFrom = variables.preferencesManager.get(path,"blogSubscriberFrom","") />
		<cfset variables.blogSubscriberSubject = variables.preferencesManager.get(path,"blogSubscriberSubject","") />
		<cfset variables.blogSubscriberBody = variables.preferencesManager.get(path,"blogSubscriberBody","") />
		<cfset variables.title = variables.preferencesManager.get(path,"podTitle","") />

		<!--- Default confirmation to false --->
		<cfif len(variables.blogSubscriberConfirm) eq 0>
			<cfset variables.blogSubscriberConfirm = false />
		</cfif>
		
		<!--- Give default values for subject and body --->
		<cfif len(variables.blogSubscriberSubject) eq 0>
			<cfset variables.blogSubscriberSubject = "New posting on {blogTitle}" />			
		</cfif>
		<cfif len(variables.blogSubscriberBody) eq 0>
			<cfset variables.blogSubscriberBody = "A new posting has been added to {blogTitle}: {postTitle}#newLine##newLine#{postLink}#newLine##newLine#To unsubscribe: {unsubscribeLink}" />
		</cfif>
		
		<cfreturn this/>
	</cffunction>

<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->	
	<cffunction name="getName" access="public" output="false" returntype="string">
		<cfreturn variables.name />
	</cffunction>

<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->	
	<cffunction name="setName" access="public" output="false" returntype="void">
		<cfargument name="name" type="string" required="true" />
		<cfset variables.name = arguments.name />
		<cfreturn />
	</cffunction>

<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->	
	<cffunction name="getId" access="public" output="false" returntype="any">
		<cfreturn variables.id />
	</cffunction>
	
<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->	
	<cffunction name="setId" access="public" output="false" returntype="void">
		<cfargument name="id" type="any" required="true" />
		<cfset variables.id = arguments.id />
		<cfreturn />
	</cffunction>

<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->	
	<cffunction name="setup" hint="This is run when a plugin is activated" access="public" output="false" returntype="any">
		<cfset var path = variables.manager.getBlog().getId() & "/" & variables.package />
		<cfset var msg = "The Blog Subscriber plugin activated. Would you like to <a href='generic_settings.cfm?event=showBlogSubscriberSettings&amp;owner=BlogSubscriber&amp;selected=showBlogSubscriberSettings'>configure it now</a>?" />
		
		<cfreturn msg />
	</cffunction>
	
<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->	
	<cffunction name="unsetup" hint="This is run when a plugin is de-activated" access="public" output="false" returntype="any">
		<cfreturn />
	</cffunction>

<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->	
	<cffunction name="handleEvent" hint="Asynchronous event handling" access="public" output="false" returntype="any">
		<cfargument name="event" type="any" required="true" />	
			
			<cfset var body = "" />
			<cfset var email = "" />	
			<cfset var eventName = arguments.event.name />
			<cfset var mail = "" />
			<cfset var mailer = variables.manager.getMailer() />
			<cfset var oldPost = "" />
			<cfset var post = "" />
			<cfset var posting = "" />
			<cfset var postLink = "" />
			<cfset var sendEmail = false />
			<cfset var subject = "" />
			<cfset var tempBody = "" />
			<cfset var unsubscribe = variables.manager.getBlog().getUrl() & "generic.cfm?action=event&event=unsubscribeBlog&email" />

			<!--- New posting --->
			<cfif eventName eq "afterPostAdd">
				<cfset post = arguments.event.getNewItem() />
								
				<!--- Only send email if the posting is "published" --->
				<cfif compareNoCase(post.getStatus(),"published") eq 0>
					<cfset sendEmail = true />
					<cfset posting = variables.manager.getPostsManager().getPostById(post.id) />
					<cfset postlink = variables.manager.getBlog().getUrl() & posting.getURL() />
				</cfif>
			</cfif>
			
			<!--- Posting has been updated --->
			<cfif eventName eq "afterPostUpdate">
				<cfset post = arguments.event.getNewItem() />
				<cfset oldPost = arguments.event.getOldItem() />
				<cfset posting = variables.manager.getPostsManager().getPostById(oldPost.id) />
				<cfset postlink = variables.manager.getBlog().getUrl() & posting.getURL() />
				
				<!--- Only send email if status changed from "draft" to "published" --->
				<cfif compareNoCase(post.getStatus(),"published") eq 0 AND compareNoCase(oldPost.getStatus(),"draft") eq 0>
					<cfset sendEmail = true />
				</cfif>
			</cfif>
			
			<!--- Need to send an email --->
			<cfif sendEmail>
				<!--- Prep subject --->
				<cfset subject = replaceNoCase(variables.blogSubscriberSubject,"{postTitle}",post.getTitle(),"all") />
				<cfset subject = replaceNoCase(subject,"{blogUrl}",variables.manager.getBlog().getUrl(),"all") />
				<cfset subject = replaceNoCase(subject,"{blogTitle}",variables.manager.getBlog().getTitle(),"all") />

				<!--- Prep body --->
				<cfset body = replaceNoCase(variables.blogSubscriberBody,"{postTitle}",post.getTitle(),"all") />
				<cfset body = replaceNoCase(body,"{blogUrl}",variables.manager.getBlog().getUrl(),"all") />
				<cfset body = replaceNoCase(body,"{blogTitle}",variables.manager.getBlog().getTitle(),"all") />
				<cfset body = replaceNoCase(body,"{postLink}",postLink,"all") />
				
				<!--- Send email --->
				<cfloop list="#variables.blogSubscriberContent#" index="email">
					<!--- Set unsubscribe link --->
					<cfset tempBody = replaceNoCase(body,"{unsubscribeLink}","#unsubscribe#=#email#","all") />

					<cfset mail = structnew() />
					<cfset mail.from = variables.blogSubscriberFrom />
					<cfset mail.to = email />
					<cfset mail.subject = subject />
					<cfset mail.body = tempBody />
					
					<cfset mailer.sendEmail(argumentCollection=mail) />
				</cfloop>
			</cfif>
			
		<cfreturn />
	</cffunction>

<!--- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->	
	<cffunction name="processEvent" hint="Synchronous event handling" access="public" output="false" returntype="any">
		<cfargument name="event" type="any" required="true" />

			<cfset var blogSubscriberBody = "" />
			<cfset var blogSubscriberContent = "" />
			<cfset var blogSubscriberFrom = "" />
			<cfset var blogSubscriberSubject = "" />
			<cfset var confirmLink = variables.manager.getBlog().getUrl() & "generic.cfm?action=event&event=confirmSubscribeBlog&email" />
			<cfset var data = "" />
			<cfset var email = "" />
			<cfset var eventName = arguments.event.name />
			<cfset var itemID = "" />
			<cfset var link = structnew() />
			<cfset var mail = "" />
			<cfset var mailer = variables.manager.getMailer() />
			<cfset var newLine = "#chr(13)##chr(10)#" />
			<cfset var outputData = "" />
			<cfset var page = "" />
			<cfset var path = "" />
			<cfset var pod = structnew() />

			<cfswitch expression="#eventName#">
				
				<!--- Sign-up event --->
				<cfcase value="blogSubscribe">
					<cfset data = arguments.event.getData() />				

					<!--- If confirmation email required, send it now --->
					<cfif variables.blogSubscriberConfirm>
						<!--- Set unsubscribe link --->
						<cfset confirmLink = "#confirmLink#=#data.externaldata.subscribeEmail#" />

						<cfset mail = structnew() />
						<cfset mail.from = variables.blogSubscriberFrom />
						<cfset mail.to = data.externaldata.subscribeEmail />
						<cfset mail.subject = "Subscription Confirmation to #variables.manager.getBlog().getTitle()#" />
						<cfset mail.body = "Click the link below to confirm your subscription:#newLine##newLine##confirmLink#" />
						
						<cfset mailer.sendEmail(argumentCollection=mail) />						
					<cfelse>
						<cfif structkeyexists(data.externaldata,"apply")>
							<!--- Add if doesn't already exist --->
							<cfif listContainsNoCase(variables.blogSubscriberContent,data.externaldata.subscribeEmail) eq 0>
								<cfset variables.blogSubscriberContent = listAppend(variables.blogSubscriberContent,data.externaldata.subscribeEmail) />
								<cfset variables.blogSubscriberContent = listSort(variables.blogSubscriberContent,"Text") />
								<cfset path = variables.manager.getBlog().getId() & "/" & variables.package />
								<cfset variables.preferencesManager.put(path,"blogSubscriberContent",variables.blogSubscriberContent) />
							</cfif>
						</cfif>
					</cfif>
				</cfcase>
				
				<!--- Confirm subscription to blog --->
				<cfcase value="confirmSubscribeBlog">
					<cfset data = arguments.event.getData() />	
					<cfset email = data.externaldata.email />
					
					<!--- Add if doesn't already exist --->
					<cfif listContainsNoCase(variables.blogSubscriberContent,email) eq 0>
						<cfset variables.blogSubscriberContent = listAppend(variables.blogSubscriberContent,email) />
						<cfset variables.blogSubscriberContent = listSort(variables.blogSubscriberContent,"Text") />
						<cfset path = variables.manager.getBlog().getId() & "/" & variables.package />
						<cfset variables.preferencesManager.put(path,"blogSubscriberContent",variables.blogSubscriberContent) />
					</cfif>

					<cfsavecontent variable="page">
						<cfinclude template="subscribeConfirmForm.cfm" />
					</cfsavecontent>
					
					<!--- change message --->
					<cfset data.message.setTitle("Subscribed") />
					<cfset data.message.setData(page) />
				</cfcase>

				<!--- Display sign-up form --->
				<cfcase value="getPods">
					<cfsavecontent variable="page">
						<cfinclude template="admin/signupForm.cfm" />
					</cfsavecontent>
					<cfset arguments.event.setOutputData(page) />
					
					<cfset pod.title = variables.title />
					<cfset pod.content = page />
					<cfset pod.id = "BlogSubscriber" />
					<cfset arguments.event.addPod(pod)>
				</cfcase>
				
				<!--- Respond with pod info --->
				<cfcase value="getPodsList">
					<cfset pod = structnew() />
					<cfset pod.title = "Blog Subscriber" />
					<cfset pod.id = "BlogSubscriber" />
					<cfset arguments.event.addPod(pod) />
				</cfcase>
				
				<!--- Admin nav event --->	
				<cfcase value="settingsNav">
					<cfset link.owner = "BlogSubscriber" />
					<cfset link.page = "settings" />
					<cfset link.title = "Blog Subscriber" />
					<cfset link.eventName = "showBlogSubscriberSettings" />
					
					<cfset arguments.event.addLink(link)>
				</cfcase>
				
				<!--- Admin event --->
				<cfcase value="showBlogSubscriberSettings">
					<cfset data = arguments.event.getData() />				
					<cfif structkeyexists(data.externaldata,"apply")>
						<cfset variables.blogSubscriberConfirm = data.externaldata.blogSubscriberConfirm />
						<cfset variables.blogSubscriberFrom = data.externaldata.blogSubscriberFrom />
						<cfset variables.blogSubscriberSubject = data.externaldata.blogSubscriberSubject />
						<cfset variables.blogSubscriberBody = data.externaldata.blogSubscriberBody />
						
						<!--- Clean and sort the email list --->
						<cfset variables.blogSubscriberContent = replace(data.externaldata.blogSubscriberContent," ","","all") />
						<cfset variables.blogSubscriberContent = listSort(variables.blogSubscriberContent,"text") />
						
						<cfset path = variables.manager.getBlog().getId() & "/" & variables.package />
						<cfset variables.preferencesManager.put(path,"blogSubscriberConfirm",variables.blogSubscriberConfirm) />
						<cfset variables.preferencesManager.put(path,"blogSubscriberContent",variables.blogSubscriberContent) />
						<cfset variables.preferencesManager.put(path,"blogSubscriberFrom",variables.blogSubscriberFrom) />
						<cfset variables.preferencesManager.put(path,"blogSubscriberSubject",variables.blogSubscriberSubject) />
						<cfset variables.preferencesManager.put(path,"blogSubscriberBody",variables.blogSubscriberBody) />
				
						<cfset data.message.setstatus("success") />
						<cfset data.message.setType("settings") />
						<cfset data.message.settext("Blog Subscriber updated successfully") />
					</cfif>
					
					<cfsavecontent variable="page">
						<cfinclude template="admin/settingsForm.cfm" />
					</cfsavecontent>
						
					<!--- Set page title --->
					<cfset data.message.setTitle("Blog Subscriber Settings") />
					<cfset data.message.setData(page) />
				</cfcase>
				
				<!--- Unsubscribe user from blog emails --->
				<cfcase value="unsubscribeBlog">
					<cfset data = arguments.event.getData() />	
					<cfset email = data.externaldata.email />
					
					<!--- Remove email from list --->
					<cfset itemID = listFindNoCase(variables.blogSubscriberContent,email) />
					<cfif itemID gt 0>
						<cfset variables.blogSubscriberContent = listDeleteAt(variables.blogSubscriberContent,itemID) />
					</cfif>
					<cfset path = variables.manager.getBlog().getId() & "/" & variables.package />
					<cfset variables.preferencesManager.put(path,"blogSubscriberContent",variables.blogSubscriberContent) />

					<cfsavecontent variable="page">
						<cfinclude template="unsubscribeForm.cfm" />
					</cfsavecontent>
					
					<!--- change message --->
					<cfset data.message.setTitle("Unsubscribed") />
					<cfset data.message.setData(page) />
				</cfcase>
				
			</cfswitch>
			
		<cfreturn arguments.event />
	</cffunction>
	
</cfcomponent>