<cfoutput>

<form method="post" action="#cgi.script_name#">
	<fieldset>
		<legend>Settings</legend>
		<p>
			<label for="blogSubscriberConfirm">Require email confirmation for sign up?</label>
			<span class="hint">Subscribers will be required to click a link in a confirmation email to complete sign-up.</span>
			<span class="field">
				<input type="radio" name="blogSubscriberConfirm" id="blogSubscriberConfirmTrue" value="true" <cfif variables.blogSubscriberConfirm>checked="checked"</cfif> />Yes 
				<input type="radio" name="blogSubscriberConfirm" id="blogSubscriberConfirmFalse" value="false" <cfif not variables.blogSubscriberConfirm>checked="checked"</cfif> />No
			</span>
		</p>
		<p>
			<label for="blogSubscriberFrom">Send email from address:</label>
			<span class="hint">The email address that subscription emails will be sent from</span>
			<span class="field"><input type="text" name="blogSubscriberFrom" id="blogSubscriberFrom" value="#variables.blogSubscriberFrom#" size="50" class="required {email:true}" /></span>
		</p>
		<p>
			<label for="blogSubscriberSubject">Subject</label>
			<span class="hint">Available variables:<br />
				{blogTitle}<br />
				{blogUrl}<br />
				{postTitle}</span>
			<span class="field"><input type="text" id="blogSubscriberSubject" name="blogSubscriberSubject" value="#variables.blogSubscriberSubject#" size="60" class="required"/></span>
		</p>
		
		<p>
			<label for="blogSubscriberBody">Email content</label>
			<span class="hint">Available variables:<br />
				{blogTitle}<br />
				{blogUrl}<br />
				{postLink}<br />
				{postTitle}<br />
				{unsubscribeLink}: link to unsubscribe</span>
			<span class="field"><textarea id="blogSubscriberBody" name="blogSubscriberBody" cols="100" rows="8" class="required">#variables.blogSubscriberBody#</textarea></span>
		</p>
	</fieldset>
	
	<fieldset>
		<legend>List of Blog Subscribers</legend>
		<p>
			<label for="blogSubscriberContent">Separate email addresses with a comma</label>
			<span class="field"><textarea id="blogSubscriberContent" name="blogSubscriberContent" rows="10" cols="100">#replace(variables.blogSubscriberContent,",",", ","ALL")#</textarea></span>
		</p>
		
	</fieldset>
	<div class="actions">
	    <input type="submit" class="primaryAction" value="Submit"/>
		<input type="hidden" name="action" value="event" />
		<input type="hidden" name="event" value="showBlogSubscriberSettings" />
		<input type="hidden" name="apply" value="true" />
		<input type="hidden" name="selected" value="BlogSubscriber" />
	</div>
</form>

</cfoutput>