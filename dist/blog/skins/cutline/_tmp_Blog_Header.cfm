<cfimport prefix="mango" taglib="../../tags/mango">
<cfimport prefix="mangox" taglib="../../tags/mangoextras">
<cfimport prefix="template" taglib=".">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><mango:Blog title /> &#8212; <mango:Blog tagline /></title>
<meta http-equiv="X-UA-Compatible" content="IE=9" />
<meta name="generator" content="Mango <mango:Blog version />" />
<meta name="description" content="<mango:Blog description />" />
<meta name="robots" content="index, follow" />
<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<mango:Blog rssurl />" />
<link rel="alternate" type="application/atom+xml" title="Atom" href="<mango:Blog atomurl />" />	
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="<mango:Blog apiurl />" />
<mango:Event name="beforeHtmlHeadEnd" />
<link rel="stylesheet" href="<mango:Blog skinurl />assets/styles/style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<mango:Blog skinurl />assets/styles/custom.css" type="text/css" media="screen" />
<!--[if lte IE 7]>
<link rel="stylesheet" type="text/css" href="<mango:Blog skinurl />assets/styles/ie7.css" media="screen" />
<![endif]-->
<!--[if lte IE 6]>
<link rel="stylesheet" type="text/css" href="<mango:Blog skinurl />assets/styles/ie6.css" media="screen" />
<![endif]-->
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="/css/ui-custom/jquery-ui-1.8.22.custom.css" />
<script type="text/javascript" src="/js/jquery-1.8.3.min.js"></script>
<script src="/js/jquery-ui-1.8.21.custom.min.js" language="JavaScript" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.lightbox-0.5.min.js"></script>
<link rel="stylesheet" type="text/css" href="/css/style.box.css" media="screen" />
<link rel="stylesheet" type="text/css" href="/css/style.cer.css" />
<link rel="stylesheet" type="text/css" href="/css/style.ddm.css" />
<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-32497096-1']);
	_gaq.push(['_setDomainName', '.hme.com']);
	_gaq.push(['_trackPageview']);
	
	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
</script>
</head>
<body>
<div id="Page"><cfoutput>
	<div id="Header" class="clear">
		<div class="leftLogo">
			<a href="https://#APPLICATION.rootURL#"><img src="/images/celogo.png" /></a>
		</div>
		<div class="rightLogo">
			<h4>Call Us: 877-731-0334</h4>
			<div class="account clear">
				<ul>
					<li>
					<form name="form1" method="post" action="https://#APPLICATION.rootURL#/?pg=Search" />
						<div class="searchFrame">
						<input name="criteria" type="text" class="searchBox" id="criteria" size="8" value="Search" onFocus="this.value=''" />
						<input type="image" src="/images/search.png" class="searchBox_submit" value="" />
						</div>
					</form>
					</li>
					<li class="div_pipe"> | </li>
					<li class="cart">
						<a href="https://#APPLICATION.rootURL#/?pg=Cart">Cart</a><span id="cartcount"><cfif ((IsDefined("qry_getCart") AND IsQuery(qry_getCart)) AND qry_getCart.RecordCount NEQ 0)> (<strong>#qry_getCart.RecordCount#</strong>)</cfif></span>
						<div class="close_cart">
							<a href="javascript:void(0);" onclick="return closeAdd();" role="button"><span>CLOSE</span></a>
						</div>
						<div class="show_cart"></div>
					</li>
				</ul>
			</div>
		</div>
	</div>
<div id="Navbar" class="clear">
		<div class="main-nav">
			<ul id="nav" class="dropdown dropdown-horizontal">
				<li class="ddm1">
					<h4 class="small">Drive-Thru Repairs Acessories &amp; Sales</h4>
					<ul class="ddm2 clear">
						<li class="subhead2"><h5>Drive-Thru Repairs</h5>
							<ul>
								<li><a href="https://#APPLICATION.rootURL#/?pg=SendRepair"><img src="/images/sendrepair.png" /></a></li>
							</ul>
						</li>
						<li class="subhead3"><h5>Drive-Thru Accessories &amp; Sales</h5>
							<ul class="ddm3">
								<li class="manufact"><a href="https://#APPLICATION.rootURL#/?pg=Product&pmu=7SUHDG6FFPYDV2ZOMNFXJIVCPW9Z5O40">HME</a>
									<ul>
									<li><a href="https://#APPLICATION.rootURL#/?pg=Product&ptu=WVNYMCGU3QVY9UWFYP2GT59FBBLKAXZT">ION IQ</a></li><li><a href="https://#APPLICATION.rootURL#/?pg=Product&ptu=DHSM3W4R5NAC3L09DS1XFDSWOK9ZWHN0">Wireless IQ</a></li><li><a href="https://#APPLICATION.rootURL#/?pg=Product&ptu=AC165D4B079HH0RQY57DAW82MGWTBPFL">System 2000/2500</a></li><li><a href="https://#APPLICATION.rootURL#/?pg=Product&ptu=LB0H7GTJY41OPOM9G2EFANAIO77X4LSP">System 400</a></li><li><a href="https://#APPLICATION.rootURL#/?pg=Product&ptu=G3LZG48F2IMBIFT8PYUY77YRD3FFPF02">ZOOM Timer System</a></li><li><a href="https://#APPLICATION.rootURL#/?pg=Product&ptu=JNRWPKK8G7VS3WBY9BZNEL0NRWEBBRY4">DASH Timer System</a></li><li><a href="https://#APPLICATION.rootURL#/?pg=Product&ptu=N4FRYVRMFPACY548AN6F6NCFVUKZGJQQ">System 30A</a></li>
									</ul>
								</li>
								<li class="manufact"><a href="https://#APPLICATION.rootURL#/?pg=Product&pmu=30VFA9TK8LXGQD674U1Z20FDKWSLC8N3">Panasonic</a>
									<ul>
									<li><a href="https://#APPLICATION.rootURL#/?pg=Product&ptu=VMBJN5JSOSBFTQ9W7Z9UP9GIFYFGNABE">WX-C1020</a></li><li><a href="https://#APPLICATION.rootURL#/?pg=Product&ptu=EMF02DVUKMMH68JQNHI28ERNRE7H3ABA">Attune</a></li><li><a href="https://#APPLICATION.rootURL#/?pg=Product&ptu=QSFH5AT32P0SODSECDPWTVTPX7Y6O0VY">WX-C520</a></li><li><a href="https://#APPLICATION.rootURL#/?pg=Product&ptu=X3W22XDWFPKKQTX57KGJ9Z3YMT67GNA4">WX-C920</a></li><li><a href="https://#APPLICATION.rootURL#/?pg=Product&ptu=5VLFBYLI5OR4Z6H38YVS8LIG9FBZ1IZO">WX-C2020</a></li><li><a href="https://#APPLICATION.rootURL#/?pg=Product&ptu=PQN98FTE2FV3YBKG9PFLE21PZ6YBKZVT">WX-CH2050</a></li>
									</ul>
								</li>
								<li class="manufact"><a href="https://#APPLICATION.rootURL#/?pg=Product&pmu=XG822OZAORX6P4KPMU4I0OHWDJQASWU4">3M</a>
									<ul>
									<li><a href="https://#APPLICATION.rootURL#/?pg=Product&ptu=XQIL5T87W9M0K3X4XS070D9HOZ0VPX79">XT-1</a></li><li><a href="https://#APPLICATION.rootURL#/?pg=Product&ptu=XC51MM4JM1OYTNTAJPSJL9VRQ5DW0J30">C1060</a></li><li><a href="https://#APPLICATION.rootURL#/?pg=Product&ptu=6Q2U3KX2N7GZU8KAAGXQ9WDC9Y03B6JA">C960</a></li><li><a href="https://#APPLICATION.rootURL#/?pg=Product&ptu=P41WI64IY6WAP30M2KQAMF6TENZIP43Q">C760</a></li><li><a href="https://#APPLICATION.rootURL#/?pg=Product&ptu=FMOIJVUAAGBLY4ZJE3BQELS67VXLI1BR">Cabled Intercom System D-15</a></li>
									</ul>
								</li>
								<li class="manufact"><a href="https://#APPLICATION.rootURL#/?pg=Product&pmu=DBD4VG9F06M45F0G7CI5EIUBBQ0CCI5T">CE</a>
									<ul>
									<li><a href="https://#APPLICATION.rootURL#/?pg=Product&ptu=NVZTZVPLU8MMIVSTP0N76L0N86UNII61">CE Accessories</a></li><li><a href="https://#APPLICATION.rootURL#/?pg=Product&ptu=FQ511Z4ZDEV2Y8K2KORWXSA65OBR1D1P">Cabled Intercom System C-15</a></li>
									</ul>
								</li>
								<li class="manufact"><a href="https://#APPLICATION.rootURL#/?pg=Product&pmu=S85830CIVPZUVTEQ5H9SWGW6I9YVO3T3">Fast Track</a>
									<ul>
									<li><a href="https://#APPLICATION.rootURL#/?pg=Product&ptu=685SDXJYAJHW7X9CPFU2PRCT6U9YJ2B9">2+2 1000 Timer System</a></li><li><a href="https://#APPLICATION.rootURL#/?pg=Product&ptu=WGFUGYBANV7PPN6N38PLTKJAXI82DZZC">2+2 2000 Timer System</a></li>
									</ul>
								</li>
							</ul>
						</li>
					</ul>
				</li>
				
				<li class="ddm1">
					<a href="https://#APPLICATION.rootURL#/?pg=SendRepair">
						<h4>Send In A Repair</h4>
						<h5>Free Shipping</h5>
					</a>
				</li>
				<li class="ddm1">
					<h4>Why Choose CE?</h4>
					<h5>Drive-Thru Repairs</h5>
					<ul class="ddm2 clear">
						<li class="subhead nolight"><a href="https://#APPLICATION.rootURL#/about-ce">About CE</a></li>
						<li class="subhead nolight"><a href="https://#APPLICATION.rootURL#/about-our-repairs">About Our Repairs</a>
							<ul>
								<li><a href="https://#APPLICATION.rootURL#/Factory-Authorized-Repairs">Factory-Authorized Repairs</a></li>
								<li><a href="https://#APPLICATION.rootURL#/same-day-service">Same-Day Service</a></li>
								<li><a href="https://#APPLICATION.rootURL#/nationwide-installation">Nationwide Installation</a></li>
								<li><a href="https://#APPLICATION.rootURL#/send-in-a-repair-free-shipping">Free Inbound Shipping</a></li>
								<li><a href="https://#APPLICATION.rootURL#/tech-support">Drive-Thru Repair Technical Support</a></li>
							</ul>
						</li>
						<li class="subhead nolight"><a href="https://#APPLICATION.rootURL#/drive-thru-warranty-and-emas">Repair Warranty &amp; Maintenance Programs</a>
							<ul>
								<li><a href="https://#APPLICATION.rootURL#/4-month-warranty">4-Month Repair Warranty</a></li>
								<li><a href="https://#APPLICATION.rootURL#/CE-EMA">Monthly Repair Program</a></li>
								<li><a href="https://#APPLICATION.rootURL#/advanceexchange">Advance Exchange</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li class="ddm1">
					<h4>Special Offers</h4>
					<h5>Repairs &amp; Accessories</h5>
					<ul class="ddm2 clear">
						<li class="subhead nolight"><a href="https://#APPLICATION.rootURL#/new-customer-specials">New Customer Specials</a>
							<ul>
								<li><a href="https://#APPLICATION.rootURL#/half-off-aio-repairs">50% OFF Headset or Belt-Pac Repair</a></li>
								<!---20140114 CG: Removed Headset special per Ticket#48303 --->
								<!---<li><a href="https://#APPLICATION.rootURL#/freeheadset">Free Headset Special</a></li>--->
								<li><a href="https://#APPLICATION.rootURL#/freebattery">Free Battery Special</a></li>
							</ul>
						</li>
						<li class="subhead nolight"><a href="https://#APPLICATION.rootURL#/current-customer-specials">Current Customer Specials</a>
							<ul>
								<li><a href="https://#APPLICATION.rootURL#/bonusbucks">Bonus Bucks</a></li>
								<!--- 20140514 CG: Removed referal program section per Ticket#51757  --->
								<!---<li><a href="https://#APPLICATION.rootURL#/referralprogram">Referral Program</a></li>--->
								<li><a href="https://#APPLICATION.rootURL#/multistorediscount">Multi-Store Discount</a></li>
								<!---<li><a href="https://#APPLICATION.rootURL#/freeheadset">Free Headset Special</a></li>--->
								<li><a href="https://#APPLICATION.rootURL#/freebattery">Free Battery Special</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li class="ddm1">
					<h4>Help Center</h4>
					<h5>Drive-Thru Repairs</h5>
					<ul class="ddm2 clear">
						<li class="subhead nolight"><a href="https://#APPLICATION.rootURL#/user-manuals">User Manuals</a></li>
						<li class="subhead nolight"><a href="https://#APPLICATION.rootURL#/tech-support">Technical-support</a>
							<ul>
								<li><a href="https://#APPLICATION.rootURL#/Drive-Thru-Maintenance-Videos">Drive-Thru Maintenance Videos</a></li>
							</ul>
						</li>
						<li class="subhead nolight"><a href="https://#APPLICATION.rootURL#/drive-thru-repair-help-center">Help Center</a>
							<ul>
								<li><a href="https://#APPLICATION.rootURL#/drive-thru-faqs">FAQs</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li class="ddm1">
					<a href="https://#APPLICATION.rootURL#/blog">
						<h4>CE Blog</h4>
						<h5>Drive-Thru Repairs</h5>
					</a>
				</li>
				<li class="ddm1" style="border:none">
					<a href="https://#APPLICATION.rootURL#/contact-ce-drive-thru-repairs">
						<h4>Contact CE</h4>
						<h5>Drive-Thru Repairs</h5>
					</a>
				</li>
				
			</ul>
		</div>
	</div></cfoutput>
<div id="Content">
	<div class="col1" style="width:745px;">
		<div class="info">
			<div class="single clear">