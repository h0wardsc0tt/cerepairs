
<cfcomponent displayname="Pagination" hint="Handles Pagination Requests">
	<cffunction name="NextPrev" returntype="any" output="yes">
		<!---These vars control query records--->
		<cfargument name="QueryName" default="">
		<cfargument name="Str_Row" default="0">
		<cfargument name="Tot_Row" default="10">
		
		<!---These vars are for display output--->
		<cfargument name="navClass" default="NextPrev">
		<cfargument name="preClass" default="prev">
		<cfargument name="nexClass" default="next">
		<cfargument name="pagClass" default="pages">
		
		<cfscript>
			/*queryService = new query();
			queryService.setName("#QueryName#_total");
			queryService.setDBType("query");
			queryService.setAttributes(sourceQuery=#QueryName#);
			objQueryResult = queryService.execute(sql="SELECT Count(*) AS Tot_Records FROM sourceQuery");
			queryResult = objQueryResult.getResult();*/
		</cfscript>
		
		<!---<cfquery name="#QueryName#" dbtype="query">
			SELECT Count(*) AS Tot_Records
			FROM #QueryData#
		</cfquery>
		
		<cfset thisTotal = QueryName & ".Tot_Records">
		
		<cfset Pages = INT(thisTotal/Tot_Row)>
		
		<cfsavecontent variable="Pagination">
			<div class="#navClass#">
				<cfif IsNumeric(Str_Row) OR (Str_Row LT thisTotal AND Str_Row GT 1)>
				<div class="#preClass#">&lt; prev</div></cfif>
				<div class="#pagClass#"><cfloop from="1" to="#Pages#" index="i">#i#</cfloop></div>
				<div class="#nexClass#">next &gt;</div>
			</div>
		</cfsavecontent>
		
		<cfreturn Pagination>--->
	</cffunction>
</cfcomponent>