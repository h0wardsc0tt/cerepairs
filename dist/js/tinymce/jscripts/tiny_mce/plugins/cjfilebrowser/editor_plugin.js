/*globals window,tinyMCE*/
/*
	tinyMCE Editor Stub
	http://wiki.moxiecode.com/index.php/TinyMCE:Custom_filebrowser
*/
function CJFileBrowser_tinyMCE_Callback(a,b,c,d){var e;if(!b){e=tinyMCE.activeEditor.getParam('plugin_cjfilebrowser_browseUrl')+"?type="+c}else{e=window.location.toString();if(e.indexOf("?")<0){e=e+"?type="+c}else{e=e+"&type="+c}}tinyMCE.activeEditor.windowManager.open({file:e,title:'File Browser',width:typeof tinyMCE.activeEditor.getParam('plugin_cjfilebrowser_winWidth')==="number"?tinyMCE.activeEditor.getParam('plugin_cjfilebrowser_winWidth'):720,height:typeof tinyMCE.activeEditor.getParam('plugin_cjfilebrowser_winHeight')==="number"?tinyMCE.activeEditor.getParam('plugin_cjfilebrowser_winHeight'):500,resizable:"yes",inline:"yes",close_previous:"no"},{window:d,input:a});return false}