<cfcomponent name="CJ_FileBrowser" displayName="CJ FileBrowser" output="true" hint="Creative Juices TinyMCE File Browser by Doug Jones (www.cjboco.com)">
	
	<!--- ****************************************************************************
	
		CJ File Browser (ColdFusion 8 Engine)
		Version: 1.0
		Date Modified: April 14th, 2010
		
		Author: Doug Jones
		http://www.cjboco.com/
		
		Copyright (c) 2007-2010, Doug Jones. All rights reserved.
	
		Redistribution and use in source and binary forms, with or without
		modification, are permitted provided that the following conditions
		are met:
		
		a) Redistributions of source code must retain the above copyright
		notice, this list of conditions, the following disclaimer and any
		website links included within this document.
		
		b) Redistributions in binary form must reproduce the above copyright
		notice, this list of conditions, the following disclaimer and any
		website links included within this document in the documentation 
		and/or other materials provided with the distribution. 
		
		c) Neither the name of the Creative Juices, Bo. Co. nor the names of its
		contributors may be used to endorse or promote products derived from
		this software without specific prior written permission.
		
		THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
		"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
		LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
		A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
		OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
		SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
		LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
		DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
		THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
		
		For further information, visit the Creative Juices website: www.cjboco.com.
	
	**************************************************************************** --->
	
	<!--- these are theme specific, so know what your doing before changing these --->
	<cfset variables.thumb_width = 63 />
	<cfset variables.thumb_height = 87 />
	
	<!--- a list of valid image types that can be displated in a web browser (only valid WEB IMAGES) --->
	<cfset variables.webImgFileList = "gif,jpg,jpeg,png" />
	
	
	
	
	<!--- ------------------------------------------------------------------------
	
		isHandlerReady - Informs CJ File Browser that the handler exists
						 and also check to make sure the security.xml 
						 file is present and appears to be valid.
			
		Author: Doug Jones
		http://www.cjboco.com/
			
	------------------------------------------------------------------------ --->
	<cffunction name="isHandlerReady" access="remote" returntype="any" output="no">
		<cfargument name="version" type="string" required="yes" />
		<cfargument name="timeOut" type="numeric" required="no" default="900" />
		<cfset var locVar = StructNew() />
		<cfset var result = StructNew() />
		<cfset result.error = false />
		<cfset result.error_msg = ArrayNew(1) />
		<cftry>
			<cfif Len(arguments.version) gt 0>
				
				<!--- check to see if they passed a timout value --->
				<cfif isDefined("arguments.timeOut") and isNumeric(arguments.timeOut) and arguments.timeOut gt 0>
					<cfsetting requestTimeout="#arguments.timeOut#" />
				</cfif>
				
				<!--- check security.xml version --->
				<cfset locVar.version = getSecuritySettings('version') />
				<cfif isStruct(locVar.version) and isDefined("locVar.version.error") and NOT locVar.version.error>
					<cfif isDefined("locVar.version.version") and locVar.version.version eq arguments.version>
						<!--- everything ok --->
					<cfelse>
						<cfset result.error = true />
						<cfset ArrayAppend(result.error_msg, "Security.xml version does not match the CJ File Browser version.") />
					</cfif>
				<cfelse>
					<cfset result.error = true />
					<cfif isDefined("locVar.version.error_msg")>
						<cfset ArrayAppend(result.error_msg, locVar.version.error_msg) />
					<cfelse>
						<cfset ArrayAppend(result.error_msg, "Problems checking security.xml version.") />
					</cfif>
				</cfif>
				
				<!--- check security.xml directories --->
				<cfset locVar.validatePath = getSecuritySettings('directories') />
				<cfif isStruct(locVar.validatePath) and isDefined("locVar.validatePath.error") and NOT locVar.validatePath.error>
					<!--- everything checks out --->
				<cfelse>
					<cfset result.error = true />
					<cfif isDefined("locVar.version.error_msg")>
						<cfset ArrayAppend(result.error_msg, locVar.version.error_msg) />
					<cfelse>
						<cfset ArrayAppend(result.error_msg, "Problems checking security.xml authorized directories.") />
					</cfif>
				</cfif>
				
				<!--- check security.xml actions --->
				<cfset locVar.validateAction = getSecuritySettings('actions') />
				<cfif isStruct(locVar.validateAction) and isDefined("locVar.validateAction.error") and NOT locVar.validateAction.error>
					<!--- everything checks out --->
				<cfelse>
					<cfset result.error = true />
					<cfif isDefined("locVar.version.error_msg")>
						<cfset ArrayAppend(result.error_msg, locVar.version.error_msg) />
					<cfelse>
						<cfset ArrayAppend(result.error_msg, "Problems checking security.xml authorized actions.") />
					</cfif>
				</cfif>
				
				<!--- check security.xml file extensions --->
				<cfset locVar.isFileExtValid = getSecuritySettings('fileExts') />
				<cfif isStruct(locVar.isFileExtValid) and isDefined("locVar.isFileExtValid.error") and NOT locVar.isFileExtValid.error>
					<!--- everything checks out --->
				<cfelse>
					<cfset result.error = true />
					<cfif isDefined("locVar.version.error_msg")>
						<cfset ArrayAppend(result.error_msg, locVar.version.error_msg) />
					<cfelse>
						<cfset ArrayAppend(result.error_msg, "Problems checking security.xml authorized file extensions.") />
					</cfif>
				</cfif>
			
			<!--- we weren't passed the version --->
			<cfelse>
			
				<cfset result.error = true />
				<cfset ArrayAppend(result.error_msg, "Version information not provided.") />
				
			</cfif>
			<cfcatch type="any">
				<cfset result.error = true />
				<cfset ArrayAppend(result.error_msg, cfcatch.message) />
			</cfcatch>
		</cftry>
		<cfreturn result />
	</cffunction>
	
	
	<!--- ------------------------------------------------------------------------
	
		isPathValid - Validates that the given relative path is allowed
					  within the security settings.
			
		Author: Doug Jones
		http://www.cjboco.com/
			
	------------------------------------------------------------------------ --->
	<cffunction name="isPathValid" access="private" returntype="boolean" output="no" hint="Validates that the given relative path is allowed within the security settings.">
		<cfargument name="baseUrl" type="string" required="yes" />
		<cfargument name="exact" type="boolean" required="no" default="false" />
		<cfargument name="settings" type="any" required="no" default="" />
		<cfset var locVar = StructNew() />
		<cftry>
			<cfif Len(arguments.baseUrl) gt 0>
				<!--- to save time on disk reads, we can pass the directory list --->
				<cfif isStruct(arguments.settings) and isDefined("arguments.settings.error") and NOT arguments.settings.error and isDefined("arguments.settings.dirList") and ListLen(arguments.settings.dirList) gt 0>
					<cfset locVar.authDirs = arguments.settings />
				<cfelse>
					<cfset locVar.authDirs = getSecuritySettings('directories') />
				</cfif>
				<cfif isDefined("locVar.authDirs.error") and locVar.authDirs.error>
					<cfreturn false />
				<cfelseif isDefined("locVar.authDirs.error") and NOT locVar.authDirs.error and isDefined("locVar.authDirs.dirList") and ListLen(locVar.authDirs.dirList) gt 0>
					<cfloop index="locVar.dir" list="#locVar.authDirs.dirList#">
						<cfif arguments.exact>
							<cfif LCase(arguments.baseUrl) eq LCase(locVar.dir)>
								<cfreturn true />
								<cfbreak />
							</cfif>
						<cfelse>
							<cfif Left(LCase(arguments.baseUrl), Len(locVar.dir)) eq LCase(locVar.dir)>
								<cfreturn true />
								<cfbreak />
							</cfif>
						</cfif>
					</cfloop>
				</cfif>
			</cfif>
			<cfcatch type="any">
				<cfreturn false />
			</cfcatch>
		</cftry>
		<cfreturn false />
	</cffunction>
	
	
	<!--- ------------------------------------------------------------------------
	
		isActionValid - Validates that the given action is allowed
						within the security settings.
			
		Author: Doug Jones
		http://www.cjboco.com/
			
	------------------------------------------------------------------------ --->
	<cffunction name="isActionValid" access="private" returntype="boolean" output="no" hint="Validates that the given action is allowed within the security settings.">
		<cfargument name="userAction" type="string" required="yes" />
		<cfargument name="settings" type="any" required="no" default="" />
		<cfset var locVar = StructNew() />
		<cftry>
			<cfif Len(arguments.userAction) gt 0>
				<!--- to save time on disk reads, we can pass the directory list --->
				<cfif isStruct(arguments.settings) and isDefined("arguments.settings.error") and NOT arguments.settings.error and isDefined("arguments.settings.actionList") and ListLen(arguments.settings.actionList) gt 0>
					<cfset locVar.authActions = arguments.settings />
				<cfelse>
					<cfset locVar.authActions = getSecuritySettings('actions') />
				</cfif>
				<cfif isDefined("locVar.authActions.error") and locVar.authActions.error>
					<cfreturn false />
				<cfelseif isDefined("locVar.authActions.error") and NOT locVar.authActions.error and isDefined("locVar.authActions.actionList") and ListLen(locVar.authActions.actionList) gt 0>
					<cfif ListFindNoCase(locVar.authActions.actionList, arguments.userAction) gt 0>
						<cfreturn true />
					<cfelse>
						<cfreturn false />
					</cfif>
				</cfif>
			</cfif>
			<cfcatch type="any">
				<cfreturn false />
			</cfcatch>
		</cftry>
		<cfreturn false />
	</cffunction>
	
	
	<!--- ------------------------------------------------------------------------
	
		isFileExtValid - Validates that the given action is allowed
						 within the security settings.
			
		Author: Doug Jones
		http://www.cjboco.com/
			
	------------------------------------------------------------------------ --->
	<cffunction name="isFileExtValid" access="private" returntype="boolean" output="no" hint="Validates that the given file extension is allowed within the security settings.">
		<cfargument name="fileExt" type="string" required="yes" />
		<cfargument name="settings" type="any" required="no" default="" />
		<cfset var locVar = StructNew() />
		<cftry>
			<cfif Len(arguments.fileExt) gt 0>
				<!--- to save time on disk reads, we can pass the directory list --->
				<cfif isStruct(arguments.settings) and isDefined("arguments.settings.error") and NOT arguments.settings.error and isDefined("arguments.settings.extList") and ListLen(arguments.settings.extList) gt 0>
					<cfset locVar.authExts = arguments.settings />
				<cfelse>
					<cfset locVar.authExts = getSecuritySettings('fileExts') />
				</cfif>
				<cfif isDefined("locVar.authExts.error") and locVar.authExts.error>
					<cfreturn false />
				<cfelseif isDefined("locVar.authExts.error") and NOT locVar.authExts.error and isDefined("locVar.authExts.extList") and ListLen(locVar.authExts.extList) gt 0>
					<cfif locVar.authExts.extList eq "*">
						<cfreturn true />
					<cfelseif ListFindNoCase(locVar.authExts.extList, arguments.fileExt) gt 0>
						<cfreturn true />
					<cfelse>
						<cfreturn false />
					</cfif>
				</cfif>
			</cfif>
			<cfcatch type="any">
				<cfreturn false />
			</cfcatch>
		</cftry>
		<cfreturn false />
	</cffunction>
	
	
	<!--- ------------------------------------------------------------------------
	
		getSecuritySettings	- Reads the cjFileBrowser security.xml file and
							  returns no error with a comma seperated list of valid
							  directories that can be modified or an error 
							  with the error message
			
		Author: Doug Jones
		http://www.cjboco.com/
			
	------------------------------------------------------------------------ --->
	<cffunction name="getSecuritySettings" access="private" returntype="any" output="no" hint="Reads the cjFileBrowser security.xml file and returns a comma seperated list of valid  directories that can be modified.">
		<cfargument name="settingType" type="string" required="yes" />
		<cfset var locVar = StructNew() />
		<cfset var result = StructNew() />
		<cfset result.error = false />
		<cfset result.error_msg = "" />
		<cftry>
		
			<!--- called from assets/engines/ENGINE folder --->
			<cfif FileExists(ExpandPath('../../../security.xml'))>
				<cffile action="read" file="#ExpandPath('../../../security.xml')#" variable="locVar.xml">
				<cfset locVar.xml = XMLParse(locVar.xml) />
				<cfif isXml(locVar.xml)>
				
					<!--- security paths --->
					<cfif arguments.settingType eq "directories">
					
						<cfset locVar.xmlSettings = XMLSearch(locVar.xml,"cjFileBrowser/directoriesAllowed/directory") />
						<cfif ArrayLen(locVar.xmlSettings) gt 0>
							<cfset locVar.dirList = "" />
							<cfloop index="locVar.cnt" from="1" to="#ArrayLen(locVar.xmlSettings)#">
								<cfif Len(locVar.xmlSettings[locVar.cnt].XmlText) gt 0>
									<cfset locVar.dirList = ListAppend(locVar.dirList, locVar.xmlSettings[locVar.cnt].XmlText) />
								</cfif>
							</cfloop>
							<!--- they may have provided blank entries, this is not allowed --->
							<cfif ListLen(locVar.dirList) gt 0>
								<cfset result.error = false />
								<cfset result.dirList = locVar.dirList />
							<cfelse>
								<cfset result.error = true />
								<cfset result.error_msg = "There are no authorized directories set in the security.xml file. (Cannot be blank)" />
							</cfif>
						<cfelse>
							<cfset result.error = true />
							<cfset result.error_msg = "There are no authorized directories set in the security.xml file. (Cannot be blank)" />
						</cfif>
						
					
					<!--- security actions --->
					<cfelseif arguments.settingType eq "actions">
					
						<cfset locVar.xmlSettings = XMLSearch(locVar.xml,"cjFileBrowser/actionsAllowed/action") />
						<cfif ArrayLen(locVar.xmlSettings) gt 0>
							<cfset locVar.actionList = "" />
							<cfloop index="locVar.cnt" from="1" to="#ArrayLen(locVar.xmlSettings)#">
								<cfif Len(locVar.xmlSettings[locVar.cnt].XmlText) gt 0>
									<cfset locVar.actionList = ListAppend(locVar.actionList, locVar.xmlSettings[locVar.cnt].XmlText) />
								</cfif>
							</cfloop>
							<!--- they may have provided blank entries, this is not allowed --->
							<cfif ListLen(locVar.actionList) gt 0>
								<cfset result.error = false />
								<cfset result.actionList = locVar.actionList />
							<cfelse>
								<cfset result.error = true />
								<cfset result.error_msg = "There are no authorized actions set in the security.xml file. (No settings will not allow any action)" />
							</cfif>
						<cfelse>
							<cfset result.error = true />
							<cfset result.error_msg = "There are no authorized actions set in the security.xml file. (No settings will not allow any action)" />
						</cfif>
						
					
					<!--- security file extensions --->
					<cfelseif arguments.settingType eq "fileExts">
					
						<cfset locVar.xmlSettings = XMLSearch(locVar.xml,"cjFileBrowser/fileExtsAllowed/fileExt") />
						<cfif ArrayLen(locVar.xmlSettings) gt 0>
							<cfset locVar.extList = "" />
							<cfloop index="locVar.cnt" from="1" to="#ArrayLen(locVar.xmlSettings)#">
								<cfif Len(locVar.xmlSettings[locVar.cnt].XmlText) gt 0>
									<cfset locVar.extList = ListAppend(locVar.extList, Trim(locVar.xmlSettings[locVar.cnt].XmlText)) />
								</cfif>
							</cfloop>
							<!--- remove any spaced between the list items ", " or " ," --->
							<cfset locVar.extList = ReReplace(locVar.extList, "[\s]*,[\s]*",",","ALL") />
							<cfif ListLen(locVar.extList) gt 0>
								<cfset result.error = false />
								<cfset result.extList = locVar.extList />
							<cfelse>
								<cfset result.error = true />
								<cfset result.error_msg = "There are no authorized file extensions set in the security.xml file. (Cannot be blank)" />
							</cfif>
						<cfelse>
							<cfset result.error = true />
							<cfset result.error_msg = "There are no authorized file extensions set in the security.xml file. (Cannot be blank)" />
						</cfif>
						
					<!--- security file extensions --->
					<cfelseif arguments.settingType eq "version">
					
						<cfset result.error = false />
						<cfset result.version = locVar.xml.cjFileBrowser.XmlAttributes.version />
					
					<!--- unnknown security parameter --->
					<cfelse>
						
						<cfset result.error = true />
						<cfset result.error_msg = "Unknown security parameter check." />
					
					</cfif>
				
				<!--- not XML --->
				<cfelse>
				
					<cfset result.error = true />
					<cfset result.error_msg = "Problems reading in the security settings." />
					
				</cfif>
				
			<cfelse>
			
				<cfset result.error = true />
				<cfset result.error_msg = "Security.xml file could not be found." />
				
			</cfif>
			<cfcatch type="any">
				<cfset result.error = true />
				<cfset result.error_msg = cfcatch.message />
			</cfcatch>
		</cftry>
		<cfreturn result />
	</cffunction>
	
	
	<!--- ------------------------------------------------------------------------
	
		getDirectoryList - Reads and returns the contents of a given directory."",
		
		Author: Doug Jones
		http://www.cjboco.com/
			
	------------------------------------------------------------------------ --->
	<cffunction name="getDirectoryList" access="remote" output="false" returntype="any">
		<cfargument name="baseUrl" type="string" required="yes" />
		<cfargument name="fileExts" type="string" required="no" default="" />
		<cfargument name="showInv" type="boolean" required="no" default="false" />
		<cfargument name="timeOut" type="numeric" required="no" default="900" />
		<cfset var locVar = StructNew() />
		<cfset var result = StructNew() />
		<cftry>
			
			<!--- check to see if they passed a timout value --->
			<cfif isDefined("arguments.timeOut") and isNumeric(arguments.timeOut) and arguments.timeOut gt 0>
				<cfsetting requestTimeout="#arguments.timeOut#" />
			</cfif>
			
			<!--- preload our security settings (since we have to read this in each time) --->
			<cfset locVar.authDirs = getSecuritySettings('directories') />
			<cfset locVar.authActions = getSecuritySettings('actions') />
			<cfset locVar.authExts = getSecuritySettings('fileExts') />
			
			<!--- validate "navigateDirectory" action and path --->
			<cfif (NOT isActionValid("navigateDirectory",locVar.authActions) and NOT isPathValid(arguments.baseUrl,true,locVar.authDirs)) or NOT isPathValid(arguments.baseUrl,false,locVar.authDirs)>
				
				<cfset result.error = true />
				<cfset result.error_msg = "Directory access denied." />
			
			<!--- validate that the "baseUrl" exists ---> 
			<cfelseif NOT DirectoryExists(ExpandPath(URLDecode(arguments.baseUrl)))>
			
				<cfset result.error = true />
				<cfset result.error_msg = "Directory does not exist." />
				
			<cfelse>
			
				<cfset result.error = false />
				<cfset result.error_msg = "" />
				<cfset locVar.absBaseUrl = ExpandPath(arguments.baseUrl) />
				<cfset locVar.dir = ArrayNew(1) />
				
				<!--- read the directory and return contents--->
				<cfif DirectoryExists(locVar.absBaseUrl)>
					<cfdirectory action="list" directory="#locVar.absBaseUrl#" name="locVar.qry" sort="name" />
					<cfif locVar.qry.recordCount gt 0>
						<cfset locVar.idx = 1 />
						<cfloop query="locVar.qry">
							<cfif (arguments.showInv or (NOT arguments.showInv and Left(locVar.qry.name,1) neq "."))>
								<cfif locVar.qry.type eq "file" and Find(".",locVar.qry.name) gt 1>
									<cfset locVar.ext = LCase(ListLast(locVar.qry.name,'.')) />
								<cfelse>
									<cfset locVar.ext = "" />
								</cfif>
								<!--- remove any spaced between the list items ", " or " ," --->
								<cfset locVar.fileExts = ReReplace(arguments.fileExts, "[\s]*,[\s]*",",","ALL") />
								<cfif locVar.fileExts eq "*" or locVar.qry.type eq "Dir" or (locVar.fileExts neq "*" and ListFindNoCase(locVar.fileExts, locVar.ext) gt 0)>
									<cfif isFileExtValid(locVar.ext,locVar.authExts) or (locVar.qry.type eq "Dir" and isActionValid('navigateDirectory',locVar.authActions))>
										<cfif ArrayLen(locVar.dir) lt locVar.idx>
											<cfset ArrayAppend(locVar.dir, StructNew())>
										</cfif>
										<cfset locVar.dir[locVar.idx].name = HTMLEditFormat(locVar.qry.name) />
										<cfset locVar.dir[locVar.idx].size = locVar.qry.size />
										<cfset locVar.dir[locVar.idx].type = UCase(locVar.qry.type) />
										<cfset locVar.dir[locVar.idx].datelastmodified = locVar.qry.datelastmodified />
										<cfset locVar.dir[locVar.idx].attributes = locVar.qry.attributes />
										<cfset locVar.dir[locVar.idx].directory = arguments.baseUrl />
										<cfset locVar.dir[locVar.idx].extension = UCase(locVar.ext) />
										<cfif locVar.qry.type eq "file">
											<cfset locVar.dir[locVar.idx].mime = getPageContext().getServletContext().getMimeType(locVar.qry.name) />
										<cfelse>
											<cfset locVar.dir[locVar.idx].mime = "" />
										</cfif>
										<cfset locVar.dir[locVar.idx].fullpath = locVar.absBaseUrl & locVar.qry.name />
										<!--- is this an image file? we can pass dimensions if it is --->
										<cfif ListFindNoCase(variables.webImgFileList, locVar.ext) gt 0>
											<cfimage action="read" name="locVar.img_input" source="#locVar.dir[locVar.idx].fullpath#" />
											<cfset locVar.dir[locVar.idx].width = locVar.img_input["width"] />
											<cfset locVar.dir[locVar.idx].height = locVar.img_input["height"] />
										<cfelse>
											<cfset locVar.dir[locVar.idx].width = "" />
											<cfset locVar.dir[locVar.idx].height = "" />
										</cfif>
										<cfset locVar.idx = locVar.idx + 1 />
									</cfif>
								</cfif>
							</cfif>
						</cfloop>
					</cfif>
					<cfset result.dirlisting = locVar.dir />
					
				<cfelse>
				
					<!--- if no contents, then return an empty array --->
					<cfset result.dirlisting = ArrayNew(1) />
					
				</cfif>
				
			</cfif>
			<cfcatch type="any">
				<cfset result.error = true />
				<cfset result.error_msg = cfcatch.message />
			</cfcatch>
		</cftry>
		<cfreturn result />
	</cffunction>
	
	
	<!--- ------------------------------------------------------------------------
	
		getImageThumb	- Returns a STRUCT of image data which can be used
						  to display an images scaled preview (or thumb).
			
		Author: Doug Jones
		http://www.cjboco.com/
			
	------------------------------------------------------------------------ --->
	<cffunction name="getImageThumb" access="remote" output="false" returntype="any" hint="Returns a STRUCT of image data which can be used to display an images scaled preview (or thumb).">
		<cfargument name="baseUrl" type="string" required="yes" />
		<cfargument name="fileName" type="string" required="yes" />
		<cfargument name="elemID" type="string" required="yes" />
		<cfargument name="timeOut" type="numeric" required="no" default="900" />
		<cfset var locVar = StructNew() />
		<cfset var result = StructNew() />
		<cftry>
			
			<!--- check to see if they passed a timout value --->
			<cfif isDefined("arguments.timeOut") and isNumeric(arguments.timeOut) and arguments.timeOut gt 0>
				<cfsetting requestTimeout="#arguments.timeOut#" />
			</cfif>
			
			<!--- preload our security settings (since we have to read this in each time) --->
			<cfset locVar.authDirs = getSecuritySettings('directories') />
			<cfset locVar.authActions = getSecuritySettings('actions') />
		
			<!--- validate "navigateDirectory" action and path --->
			<cfif (NOT isActionValid("navigateDirectory",locVar.authActions) and NOT isPathValid(arguments.baseUrl,true,locVar.authDirs)) or NOT isPathValid(arguments.baseUrl,false,locVar.authDirs)>
			
				<cfset result.error = true />
				<cfset result.error_msg = "Directory access denied." />
			
			<!--- validate action ---> 
			<cfelseif NOT isActionValid("filePreviews",locVar.authActions)>
			
				<cfset result.error = true />
				<cfset result.error_msg = 'Image previews are not allowed.' />
			
			<!--- validate that the "baseUrl" exists ---> 
			<cfelseif NOT DirectoryExists(ExpandPath(URLDecode(arguments.baseUrl)))>
			
				<cfset result.error = true />
				<cfset result.error_msg = "Directory does not exist." />
				
			<cfelse>
			
				<cfset locVar.absFilePath = ExpandPath(arguments.baseUrl & arguments.fileName) />
				<cfset result.error = false />
				<cfset result.error_msg = "" />
				<cfset result.elemID = arguments.elemID />
				<cfset result.imgStr = "" />
				<cfif FileExists(locVar.absFilePath)>
					<cfimage action="read" name="locVar.img_input" source="#locVar.absFilePath#" />
					<cfif locVar.img_input["width"] gt variables.thumb_width or locVar.img_input["height"] gt variables.thumb_height>
						<cfset locVar.img_info = calcScaleInfo(locVar.img_input["width"], locVar.img_input["height"], variables.thumb_width, variables.thumb_height, "fit")>
						<cfset result.imgStr = '<img src="#arguments.baseUrl##arguments.fileName#" border="0" width="#locVar.img_info.width#" height="#locVar.img_info.height#" style="margin-top:#locVar.img_info.offset.y#px;margin-left:#locVar.img_info.offset.x#px;" />' />
					<cfelse>
						<cfset locVar.img_info.offset = StructNew() />
						<cfset locVar.img_info.offset.x = Int((variables.thumb_width / 2) - (locVar.img_input["width"] / 2)) />
						<cfset locVar.img_info.offset.y = Int((variables.thumb_height / 2) - (locVar.img_input["height"] / 2)) />
						<cfset result.imgStr = '<img src="#arguments.baseUrl##arguments.fileName#" border="0" width="#locVar.img_input["width"]#" height="#locVar.img_input["height"]#" style="margin-top:#locVar.img_info.offset.y#px;margin-left:#locVar.img_info.offset.x#px;" />' />
					</cfif>
				<cfelse>
					<cfset result.error = true />
					<cfset result.error_msg = "Problems reading image thumbnail. Invalid path. (#locVar.absFilePath#)" />
				</cfif>
				
			</cfif>
			<cfcatch type="any">
			
				<cfset result.error = true />
				<cfset result.error_msg = "Problems reading image thumbnail. (#cfcatch.message#)" />
				
			</cfcatch>
		</cftry>
		<cfreturn result />
	</cffunction>
	
	
	<!--- ------------------------------------------------------------------------
	
		doFileUpload - Uploads a file to the server (Handles a form POST operation)
		
		Author: Doug Jones
		http://www.cjboco.com/
			
	------------------------------------------------------------------------ --->
	<cffunction name="doFileUpload" access="remote" output="true" returntype="none">
		<cfset var locVar = StructNew() />
		<cfset var result = StructNew() />
		<cfset result.error = false />
		<cfset result.error_msg = ArrayNew(1) />
		<cftry>
			
			<!--- make sure this is a post operation --->
			<cfif cgi.request_method neq "post">
				
				<cfset result.error = true />
				<cfset result.error_msg = "HTTP request method not allowed." />
			
			<!--- double check we have all our variables --->
			<cfelseif 
				NOT StructKeyExists(arguments,"baseUrl") or
				NOT StructKeyExists(arguments,"maxWidth") or	
				NOT StructKeyExists(arguments,"maxHeight") or
				NOT StructKeyExists(arguments,"maxSize") or
				NOT StructKeyExists(arguments,"fileExts") or
				NOT StructKeyExists(arguments,"fileUploadField") or
				NOT StructKeyExists(arguments,"timeOut")>
			
				<cfset result.error = true />
				<cfset result.error_msg = "Could not complete upload. Required form variables missing." />
				
			<cfelse>
				
				<!--- check to see if they passed a timout value --->
				<cfif StructKeyExists(arguments,"timeOut") and isNumeric(arguments['timeOut']) and arguments['timeOut'] gt 0>
					<cfsetting requestTimeout="#arguments['timeOut']#" />
				</cfif>
				
				<!--- preload our security settings (since we have to read this in each time) --->
				<cfset locVar.authDirs = getSecuritySettings('directories') />
				<cfset locVar.authActions = getSecuritySettings('actions') />
				<cfset locVar.authExts = getSecuritySettings('fileExts') />
				<!---
					We can check the file size in the temp folder! Thanks Dave (aka Mister Dai)
					http://misterdai.wordpress.com/2010/02/26/upload-size-before-cffile-upload/
				--->
				<cfset locVar.fileSize = GetFileInfo(GetTempDirectory() & GetFileFromPath(arguments['fileUploadField'])) />
				
				<!--- validate "navigateDirectory" action and path --->
				<cfif (NOT isActionValid("navigateDirectory",locVar.authActions) and NOT isPathValid(arguments['baseUrl'],true,locVar.authDirs)) or NOT isPathValid(arguments['baseUrl'],false,locVar.authDirs)>
			
					<cfset result.error = true />
					<cfset result.error_msg = "Directory access denied." />
				
				<!--- validate action --->
				<cfelseif NOT isActionValid("fileUpload",locVar.authActions)>
				
					<cfset result.error = true />
					<cfset result.error_msg = "Not authorized to upload files." />
				
				<!--- validate that the "baseUrl" exists ---> 
				<cfelseif NOT DirectoryExists(ExpandPath(URLDecode(arguments['baseUrl'])))>
				
					<cfset result.error = true />
					<cfset result.error_msg = "Directory does not exist." />
				
				<cfelseif isDefined("locVar.fileSize.size") and locVar.fileSize.size gt (form.maxSize * 1024)>
				
					<cfset result.error = true />
					<cfset ArrayAppend(result.error_msg, "The file size of your upload excedes the allowable limit. Please upload a file smaller than #NumberFormat(form.maxSize,'9,999')#KB.")>
				
				<cfelse>
				
					<!--- <cfset form = arguments /> --->
					
					<!--- validate that we have the proper form fields --->
					<cfif NOT DirectoryExists(ExpandPath(URLDecode(form.baseUrl)))>
						<cfset result.error = true />
						<cfset ArrayAppend(result.error_msg, "You must provide a valid UPLOAD DIRECTORY.<br /><small>(Could not find directory)</small>")>
					</cfif>
					<cfif NOT isDefined("form.baseUrl") or (isDefined("form.baseUrl") and (Len(form.baseUrl) eq 0 or ReFind("[^a-zA-Z0-9\,\$\-\_\.\+\!\*\'\(\)\/]+",URLDecode(form.baseUrl)) gt 0))>
						<cfset result.error = true />
						<cfset ArrayAppend(result.error_msg, "Variable BASEURL not defined or invalid data.") />
					</cfif>
					<cfif NOT isDefined("form.fileExts") or (isDefined("form.fileExts") and form.fileExts neq "*" and ReFind("[^a-zA-Z0-9\,]+",URLDecode(form.fileExts)) gt 0)>
						<cfset result.error = true />
						<cfset ArrayAppend(result.error_msg, "Variable FILEEXTS not defined or invalid data.") />
					<cfelse>
						<!--- remove any spaced between the list items ", " or " ," --->
						<cfset form.fileExts = ReReplace(URLDecode(form.fileExts), "[\s]*,[\s]*",",","ALL") />
					</cfif>
					<cfif NOT isNumeric(form.maxSize) or (isNumeric(form.maxSize) and (form.maxSize lt 1 or form.maxSize gt 9999999))>
						<cfset result.error = true />
						<cfset ArrayAppend(result.error_msg, "Variable MAXSIZE not defined or invalid data.") />
					</cfif>
					<cfif NOT isNumeric(form.maxWidth) or (isNumeric(form.maxWidth) and (form.maxWidth lt 1 or form.maxSize gt 9999999))>
						<cfset result.error = true />
						<cfset ArrayAppend(result.error_msg, "Variable MAXWIDTH not defined or invalid data.") />
					</cfif>
					<cfif NOT isNumeric(form.maxHeight) or (isNumeric(form.maxHeight) and (form.maxHeight lt 1 or form.maxHeight gt 9999999))>
						<cfset result.error = true />
						<cfset ArrayAppend(result.error_msg, "Variable MAXHEIGHT not defined or invalid data.") />
					</cfif>
					<cfif NOT isDefined("form.fileUploadField") or (isDefined("form.fileUploadField") and Len(URLDecode(form.fileUploadField)) eq 0)>
						<cfset result.error = true />
						<cfset ArrayAppend(result.error_msg, "FILE INPUT FILED not defined or invalid data.") />
					</cfif>
					
					<cfif NOT result.error and ArrayLen(result.error_msg) eq 0>
						
						<!---
							File Upload Notes:
							I would have rather uploaded to the temp directory and then move the file. but ColdFusion doesn't seem to check for name
							conflicts on "rename" or "move". If anyone knows an easy way around this, please share.
						--->
						
						<!--- upload the file --->
						<cffile action="upload" filefield="fileUploadField" destination="#ExpandPath(URLDecode(form.baseUrl))#" nameconflict="makeunique" />
						
						<!--- if the file uploaded, then continue --->
						<cfif cffile.filewassaved eq "Yes" and cffile.fileSize lte (form.maxSize * 1024)>
						
							<!--- make sure that the uploaded file has the correct file extension (This still doesn't mean it VALID! --->
							<cfif isFileExtValid(cffile.serverFileExt,locVar.authExts) and (form.fileExts eq "*" or (form.fileExts neq "*" and ListFindNoCase(form.fileExts, cffile.serverFileExt) gt 0))>
								
								<!---
									check file name and move out of temp directory
								—————————————————————————————————————————————————————————————————————————————————————— --->
								<cfset locVar.file_name = safeFileName(cffile.serverFileName) & "." & cffile.serverFileExt />
								<cffile action="rename" source="#ExpandPath(URLDecode(form.baseUrl)&cffile.ServerFile)#" destination="#ExpandPath(URLDecode(form.baseUrl)&locVar.file_name)#" />
								
								<!--- CHECK TO SEE IF THE WAS AN IMAGE BEFORE WE SCALE IT --->
								<!--- We could use isImageFile(), but if CF8 is missing an update it could crash the server. --->
								<cfif ListFindNoCase(variables.webImgFileList, cffile.serverFileExt) gt 0 and isNumeric(form.maxWidth) and isNumeric(form.maxHeight)>
									<!---
										resize the image if it's to big
									—————————————————————————————————————————————————————————————————————————————————————— --->
									<cfimage action="read" name="locVar.temp_file" source="#ExpandPath('#URLDecode(form.baseUrl)##locVar.file_name#')#" />
									<cfif locVar.temp_file["width"] gt form.maxWidth or locVar.temp_file["height"] gt form.maxHeight>
										<cfset ImageSetAntialiasing(locVar.temp_file, "on") />
										<cfset locVar.img_info = calcScaleInfo(locVar.temp_file["width"], locVar.temp_file["height"], form.maxWidth, form.maxHeight, "fit") />
										<cfset ImageScaleToFit(locVar.temp_file, locVar.img_info.width, locVar.img_info.height, "highestQuality", "1") />
										<cfimage action="write" source="#locVar.temp_file#" destination="#ExpandPath('#URLDecode(form.baseUrl)##locVar.file_name#')#" overwrite="yes" />
									</cfif>
								
								</cfif>
							
							<!--- not a valid file extension --->
							<cfelse>
								
								<cfset result.error = true />
								<cfset ArrayAppend(result.error_msg, "You are not allowed to upload #UCase(cffile.serverFileExt)# files.") />
								
								<!--- delete the file --->
								<cffile action="delete" file="#ExpandPath(URLDecode(form.baseUrl)&cffile.ServerFile)#" />
						
							</cfif>
							
						<cfelseif cffile.filewassaved eq "Yes" and cffile.fileSize gt (form.maxSize * 1024)>
							<!--- file is too big --->
							<cfif FileExists(ExpandPath('#URLDecode(form.baseUrl)##cffile.ServerFile#'))>
								<cffile action="delete" file="#ExpandPath('#URLDecode(form.baseUrl)##cffile.ServerFile#')#" />
							</cfif>
							<cfset result.error = true />
							<cfset ArrayAppend(result.error_msg, "The file size of your upload excedes the allowable limit. Please upload a file smaller than #NumberFormat(form.maxSize,'9,999')#KB.")>
						<cfelse>
							<!--- cffile.filewassaved = "no" --->
							<cfset result.error = true />
							<cfset ArrayAppend(result.error_msg, "Problems encountered uploading the file.")>
						</cfif>
						
					<cfelse>
						<!--- ArrayLen(result.error_msg) > 0 --->
						<cfset result.error = true />
						<cfset result.error_msg = result.error_msg />
					</cfif>
					
				</cfif>
			</cfif>
			
			<cfcatch type="any">
				<cfset result.error = true />
				<cfset result.error_msg = cfcatch.message />
			</cfcatch>
		</cftry>
		
		<cfsavecontent variable="locVar.strHTML">
			<cfoutput>
				<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
				<html>
				<head></head>
				<body>#ReReplace(Trim(HTMLEditFormat(SerializeJSON(result))),"[\t]+","","ALL")#</body>
				</html>
			</cfoutput>
		</cfsavecontent>
		<cfset locVar.binResponse = ToBinary( ToBase64( locVar.strHTML ) ) />
		<cfcontent reset="true" /><cfheader name="content-length" value="#ArrayLen( locVar.binResponse )#" /><cfcontent type="text/html" variable="#locVar.binResponse#" />
	</cffunction>
	
	
	<!--- ------------------------------------------------------------------------
	
		doDeleteFile - Deletes a given file from the server.
		
		Author: Doug Jones
		http://www.cjboco.com/
			
	------------------------------------------------------------------------ --->
	<cffunction name="doDeleteFile" access="remote" output="false" returntype="any">
		<cfargument name="baseUrl" type="string" required="yes" />
		<cfargument name="fileName" type="string" required="yes" />
		<cfargument name="timeOut" type="numeric" required="no" default="900" />
		<cfset var locVar = StructNew() />
		<cfset var result = StructNew() />
		<cftry>
			
			<!--- check to see if they passed a timout value --->
			<cfif isDefined("arguments.timeOut") and isNumeric(arguments.timeOut) and arguments.timeOut gt 0>
				<cfsetting requestTimeout="#arguments.timeOut#" />
			</cfif>
			
			<!--- preload our security settings (since we have to read this in each time) --->
			<cfset locVar.authDirs = getSecuritySettings('directories') />
			<cfset locVar.authActions = getSecuritySettings('actions') />
			
			<!--- validate "navigateDirectory" action and path --->
			<cfif (NOT isActionValid("navigateDirectory",locVar.authActions) and NOT isPathValid(arguments.baseUrl,true,locVar.authDirs)) or NOT isPathValid(arguments.baseUrl,false,locVar.authDirs)>
			
				<cfset result.error = true />
				<cfset result.error_msg = "Directory access denied." />
			
			<!--- validate action --->
			<cfelseif NOT isActionValid("fileDelete",locVar.authActions)>
				
				<cfset result.error = true />
				<cfset result.error_msg = "Not authorized to delete files." />
			
			<!--- validate that the "baseUrl" exists ---> 
			<cfelseif NOT DirectoryExists(ExpandPath(URLDecode(arguments.baseUrl)))>
			
				<cfset result.error = true />
				<cfset result.error_msg = "Directory does not exist." />
				
			<cfelse>
			
				<cfif FileExists(ExpandPath("#arguments.baseUrl##arguments.fileName#"))>
					<cffile action="delete" file="#ExpandPath("#arguments.baseUrl##arguments.fileName#")#">
				</cfif>
				<cfset result.error = false />
				<cfset result.error_msg = "" />
				
			</cfif>
			<cfcatch type="any">
				<cfset result.error = true />
				<cfset result.error_msg = cfcatch.message />
			</cfcatch>
		</cftry>
		<cfreturn result />
	</cffunction>
	
	
	<!--- ------------------------------------------------------------------------
	
		doDeleteDirectory - Deletes a given directory and its contents from the server.
							This is a RECURSIVE function so it will delete
							everything inside the directory!
			
		Author: Doug Jones
		http://www.cjboco.com/
			
	------------------------------------------------------------------------ --->
	<cffunction name="doDeleteDirectory" access="remote" output="false" returntype="any">
		<cfargument name="baseUrl" type="string" required="yes" />
		<cfargument name="dirName" type="string" required="yes" />
		<cfargument name="timeOut" type="numeric" required="no" default="900" />
		<cfset var locVar = StructNew() />
		<cfset var result = StructNew() />
		<cftry>
			
			<!--- check to see if they passed a timout value --->
			<cfif isDefined("arguments.timeOut") and isNumeric(arguments.timeOut) and arguments.timeOut gt 0>
				<cfsetting requestTimeout="#arguments.timeOut#" />
			</cfif>
			
			<!--- preload our security settings (since we have to read this in each time) --->
			<cfset locVar.authDirs = getSecuritySettings('directories') />
			<cfset locVar.authActions = getSecuritySettings('actions') />
			
			<!--- validate "navigateDirectory" action and path --->
			<cfif (NOT isActionValid("navigateDirectory",locVar.authActions) and NOT isPathValid(arguments.baseUrl,true,locVar.authDirs)) or NOT isPathValid(arguments.baseUrl,false,locVar.authDirs)>
			
				<cfset result.error = true />
				<cfset result.error_msg = "Directory access denied." />
			
			<!--- validate action --->
			<cfelseif NOT isActionValid("deleteDirectory",locVar.authActions)>
			
				<cfset result.error = true />
				<cfset result.error_msg = "Deleting directories is not allowed." />
			
			<!--- validate that the "baseUrl" exists ---> 
			<cfelseif NOT DirectoryExists(ExpandPath(URLDecode(arguments.baseUrl)))>
			
				<cfset result.error = true />
				<cfset result.error_msg = "Directory does not exist." />
			
			<!--- make sure it's not the root directory!! --->
			<cfelseif Len(URLDecode(arguments.dirName)) eq 0 or URLDecode(arguments.dirName) eq "/" or URLDecode(arguments.dirName) eq "\">
			
				<cfset result.error = true />
				<cfset result.error_msg = "You cannot delete the root directory." />
				
			<cfelse>
			
				<!--- do a recursive delete --->
				<cfset result.error = deleteDirectory(ExpandPath("#arguments.baseUrl##arguments.dirName#"),true) />
				<cfif result.error eq true>
					<cfset result.error_msg = "" />
				<cfelse>
					<cfset result.error_msg = "There was a problem deleting the directory." />
				</cfif>
				
			</cfif>
			<cfcatch type="any">
				<cfset result.error = true />
				<cfset result.error_msg = cfcatch.message />
			</cfcatch>
		</cftry>
		<cfreturn result />
	</cffunction>
	
	
	<!--- ------------------------------------------------------------------------
	
		doCreateNewDirectory - Creates a new directory on the server.
		
		Author: Doug Jones
		http://www.cjboco.com/
			
	------------------------------------------------------------------------ --->
	<cffunction name="doCreateNewDirectory" access="remote" output="false" returntype="any">
		<cfargument name="baseUrl" type="string" required="yes" />
		<cfargument name="dirName" type="string" required="yes" />
		<cfargument name="timeOut" type="numeric" required="no" default="900" />
		<cfset var locVar = StructNew() />
		<cfset var result = StructNew() />
		<cftry>
			
			<!--- check to see if they passed a timout value --->
			<cfif isDefined("arguments.timeOut") and isNumeric(arguments.timeOut) and arguments.timeOut gt 0>
				<cfsetting requestTimeout="#arguments.timeOut#" />
			</cfif>
			
			<!--- preload our security settings (since we have to read this in each time) --->
			<cfset locVar.authDirs = getSecuritySettings('directories') />
			<cfset locVar.authActions = getSecuritySettings('actions') />
			
			<!--- validate "navigateDirectory" action and path --->
			<cfif (NOT isActionValid("navigateDirectory",locVar.authActions) and NOT isPathValid(arguments.baseUrl,true,locVar.authDirs)) or NOT isPathValid(arguments.baseUrl,false,locVar.authDirs)>
			
				<cfset result.error = true />
				<cfset result.error_msg = "Directory access denied." />
			
			<!--- validate action --->
			<cfelseif NOT isActionValid("createDirectory",locVar.authActions)>
			
				<cfset result.error = true />
				<cfset result.error_msg = "Creating directories is not allowed." />
			
			<!--- validate that the "baseUrl" exists ---> 
			<cfelseif NOT DirectoryExists(ExpandPath(URLDecode(arguments.baseUrl)))>
			
				<cfset result.error = true />
				<cfset result.error_msg = "Directory does not exist." />
				
			<cfelse>
			
				<!--- make sure we don't have any funky characters in the name --->
				<cfif ReFindNoCase("[^a-zA-Z0-9_\-]",arguments.dirName) gt 0>
					<cfset result.error = true />
					<cfset result.error_msg = "Invalid characters detected in directory name. (Valid [a-zA-Z0-9_-])" />
				<cfelseif Len(arguments.dirName) lt 1 or Len(arguments.dirName) gt 64>
					<cfset result.error = true />
					<cfset result.error_msg = "Invalid directory name length. (Valid 1-64 characters)" />
				<cfelse>
					<!--- make sure the directory doesn't already exists --->
					<cfif NOT DirectoryExists(ExpandPath(URLDecode("#arguments.baseUrl##arguments.dirName#")))>
						<cfdirectory action="create" directory="#ExpandPath(URLDecode('#arguments.baseUrl##arguments.dirName#'))#" >
						<cfset result.error = false />
						<cfset result.error_msg = "" />
					<cfelse>
						<cfset result.error = true />
						<cfset result.error_msg = "Directory already exists." />
					</cfif>
				</cfif>
				
			</cfif>
			<cfcatch type="any">
				<cfset result.error = true />
				<cfset result.error_msg = cfcatch.message />
			</cfcatch>
		</cftry>
		<cfreturn result />
	</cffunction>
	
	
	
	
	
	
	
	<!--- UTILITY FUNCTION (Not called directly from CJ File Browser) --->
	
	<!--- ------------------------------------------------------------------------
	
		Method:
			calcScaleInfo - A simple function that will return the width, height
							and offset of scaled image thumbnail.
		
		Arguments:
			srcWidth	- $Numeric (required)
						  The width of the source image.
						  
			srcHeight	- $Numeric (required)
						  The height of the source image.
			
			destWidth	- $Numeric (required)
						  The width of the destination image.
			
			destHeight	- $Numeric (required)
						  The height of the destination image.
			
			method		- $String (required)
						  The scaling method to use to calculate the thumbnail image dimensions.
		
		Return:
			STRUCT (JSON)
			
		** Do better understand what this function does, please visit my blog:
			http://www.cjboco.com/projects.cfm/project/cj-object-scaler/2.0.1
			http://www.cjboco.com/blog.cfm/post/easily-calculate-image-scaling
			
		Author: Doug Jones
		http://www.cjboco.com/
			
	------------------------------------------------------------------------ --->
	<cffunction name="calcScaleInfo" access="private" returntype="struct" output="false" hint="A simple function that will return the width, height and offset of scaled image thumbnail.">
		<cfargument name="srcWidth" type="numeric" required="yes" hint="The width of the source image." />
		<cfargument name="srcHeight" type="numeric" required="yes" hint="The height of the source image." />
		<cfargument name="destWidth" type="numeric" required="yes" hint="The width of the destination image." />
		<cfargument name="destHeight" type="numeric" required="yes" hint="The height of the destination image." />
		<cfargument name="method" type="string" required="no" default="fit" hint="The scaling method to use to calculate the thumbnail image dimensions." />
		<!--- Initialize our variables --->
		<cfset var err = "" />
		<cfset var scale = "" />
		<cfset var fits = false />
		<cfset var thumbInfo = StructNew() />
		<!--- Grab scale ratios --->
		<cfset var xscale = destWidth / srcWidth />
		<cfset var yscale = destHeight / srcHeight />
		<!--- Determine which scaling method is to be used --->
		<cfif method eq "fit">
			<cfset scale = Min(xscale, yscale) />
		<cfelseif method eq "fill">
			<cfset scale = Max(xscale, yscale) />
		</cfif>
		<!--- Determine if the destination is smaller or equal to the source image --->
		<cfif srcWidth gte destWidth and srcWidth gte destWidth>
			<cfset fits = true />
		</cfif>
		<!--- Set new dimensions --->
		<cfset err = StructInsert(thumbInfo, "width", Round(srcWidth * scale)) />
		<cfset err = StructInsert(thumbInfo, "height", Round(srcHeight * scale)) />
		<cfset err = StructInsert(thumbInfo, "offset", StructNew()) />
		<cfset err = StructInsert(thumbInfo.offset, "x", Round((destWidth - (srcWidth * scale)) / 2)) />
		<cfset err = StructInsert(thumbInfo.offset, "y", Round((destHeight - (srcHeight * scale)) / 2)) />
		<cfset err = StructInsert(thumbInfo, "fits", fits) />
		<!--- Return the information --->
		<cfreturn thumbInfo />
	</cffunction>
	
	
	<!--- ------------------------------------------------------------------------
	
		Method:
			safeFileName - 	Searches a string for illegal filename characters,
							strips them and then returns the modified string.
		
		Arguments:
			input   - $String (required)
						A filename that may require stripping of invalid characters.
						Example: my Fi&<lename.jpg
		
		Return:
			STRING
			
		Example:
			"my Fi&<lename.jpg" => "my_Fi_lename.jpg"
			
		Author: Doug Jones
		http://www.cjboco.com/
			
	------------------------------------------------------------------------ --->
	<cffunction name="safeFileName" access="private" returntype="string" output="no" hint="Searches a string for illegal filename characters, strips them and then returns the modified string.">
		<cfargument name="input" type="string" required="yes" />
		<cfset var output = arguments.input />
		<cfif Len(input) eq 0>
			<cfreturn "" />
		</cfif>
		<cfset output = REReplaceNoCase(output,'[^a-zA-Z0-9_]+','_','ALL')>
		<cfset output = REReplaceNoCase(output,'__','_','ALL')>
		<cfreturn LCase(output) />
	</cffunction>
	
	
	<!--- –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
	RECURSIVELY DELETE A DIRECTORY.
	Author: Rick Root (rick@webworksllc.com) 
		@param directory 	 The directory to delete. (Required)
		@param recurse 	 Whether or not the UDF should recurse. Defaults to false. (Optional)
		@return Return a boolean false = no-errors; true = error (DSJ 2010)
		@version 1, July 28, 2005 
	––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– --->
	<cffunction name="deleteDirectory" access="private" returntype="any" output="false">
		<cfargument name="directory" type="string" required="yes" />
		<cfargument name="recurse" type="boolean" required="no" default="false" />
		<cfset var myDirectory = "" />
		<cfset var count = 0 />
		<cftry>
			<cfif Right(arguments.directory, 1) is not "\" AND Right(arguments.directory, 1) is not "/">
				<cfif Find("\",arguments.directory) gt 0>
					<cfset arguments.directory = arguments.directory & "\" />
				<cfelse>
					<cfset arguments.directory = arguments.directory & "/" />
				</cfif>
			</cfif>
			<cfdirectory action="list" directory="#arguments.directory#" name="myDirectory">
			<cfloop query="myDirectory">
				<cfif myDirectory.name is not "." AND myDirectory.name is not "..">
					<cfset count = count + 1>
					<cfswitch expression="#myDirectory.type#">
						<cfcase value="dir">
							<!--- If recurse is on, move down to next level --->
							<cfif arguments.recurse>
								<cfset deleteDirectory(arguments.directory & myDirectory.name, arguments.recurse) />
							</cfif>
						</cfcase>
						<cfcase value="file">
							<!--- delete file --->
							<cfif arguments.recurse>
								<cffile action="delete" file="#arguments.directory##myDirectory.name#" />
							</cfif>
						</cfcase>			
					</cfswitch>
				</cfif>
			</cfloop>
			<cfif count is 0 or arguments.recurse>
				<cfdirectory action="delete" directory="#arguments.directory#" />
			</cfif>
			<cfreturn false />
			<cfcatch type="any">
				<cfreturn true />
			</cfcatch>
		</cftry>
	</cffunction>
	
</cfcomponent>
