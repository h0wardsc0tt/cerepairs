<cfsetting showdebugoutput="no">

<cfsilent>
<cfset countrydropdownlist = "United States, Afghanistan, Aland Islands, Albania, Algeria, American Samoa, Andorra, Angola, Anguilla, Antigua & Barbuda, Argentina, Armenia, Aruba, Australia, Austria, Azerbaijan, Bahamas, Bahrain, Bangladesh, Barbados, Belarus, Belgium, Belize, Benin, Bermuda, Bhutan, Bolivia, Bosnia, Botswana, Brazil, British Virgin Isles, Brunei, Bulgaria, Burkina Faso, Burundi, Cambodia, Cameroon, Canada, Cape Verde, Cayman Islands, Central African Republic, Chad, Chile, China People's Republic of, Colombia, Congo, Cook Islands, Costa Rica, Croatia, Cyprus, Czech Republic, Denmark, Djibouti, Dominica, Dominican Republic, Ecuador, Egypt, El Salvador, Equatorial Guinea, Eritrea, Estonia, Ethiopia, Faeroe Islands, Fiji, Finland, France, French Guiana, French Polynesia, Gabon, Gambia, Georgia, Germany, Ghana, Gibraltar, Greece, Greenland, Grenada, Guadeloupe, Guam, Guatemala, Guernsey, Guinea, Guinea-Bissau, Guyana, Haiti, Honduras, Hong Kong, Hungary, Iceland, India, Indonesia, Iraq, Ireland Republic of, Israel, Italy, Ivory Coast, Jamaica, Japan,  Jersey,Jordan, Kazakhstan, Kenya, Kiribati, Kuwait, Kyrgyzstan, Laos, Latvia, Lebanon, Lesotho, Liberia, Liechtenstein, Lithuania, Luxembourg, Macau, Macedonia (Fyrom), Madagascar, Malawi, Malaysia, Maldives, Mali, Malta, Marshall Islands, Martinique, Mauritania, Mauritius, Mexico, Micronesia, Moldova, Monaco, Mongolia, Montenegro, Montserrat, Morocco, Mozambique, N. Mariana Islands, Namibia, Nepal, Netherlands, Netherlands Antilles, New Caledonia, New Zealand, Nicaragua, Niger, Nigeria, Norfolk Island, Norway, Oman, Pakistan, Palau, Panama, Papua New Guinea, Paraguay, Peru, Philippines, Poland, Portugal, Puerto Rico, Qatar, Reunion, Romania, Russia, Rwanda, Samoa, San Marino, Saudi Arabia, Senegal, Serbia, Seychelles, Sierra Leone, Singapore, Slovakia, Solomon Islands, South Africa, South Korea, Spain, Sri Lanka, St. Kitts & Nevis, St. Lucia, St. Vincent/Grenadines, Suriname, Swaziland, Sweden, Switzerland, Syria, Taiwan, Tajikistan, Tanzania, Thailand, Togo, Trinidad & Tobago, Tunisia, Turkey, Turkmenistan, Turks & Caicos Islands, Tuvalu, Uganda, Ukraine, United Arab Emirates, United Kingdom, United States, Uruguay, US Virgin Islands, Uzbekistan, Vanatu, Venezuela, Vietnam, Wallia & Futuna Islands, Yemen, Zambia, Zimbabwe">
<cfparam name="FORM.SubmitInfo" default="">
<cfparam name="FORM.Lead_Title" default="">
<cfparam name="FORM.Lead_FirstName" default="">
<cfparam name="FORM.Lead_LastName" default="">
<cfparam name="FORM.Lead_Phone" default="">
<cfparam name="FORM.Lead_AltPhone" default="">
<cfparam name="FORM.Lead_Fax" default="">
<cfparam name="FORM.Lead_Email" default="">
<cfparam name="FORM.Lead_City" default="">
<cfparam name="FORM.Lead_State" default="">
<cfparam name="FORM.Lead_Zip" default="">
<cfparam name="FORM.Lead_Interest" default="">
<cfparam name="FORM.Lead_Company" default="">
<cfparam name="FORM.Lead_Address" default="">
<cfparam name="FORM.Lead_Website" default="">
<cfparam name="FORM.Lead_Location" default="">
<cfparam name="FORM.Lead_Comment" default="">

<cfparam name="ERR" default="#StructNew()#">
<cfparam name="ERR.ErrorFound" default="false">
<cfparam name="ERR.ErrorMessage" default="">

<cfparam name="URL.rl" default="#APPLICATION.rootURL##CGI.PATH_INFO#">
<cfparam name="URL.rs" default="Info Request from the Web">
<cfparam name="URL.rt" default="qsr">

<cfparam name="MailSent" default="false">
<cfparam name="EmailTo" default="">

<cfswitch expression="#URL.rt#">
	<cfcase value="cer">
		<cfset EmailTo = "info@ceinstl.com">
	</cfcase>
	<cfdefaultcase>
		<cfset EmailTo = "info@ceinstl.com">
	</cfdefaultcase>
</cfswitch>

<cfif IsDefined("FORM.SubmitInfo") AND Len(FORM.SubmitInfo) NEQ 0>
	<cfscript>
		//Check Req Fields
		if(ReFindNoCase("([A-Z].+)", FORM.Lead_FirstName) EQ 0) {
			ERR.ErrorFound = true;
			ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Please enter your first name");
		}
		if(ReFindNoCase("([A-Z].+)", FORM.Lead_LastName) EQ 0) {
			ERR.ErrorFound = true;
			ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Please enter your last name");
		}
		// 20150428 CG: Added Company and Address to required fields
		if(ReFindNoCase("([A-Z].+)", FORM.Lead_Company) EQ 0) {
			ERR.ErrorFound = true;
			ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Please enter your company");
		}
		if(ReFindNoCase("([A-Z].+)", FORM.Lead_Address) EQ 0) {
			ERR.ErrorFound = true;
			ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Please enter your address");
		}
		if(ReFindNoCase("([0-9].+)", FORM.Lead_Phone) EQ 0) {
			ERR.ErrorFound = true;
			ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Please enter a phone number");
		}
		if(ReFindNoCase("([A-Z].+)", FORM.Lead_Email) EQ 0) {
			ERR.ErrorFound = true;
			ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Please enter your email");
		} else {
			if(ReFindNoCase("^([a-zA-Z0-9_\-\.])+@(([0-2]?[0-5]?[0-5]\.[0-2]?[0-5]?[0-5]\.[0-2]?[0-5]?[0-5]\.[0-2]?[0-5]?[0-5])|((([a-zA-Z0-9\-])+\.)+([a-zA-Z\-])+))$", FORM.Lead_Email) EQ 0) {
				ERR.ErrorFound = true;
				ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Please enter a valid email address");
			}
		}
		if(ReFindNoCase("([A-Z].+)", FORM.Lead_State) EQ 0) {
			ERR.ErrorFound = true;
			ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Please enter your state");
		}
		if(ReFindNoCase("([A-Z].+)", FORM.Lead_Location) EQ 0) {
			ERR.ErrorFound = true;
			ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Please enter your location");
		}
	</cfscript>

	<cfif NOT ERR.ErrorFound>
		<cfinvoke component="_cfcs.Generate_UID" method="genUID" returnvariable="Lead_UID">
			<cfinvokeargument name="Length" value="32">
		</cfinvoke>
		
		<cfquery name="qry_insLead" datasource="db_leads">
			INSERT INTO tbl_Leads
				(Lead_UID,
				Lead_Title,
				Lead_FirstName,
				Lead_LastName,
				Lead_Phone,
				Lead_AltPhone,
				Lead_Fax,
				Lead_Email,
				Lead_City,
				Lead_State,
				Lead_Zip,
				Lead_Interest,
				Lead_Company,
				Lead_Address,
				Lead_Website,
				Lead_Location,
				Lead_Comment,
				Lead_Type,
				Lead_Origin,
				Lead_IP,
				Lead_UserAgent,
				Lead_Created_DTS)
			VALUES
				(<cfqueryparam cfsqltype="cf_sql_varchar" value="#Lead_UID#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.Lead_Title#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.Lead_FirstName#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.Lead_LastName#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.Lead_Phone#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.Lead_AltPhone#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.Lead_Fax#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.Lead_Email#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.Lead_City#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.Lead_State#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.Lead_Zip#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.Lead_Interest#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.Lead_Company#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.Lead_Address#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.Lead_Website#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.Lead_Location#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#FORM.Lead_Comment#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#URL.rs#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#URL.rl#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.REMOTE_ADDR#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.HTTP_USER_AGENT#">,
				<cfqueryparam cfsqltype="cf_sql_timestamp" value="#CreateODBCDateTime(Now())#">)
		</cfquery>

		<cfmail 
			to="#EmailTo#" 
			bcc="atnelson@hme.com" 
			from="no-reply@hme.com" 
			subject="#URL.rs#" 
			type="html">
			<table style="font-family:Arial, Helvetica, sans-serif">
				<tr>
					<td>Name: <strong>#FORM.Lead_FirstName# #FORM.Lead_LastName#</strong></td>
				</tr>
				<tr>
					<td>Company: <strong>#FORM.Lead_Company#</strong></td>
				</tr>
				<tr>
					<td>Email: <strong>#FORM.Lead_Email#</strong></td>
				</tr>
				<tr>
					<td>Website: <strong>#FORM.Lead_Website#</strong></td>
				</tr>
				<tr>
					<td>Phone: <strong>#FORM.Lead_Phone#</strong></td>
				</tr>
				<tr>
					<td>Alt Phone: <strong>#FORM.Lead_AltPhone#</strong></td>
				</tr>
				<tr>
					<td>Address: <strong>#FORM.Lead_Address#</strong></td>
				</tr>
				<tr>
					<td>City: <strong>#FORM.Lead_City#</strong></td>
				</tr>
				<tr>
					<td>State: <strong>#FORM.Lead_State#</strong></td>
				</tr>
				<tr>
					<td>Zip: <strong>#FORM.Lead_Zip#</strong></td>
				</tr>
				<tr>
					<td>Location: <strong>#FORM.Lead_Location#</strong></td>
				</tr>
				<tr>
					<td>Comment: <strong>#FORM.Lead_Comment#</strong></td>
				</tr>
				<tr>
					<td>Referred URL: <strong>http://#URL.rl#</strong></td>
				</tr>
				<tr>
					<td>Contact Information Submitted: <strong>#DateFormat(Now(), "mm/dd/yyyy")# - #TimeFormat(Now(), "HH:mm:ss")#</strong></td>
				</tr>
			</table>
		</cfmail>
		
		<cfset mailSent = true>
	</cfif>
</cfif>
</cfsilent>
<cfsavecontent variable="form_stuff">
<style type="text/css" media="all">
@import "/css/master.css";
.form_col1 {
	float:left;
	margin: 10px;
	width: 45%;
}
.form_col2 {
	float:right;
	margin: 10px;
	width: 45%;
}
.form_col_wide {
	margin: 10px;
	width: 95%;
}
.form_col_wide textarea {
	padding: 5px;
	height: 150px;
	width: 475px;
}
.form_col_wide div {
	margin-right: 10px;
	margin-top: 10px;
	text-align:right;
}
.form_col1 div, .form_col2 div {
	padding: 0 0 8px;
}
.form_col1 table {
	color: #666;
}
td.form_lname input[type=text] { 
	width: 100px;
	margin-right: 20px;
}
td.form_city input[type=text] {
	width: 100px;
	margin-right: 10px;
}
td.form_state input[type=text]  {
	width: 30px;
	margin-right: 10px;
}
td.form_zip input[type=text]  {
	width: 70px;
}
.req:before {
	color: #ff0000;
	content: "*";
}
#cboxIframe {
	overflow: auto;
}
</style>
</cfsavecontent>
<cfhtmlhead text="#form_stuff#">

<div class="promoformstyle">
<div style=" background: url('/images/grad_08.gif') repeat scroll 0 50% transparent;color: #FFFFFF;font-size: 1.17em;font-weight: bold; margin-bottom: 0;padding: 6px 6px 8px;top: -5px;">Information Request</div>
<br />
<br>

	<cfif ERR.ErrorFound>
	<strong class="red">Please complete/correct the following information:</strong>
	<ul class="red">
		<cfloop list="#ERR.ErrorMessage#" index="key"><cfoutput><li><strong>#key#</strong></li></cfoutput></cfloop>	   		
	</ul>
	</cfif>
	<cfif mailSent>
	<p class='example8'>Thank you for your interest in CE.<br /> We will process your request as soon as possible.</p>
	<cfelse>
	<cfoutput>
		<form action="#CGI.PATH_INFO#?rl=#URL.rl#&rs=#URL.rs#&rt=#URL.rt#" method="post" class="input-x" name="GetUserInfo">
		<div class="form_col1">
			<div>
				<table>
					<tr>
						<td><label for="Lead_FirstName" class="req">First Name</label></td>
						<td><label for="Lead_LastName" class="req">Last Name</label></td>
					</tr>
					<tr>
						<td class="form_lname"><input type="text" name="Lead_FirstName" value="#FORM.Lead_FirstName#" /></td>
						<td class="form_lname"><input type="text" name="Lead_LastName" value="#FORM.Lead_LastName#" /></td>
					</tr>
				</table>
			</div>
			<div>
				<label for="Lead_Phone" class="req">Phone Number</label><br />
				<input type="text" name="Lead_Phone" value="#FORM.Lead_Phone#" />
			</div>
			<div>
				<label for="Lead_AltPhone">Alternate Phone Number</label><br />
				<input type="text" name="Lead_AltPhone" value="#FORM.Lead_AltPhone#" />
			</div>
			<div>
				<label for="Lead_Fax">Fax</label><br />
				<input type="text" name="Lead_Fax" value="#FORM.Lead_Fax#" />
			</div>
			<div>
				<table>
					<tr>
						<td><label for="Lead_City">City</label></td>
						<td><label for="Lead_State" class="req">State</label></td>
						<td><label for="Lead_Zip">Zip</label></td>
					</tr>
					<tr>
						<td class="form_city"><input type="text" name="Lead_City" value="#FORM.Lead_City#" /></td>
						<td class="form_state"><input type="text" name="Lead_State" value="#FORM.Lead_State#" /></td>
						<td class="form_zip"><input type="text" name="Lead_Zip" value="#FORM.Lead_Zip#" /></td>
					</tr>
				</table>
			</div>
		</div>
		<div class="form_col2">
			<div>
				<label for="Lead_Company" class="req">Company</label><br />
				<input type="text" name="Lead_Company" value="#FORM.Lead_Company#" />
			</div>
			<div>
				<label for="Lead_Website">Website</label><br />
				<input type="text" name="Lead_Website" value="#FORM.Lead_Website#" />
			</div>
			<div>
				<label for="Lead_Email" class="req">Email</label><br />
				<input type="text" name="Lead_Email" value="#FORM.Lead_Email#" />
			</div>
			<div>
				<label for="Lead_Address" class="req">Address</label><br />
				<input type="text" name="Lead_Address" value="#FORM.Lead_Address#" />
			</div>
			<div>
				<label for="Lead_Location" class="req">Country</label><br />
				<select name="Lead_Location">
				<cfloop list="#countrydropdownlist#" index="thisCountry"><option value="#thisCountry#" <cfif FORM.Lead_Location IS thisCountry>selected="selected"</cfif>>#thisCountry#</option></cfloop>
				</select>
			</div>
		</div>
		<br clear="all" />
		<div class="form_col_wide">
			<label for="Lead_Comment">Comments</label>
			<textarea name="Lead_Comment">#FORM.Lead_Comment#</textarea>
			<div><span class="req">Required Field</span> <input class="submit" type="Submit" value="Submit" name="SubmitInfo" /></div>
		</div>
		</form>
		</cfoutput>
	</cfif>
	</div>
</div>