<cfsilent>
	<cfparam name="VARIABLES.subTotal" default="0">
	<cfscript>
		QUERY = StructNew();
		QUERY.QueryName = "qry_getCart";
		QUERY.Datasource = APPLICATION.Datasource;
		QUERY.Cart_Session_UID = SESSION.Session_UID;
		QUERY.Cart_User_Cookie = COOKIE.User_Cookie;
		QUERY.Cart_IsCompleted = 0;
		QUERY.OrderBy = "cart.Cart_DTS";
	</cfscript>
	<cfinclude template="/_qry_select_cart_product.cfm">
</cfsilent>

<cfsavecontent variable="UpdateQuantity">
	<script type="text/javascript"> 
		function actionQuantity(field_id) {
			var upd_field = document.getElementById(field_id + "_upd");
			upd_field.style.visibility = "visible";
			
			var sav_field = document.getElementById(field_id + "_sav");
			sav_field.style.visibility = "hidden";
		}
		function htmlQuantity(data,field_id,qty,base_price) {
			var ret_result = "";
			$.each(data, function(index, data) {
				if(data.subtotal != "fail") {
					var sub_field = document.getElementById("subprice");
					sub_field.innerHTML = data.subtotal;
					
					var tot_field = document.getElementById(field_id + "_tot");
					field_total = qty * base_price;
					field_total = "$" + field_total.toFixed(2);//Simple Dollar format
					tot_field.innerHTML = field_total;
					
					var sav_field = document.getElementById(field_id + "_sav");
					sav_field.style.visibility = "visible";
					
					var upd_field = document.getElementById(field_id + "_upd");
					upd_field.style.visibility = "hidden";
					ret_result = "pass";
				} else {
					ret_result = "fail";
				}
			});
			
			return ret_result;
		}
		function updateQuantity(field_id,qty,base_price) {
			var jqty_act = "/js/qtyCart.cfm";
			$.ajax({
				url: jqty_act, 
				data: ({ sess: '<cfoutput>#SESSION.Session_UID#</cfoutput>', pdt: field_id, qty: qty }),
				dataType: "json",
				success: function(data) {
					var set_msg = htmlQuantity(data,field_id,qty,base_price);
					if(set_msg == "fail") {
						alert("Error occured");
						<cfoutput>
						window.location.href = "https://#APPLICATION.rootURL#";
						</cfoutput>
					}
				}
			});
		}
		function confirmClear() {
			var $con_window = $('<div class="itemadded"></div>')
				.html('This will remove all of the items in your shopping cart. Are you sure?')
				.dialog({
					title: 'Confirm',
					modal:true,
					buttons: {
						"Remove All Items": function() {
							window.location = "./?pg=Cart&st=Clear";
						},
						Cancel: function() {
							$(this).dialog('close');
						}
					}
				});
		}
	</script>
</cfsavecontent>
<cfhtmlhead text="#UpdateQuantity#">

<cfoutput>
<div class="products clear">
	<h5>Cart</h5>
	<cfif qry_getCart.RecordCount NEQ 0>
	<form action="./?pg=Cart&st=Checkout" name="frm_cart" method="post" class="sscart">
	<table class="quick_cart">
		<tr>
			<th style="width:50%" align="left">Product</th>
			<th style="width:20%" align="right">Price</th>
			<th style="width:20%" align="right">Qty</th>
			<th style="width:10%"></th>
		</tr>
		<cfloop query="qry_getCart"><cfset VARIABLES.subTotal = VARIABLES.subTotal + (Cart_Product_QTY * Cart_Product_Price_SubTotal)><cfif qry_getCart.CurrentRow LT 5>
		<tr>
			<td valign="top"><p><cfif FileExists("#APPLICATION.rootDir#\images\products\#Product_Image_Thum#")><img src="/images/products/#Product_Image_Thum#" class="p_thumb_qv" /><cfelse><img src="/images/products/noimage.png" class="p_thumb_qv" /></cfif>
				#Product_Short_Description#</p>
				<p>Part ##: #Supplier_Part_Number#</p><cfsilent><!---Get Discounts--->
				<cfscript>
					QUERY = StructNew();
					QUERY.QueryName = "qry_getDiscounts";
					QUERY.Datasource = APPLICATION.Datasource;
					
					QUERY.Offer_IsActive = 1;
					QUERY.Offer_ValidTo = Now();
					QUERY.Offer_ValidFrom = Now();
					QUERY.Product_UID = Product_UID;
				</cfscript>
				<cfinclude template="/_qry_select_offer.cfm">
				</cfsilent><cfif qry_getDiscounts.RecordCount NEQ 0><br clear="all" />
				<div class="specialoffer">
					<p>#qry_getDiscounts.Offer_Description# - applied at checkout</p>
				</div>
				</cfif>
			</td>
			<td valign="top" class="p_tot"><div id="#Product_UID#_tot">#DollarFormat(Evaluate(Retail_Price*Cart_Product_QTY))#</div></td>
			<td class="qty">
				<input onkeyup="return actionQuantity('#Product_UID#');" onchange="return actionQuantity('#Product_UID#');" type="text" name="cqty_#Product_UID#" value="#Cart_Product_QTY#">
				<div id="#Product_UID#_upd" class="status" style="visibility:hidden;"><a onclick="return updateQuantity('#Product_UID#',frm_cart.cqty_#Product_UID#.value,#Retail_Price#);" href="javascript:void(false);">Update</a></div>
				<div id="#Product_UID#_sav" class="status ok" style="visibility:hidden;">Saved</div>
			</td>
			<td class="remp" style="top:50%"><a href="./?pg=Cart&st=Del&pdt=#Product_UID#">Remove</a></td>
		</tr></cfif></cfloop>
		<cfif qry_getCart.RecordCount GT 4>
		<tr>
			<td colspan="4">
				<div class="clearcart"><a href="./?pg=Cart">View Full Shopping Cart (#qry_getCart.RecordCount#)</a></div>
			</td>
		</tr>
		</cfif>
		<tr>
			<td colspan="4">
				<div class="clearcart"><a href="javascript:void(false);" onclick="return confirmClear();">Clear Shopping Cart</a></div>
				<div id="subprice" class="subtotal">Sub-Total:&nbsp;#DollarFormat(VARIABLES.subTotal)#</div>
			</td>
		</tr>
		<tr>
			<td colspan="4" class="checkout"><a href="javascript:void(0);" onclick="return closeAdd();" role="button"><img src="/images/conshop_but.png" /></a><input type="image" src="/images/cart_checkout.png" /></td>
		</tr>
	</table></form><cfelse>
	<table>
		<tr>
			<th colspan="2">Product</th>
			<th>Price</th>
			<th>Qty</th>
		</tr>
		<tr>
			<td colspan="3"><h3>Your cart is empty</h3></td>
		</tr>
	</table></cfif>
	<div class="clear"></div>
</div>
</cfoutput>