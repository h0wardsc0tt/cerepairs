<cfoutput>
<div class="products clear">
    <!---20161003 HRS Change to 3M logo to use text per Ticket_#77518 --->
	<cfif qry_getManufacturerProdType.ProductType_UID eq '3HMHWR7WJMZKB46G4G7MI7WJYUUTBKAE'>
        	<h2 class="small_text_logo_3m">3M<sup>&reg;&nbsp;*</sup></h2>
    <cfelse>
        <h2><cfif FileExists("#APPLICATION.rootDir#\images\#qry_getManufacturerProdType.Manufacturer_Logo#")><img src="/images/#qry_getManufacturerProdType.Manufacturer_Logo#" style="height:15px;width:auto;" /><cfelse><img src="/images/noimage.gif" /></cfif></h2>
    </cfif>
    <!--- End Ticket_#77518 --->
	<h4>Products and Accessories for use with #qry_getManufacturerProdType.Manufacturer_Name# Systems</h4>
	<div class="type_list clear">
		<cfloop query="qry_getManufacturerProdType">
        <cfif CurrentRow EQ 4>
        	<div class="clear"></div>
        </cfif>
		<div class="prod_type">
			<cfif FileExists("#APPLICATION.rootDir#\images\#ProductType_Image#")><a href="./?pg=Product&ptu=#ProductType_UID#"><img src="/images/#ProductType_Image#" /></a><cfelse><a href="./?pg=Product&ptu=#ProductType_UID#"><img src="/images/noimage.gif" /></a></cfif>
			<h3><a href="./?pg=Product&ptu=#ProductType_UID#">#ProductType#</a></h3>
		</div>
		</cfloop>
	</div>
	<div class="info type_desc">
		#qry_getManufacturerProdType.Manufacturer_Description#
	</div>
</div>
</cfoutput>
<div class="clear"></div>
<cfinclude template="./_tmp_product_recommended.cfm">