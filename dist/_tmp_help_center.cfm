<cfoutput>
<div class="info">
	<h1 class="pagetitle">CE DRIVE-THRU REPAIR<br>HELP CENTER</h1>
	<div class="single clear">
		<div class="plug">
			<div class="left_imgplug">
				<img src="/images/pages/Help_Center_Icon_01.jpg" />
			</div>
			<div class="left_textplug">
				<h1>Download Drive-Thru Repair User Manuals</h1>
				<p>Need help figuring out your drive-thru equipment? CE offers a list of available <a href="https://#APPLICATION.rootURL#/user-manuals">drive-thru repair user manuals</a> for HME equipment, or you can give us a call anytime at 877-731-0334.</p>
			</div>
		</div>
		<div class="plug">
			<div class="left_imgplug">
				<img src="/images/pages/Help_Center_Icon_02.jpg" />
			</div>
			<div class="left_textplug">
				<h1>Helpful Drive-Thru Repair Technical Support</h1>
				<p>Need some <a href="https://#APPLICATION.rootURL#/tech-support">drive-thru technical support</a>? CE offers product tutorials in the form of helpful <a href="https://#APPLICATION.rootURL#/Drive-Thru-Maintenance-Videos">"how-to"</a> videos.</p>
			</div>
		</div>
		<div class="plug">
			<div class="left_imgplug">
				<img src="/images/pages/Help_Center_Icon_03.jpg" />
			</div>
			<div class="left_textplug">
				<h1>Drive-Thru Repair FAQs</h1>
				<p>We've provided a list of <a href="https://#APPLICATION.rootURL#/drive-thru-faqs">frequently asked questions</a> about drive-thru repairs and products for your convenience.</p>
			</div>
		</div>
	</div>
</div>
</cfoutput>

