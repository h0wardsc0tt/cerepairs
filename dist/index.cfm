<!---<cfsilent>--->
<cfif CGI.HTTPS IS "off">
<!---
if the user browsed to here from the secure site, 
redirect them back to the non-secure if they're on any page except "Cart"
--->
	<!---<cfif NOT IsDefined("URL.pg") OR URL.pg IS NOT "Cart">
		<cfif CGI.QUERY_STRING CONTAINS "pgs=">
			<cfset clearString = ReplaceNoCase(CGI.QUERY_STRING, 'pgs=', '')>
			<cfset URL_Request = "https://#APPLICATION.rootURL##clearString#">
		<cfelse>
			<cfset URL_Request = "https://#APPLICATION.rootURL#/?#CGI.QUERY_STRING#">
		</cfif>
		<cflocation url="#URL_Request#" addtoken="no">
		<cfabort>
	</cfif>--->
</cfif>

<cfparam name="URL.pg" default="">
<cfparam name="URL.st" default="">
<cfparam name="URL.pc" default="">
<cfparam name="rl" default="#APPLICATION.rootURL##CGI.PATH_INFO#"> 
<cfparam name="rs" default="CE Sign Up For Special Offers"> 
<cfparam name="rt" default="cer"> 
<cfparam name="VARIABLES.Page_File" default="./_tmp_default.cfm">

<!--- 20150715 WW: User needs to enter a valid promo code in order to get discount Ticket#63410 --->
<cfif IsDefined("URL.promocode") AND Len(URL.promocode) NEQ 0>
	<cfset SESSION.Promo_Code = Trim(URL.promocode)>
</cfif>

<!--- if promo code is not present, use default promo flag from db --->

<cfif SESSION.Promo_Page IS "">
	<cfscript>
    	objPromo = CreateObject("component", "_cfcs.CartMgmt");
		qryPromo = objPromo.getDefaultPromoCode(SESSION.Promo_Code);
		
		SESSION.Promo_Banner = qryPromo.PromoCode;
        SESSION.Promo_Page = qryPromo.PromoPage;
    </cfscript>
</cfif>

<cfset PromoRelativePath = "./images/promos/">
<cfset PromoBanner = "#PromoRelativePath#/#SESSION.Promo_Banner#.jpg">
<cfset HomeBanner  = "CE-Banner-04-#SESSION.Promo_Banner#.jpg">
<!--- End of Ticket#63410 --->

<cfscript>
function get_QUERY_STRING(string) {
	var rawString = string;
	//HRS: CERepairs Google Ad Links not working Ticket#84000
	rawString = ReReplace(rawString,"&gclid.*","");
	//End  Ticket#84000
	var rawPath = getDirectoryFromPath(getCurrentTemplatePath());
	var retString = ReplaceNoCase(rawString, rawPath, "");

	return retString;
}

function get_Page(request) {
	var UserRequest = get_QUERY_STRING(request);
	var RequestArray = ListToArray(UserRequest, "/");
	var Page_UID = "";
	var Page_URL_String = "";
	
	for(i=1; i LTE ArrayLen(RequestArray); i=i+1) {
		if(ReFindNoCase("[a-z0-9]{32}", RequestArray[i]) GT 0) {
			Page_UID = RequestArray[i];
			break;
		} else {
			if(ReFindNoCase("[a-z]{2,}\-{0,1}",RequestArray[i]) GT 0) {
				Page_URL_String = RequestArray[i];
				break;
			}
		}
	}
	//get anchor tag
	if(FindNoCase("##", Page_URL_String) GT 0) {
		len_page = Len(Page_URL_String) - FindNoCase("##", Page_URL_String);
		jump = Right(Page_URL_String, len_page);
		jump = page_Jumper(jump);
		Page_URL_String = Mid(Page_URL_String, 1, Evaluate(FindNoCase("##", Page_URL_String)-1));
	}
	returnPage = CreateObject("component", "_cfcs.Content");
	thisPage = returnPage.getPage(Page_UID=Page_UID,Page_URL_String=Page_URL_String);
	
	return thisPage;
}

function page_Jumper(anchor) {
	jumpAnchor = CreateObject("component", "_cfcs.Content");
	jumpPage = jumpAnchor.createJump(anchor=anchor);
}
</cfscript>

<cfif StructKeyExists(URL, "pgs")>
	<cfset Content = ReplaceNoCase(CGI.QUERY_STRING, "pgs=", "")>
	<cfset Content = get_Page(Content)>
	<cfif Content.Tot_Records EQ 1>
		<cfset Page_Content = Content.Dat_Records.Page_Content>
		<cfset Page_Title = Content.Dat_Records.Page_Title>
		<cfset Page_MetaTitle = Content.Dat_Records.Page_MetaTitle>
		<cfset Page_MetaKeywords = Content.Dat_Records.Page_MetaKeywords>
		<cfset Page_MetaDescription = Content.Dat_Records.Page_MetaDescription>
		<cfset Page_BackgroundImage = Content.Dat_Records.Page_BackgroundImage>
		<cfset Page_Template = Content.Dat_Records.Page_Template>
		<cfif Len(Page_Template) GT 3>
			<cfset VARIABLES.Page_File = "./#Page_Template#">
		<cfelse>
			<cfset VARIABLES.Page_File = "./_tmp_info.cfm">
		</cfif>
	</cfif>
<cfelse>
	<cfset VARIABLES.Page_File = "./_tmp_default.cfm">
</cfif>

<!---20140114 CG: Removed freeheadset special per Ticket#48303 --->
<cfif CGI.QUERY_STRING EQ "pgs=/freeheadset">
	<cflocation url="index.cfm" addtoken="no">
</cfif>

<!---</cfsilent>--->

<cfinclude template="./_tmp_HTML_Header.cfm">
<div id="Content">
		<div class="col1">
<cfswitch expression="#URL.pg#">
	<cfcase value="NewHeadset">
		<cfinclude template="./_tmp_wizard.cfm">
	</cfcase>
	<cfcase value="SendRepair">
		<cfswitch expression="#URL.st#">
			<cfcase value="GetLabel">
				<cfinclude template="./_tmp_ship_process.cfm">
			</cfcase>
			<cfcase value="SendLabel">
				<cfinclude template="./_tmp_ship_email.cfm">
			</cfcase>
			<cfdefaultcase>
				<cfinclude template="./_tmp_ship_default.cfm">
			</cfdefaultcase>
		</cfswitch>
	</cfcase>
	<!--- 20140910 CG: Added Case for 10% email blast page Ticket#55275 --->
	<cfcase value="SendRepairBlast">
		<cfinclude template="./_tmp_ship_default_blast.cfm">
	</cfcase>
	<cfcase value="UpgHeadset">
		<cfinclude template="./_tmp_wizard.cfm">
	</cfcase>
	<cfcase value="NewTimer">
		<cfinclude template="./_tmp_wizard.cfm">
	</cfcase>
	<cfcase value="UpgTimer">
		<cfinclude template="./_tmp_wizard.cfm">
	</cfcase>
	<cfcase value="Cart">
		<cfswitch expression="#URL.st#">
			<cfcase value="Add">
				<cfinclude template="./_tmp_cart_add.cfm">
			</cfcase>
			<cfcase value="Del">
				<cfinclude template="./_tmp_cart_del.cfm">
			</cfcase>
			<cfcase value="Clear">
				<cfinclude template="./_tmp_cart_clr.cfm">
			</cfcase>
			<cfcase value="Qty">
				<cfinclude template="./_tmp_cart_qty.cfm">
			</cfcase>
			<cfcase value="Checkout">
				<cfinclude template="./_tmp_cart_cko.cfm">
			</cfcase>
			<cfcase value="Order">
				<cfinclude template="./_tmp_cart_ord.cfm">
			</cfcase>
			<cfcase value="Billing">
				<cfinclude template="./_tmp_cart_bil.cfm">
			</cfcase>
			<cfcase value="ReOrder">
				<cfinclude template="./_tmp_cart_reorder.cfm">
			</cfcase>
			<cfdefaultcase>
				<cfinclude template="./_tmp_cart_default.cfm">
			</cfdefaultcase>
		</cfswitch>
	</cfcase>
	<cfcase value="Product">
		<cfinclude template="./_tmp_product_default.cfm">
	</cfcase>
	<cfcase value="Detail">
		<cfinclude template="./_tmp_product_detail.cfm">
	</cfcase>
	<cfcase value="Wishlist">
		<cfswitch expression="#URL.st#">
			<cfcase value="Del">
				<cfinclude template="./_tmp_wishlist_del.cfm">
			</cfcase>
			<cfcase value="DelAll">
				<cfinclude template="./_tmp_wishlist_delall.cfm">
			</cfcase>
			<cfdefaultcase>
				<cfinclude template="./_tmp_product_wishlist.cfm">
			</cfdefaultcase>
		</cfswitch>
	</cfcase>
	<cfcase value="Account">
		<cfinclude template="./_tmp_account_default.cfm">
	</cfcase>
	<cfcase value="Search">
		<cfinclude template="./_tmp_product_search.cfm">
	</cfcase>
	<cfcase value="NoAuth">
		<cfinclude template="./_tmp_no_auth.cfm">
	</cfcase>
	<cfdefaultcase>
		<cfinclude template="#VARIABLES.Page_File#">
	</cfdefaultcase>
</cfswitch>
	</div>
	<div class="col2">
    	<cfoutput>
		<div class="offers"><a href="/js/requestinfo.cfm?rl=#rl#&rs=#rs#&rt=#rt#" target="emailblastform" id="blast" class="cboxElement"><img src="/images/signup_optin.png" /></a></div>
		<div class="offers"><a href="/CE-EMA"><img src="/images/protection-plan-ad.jpg" /></a></div>
		<!---20140114 CG: Removed freeheadset special per Ticket#48303 --->
		<!---<div class="offers"><a href="/freeheadset"><img src="/images/ad01.jpg" /></a></div>--->
        
        <!--- 20150715 WW: display promo banner depending on promo code or default promo Ticket#63410 --->
		<cfif FileExists(ExpandPath(PromoBanner))>
        	<div class="offers"><a href="/#SESSION.Promo_Page#"><img src="#PromoBanner#" /></a></div>
        </cfif>
		
        <div class="offers"><a href="/financing"><img src="/images/ad04.jpg" /></a></div>
		<div class="offers">
			<div class="textoffer_top"></div>
			<div class="textoffer">
				<div class="offer_text">
					<p>Free Shipping On All Inbound Repairs</p>
					<p><a href="./?pg=SendRepair">Learn More</a></p>
				</div>
				<div class="offer_text">
					<p>Can't Wait for the Repair? Advance Exchange Available</p>
					<p><a href="/advanceexchange">Learn More</a></p>
				</div>
				<div class="offer_text">
					<p>Same Day Repair Service or it's Free</p>
					<p><a href="/same-day-service">Learn More</a></p>
				</div>
				<div class="offer_text" style="border:none;">
					<p>Free, Live Tech Support on All Brands &amp; Models</p>
					<p><a href="/tech-support">Learn More</a></p>
				</div>
			</div>
			<div class="textoffer_bot"></div>
		</div>
        </cfoutput>
	</div>
</div>

<cfinclude template="./_tmp_HTML_Footer.cfm">