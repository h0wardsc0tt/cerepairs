<cfparam name="QUERY" default="#StructNew()#">
<cfparam name="QUERY.QueryName" default="qry_getInvoice">
<cfparam name="QUERY.Datasrouce" default="#APPLICATION.Datasource#">
<cfparam name="QUERY.SelectSQL" default="prod.*, invc.*">
<cfparam name="QUERY.OrderBy" default="invc.Invoice_Session_UID, invc.Invoice_Order, invc.Invoice_Created_DTS">

<cfquery name="#QUERY.QueryName#" datasource="#QUERY.Datasource#">
	SELECT #QUERY.SelectSQL#
	FROM 
		stbl_User_Invoice invc 
		INNER JOIN stbl_User_Cart_Products prod ON invc.Invoice_User_Cookie = prod.Cart_User_Cookie
	WHERE 0=0
	<cfif StructKeyExists(QUERY, "Session_UID")>
	AND invc.Invoice_Session_UID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.Session_UID#" list="yes">)
	</cfif>
	<cfif StructKeyExists(QUERY, "User_Cookie")>
	AND invc.Invoice_User_Cookie = <cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.User_Cookie#">
	</cfif>
	<cfif StructKeyExists(QUERY, "Date_From")>
	AND invc.Invoice_Created_DTS >= <cfqueryparam cfsqltype="cf_sql_date" value="#CreateODBCDate(QUERY.Date_From)#">
	</cfif>
	<cfif StructKeyExists(QUERY, "Date_To")>
	AND invc.Invoice_Created_DTS <= <cfqueryparam cfsqltype="cf_sql_date" value="#CreateODBCDate(QUERY.Date_To)#">
	</cfif>
	<cfif StructKeyExists(QUERY, "Cart_IsCompleted")>
	AND prod.Cart_IsCompleted IN (<cfqueryparam cfsqltype="cf_sql_tinyint" value="#QUERY.Cart_IsCompleted#" list="yes">)
	</cfif>
	<cfif StructKeyExists(QUERY, "GroupBy")>
	GROUP BY #QUERY.GroupBy#
	</cfif>
	ORDER BY #QUERY.OrderBy#
</cfquery>

<cfquery name="#QUERY.QueryName#_Total" dbtype="query">
	SELECT Count(*) AS Records
	FROM #QUERY.QueryName#
</cfquery>
<cfif StructClear(QUERY)></cfif>