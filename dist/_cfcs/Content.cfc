<cfcomponent displayname="InfoPages" hint="Controls page content on info pages">
	<cfparam name="APPLICATION.Datasource" default="#APPLICATION.Datasource#">
	
<!---Retrieve the page--->
	<cffunction name="getPage" output="no" returntype="struct" hint="Validates an item">
		<cfargument name="Page_UID" default="">
		<cfargument name="Page_URL_String" default="">
		
		<cfscript>
			QUERY = StructNew();
			QUERY.QueryName = "qry_getPage";
			QUERY.Datasource = APPLICATION.Datasource;
			if(Page_UID IS NOT "") {
				QUERY.Page_UID = Page_UID;
			}
			if(Page_URL_String IS NOT "") {
				QUERY.Page_URL_String = Page_URL_String;
			}
			QUERY.Page_IsActive = 1;
		</cfscript>
		
		<cfquery name="#QUERY.QueryName#" datasource="#QUERY.Datasource#">
			SELECT page.*
			FROM tbl_Pages page
			WHERE 0=0
			<cfif StructKeyExists(QUERY, "Page_UID")>
			AND page.Page_UID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.Page_UID#">
			</cfif>
			<cfif StructKeyExists(QUERY, "Page_URL_String")>
			AND page.Page_URL_String = <cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.Page_URL_String#">
			</cfif>
		</cfquery>
		<cfif StructClear(QUERY)></cfif>
		<!---cfinclude template="/_qry_select_page.cfm">--->
		
		<cfscript>
			Ret_Page = StructNew();
			if(qry_getPage.RecordCount EQ 1) { //Only Return Single Records Since Only One Page Should Be Returned Per Query
				Ret_Page.Tot_Records = qry_getPage.RecordCount;
				Ret_Page.Dat_Records = qry_getPage;
			} else {
				Ret_Page.Tot_Records = 0;
				Ret_Page.Dat_Records = "";
			}
		</cfscript>
		
		<cfreturn Ret_Page>
	</cffunction>
<!---Create Page Jump for Anchor Tags--->
	<cffunction name="createJump" returntype="any" output="yes">
		<cfargument name="anchor" required="yes" type="string">
		
		<cfsavecontent variable="HTML_PageJumper">
			<script language="javascript" type="text/javascript">
				$(function() {
					$(document).scrollTop( $("###anchor#").offset().top );  
				});
			</script>
		</cfsavecontent>
		
		<cfhtmlhead text="#HTML_PageJumper#">
	</cffunction>
</cfcomponent>