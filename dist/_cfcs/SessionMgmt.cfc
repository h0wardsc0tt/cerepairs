<cfcomponent 
	displayname="Session" 
	output="yes" 
	hint="Initialize User Session">
	
	<cfparam name="THIS.SessionTimeout" default="#APPLICATION.SessionTimeout#">
	
	<cffunction 
		name="initSession" 
		returntype="void" 
		hint="Initializes Session">
		
		<!---Get SESSION_UID--->
		<cflock scope="session" type="exclusive" timeout="#THIS.SessionTimeout#">
			<cfinvoke 
				method="getSession_UID" 
				returnvariable="Session_UID"></cfinvoke>
			<cfset SESSION.Session_UID = Session_UID>
		</cflock>
        
        <cfset SESSION.Promo_Code = "">
        <cfset SESSION.Promo_Page = "">
        <cfset SESSION.Promo_Banner = "">
		
		<cfset defaultCookie_exp = DateAdd("d", 30, Now())> <!---30 days from today--->
		<cfif NOT StructKeyExists(COOKIE, "USER_COOKIE")><!---Remember User Browser Must Be Setup to Allow COOKIES--->
			<cfscript>
				getUID = CreateObject("component", "_cfcs.Generate_UID");
				thisUser_Cookie = getUID.genUID(Length=32);
			</cfscript>
			<cfcookie name="USER_COOKIE" value="#thisUser_Cookie#" expires="#defaultCookie_exp#" domain=".cerepairs.com">
		<cfelse>
			<cfcookie name="USER_COOKIE" value="#COOKIE.User_Cookie#" expires="#defaultCookie_exp#" domain=".cerepairs.com">
		</cfif>
		
		<!---Track Session--->
		<cfinvoke method="TrackSession">
			<cfinvokeargument name="Session_UID" value="#SESSION.Session_UID#">
			<cfinvokeargument name="User_Cookie" value="#COOKIE.User_Cookie#">
		</cfinvoke>
	</cffunction>
	
	<cffunction 
		name="extnSession" 
		returntype="void" 
		hint="Renews Current Session">
		
		<cfargument name="session_uid" type="string" required="yes">
		<!---Verify Session--->
		<!---///Place Holder. Need to write routine--->
		
		<!---Renew Session--->
		<cflock scope="session" type="exclusive" timeout="#THIS.SessionTimeout#">
			<cfset SESSION.SESSION_UID = SESSION_UID>
		</cflock>
		<!---<cfheader 
			name="Set-Cookie" 
			value="session_uid=#SESSION.SESSION_UID#;expires=##THIS.SessionTimeout##;HTTPOnly;secure=true;">--->
	</cffunction>
	
	<cffunction 
		name="purgeSession" 
		returntype="void" 
		hint="Purges dtbl_User_Session of any events > Now()-1day">
		
		<cfargument name="RemoveDate" default="#DateAdd('d', -1, Now())#">
		
		<cfscript>
			QUERY = StructNew();
			QUERY.QueryName = "qry_delOldSessions";
			QUERY.Datasource = APPLICATION.Datasource;
			QUERY.fromDate = RemoveDate;
		</cfscript>
		
		<cfquery name="#QUERY.QueryName#" datasource="#QUERY.Datasource#">
			DELETE 
			FROM dtbl_User_Session
			WHERE User_DTS <= <cfqueryparam cfsqltype="cf_sql_date" value="#QUERY.fromDate#">
		</cfquery>
		<cfif StructClear(QUERY)></cfif>
	</cffunction>
	
	<cffunction 
		name="getSession_UID" 
		returntype="string" 
		access="public"
		hint="Gets new Session_UID">
		
		<cfscript>
			getUID = CreateObject("component", "_cfcs.Generate_UID");
			thisSESSION_UID = getUID.genUID(Length=32);
		</cfscript>
		
		<cfreturn thisSESSION_UID>
	</cffunction>
	
	<cffunction 
		name="veriSession" 
		returntype="any" 
		hint="Creates DB Record" 
		access="public">
		
		<cfargument name="Session_UID" default="" type="string" required="yes">
		
		<cfscript>
			QUERY = StructNew();
			QUERY.QueryName = "qry_GetSess";
			QUERY.Datasource = APPLICATION.Datasource;
			QUERY.Session_UID = Session_UID;
			QUERY.MaxSession = DateAdd('n', -20, Now()); //Set Max Session Timeout
		</cfscript>
		
		<cfquery name="#QUERY.QueryName#" datasource="#QUERY.Datasource#">
			SELECT sess.*
			FROM dtbl_User_Session sess
			WHERE 0=0
			<cfif StructKeyExists(QUERY, "Session_UID")>
			AND User_Session_UID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Session_UID#">
			</cfif>
			AND User_DTS >= <cfqueryparam cfsqltype="cf_sql_timestamp" value="#QUERY.MaxSession#">
		</cfquery>
		
		<cfif qry_GetSess.RecordCount NEQ 0>
			<cfquery name="ins_#QUERY.QueryName#" datasource="#QUERY.Datasource#">
				INSERT INTO dtbl_User_Session
					(User_Session_UID,
					User_Cookie,
					User_IP,
					User_DTS)
				VALUES
					(<cfqueryparam cfsqltype="cf_sql_varchar" value="#qry_GetSess.User_Session_UID#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#qry_GetSess.User_Cookie#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.REMOTE_ADDR#">,
					<cfqueryparam cfsqltype="cf_sql_timestamp" value="#CreateODBCDateTime(Now())#">)
			</cfquery>
			<cfset verified_sess = true>
			
			<cfinvoke method="extnSession">
				<cfinvokeargument name="Session_UID" value="#Session_UID#">
			</cfinvoke>
		<cfelse>
			<cfset verified_sess = false>
		</cfif>
		<cfif StructClear(QUERY)></cfif>
		<cfreturn verified_sess>
	</cffunction>
	
	<cffunction 
		name="TrackSession" 
		returntype="void" 
		hint="Creates DB Record" 
		access="public">
		
		<cfargument name="Session_UID" type="string" required="yes">
		<cfargument name="User_Cookie" type="string" required="yes">
		
		<cfscript>
			QUERY = StructNew();
			QUERY.QueryName = "qry_InsertTracking";
			QUERY.Datasource = APPLICATION.Datasource;
		</cfscript>

		<cfquery name="#QUERY.QueryName#" datasource="#QUERY.Datasource#">
			INSERT INTO dtbl_User_Session
				(User_Session_UID,
				User_Cookie,
				User_IP,
				User_DTS)
			VALUES
				(<cfqueryparam cfsqltype="cf_sql_varchar" value="#Session_UID#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#COOKIE.User_Cookie#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.REMOTE_ADDR#">,
				<cfqueryparam cfsqltype="cf_sql_timestamp" value="#CreateODBCDateTime(Now())#">)
		</cfquery>
		<cfif StructClear(QUERY)></cfif>
	</cffunction>
	
	<cffunction 
		name="TrackView" 
		returntype="void" 
		hint="Creates DB Record to track user item views" 
		access="public">
		
		<cfargument name="Session_UID" type="string" required="yes">
		<cfargument name="User_Cookie" type="string" required="yes">
		<cfargument name="Product_UID" type="string" required="yes">
		<cfargument name="Wishlist" default="0" type="numeric" required="no">
		
		<cfscript>
			QUERY = StructNew();
			QUERY.QueryName = "qry_InsertTracking";
			QUERY.Datasource = APPLICATION.Datasource;
			
			getH_UID = CreateObject("component", "_cfcs.Generate_UID");
			thisHistory_UID = getH_UID.genUID(Length=32);
		</cfscript>

		<cfquery name="#QUERY.QueryName#" datasource="#QUERY.Datasource#">
			INSERT INTO stbl_User_History
				(History_UID,
				History_User_Cookie,
				History_Session_UID,
				History_Wishlist,
				History_Product_UID,
				History_User_IP,
				History_User_DTS,
				History_User_Agent)
			VALUES
				(<cfqueryparam cfsqltype="cf_sql_varchar" value="#thisHistory_UID#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#COOKIE.User_Cookie#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#Session_UID#">,
				<cfqueryparam cfsqltype="cf_sql_tinyint" value="#Wishlist#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#Product_UID#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.REMOTE_ADDR#">,
				<cfqueryparam cfsqltype="cf_sql_timestamp" value="#CreateODBCDateTime(Now())#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.HTTP_USER_AGENT#">)
		</cfquery>
		<cfif StructClear(QUERY)></cfif>
	</cffunction>
	<cffunction 
		name="remWish" 
		returntype="void" 
		hint="Remove item from wishlist" 
		access="public">
		
		<cfargument name="User_Cookie" type="string" required="yes">
		<cfargument name="Product_UID" type="string" required="yes">
		
		<cfloop list="#Product_UID#" index="thisProduct">
			<cfquery name="qry_updWishlist" datasource="#APPLICATION.Datasource#">
				UPDATE stbl_User_History
				SET History_Wishlist = <cfqueryparam cfsqltype="cf_sql_tinyint" value="0">
				WHERE 
					History_User_Cookie = <cfqueryparam cfsqltype="cf_sql_varchar" value="#COOKIE.User_Cookie#">
				AND History_Product_UID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#thisProduct#">
			</cfquery>
		</cfloop>
	</cffunction>
	<cffunction name="authUser" returntype="any">
		<cfscript>
			USER = arguments;
		</cfscript>
		
		<cfparam name="USER.UserFrom" default="">
		<cfparam name="USER.UserTo" default="">
		
		<cfparam name="USER.BuyerCookie" default="">
		<cfparam name="USER.BrowserFormPost" default="">
		<cfparam name="USER.ContactName" default="">
		<cfparam name="USER.Email" default="">
		
		<cfparam name="USER.Address.ShipTo" default="">
		<cfparam name="USER.Address.Street1" default="">
		<cfparam name="USER.Address.Street2" default="">
		<cfparam name="USER.Address.City" default="">
		<cfparam name="USER.Address.State" default="">
		<cfparam name="USER.Address.Zip" default="">
		<cfparam name="USER.Address.Country" default="">
		
		<cfquery name="qry_VerifyUser" datasource="#APPLICATION.Datasource#">
			SELECT *
			FROM tbl_Users
			WHERE Buyer_Cookie = <cfqueryparam cfsqltype="cf_sql_varchar" value="#USER.BuyerCookie#">
			ORDER BY User_Created_DTS DESC
		</cfquery>
		
		<cfif qry_VerifyUser.RecordCount EQ 0>
			<!---Create New User--->
			<cfscript>
				getUID = CreateObject("component", "_cfcs.Generate_UID");
				User_UID = getUID.genUID(Length=32);
				USER.User_Cookie = User_UID;
			</cfscript>
			
			<cfquery name="qry_insNew" datasource="#APPLICATION.Datasource#" maxrows="1">
				INSERT INTO tbl_Users
					(User_From,
					User_To,
					Buyer_Cookie,
					Browser_FormPost,
					User_Cookie,
					Contact_Name,
					Contact_Email,
					Ship_Name,
					Ship_Address1,
					Ship_Address2,
					Ship_City,
					Ship_State,
					Ship_Zip,
					Ship_Country,
					User_Created_DTS,
					User_Created_IP,
					User_Created_Agent)
				VALUES
					(<cfqueryparam cfsqltype="cf_sql_varchar" value="#USER.UserFrom#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#USER.UserTo#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#USER.BuyerCookie#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#USER.BrowserFormPost#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#USER.User_Cookie#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#USER.ContactName#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#USER.Email#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#USER.Address.ShipTo#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#USER.Address.Street1#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#USER.Address.Street2#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#USER.Address.City#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#USER.Address.State#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#USER.Address.Zip#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#USER.Address.Country#">,
					<cfqueryparam cfsqltype="cf_sql_timestamp" value="#CreateODBCDateTime(Now())#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.REMOTE_ADDR#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.HTTP_USER_AGENT#">)
			</cfquery>
			
			<cfset USER.OKGO = true>
		<cfelse>
			<cfscript>
				USER.User_Cookie = qry_VerifyUser.User_Cookie;
				USER.OKGO = true;
			</cfscript>
		</cfif>
		
		<cfscript>
			if(USER.OKGO) {
				thisSession = CreateObject("component", "_cfcs.SessionMgmt");
				SESSION_UID = thisSession.getSession_UID();
				USER.Session_UID = SESSION_UID;
			} else {
				USER.Session_UID = "bad credentials";
			}
		</cfscript>
		
		<cfreturn USER />
	</cffunction>
	
	<cffunction name="TrackIncomingRequest" hint="Insert all raw incoming requests" returntype="void" output="no">
		<cfargument name="XML_Request" default="" type="any" required="yes">
		
		<cftry>
		<cfquery name="qry_insRaw" datasource="#APPLICATION.Datasource#">
			INSERT INTO dtbl_Request_Auth
				(Request_Raw,
				Request_DTS,
				Request_IP,
				Request_User_Agent)
			VALUES
				(<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#XML_Request#">,
				<cfqueryparam cfsqltype="cf_sql_timestamp" value="#CreateODBCDateTime(Now())#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.REMOTE_ADDR#">,
				<cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.HTTP_USER_AGENT#">)
		</cfquery>
		<cfcatch type="any">
			<cfmail to="atnelson@hme.com, chrisg@hme.com" from="no-reply@hme.com" subject="Bad XML Req">
				#XML_Request#
				
				
				#CFCATCH.Type#
				
				#CFCATCH.Message#
				
				#CFCATCH.Detail#
			</cfmail>
		</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>