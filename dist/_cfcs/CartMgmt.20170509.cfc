<cfcomponent displayname="ShoppingCart" hint="Controls the user's shopping cart">
	<cfparam name="APPLICATION.Datasource" default="#APPLICATION.Datasource#">
    
<!---Set Named Value Pairs--->
	<cffunction name="buildNVP" returntype="any" hint="builds the Named Value Pairs to send to PayPal">
		<!---Constants--->
		<cfargument name="PayPal_USER" default="#APPLICATION.PayPal_USER#">
		<cfargument name="PayPal_PASS" default="#APPLICATION.PayPal_PASS#">
		<cfargument name="PayPal_SIGN" default="#APPLICATION.PayPal_SIGN#">
		<cfargument name="PayPal_CURR" default="#APPLICATION.PayPal_CURR#">
		<cfargument name="PayPal_APIV" default="#APPLICATION.PayPal_APIV#">
		
		<cfargument name="METHOD" default="DoDirectPayment">
		<cfargument name="PAYMENTACTION" default="SALE">
		<cfargument name="IPADDRESS" default="#CGI.REMOTE_ADDR#">
		
		<!---User Input--->
		<cfargument name="CREDITCARDTYPE" required="yes">
		<cfargument name="ACCT" required="yes">
		<cfargument name="EXPDATE" required="yes">
		<cfargument name="CVV2" required="yes">
		<cfargument name="FIRSTNAME" required="yes">
		<cfargument name="LASTNAME" required="yes">
		<cfargument name="AMT" required="yes">
		
		<cfset requestString = 
			"USER=#PayPal_USER#" & 
			"&PWD=#PayPal_PASS#" & 
			"&SIGNATURE=#PayPal_SIGN#" & 
			"&VERSION=#PayPal_APIV#" & 
			"&CURRENCYCODE=#PayPal_CURR#" & 
			"&METHOD=#METHOD#" & 
			"&PAYMENTACTION=#PAYMENTACTION#" & 
			"&IPADDRESS=#IPADDRESS#" & 
			"&CREDITCARDTYPE=#CREDITCARDTYPE#" & 
			"&ACCT=#ACCT#" & 
			"&EXPDATE=#EXPDATE#" & 
			"&CVV2=#CVV2#" & 
			"&FIRSTNAME=#FIRSTNAME#" & 
			"&LASTNAME=#LASTNAME#" & 
			"&AMT=#AMT#">
		<cfreturn requestString />
		
	</cffunction>
    
<!---Make PayPal Call--->
	<cffunction name="makeHTTPCall" returntype="any">
		<cfargument name="PayPal_URL" default="#APPLICATION.PayPal_URL#">
		<cfargument name="requestString" required="yes">
		
		<cfhttp url="#PayPal_URL#" method="post"></cfhttp>
	</cffunction>
    
<!---Validate the product as both existing as well as active --->
	<cffunction name="valItem" output="no" returntype="struct" hint="Validates an item">
		<cfargument name="Product_UID" required="yes">
		<cfargument name="Product_IsActive" default="1" required="no">
		
		<cfscript>
			QUERY = StructNew();
			QUERY.QueryName = "qry_validateProduct";
			QUERY.Datasource = APPLICATION.Datasource;
			QUERY.Product_UID = Product_UID;
			QUERY.Product_IsActive = Product_IsActive;
		</cfscript>
		<cfinclude template="/_qry_select_products.cfm">
		
		<cfscript>
			Ret_Records = StructNew();
			Ret_Records.Tot_Records = qry_validateProduct.RecordCount;
			Ret_Records.Dat_Records = qry_validateProduct;
		</cfscript>
		
		<cfreturn Ret_Records>
	</cffunction>
    
<!---Add an item to the shopping cart--->
	<cffunction name="addItem" output="no" returntype="void" hint="Adds an item to the shopping cart">
		<cfargument name="Session_UID" required="yes">
		<cfargument name="User_Cookie" required="yes">
		<cfargument name="Product_UID" required="yes">
		<cfargument name="Product_QTY" default="0" required="yes">
		<cfargument name="Product_Price_Type" required="yes">
		<cfargument name="Add_Freight" required="yes"><!---Include any additional frieght charges--->
		
		<!---Need to write routine to accomodate discounts--->
		
		<cfscript>
			insResult = "";
			
			QUERY = StructNew();
			QUERY.QueryName = "qry_checkCart";
			QUERY.Datasource = APPLICATION.Datasource;
			QUERY.Cart_Product_UID = Product_UID;
			QUERY.Cart_Session_UID = Session_UID;
			QUERY.Cart_User_Cookie = User_Cookie;
			QUERY.OrderBy = "cart.Cart_DTS";
		</cfscript>
		<cfinclude template="/_qry_select_cart_product.cfm">
		
		<!--- Only insert new items into cart db--->
		<cfif qry_checkCart.RecordCount EQ 0>
			<cftry>
				<cfquery name="qry_insItem" datasource="#APPLICATION.Datasource#" maxrows="1">
					INSERT INTO stbl_User_Cart_Products
						(Cart_Session_UID,
						Cart_User_Cookie,
						Cart_Product_UID,
						Cart_Product_QTY,
						Cart_Product_Price_SubTotal,
						Cart_Product_Additional_Freight,
						Cart_Product_Price_Type,
						Cart_DTS)
					VALUES
						(<cfqueryparam cfsqltype="cf_sql_varchar" value="#Session_UID#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#User_Cookie#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#Product_UID#">,
						<cfqueryparam cfsqltype="cf_sql_integer" value="#Product_QTY#">,
						<cfqueryparam cfsqltype="cf_sql_money" value="#Product_Price#">,
						<cfqueryparam cfsqltype="cf_sql_money" value="#Add_Freight#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#Product_Price_Type#">,
						<cfqueryparam cfsqltype="cf_sql_timestamp" value="#CreateODBCDateTime(Now())#">)
				</cfquery>
                
				<cfcatch type="any">
					<cfmail 
						to="atnelson@hme.com, chrisg@hme.com, wwang@hme.com" 
						from="no-reply@cerepairs.com" 
						subject="Error Inserting Record Into Cart - #Session_UID#" 
						type="html">
						There was an error adding #Product_UID# to the user's shopping cart
					</cfmail><!---Need to write onError event. Just sending email for now--->
				</cfcatch>
			</cftry>
		</cfif>
	</cffunction>
    
<!---Update Quantities--->
	<cffunction name="qtyItem" output="no" returntype="void" hint="Changes product qty">
		<cfargument name="Session_UID" required="yes">
		<cfargument name="User_Cookie" required="yes">
		<cfargument name="Product_UID" required="yes">
		<cfargument name="Product_QTY" default="0" required="yes">
		
		<cfquery name="qry_qtyItem" datasource="#APPLICATION.Datasource#" maxrows="1">
			UPDATE stbl_User_Cart_Products
			SET 
				Cart_Product_QTY = <cfqueryparam cfsqltype="cf_sql_integer" value="#Product_QTY#">
			WHERE 0=0
			AND	Cart_Session_UID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Session_UID#">
			AND Cart_User_Cookie = <cfqueryparam cfsqltype="cf_sql_varchar" value="#User_Cookie#">
			AND Cart_Product_UID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Product_UID#">
		</cfquery>
	</cffunction>
    
<!---Remove an item from the shopping cart--->
	<cffunction name="delItem" output="no" returntype="void" hint="Deletes an item to the shopping cart">
		<cfargument name="Session_UID" required="yes">
		<cfargument name="User_Cookie" required="yes">
		<cfargument name="Product_UID" required="yes">
		
		<cfquery name="qry_delItem" datasource="#APPLICATION.Datasource#" maxrows="1">
			DELETE
			FROM stbl_User_Cart_Products
			WHERE 0=0
			AND	Cart_Session_UID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Session_UID#">
			AND Cart_User_Cookie = <cfqueryparam cfsqltype="cf_sql_varchar" value="#User_Cookie#">
			AND Cart_Product_UID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Product_UID#">
		</cfquery>
	</cffunction>
    
<!----Clear all items currently in shopping cart--->	
	<cffunction name="clearCart" output="no" returntype="void" hint="Clears the shopping cart">
		<cfargument name="Session_UID" required="yes">
		<cfargument name="User_Cookie" required="yes">
		
		<cfquery name="qry_delItem" datasource="#APPLICATION.Datasource#">
			DELETE
			FROM stbl_User_Cart_Products
			WHERE 0=0
			AND	Cart_Session_UID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Session_UID#">
			AND Cart_User_Cookie = <cfqueryparam cfsqltype="cf_sql_varchar" value="#User_Cookie#">
		</cfquery>
        
        <cfset SESSION.Promo_Code = "">
	</cffunction>
    
<!---Checkout items in cart and create invoice--->
	<cffunction name="invSess" output="yes" returntype="void" hint="Creates the invoice prior to checkout">
		<cfargument name="Session_UID" required="yes">
		<cfargument name="User_Cookie" required="yes">
		<cfargument name="User_ShipState" required="yes">
		
		<cfscript>
			QUERY = StructNew();
			QUERY.QueryName = "qry_getCart";
			QUERY.Datasource = APPLICATION.Datasource;
			QUERY.Cart_Session_UID = Session_UID;
			QUERY.Cart_User_Cookie = User_Cookie;
			QUERY.Cart_IsCompleted = 0;
			QUERY.OrderBy = "cart.Cart_DTS";
		</cfscript>
        
		<cfinclude template="/_qry_select_cart_product.cfm">
		
		<cfif qry_getCart.RecordCount NEQ 0><!---Make sure user didn't manage to submit an empty cart--->
			<cfquery name="qry_clearInvoice" datasource="#APPLICATION.Datasource#"><!---Clear Invoice Table On Each Request--->
				DELETE
				FROM stbl_User_Invoice
				WHERE 0=0
				AND Invoice_Session_UID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Session_UID#">
				AND Invoice_User_Cookie = <cfqueryparam cfsqltype="cf_sql_varchar" value="#User_Cookie#">
			</cfquery>
			
			<cfset itemTotal = 0>
			<cfset cartTotal = 0>
			<cfset Discount = 0>
			<cfset cartQty = 0>
			<cfset Freight = 0>
			
			<cfoutput query="qry_getCart">
				<!---Find Discounts--->
				<cfscript>
					QUERY = StructNew();
					QUERY.QueryName = "qry_getDiscounts";
					QUERY.Datasource = APPLICATION.Datasource;
					
					QUERY.Offer_ValidTo = Now();
					QUERY.Offer_ValidFrom = Now();
					QUERY.Product_UID = Product_UID;
					QUERY.Offer_Qty = Cart_Product_Qty;
					
					Discount = 0;
					DiscountMultiplier = 1;
					IsValidPromoCode = false;
				</cfscript>
                
                <!--- find current discount applied to this product --->
				<cfinclude template="/_qry_select_offer.cfm">
				
				<!---20131227 CG: Modified DiscountMultiplier and cartQty per Ticket#46432 --->
				<cfif qry_getDiscounts.RecordCount NEQ 0>
					<!---Set Multipler Discount EG Buy 4 get 1 free. Cust_Qty = 8 then 2 free--->
					<cfset DiscountMultiplier = int(Cart_Product_Qty/qry_getDiscounts.Offer_Qty)>
                    
                    <!--- varify if the promo code matches the one user entered --->
                    <cfif (SESSION.Promo_Code NEQ "") AND (qry_getDiscounts.Offer_PromoCode IS SESSION.Promo_Code)>
                    	<cfset IsValidPromoCode = true>
                    </cfif>
				</cfif>
				
                <!--- 20150715 WW: User needs to enter a valid promo code in order to get discount Ticket#63410 --->
                <!--- 20160105 WW: Automatically apply discount when there's no promo code. Otherwise only appply it when it's valid --->
                <cfif (qry_getDiscounts.Offer_PromoCode IS "") OR (IsValidPromoCode)>
					<!---20140429 CG: Edited calculations to get the correct discounts Ticket#51134 --->
                    <cfswitch expression="#qry_getDiscounts.Offer_Type#">
                        <cfcase value="Percentage">
                            <cfset Discount = (qry_getDiscounts.Offer_DiscountAmount/100)*(qry_getDiscounts.Offer_DiscountQty*Cart_Product_Price_SubTotal)>
                            <cfset Discount = Discount*DiscountMultiplier>
                            
                            <cfif ((qry_getDiscounts.RecordCount NEQ 0) AND (Cart_Product_Qty GTE qry_getDiscounts.Offer_Qty))>
                                <!---To Add Free Units to BuyX GetY Free--->
                                <cfset cartQty = Cart_Product_Qty + (qry_getDiscounts.Offer_DiscountQty*DiscountMultiplier)>
                            <cfelse>
                                <cfset cartQty = Cart_Product_Qty>
                            </cfif>
                            
                            <cfset discTotal = Discount>
                            <cfset itemTotal = cartQty * Cart_Product_Price_SubTotal>
                            <cfset cartTotal = cartTotal + itemTotal - discTotal>
                        </cfcase>
                        
                        <cfcase value="Whole">
                            <cfif qry_getDiscounts.Offer_DiscountQty GT 0>
                                <!--- Multiply Discout Amount By Quantity EQ "Buy X Get $5 off Y"--->
                                <cfset Discount = (qry_getDiscounts.Offer_DiscountAmount*qry_getDiscounts.Offer_DiscountQty)>
                            <cfelse>
                                <!--- Straight Discount Off X --->
                                <cfset Discount = (qry_getDiscounts.Offer_DiscountAmount)>
                            </cfif>
                            
                            <cfif ((qry_getDiscounts.RecordCount NEQ 0) AND (Cart_Product_Qty GTE qry_getDiscounts.Offer_Qty))>
                                <!--- To Add Free Units to BuyX GetY Free--->
                                <cfset TotalWithoutDiscount = (Cart_Product_Price_SubTotal * qry_getDiscounts.Offer_Qty)>
                                <cfset UnitPostDiscount = ((TotalWithoutDiscount - Discount)/qry_getDiscounts.Offer_Qty)>
                                <cfset cartQty = Cart_Product_Qty + (qry_getDiscounts.Offer_DiscountQty*DiscountMultiplier)>
                                <cfset itemTotal = cartQty * Cart_Product_Price_SubTotal>
                                <cfset itemTotalDis = cartQty * UnitPostDiscount>
                                <cfset cartTotal = cartTotal + itemTotalDis>
                                <cfset discTotal = itemTotal - itemTotalDis>
                            <cfelse>
                                <cfset cartQty = Cart_Product_Qty>
                                <cfset itemTotal = cartQty * Cart_Product_Price_SubTotal>
                                <cfset cartTotal = cartTotal + itemTotal>
                                <cfset discTotal = itemTotal - cartTotal>
                            </cfif>
                        </cfcase>
                    </cfswitch>
                </cfif>
				
				<cfif Discount EQ 0>
					<cfset cartQty = Cart_Product_Qty>
					<cfset discTotal = Discount>
					<cfset itemTotal = cartQty * Cart_Product_Price_SubTotal>
					<cfset cartTotal = cartTotal + itemTotal - discTotal>
                    <cfset PromoCode = ''>
                <cfelse>
                	<!--- only apply promo code when there's a discount --->
                	<cfset PromoCode = qry_getDiscounts.Offer_PromoCode>
				</cfif>
				
				<!---Additional Freight--->
				<cfset Freight = Freight + Cart_Product_Additional_Freight>
				
				<cfquery name="qry_insInvoice" datasource="#APPLICATION.Datasource#">
					INSERT INTO stbl_User_Invoice
						(Invoice_Session_UID,
						Invoice_User_Cookie,
						Invoice_Product_UID,
						Invoice_Manufacturer_Part_Number,
						Invoice_Product_Description,
						Invoice_Product_Unit_Price,
						Invoice_Product_Price,
						Invoice_Product_Price_Type,
						Invoice_Product_Discount,
						Invoice_Product_Qty,
						Invoice_UOM,
						Invoice_UNSPSC,
                        Invoice_Promo_Code,
						Invoice_Order,
						Invoice_Created_DTS,
						Invoice_Created_IP)
					VALUES
						(<cfqueryparam cfsqltype="cf_sql_varchar" value="#Session_UID#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#User_Cookie#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#Product_UID#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#Manufacturer_Part_Number#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#Product_Short_Description#">,
						<cfqueryparam cfsqltype="cf_sql_money" value="#Cart_Product_Price_SubTotal#">,
						<cfqueryparam cfsqltype="cf_sql_money" value="#itemTotal#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#Cart_Product_Price_Type#">,
						<cfqueryparam cfsqltype="cf_sql_money" value="#discTotal#">,
						<cfqueryparam cfsqltype="cf_sql_integer" value="#cartQty#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#UOM#">,
						<cfqueryparam cfsqltype="cf_sql_integer" value="#UNSPSC#">,
                        <cfqueryparam cfsqltype="cf_sql_varchar" value="#PromoCode#">,
						<cfqueryparam cfsqltype="cf_sql_integer" value="#CurrentRow#">,
						<cfqueryparam cfsqltype="cf_sql_timestamp" value="#CreateODBCDateTime(Now())#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.REMOTE_ADDR#">)
				</cfquery>
			</cfoutput>
			
			<!---Sub-Total--->
			<cfset totalRow = qry_getCart.RecordCount + 1>
            
			<cfquery name="qry_insInvoice" datasource="#APPLICATION.Datasource#">
				INSERT INTO stbl_User_Invoice
					(Invoice_Session_UID,
					Invoice_User_Cookie,
					Invoice_Product_Description,
					Invoice_Product_Price,
					Invoice_Product_Discount,
					Invoice_Order,
					Invoice_Created_DTS,
					Invoice_Created_IP)
				VALUES
					(<cfqueryparam cfsqltype="cf_sql_varchar" value="#Session_UID#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#User_Cookie#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="Sub-Total">,
					<cfqueryparam cfsqltype="cf_sql_money" value="#cartTotal#">,
					<cfqueryparam cfsqltype="cf_sql_money" value="0">,
					<cfqueryparam cfsqltype="cf_sql_integer" value="#totalRow#">,
					<cfqueryparam cfsqltype="cf_sql_timestamp" value="#CreateODBCDateTime(Now())#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.REMOTE_ADDR#">)
			</cfquery>
			
			<!---For Missouri Residents Add Additional Row for Sales Tax--->
			<cfif User_ShipState IS "MO">
				<cfset addRow = 1>
				<cfset addTax = NumberFormat((cartTotal * APPLICATION.MI_TaxRate), '.99')>
				<cfset totalRow = qry_getCart.RecordCount + 2>
				
				<cfquery name="qry_insInvoice" datasource="#APPLICATION.Datasource#">
					INSERT INTO stbl_User_Invoice
						(Invoice_Session_UID,
						Invoice_User_Cookie,
						Invoice_Product_Description,
						Invoice_Product_Price,
						Invoice_Product_Discount,
						Invoice_Order,
						Invoice_Created_DTS,
						Invoice_Created_IP)
					VALUES
						(<cfqueryparam cfsqltype="cf_sql_varchar" value="#Session_UID#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#User_Cookie#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="Tax">,
						<cfqueryparam cfsqltype="cf_sql_money" value="#addTax#">,
						<cfqueryparam cfsqltype="cf_sql_money" value="0">,
						<cfqueryparam cfsqltype="cf_sql_integer" value="#totalRow#">,
						<cfqueryparam cfsqltype="cf_sql_timestamp" value="#CreateODBCDateTime(Now())#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.REMOTE_ADDR#">)
				</cfquery>
			<cfelse>
				<cfset addRow = 0>
				<cfset addTax = 0>
			</cfif>
            
            <!---For California Residents Add Additional Row for Sales Tax--->
			<cfif User_ShipState IS "CA">
				<cfset addRow = 1>
				<cfset addTax = NumberFormat((cartTotal * APPLICATION.CA_TaxRate), '.99')>
				<cfset totalRow = qry_getCart.RecordCount + 2>
				
				<cfquery name="qry_insInvoice" datasource="#APPLICATION.Datasource#">
					INSERT INTO stbl_User_Invoice
						(Invoice_Session_UID,
						Invoice_User_Cookie,
						Invoice_Product_Description,
						Invoice_Product_Price,
						Invoice_Product_Discount,
						Invoice_Order,
						Invoice_Created_DTS,
						Invoice_Created_IP)
					VALUES
						(<cfqueryparam cfsqltype="cf_sql_varchar" value="#Session_UID#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#User_Cookie#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="Tax">,
						<cfqueryparam cfsqltype="cf_sql_money" value="#addTax#">,
						<cfqueryparam cfsqltype="cf_sql_money" value="0">,
						<cfqueryparam cfsqltype="cf_sql_integer" value="#totalRow#">,
						<cfqueryparam cfsqltype="cf_sql_timestamp" value="#CreateODBCDateTime(Now())#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.REMOTE_ADDR#">)
				</cfquery>
			<cfelse>
				<cfset addRow = 0>
				<cfset addTax = 0>
			</cfif>
			
			<!---Freight--->
			<cfset totalRow = qry_getCart.RecordCount + 2 + addRow>
			
			<cfquery name="qry_getFreight" datasource="#APPLICATION.Datasource#" maxrows="1">
				SELECT ProductFreight
				FROM ltbl_Product_Freight
				WHERE 
					ProductFreight_Low <= <cfqueryparam cfsqltype="cf_sql_money" value="#NumberFormat(cartTotal,'.99')#">
				AND
					ProductFreight_High >= <cfqueryparam cfsqltype="cf_sql_money" value="#NumberFormat(cartTotal,'.99')#">
			</cfquery>
			
			<cfif qry_getFreight.RecordCount NEQ 0>
				<cfset Freight = Freight + qry_getFreight.ProductFreight>
			<cfelse>
				<cfset Freight = Freight>
			</cfif>
			
			<cfquery name="qry_insInvoice" datasource="#APPLICATION.Datasource#">
				INSERT INTO stbl_User_Invoice
					(Invoice_Session_UID,
					Invoice_User_Cookie,
					Invoice_Product_Description,
					Invoice_Product_Price,
					Invoice_Product_Discount,
					Invoice_Order,
					Invoice_Created_DTS,
					Invoice_Created_IP)
				VALUES
					(<cfqueryparam cfsqltype="cf_sql_varchar" value="#Session_UID#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#User_Cookie#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="Freight">,
					<cfqueryparam cfsqltype="cf_sql_money" value="#Freight#">,
					<cfqueryparam cfsqltype="cf_sql_money" value="0">,
					<cfqueryparam cfsqltype="cf_sql_integer" value="#totalRow#">,
					<cfqueryparam cfsqltype="cf_sql_timestamp" value="#CreateODBCDateTime(Now())#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.REMOTE_ADDR#">)
			</cfquery>
			
			<!---Grand-Total--->
			<cfset totalRow = qry_getCart.RecordCount + 3 + addRow>
			<cfset GrandTotal = cartTotal + Freight + addTax>
			
			<cfquery name="qry_insInvoice" datasource="#APPLICATION.Datasource#">
				INSERT INTO stbl_User_Invoice
					(Invoice_Session_UID,
					Invoice_User_Cookie,
					Invoice_Product_Description,
					Invoice_Product_Price,
					Invoice_Product_Discount,
					Invoice_Order,
					Invoice_Created_DTS,
					Invoice_Created_IP)
				VALUES
					(<cfqueryparam cfsqltype="cf_sql_varchar" value="#Session_UID#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#User_Cookie#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="Grand Total">,
					<cfqueryparam cfsqltype="cf_sql_money" value="#GrandTotal#">,
					<cfqueryparam cfsqltype="cf_sql_money" value="0">,
					<cfqueryparam cfsqltype="cf_sql_integer" value="#totalRow#">,
					<cfqueryparam cfsqltype="cf_sql_timestamp" value="#CreateODBCDateTime(Now())#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.REMOTE_ADDR#">)
			</cfquery>
		</cfif>
	</cffunction>
    
	<cffunction name="placeOrder" output="no" returntype="any" hint="Creates the invoice prior to checkout">
		<cfargument name="Session_UID" required="yes">
		<cfargument name="User_Cookie" required="yes">
		<cfparam name="cXML_Order" default="">
		<cfparam name="Status_Code" default="">
		<cfparam name="Status_Text" default="">
		<cfparam name="Order_Status" default="">
		
		<!---Submit Puchout Request--->
		<cfscript>
			QUERY = StructNew();
			QUERY.QueryName = "qry_getInvoice";
			QUERY.Datasource = APPLICATION.Datasource;
			QUERY.Session_UID = Session_UID;
			QUERY.User_Cookie = User_Cookie;
			QUERY.SelectSQL = "DISTINCT usrs.User_From, usrs.User_To, usrs.Browser_FormPost, usrs.Contact_Name, usrs.Contact_Email, usrs.Ship_Name, usrs.Ship_Address1, usrs.Ship_Address2, usrs.Ship_City, usrs.Ship_State, usrs.Ship_Zip, usrs.Ship_Country, invc.Invoice_Session_UID, invc.Invoice_User_Cookie, invc.Invoice_Product_UID, invc.Invoice_Manufacturer_Part_Number, invc.Invoice_Product_Description, invc.Invoice_Product_Price, invc.Invoice_Product_Discount, invc.Invoice_Product_Qty, invc.Invoice_UOM, invc.Invoice_UNSPSC, invc.Invoice_Order, invc.Invoice_Created_DTS";
			QUERY.OrderBy = "invc.Invoice_Session_UID, invc.Invoice_Order, invc.Invoice_Created_DTS";
		</cfscript>
		
		<cfinclude template="/_qry_select_user_invoice.cfm">
		
		<cfif qry_getInvoice.RecordCount NEQ 0>
			
			<cfoutput>
			<cfsavecontent variable="cXML_Order">
			<?xml version="1.0" encoding="UTF-8"?>
			<!DOCTYPE cXML SYSTEM "http://xml.cxml.org/schemas/cXML/1.2.014/cXML.dtd">
			<cXML xml:lang="en-US" payloadID="#Session_UID#" timestamp="#DateFormat(DateAdd('h', 8, Now()),'yyyy-mm-dd')#T#TimeFormat(DateAdd('h', 8, Now()), 'hh:mm:ss')#-08:00">
			<Header>
				<From>
					<Credential domain="DUNS">
						<Identity>#qry_getInvoice.User_To#</Identity>
					</Credential>
				</From>
				<To>
					<Credential domain="DUNS">
						<Identity>#qry_getInvoice.User_From#</Identity>
					</Credential>
				</To>
				<Sender>
					<Credential domain="hme.com">
						<Identity>HME_Spendsmart</Identity>
					</Credential>
					<UserAgent>HME Spendsmart Agent</UserAgent>
				</Sender>
			</Header>
			<Message>
				<Status code="200" text="OK">The shopping session completed successfully.</Status>
				<PunchOutOrderMessage>
					<BuyerCookie>#User_Cookie#</BuyerCookie>
					<PunchOutOrderMessageHeader operationAllowed="edit">
						<Total>
							<Money currency="USD">#NumberFormat(qry_getInvoice.Invoice_Product_Price[qry_getInvoice.RecordCount], ".99")#</Money>
						</Total>
					</PunchOutOrderMessageHeader>
					<cfloop query="qry_getInvoice" startrow="1" endrow="#Evaluate(qry_getInvoice.RecordCount-1)#"><ItemIn quantity="#Invoice_Product_Qty#" lineNumber="#Invoice_Order#">
						<ItemID>
							<SupplierPartID>#Invoice_Manufacturer_Part_Number#</SupplierPartID>
						</ItemID>
						<ItemDetail>
							<UnitPrice>
								<Money currency="USD">#NumberFormat(Invoice_Product_Price, ".99")#</Money>
							</UnitPrice>
							<Description xml:lang="en">
								<ShortName>#Invoice_Product_Description#</ShortName>
							</Description>
							<UnitOfMeasure>#Invoice_UOM#</UnitOfMeasure>
							<Classification domain="UNSPSC">#Invoice_UNSPSC#</Classification>
						</ItemDetail>
					</ItemIn></cfloop>
				</PunchOutOrderMessage>
			</Message>
			</cXML>
			</cfsavecontent>
			
			<cfhttp
				url="#qry_getInvoice.Browser_FormPost#"
				method="POST"
				useragent="#CGI.http_user_agent#"
				result="objGet">
				<!---
					When posting the xml data, remember to trim
					the value so that it is valid XML.
				--->
				<cfhttpparam
					type="XML"
					value="#cXML_Order.Trim()#"
					/>
			</cfhttp>
			</cfoutput>
			
			<cftry>
			<cfscript>
				res_XML = objGet.FileContent.Trim();
				if(IsXML(res_XML)) {
					dat_XML = XMLParse(res_XML);
					get_Status = XmlSearch(dat_XML, "/cXML/Response/Status");
					Status = get_Status[1].XmlAttributes;
					Status_Code = Status.code;
					Status_Text = Status.text;
				}
			</cfscript>
			<cfcatch type="any">
				<cfset Status_Code = 300>
				<cfset Order_Status = objGet.FileContent>
			</cfcatch>	
			</cftry>
		</cfif>
		
		<cfif Status_Code EQ 200><!---Need to change this var back to EQ before going live--->
			<cfset Order_Status = "Thank you. Your purchase order has been successfully placed.">

			<cfquery name="qry_FinalizeCart" datasource="#APPLICATION.Datasource#">
				UPDATE stbl_User_Cart_Products
				SET Cart_IsCompleted = 1
				WHERE 
					Cart_Session_UID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Session_UID#">
				AND Cart_User_Cookie = <cfqueryparam cfsqltype="cf_sql_varchar" value="#User_Cookie#">
			</cfquery>
            
            <!--- wipe out promo code once order is completed --->
            <cfset SESSION.Promo_Code = "">
		<cfelse>
			<cfset Order_Status = objGet.FileContent>
		</cfif>
		
		<cfreturn Order_Status>
	</cffunction>
    
    <!--- when use enters a promo code, varify if it's valid or not --->
    <cffunction name="promoValidate" access="remote" returnformat="plain" output="no" returntype="string" hint="Validate whether a promo code is valid or not">
    	<cfargument name="Session_UID" type="string" required="yes">
        <cfargument name="PromoCode" type="string" required="yes">
        
        <cfsetting showdebugoutput="false">
		
        <cfscript>
        	var rtnPromoStatus = 'OK';
			var qry_PromoCodeValid = '';
			var qry_PromoCodeExpired = '';
			var qry_PromoCodeQuatityMet = '';
			var Product_QTY = 0;
        </cfscript>
        
        <!--- step 1. varify the promo code is valid --->
        <cfquery name="qry_PromoCodeValid" datasource="#APPLICATION.Datasource#">
        	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
			SET NOCOUNT ON

            SELECT	ucp.[Cart_Product_QTY]
			FROM	[dbo].[tbl_Offers] o
            		INNER JOIN [dbo].[itbl_Offer_Product] op ON op.Offer_ID = o.Offer_ID
                    INNER JOIN [dbo].[tbl_Products] p ON p.Product_ID = op.Product_ID
                    INNER JOIN [dbo].[stbl_User_Cart_Products] ucp ON ucp.Cart_Product_UID = p.Product_UID
            WHERE	o.[Offer_IsActive] = 1
            AND		o.[Offer_PromoCode] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.PromoCode#">
            AND		ucp.[Cart_Session_UID] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.Session_UID#">
        </cfquery>
        
        <cfif qry_PromoCodeValid.RecordCount EQ 0>
        	<cfset rtnPromoStatus = 'The promo code is invalid'>
        <cfelse>
        	<!--- quantity of the product for this promo code  --->
        	<cfset Product_QTY = qry_PromoCodeValid.Cart_Product_QTY>
        </cfif>
        
        <!--- promo code is active and matches the record --->
        <!--- step 2. verify the promo code is expired --->
        <cfif rtnPromoStatus IS "OK">
            <cfquery name="qry_PromoCodeExpired" datasource="#APPLICATION.Datasource#">
            	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
				SET NOCOUNT ON
                
                DECLARE @CurrentDate DATE = GetDate()
            
                SELECT	1
                FROM	[dbo].[tbl_Offers]
                WHERE	[Offer_PromoCode] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.PromoCode#">
                AND 	@CurrentDate BETWEEN Offer_ValidFrom AND Offer_ValidTo
            </cfquery>
            
            <cfif qry_PromoCodeExpired.RecordCount EQ 0>
				<cfset rtnPromoStatus = 'The promo code has expired'>
            </cfif>
        </cfif>
        
        <!--- promo code passes above checks --->
        <!--- step 3. verify order quantity satisfies the promo code --->
        <cfif rtnPromoStatus IS "OK">
            <cfquery name="qry_PromoCodeQuatityMet" datasource="#APPLICATION.Datasource#">
            	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
				SET NOCOUNT ON
                
                SELECT	1
                FROM	[dbo].[tbl_Offers]
                WHERE	[Offer_PromoCode] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Arguments.PromoCode#">
                AND 	[Offer_Qty] <= <cfqueryparam cfsqltype="cf_sql_varchar" value="#Product_QTY#">
            </cfquery>
            
            <cfif qry_PromoCodeQuatityMet.RecordCount EQ 0>
				<cfset rtnPromoStatus = 'The mininum quantity for the product is not met'>
            </cfif>
        </cfif>
        
        <!--- the promo code is good. add it into session --->
        <cfif rtnPromoStatus IS "OK">
        	<cfset SESSION.Promo_Code = Arguments.PromoCode>
        </cfif>
        
        <cfreturn rtnPromoStatus />
    </cffunction>
    
    <!--- calculate the discount for a promo code --->
    <cffunction name="getPromoDiscount" access="remote" returnformat="json" output="no" returntype="query" hint="calculate the discount number for a promo code">
    	<cfargument name="Session_UID" type="string" required="yes">
        <cfargument name="PromoCode" type="string" required="yes">
        
        <cfsetting showdebugoutput="false">
        
        <cfscript>
			var qry_getDiscount = '';
        </cfscript>
        
        <cfstoredproc datasource="#APPLICATION.Datasource#" procedure="usp_cerepairs_GetPromoDiscount">
            <cfprocparam cfsqltype="cf_sql_varchar" dbvarname="Session_UID" value="#Arguments.Session_UID#">
            <cfprocparam cfsqltype="cf_sql_varchar" dbvarname="PromoCode" value="#Arguments.PromoCode#">
            <cfprocresult name="qry_getDiscount">
        </cfstoredproc>
        
        <cfreturn qry_getDiscount />
    </cffunction>
    
    <!--- get default promo code --->
    <cffunction name="getDefaultPromoCode" access="public" output="no" returntype="query" hint="return default promo code">
        <cfargument name="PromoCode" type="string" required="no">
        
        <cfscript>
			var qry_GetDefaultPromoCode = '';
        </cfscript>
        
        <cfquery name="qry_GetDefaultPromoCode" datasource="#APPLICATION.Datasource#">
            SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
            SET NOCOUNT ON
        
            SELECT	[PromoCode] = [Offer_PromoCode],
                    [PromoPage] = [Offer_PromoPage]
            FROM	[dbo].[tbl_Offers]
            WHERE	1 = 1
            <cfif Arguments.PromoCode NEQ "">
            AND		[Offer_PromoCode] = '#Arguments.PromoCode#'
            <cfelse>
            AND		[Offer_IsDefault] = 1
            </cfif>
        </cfquery>
        
        <cfreturn qry_GetDefaultPromoCode />
    </cffunction>
</cfcomponent>