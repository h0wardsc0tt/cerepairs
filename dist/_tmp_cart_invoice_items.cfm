<cfsilent>
	<cfparam name="FORM.Ship_state" default="">
	<cfif FORM.Ship_state IS "MO">
		<cfset addRow = 4>
	<cfelse>
		<cfset addRow = 3>
	</cfif>
    <cfif FORM.Ship_state IS "CA">
		<cfset addRow = 4>
	<cfelse>
		<cfset addRow = 3>
	</cfif>
</cfsilent>
<cfoutput>
		<tr>
			<th colspan="5">Items Ordered</th>
		</tr>
		<tr>
			<th style="width:10%"></th>
			<th style="width:50%" align="left">Product</th>
			<th style="width:10%" align="right">Unit Price</th>
			<th style="width:15%" align="right">Qty</th>
			<th style="width:15%" align="right">Extended Price</th>
		</tr>
		<cfloop query="qry_getInvoice" startrow="1" endrow="#Evaluate(qry_getInvoice.RecordCount-addRow)#">
		<tr class="#IIF(CurrentRow MOD 2, DE('noshade'), DE('shade'))#">
			<td>#Invoice_Product_Price_Type#</td>
			<td valign="top">
				<p>#Invoice_Product_Description#</p>
				<p>Part ##: #Invoice_Manufacturer_Part_Number#</p>
			</td>
			<td valign="top" class="p_tot">
				<div>#DollarFormat(Invoice_Product_Unit_Price)#</div>
			</td>
			<td class="c_qty">#Invoice_Product_Qty#</td>
			<td valign="top" class="p_tot">
				<cfif Invoice_Product_Discount NEQ 0>
				<div class="strike_disc">#DollarFormat(Invoice_Product_Price)#</div>
				<div class="discount_amount">-#DollarFormat(Invoice_Product_Discount)#</div>
				<div>#DollarFormat(Invoice_Product_Price-Invoice_Product_Discount)#</div>
				<cfelse>
				<div>#DollarFormat(Invoice_Product_Price)#</div>
				</cfif>
			</td>
		</tr></cfloop>
		<cfif FORM.Ship_state IS "MO"><!---Missouri Residents Get Charged Sales Tax--->
		<tr>
			<td colspan="5"><div class="subtotal">Subtotal:&nbsp;<span id="subprice">#DollarFormat(qry_getInvoice.Invoice_Product_Price[qry_getInvoice.RecordCount-3])#</span></div></td>
		</tr>
		<tr>
			<td colspan="5"><div class="subtotal">Sales Tax:&nbsp;<span id="subprice">#DollarFormat(qry_getInvoice.Invoice_Product_Price[qry_getInvoice.RecordCount-2])#</span><br /><em>(Missouri Residents Only)</em></div></td>
		</tr>
		<tr>
			<td colspan="5"><div class="subtotal">Freight:&nbsp;<span id="subprice">#DollarFormat(qry_getInvoice.Invoice_Product_Price[qry_getInvoice.RecordCount-1])#</span></div></td>
		</tr>
        <cfelseif FORM.Ship_state IS "CA"><!---California Residents Get Charged Sales Tax--->
		<tr>
			<td colspan="5"><div class="subtotal">Subtotal:&nbsp;<span id="subprice">#DollarFormat(qry_getInvoice.Invoice_Product_Price[qry_getInvoice.RecordCount-3])#</span></div></td>
		</tr>
		<tr>
			<td colspan="5"><div class="subtotal">Sales Tax:&nbsp;<span id="subprice">#DollarFormat(qry_getInvoice.Invoice_Product_Price[qry_getInvoice.RecordCount-2])#</span><br /><em>(California Residents Only)</em></div></td>
		</tr>
		<tr>
			<td colspan="5"><div class="subtotal">Freight:&nbsp;<span id="subprice">#DollarFormat(qry_getInvoice.Invoice_Product_Price[qry_getInvoice.RecordCount-1])#</span></div></td>
		</tr>
		<cfelse>
		<tr>
			<td colspan="5"><div class="subtotal">Subtotal:&nbsp;<span id="subprice">#DollarFormat(qry_getInvoice.Invoice_Product_Price[qry_getInvoice.RecordCount-2])#</span></div></td>
		</tr>
		<tr>
			<td colspan="5"><div class="subtotal">Freight:&nbsp;<span id="subprice">#DollarFormat(qry_getInvoice.Invoice_Product_Price[qry_getInvoice.RecordCount-1])#</span></div></td>
		</tr>
		</cfif>
		<tr>
			<td colspan="5"><div class="subtotal">Grand Total:&nbsp;<span id="subprice">#DollarFormat(qry_getInvoice.Invoice_Product_Price[qry_getInvoice.RecordCount])#</span></div></td>
		</tr>
</cfoutput>