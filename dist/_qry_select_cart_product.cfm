<cfparam name="QUERY" default="#StructNew()#">
<cfparam name="QUERY.QueryName" default="qry_checkCart">
<cfparam name="QUERY.Datasource" default="#APPLICATION.Datasource#">
<cfparam name="QUERY.Cart_IsCompleted" default="0">
<cfparam name="QUERY.OrderBy" default="cart.Cart_DTS">

<cfquery name="#QUERY.QueryName#" datasource="#QUERY.Datasource#">
	SELECT cart.*, prod.*
	FROM 
		tbl_Products prod 
		INNER JOIN stbl_User_Cart_Products cart ON prod.Product_UID = cart.Cart_Product_UID
	WHERE 0=0
	<cfif StructKeyExists(QUERY, "Cart_IsCompleted")>
	AND cart.Cart_IsCompleted IN (<cfqueryparam cfsqltype="cf_sql_tinyint" value="#QUERY.Cart_IsCompleted#" list="yes">)
	</cfif>
	<cfif StructKeyExists(QUERY, "Cart_Product_UID")>
	AND cart.Cart_Product_UID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.Cart_Product_UID#" list="yes">)
	</cfif>
	<cfif StructKeyExists(QUERY, "Cart_Session_UID")>
	AND cart.Cart_Session_UID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.Cart_Session_UID#" list="yes">)
	</cfif>
	<cfif StructKeyExists(QUERY, "Cart_User_Cookie")>
	AND cart.Cart_User_Cookie IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.Cart_User_Cookie#" list="yes">)
	</cfif>
	<cfif StructKeyExists(QUERY, "Cart_Product_QTY")>
	AND cart.Cart_Product_QTY >= <cfqueryparam cfsqltype="cf_sql_integer" value="#QUERY.Cart_Product_QTY#">
	</cfif>
	ORDER BY #QUERY.OrderBy#
</cfquery>

<cfquery name="#QUERY.QueryName#_Total" dbtype="query">
	SELECT Count(*) AS Records
	FROM #QUERY.QueryName#
</cfquery>

<cfif StructClear(QUERY)></cfif>