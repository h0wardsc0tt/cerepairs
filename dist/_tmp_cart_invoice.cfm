<cfoutput>
<table class="full_cart">
	<tr>
		<td colspan="2">
			<table>
				<tr>
					<td><strong>Transaction ID:</strong></td>
					<td>#qry_getInvoice.Invoice_Transaction_ID#</td>
				</tr>
				<tr>
					<td><strong>Status:</strong></td>
					<td>Paid</td>
				</tr>
				<tr>
					<td><strong>Customer Name:</strong></td>
					<td>#qry_getUserInfo.Contact_FirstName# #qry_getUserInfo.Contact_LastName#</td>
				</tr>
				<tr>
					<td><strong>Company:</strong></td>
					<td>#qry_getUserInfo.Ship_Company#</td>
				</tr>
				<tr>
					<td><strong>Email:</strong></td>
					<td>#qry_getUserInfo.Contact_Email#</td>
				</tr>
				<tr>
					<td><strong>Phone:</strong></td>
					<td>#qry_getUserInfo.Contact_Phone#</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table>
				<tr>
					<th colspan="2">Bill To:</th>
				</tr>
				<tr>
					<td valign="top">Address:</td>
					<td>#qry_getUserInfo.Bill_Address1#
					<cfif Len(qry_getUserInfo.Bill_Address2) NEQ 0><br />#qry_getUserInfo.Bill_Address2#</cfif>
					<br />#qry_getUserInfo.Bill_City# #qry_getUserInfo.Bill_State#, #qry_getUserInfo.Bill_Zip#, #qry_getUserInfo.Bill_Country#
					</td>
				</tr>
			</table>
		</td>
		<td>
			<table>
				<tr>
					<th colspan="2">Ship To:</th>
				</tr>
				<tr>
					<td valign="top">Address:</td>
					<td>#qry_getUserInfo.Ship_Address1#
					<cfif Len(qry_getUserInfo.Ship_Address2) NEQ 0><br />#qry_getUserInfo.Ship_Address2#</cfif>
					<br />#qry_getUserInfo.Ship_City# #qry_getUserInfo.Ship_State#, #qry_getUserInfo.Ship_Zip#, #qry_getUserInfo.Ship_Country#
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<cfset FORM.Ship_state = qry_getUserInfo.Ship_State><!---Added to Handle Sales Tax for Missouri Residents--->
	<tr>
		<td colspan="2">
			<table style="width:780px;">
				<cfinclude template="./_tmp_cart_invoice_items.cfm">
			</table>
		</td>
	</tr>
</table>
</cfoutput>