	<div id="Footer">
		<div class="top_footer clear">
			<div class="sm_logo">
				<img src="/images/ce_small.png" /> Commercial Electronics (CE)
			</div>
			<div class="bot_nav">
				<a href="./?pg=SendRepair">Repairs</a> | <a href="/drive-thru-repair-help-center">Drive-Thru Help Center</a> | <a href="California_Transparency_Supply_Chains_Act_Disclosures.pdf" target="_blank">California Supply Chains Act</a> | <a href="/about-ce">About CE</a> | <a href="/contact-ce-drive-thru-repairs">Contact Us</a>
			</div>
		</div>
		<div class="bot_footer clear">
			<div class="copyright">
				<!---20131211 CG: Added if statement for updated terms & conditions Ticket#46058 --->
				<span style="padding-right:60px">&copy;<cfoutput>#Year(Now())#</cfoutput> Commercial Electronics, Inc. (CE). All Rights Reserved.| 
				<cfif Year(Now()) GTE 2014>
					<!--- 20140918 CG: Updated Privacy Policy Ticket#55314 --->
					<a href="./CE_Terms_and_Conditions_of_Use_Final_11_18_13.pdf" target="_blank">Terms of Use</a> | 
					<a href="./CE_Online_Privacy_Policy_Final_09_05_14.pdf" target="_blank">Privacy Policy</a> | 
				<cfelse>
					<a href="./Terms_and_Conditions_of_Use.pdf" target="_blank">Terms of Use</a> | 
					<a href="./Online_Privacy_Policy.pdf" target="_blank">Privacy Policy</a> | 
				</cfif>
                <!--- 20151001 CG: Added Return and Refund Policy PDF Ticket#65979 --->
				<a href="./Return_and_Refund_Policy.pdf" target="_blank">Sales and Refunds</a></span>
                <span>You can also order by phone by calling 1-877-731-0334</span>
				<!---20140221 CG: Added text per Ticket#49326 --->
                <br />
				<span style="line-height:2;">All brand and product names are trademarks or registered trademarks of their respective companies.</span>
                <!---20161003 HRS Added text per Ticket_#77518 --->
                <br />
				<span>*CE is an independent manufacturer and service provider of parts and is not affiliated with or endorsed by 3M, nor are CE parts or services approved by 3M for use with 3M drive-thru systems. CE services are not covered by the 3M Manufacturer's Warranty.
</span>
			</div>
		</div>
	</div>
</div>
</body>
</html>
<br clear="all" />