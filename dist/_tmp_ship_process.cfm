<cfsilent>

<cfparam name="FORM.item0" default="0">
<cfparam name="FORM.item1" default="0">
<cfparam name="FORM.item8" default="0">
<cfparam name="FORM.item9" default="0">
<cfparam name="FORM.item10" default="0">
<cfparam name="FORM.Product_Name" default="0">
<cfparam name="FORM.Store_Name" default="">
<cfparam name="FORM.Company_Name" default="">
<cfparam name="FORM.RMA" default="">

<!--- send user to calling page if client side validation fails --->
<cfif NOT IsDefined("Form.Store_Number") OR NOT IsDefined("Form.ZipCode")>
	<cflocation url="#CGI.HTTP_REFERER#">
</cfif>

<cfset item9Wgt=0>
<cfset products = "">
<cfif isdefined("form.item9Wgt") eq false><cfset item9Wgt=0><cfelse><cfset item9Wgt=form.item9Wgt> </cfif>
<cfif form.item0 eq 0 OR form.item0 IS ""><cfset item0=0> <cfelse><cfset item0=form.item0> <cfset products = products & ";" & FORM.Product_Name & form.item0>  </cfif>
<cfif form.item1 eq 0 OR form.item1 IS ""><cfset item1=0> <cfelse><cfset item1=form.item1> <cfset products = products & ";" & "Headset:"& form.item1>  </cfif>
<cfif form.item2 eq 0 OR form.item2 IS ""><cfset item2=0> <cfelse><cfset item2=form.item2> <cfset products = products & ";" & "All-in-one headset:"& form.item2> </cfif>
<cfif form.item3 eq 0 OR form.item3 IS ""><cfset item3=0> <cfelse><cfset item3=form.item3>  <cfset products = products & ";" & "Communicator:"& form.item3></cfif>
<cfif form.item4 eq 0 OR form.item4 IS ""><cfset item4=0> <cfelse><cfset item4=form.item4*6> <cfset products = products & ";" & "Timer SYS30:"& form.item4> </cfif>
<cfif form.item5 eq 0 OR form.item5 IS ""><cfset item5=0> <cfelse><cfset item5=form.item5*7> <cfset products = products & ";" & "Timer DASH (control unit):"& form.item5> </cfif>
<cfif form.item6 eq 0 OR form.item6 IS ""><cfset item6=0> <cfelse><cfset item6=form.item6*10> <cfset products = products & ";" & "Timer Zoom (control unit):"& form.item6></cfif>
<cfif form.item7 eq 0 OR form.item7 IS ""><cfset item7=0> <cfelse><cfset item7=form.item7*3> <cfset products = products & ";" & "Zoom TSP:"& form.item7> </cfif>
<cfif form.item8 eq 0 OR form.item8 IS ""><cfset item8=0> <cfelse><cfset item8=form.item8*7> <cfset products = products & ";" & "Battery Charger:"& form.item8> </cfif>

<cfif isdefined("form.item9a")><cfset item9=form.item9a> <cfset products = products & ";" & form.item9a & ":"& form.item9a> <cfelse><cfset item9a=0></cfif>
<cfset item9 = item9Wgt>
<cfset weight = item0+item1+item2+item3+item4+item5+item6+item7+item8+item9>
<cfif weight EQ 0><cfset weight = 2></cfif>

<cfset Company_Name = form.Company_Name >
<cfset Store_Name = form.Store_Name>
<cfset Store_Number = form.Store_Number>
<cfset First_Name = form.First_Name>
<cfset Last_Name = form.Last_Name>
<cfset contact_name =First_Name &" "& Last_Name>
<cfset Email = form.Email>
<cfset phone_number = form.Phone>
<cfset Address1 = form.Address1>
<cfset Address2 = form.Address2>
<cfset City= form.City>
<cfset State = form.State>
<cfset zip_code = form.ZipCode>
<cfset rma_number = form.RMA>

<cfset xmlreq="
<?xml version=""1.0"" ?>
<AccessRequest xml:lang='en-US'>
	<AccessLicenseNumber>1C57A07278A3E128</AccessLicenseNumber>
	<UserId>CENRoeder</UserId>
	<Password>CEHME2010</Password>
</AccessRequest>
<?xml version=""1.0"" ?>
<ShipmentConfirmRequest>
	<Request>
		<TransactionReference>
			<CustomerContext>guidlikesubstance</CustomerContext>
			<XpciVersion>1.0001</XpciVersion>
		</TransactionReference>
		<RequestAction>ShipConfirm</RequestAction>
		<RequestOption>nonvalidate</RequestOption>
	</Request>
	<Shipment>
		<ReturnService>
			<Code>9</Code>
		</ReturnService>
		<Shipper>
			<Name>#First_Name# #Last_Name#</Name>
			<AttentionName>#First_Name# #Last_Name#</AttentionName>
			<ShipperNumber>679382</ShipperNumber>
			<PhoneNumber>#Phone#</PhoneNumber>
			<Address>
				<AddressLine1>#Address1#</AddressLine1>
				<AddressLine2>#Address2#</AddressLine2>
				<City>#City#</City>
				<StateProvinceCode>#State#</StateProvinceCode>
				<CountryCode>US</CountryCode>
				<PostalCode>#ZipCode#</PostalCode>
			</Address>
		</Shipper>
		<ShipTo>
			<CompanyName>Commercial Electronics Repair </CompanyName>
			<AttentionName>RMA</AttentionName>
			<PhoneNumber>8777310334</PhoneNumber>
			<Address>
				<AddressLine1>3421 Hollenberg Drive</AddressLine1>
				<City>Bridgeton</City>
				<StateProvinceCode>MO</StateProvinceCode>
				<CountryCode>US</CountryCode>
				<PostalCode>63044</PostalCode>
			</Address>
		</ShipTo>
		<ShipFrom>
			<CompanyName>#Store_Name# : #Store_Number#</CompanyName>
			<AttentionName>#First_Name# #Last_Name#</AttentionName>
			<ShipperNumber>679382</ShipperNumber>
			<Address>
				<AddressLine1>#Address1#</AddressLine1>
				<AddressLine2>#Address2#</AddressLine2>
				<City>#City#</City>
				<StateProvinceCode>#State#</StateProvinceCode>
				<CountryCode>US</CountryCode>
				<PostalCode>#ZipCode#</PostalCode>
			</Address>
		</ShipFrom>
		<PaymentInformation>
			<Prepaid>
				<BillShipper>
					<AccountNumber>679382</AccountNumber>
				</BillShipper>
			</Prepaid>
		</PaymentInformation>
		<Service>
			<Code>03</Code>
			<Description>Ground</Description>
		</Service>
		<Package>
			<PackagingType>
				<Code>02</Code>
			</PackagingType>
			<Description>Package Description</Description>
			<PackageWeight>
				<Weight>#weight#</Weight>
			</PackageWeight>
			<ReferenceNumber>
				<Value>#Phone#</Value>
			</ReferenceNumber>
			<ReferenceNumber>
				<Code>RZ</Code>
				<Value>#RMA#</Value>
			</ReferenceNumber>
			<PackageServiceOptions>
				<InsuredValue>
					<CurrencyCode>USD</CurrencyCode>
					<MonetaryValue>99.00</MonetaryValue>
				</InsuredValue>
			</PackageServiceOptions>
		</Package>
	</Shipment>
	<LabelSpecification>
		<LabelPrintMethod>
			<Code>GIF</Code>
		</LabelPrintMethod>
		<HTTPUserAgent>Mozilla/4.5</HTTPUserAgent>
		<LabelImageFormat>
			<Code>GIF</Code>
		</LabelImageFormat>
	</LabelSpecification>
</ShipmentConfirmRequest>">

<cfhttp url="https://onlinetools.ups.com/ups.app/xml/ShipConfirm" port="443" method="POST" throwonerror="yes">
     <cfhttpparam name="shiprequest" type="XML" value="#xmlreq#">
</cfhttp>

<cfset ShipData = XmlParse(CFHTTP.FileContent)>
</cfsilent>
<cfif ShipData.XmlRoot.Response.ResponseStatusCode.XmlText eq 0 >
	<cfset ERR.ErrorFound = true>
    <cfset ERR.ErrorMessage="#ShipData.ShipmentConfirmResponse.Response.Error.ErrorDescription.XmlText#">
	<cfinclude template="./_tmp_ship_default.cfm">
	<cfexit method="exittemplate">
</cfif>
<cfsilent>
<cfset AcceptRequest = "
<?xml version=""1.0"" ?>
<AccessRequest xml:lang='en-US'>
	<AccessLicenseNumber>1C57A07278A3E128</AccessLicenseNumber>
	<UserId>CENRoeder</UserId>
	<Password>CEHME2010</Password>
</AccessRequest>
<?xml version=""1.0"" ?>
<ShipmentAcceptRequest>
	<Request>
		<TransactionReference>
			<CustomerContext>guidlikesubstance</CustomerContext>
			<XpciVersion>1.0001</XpciVersion>
		</TransactionReference>
		<RequestAction>ShipAccept</RequestAction>
		</Request>
		<ShipmentDigest>#ShipData.XmlRoot.ShipmentDigest.XmlText#</ShipmentDigest>
</ShipmentAcceptRequest>">

<cfhttp url="https://onlinetools.ups.com/ups.app/xml/ShipAccept" port="443" method="POST" throwonerror="yes">
     <cfhttpparam name="shiprequest" type="XML" value="#AcceptRequest#">
</cfhttp>

<cfset LableData = XmlParse(CFHTTP.FileContent)>
<cfset TrackingNumber = LableData.XmlRoot.ShipmentResults.ShipmentIdentificationNumber.XmlText>

<cfset strBase64Value = "#LableData.XmlRoot.ShipmentResults.PackageResults.LabelImage.GraphicImage.XmlText#">
<!---Generate random uid for image--->
<cfscript>
	getUID = CreateObject("component", "_cfcs.Generate_UID");
	IMAGE_UID = getUID.genUID(Length=32);
</cfscript>

<cfimage 
	source="#strBase64Value#" 
	action="resize" 
	width="651" 
	height="392"
	destination="#getDirectoryFromPath(getCurrentTemplatePath())#\images\ups\#IMAGE_UID#.gif" 
	overwrite="yes"
	isBase64 = "yes">

<!--- This is the insert into the Database! --->
<cfquery name="insert" datasource="HMEUPS" >
	INSERT INTO UPS_CAPTURE 
		(store_name,
		store_number,
		contact_name,
		phone_number,
		rma_number,
		address_1,
		address_2,
		city,
		state,
		zip_code,
		email,
		products,
		product_desc,
		tracking_number,
		Packing_Label,
		HME_Company) 
	VALUES
		(<cfqueryparam cfsqltype="cf_sql_varchar" value="#store_name#">,
		'#store_number#',
		'#contact_name#',
		'#phone_number#',
		'#rma_number#',
		'#address1#',
		'#address2#',
		'#city#',
		'#state#',
		'#zip_code#',
		'#email#',
		'#products#',
		'',
		'#TrackingNumber#',
		<cfqueryparam cfsqltype="cf_sql_varchar" value="#IMAGE_UID#.gif">,
		<cfqueryparam cfsqltype="cf_sql_varchar" value="CE">)
</cfquery>
<!---20131206 CG: Added Mike Kapler to the distribution list per Ticket#47580 --->
<!---20140718 WW: Removed Kathy Moorhouse from the distribution list per Ticket#53322 --->
<cfset emailDist = "CStockton@ceinstl.com,RTurner@ceinstl.com,mkapler@ceinstl.com,abaer@hme.com,RKarsten@hme.com,bfelix@hme.com,ARathbun@ceinstl.com,JBellin@ceinstl.com,jbach@cerepairs.com">

<cfmail to="#emailDist#" bcc="atnelson@hme.com" from="no-reply@cerepairs.com" subject="UPS Electronic Return Label - User Information" type="html">
<table border="0" cellpadding="0" cellspacing="0" width="600">
	<tr>
		<td height="410" align="left" valign="top">
			<table>
            	<tr>
                	<td>Tracking Number:</td><td>#TrackingNumber#</td>
                </tr>
				<tr>
					<td>Chain:</td><td>#store_name# - #store_number#</td>
				</tr>
				<tr>
					<td>Contact:</td><td>#contact_name#</td>
				</tr>
				<tr>
					<td>Phone:</td><td>#phone_number#</td>
				</tr>
				<tr>
					<td>Address:</td><td>#address1#<br /><cfif address2 IS NOT "">#address2#<br /></cfif>#city#, #state# #zip_code#</td>
				</tr>
				<tr>
					<td>Email:</td><td>#email#</td>
				</tr>
				<tr>
					<td>Products:</td><td>#products#</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</cfmail>
</cfsilent>
<!--- Google Code for Free Shipping Label Download Conversion Page --->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1000449060;
var google_conversion_language = "en";
var google_conversion_format = "2";
var google_conversion_color = "ffffff";
var google_conversion_label = "TDNyCMyc0wQQpMiG3QM"; var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript"  
src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt=""  
src="//www.googleadservices.com/pagead/conversion/1000449060/?value=0&amp;label=TDNyCMyc0wQQpMiG3QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<cfset Page_MetaTitle = "UPS Electronic Return Label: View/Print Label">
<style> 
.small_text {font-size: 80%;}
.large_text {font-size: 115%;}
.line_break_dash {border-top:2px dashed #000000;padding-bottom:15px;}
.cust_addr {height:180px;font-size:30px;}
</style>
<div class="ship_repair">
<table border="0" cellpadding="0" cellspacing="0" width="600">
	<tr>
		<td height="210" align="left" valign="top">
		<B class="large_text">UPS Electronic Return Label: View/Print Label</B>&nbsp;<br>
		<ol class="small_text">
			<li><b>Ensure that there are no other tracking labels attached to your package.</b><br><br></li>
			<li><b>Tear this sheet of paper at the dotted line.</b> Place the top half of this sheet of paper inside of the shipping box. Place the bottom half of this sheet of paper containing the UPS label in a UPS Shipping Pouch. If you do not have a pouch, affix the label using clear plastic shipping tape over the entire label.  Take care not to cover any seams or closures.<br><br></li>
			<li><b>Collection and Drop-off<br></b>
				<ul>
					<li>Take this package to any location of The UPS Store &reg;, UPS Drop Box, UPS Customer Center, UPS Alliances (Office Depot&reg; or Staples&reg;) or Authorized Shipping Outlet near you. Items sent via UPS ReturnsSM Services(including via UPS Ground) are accepted at Drop Boxes, to find your closest UPS location visit  <a href="http://www.ups.com/content/us/en/index.jsx" target="_blank">www.ups.com/content/us/en/index.jsx</a> and select Drop Off. Click to locate Dropoff location: <a href="http://www.ups.com/dropoff?autosubmit=1&loc=en_US&appid=XOLT&country=US&Postal=<cfoutput>#ZipCode#</cfoutput>&trans_mode=002" target="_blank">Drop Off Locator</a></li>
					<li>Daily Collection customers: Have your shipment(s) ready for the driver as usual.</li>
				</ul>
			</li>
		</ol>
		</td>
	</tr>
</table>
<div class="cust_addr">
	<cfoutput>
		#contact_name#<br />
		#store_name# : #store_number#<br />
		#address1#<br />
		<cfif address2 NEQ ''>
			#address2#<br />
		</cfif>
		#city#, #state# #zip_code#<br />
	</cfoutput>
</div>
<div class="line_break_dash"></div>
<table border="0" cellpadding="0" cellspacing="0" width="600">
	<tr>
		<td class="small_text" align="left" valign="top">&nbsp;&nbsp;&nbsp;<a name="foldHere">TEAR HERE</a></td>
	</tr>
	<tr>
		<td align="left" valign="top"><hr></td>
	</tr>
</table>
<table>
	<tr>
		<td height="10">&nbsp;</td>
	</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" height="392" width="651">
	<tr>
		<td align="left" valign="top" height="392" width="651"><cfoutput><img src="/images/ups/#IMAGE_UID#.gif" /></cfoutput></td>
	</tr>
</table>
<table width="600">
	<!---20140708 WW: Added print shipping label popup per Ticket#51800 --->
	<tr>
		<td><a href="/js/print_shipping_label.cfm?tr=<cfoutput>#TrackingNumber#</cfoutput>" id="printlabel" target="_blank">Click here to print the return label</a></td>
	</tr>
    <tr>
		<td><a href="./?pg=SendRepair&st=SendLabel&tr=<cfoutput>#TrackingNumber#</cfoutput>">Click here to have a copy of the return label emailed to you</a></td>
	</tr>
	<tr>
		<td><em>Please note: This link will expire within 24 hours</em></td>
	</tr>
</table>
</div>