<!---
	REQUIRED VARIABLES:
	
	ATTRIBUTES.RECORDCOUNT (int)
	ATTRIBUTES.TOTALCOUNT (int)
	ATTRIBUTES.LIMIT (int)
	ATTRIBUTES.OFFSET (int)
	
	OPTIONAL VARIABLES:
	
	ATTRIBUTES.POST (boolean)
	ATTRIBUTES.VAR (string) variable name used for offset
	ATTRIBUTES.STRINGPREV (string) used to denote the previous page link
	ATTRIBUTES.STRINGNEXT (string) used to denote the next page link
	ATTRIBUTES.TruncatePates (int) used to shorten length of output
--->

<cfparam name="ATTRIBUTES.POST" default=false>
<cfparam name="ATTRIBUTES.VAR" default="o">
<cfparam name="ATTRIBUTES.TRUNCATEPAGES" default=0>
<cfparam name="ATTRIBUTES.StringPrev" default="<<<">
<cfparam name="ATTRIBUTES.StringNext" default=">>>">
<cfparam name="ATTRIBUTES.PageLink" default="#CGI.SCRIPT_NAME#">
<cfparam name="ATTRIBUTES.ID" default="NextPrev">
<cfparam name="ATTRIBUTES.Class" default="">
<cfparam name="ATTRIBUTES.Query_String" default="#CGI.QUERY_STRING#">
<cfparam name="FORM.FieldNames" default="">

<cfif IsDefined("ATTRIBUTES.RECORDCOUNT") AND IsDefined("ATTRIBUTES.TOTALCOUNT") AND IsDefined("ATTRIBUTES.LIMIT") AND IsDefined("ATTRIBUTES.OFFSET")>
	<cfset URLParam = REReplaceNoCase(ATTRIBUTES.Query_String,"((^|\?|&)#ATTRIBUTES.VAR#=[0-9]+)","","all")>
	<cfset TotalPages = Ceiling(ATTRIBUTES.TotalCount / ATTRIBUTES.Limit)>
	<cfset CurrentPage = Int(ATTRIBUTES.Offset / ATTRIBUTES.Limit) + 1>
	<cfoutput>
		<cfif ATTRIBUTES.Post>
			<form name="nextprev" method="post" action="#ATTRIBUTES.PageLink#?#URLParam#" class="hidden">
				<cfset FieldList = "">
				<cfloop list="#FORM.FieldNames#" index="field">
					<cfif NOT ListFind(FieldList,field)>
						<cfset FieldList = ListAppend(FieldList,field)>
					</cfif>
				</cfloop>
				<cfloop list="#FieldList#" index="field">
					<input type="hidden" name="#field#" value="#FORM[field]#" />
				</cfloop>
			</form>
		</cfif>
		<table cellpadding="0" cellspacing="0" id="#ATTRIBUTES.ID#" class="#ATTRIBUTES.Class#">
			<tr>
				<td id="Prev">
					<cfif ATTRIBUTES.Offset NEQ 0>
						<cfif ATTRIBUTES.Offset - ATTRIBUTES.Limit LT 0>
							<cfif ATTRIBUTES.Post>
								<a href="javascript:document.forms.nextprev.action += '&#ATTRIBUTES.VAR#=0';document.forms.nextprev.submit();">#ATTRIBUTES.StringPrev#</a>
							<cfelse>
								<a href="#ATTRIBUTES.PageLink#?#URLParam#&#ATTRIBUTES.VAR#=0">#ATTRIBUTES.StringPrev#</a>
							</cfif>
						<cfelse>
							<cfif ATTRIBUTES.Post>
								<a href="javascript:document.forms.nextprev.action += '&#ATTRIBUTES.VAR#=#Evaluate('ATTRIBUTES.Offset - ATTRIBUTES.Limit')#';document.forms.nextprev.submit();">#ATTRIBUTES.StringPrev#</a>
							<cfelse>
								<a href="#ATTRIBUTES.PageLink#?#URLParam#&#ATTRIBUTES.VAR#=#Evaluate('ATTRIBUTES.Offset - ATTRIBUTES.Limit')#">#ATTRIBUTES.StringPrev#</a>
							</cfif>
						</cfif>
					<cfelse>
						#ATTRIBUTES.StringPrev#
					</cfif>
				</td>
				<td id="Pages">
					<cfset DotCount = 0>
					<cfloop index="Page" from="1" to="#TotalPages#">
						<cfif Page EQ CurrentPage>
							<cfset DotCount = 0>
						</cfif>
						<cfif (ATTRIBUTES.TruncatePages EQ 0) OR (Page LTE ATTRIBUTES.TruncatePages + 1) OR (Page GT (TotalPages - (ATTRIBUTES.TruncatePages + 1))) OR ((Page GTE CurrentPage - ATTRIBUTES.TruncatePages) AND (Page LTE CurrentPage + ATTRIBUTES.TruncatePages))>
							<cfif Page EQ CurrentPage>
								<span class="curpage">#Page#</span>
							<cfelse>
								<cfif ATTRIBUTES.Post>
									<a href="javascript:document.forms.nextprev.action += '&#ATTRIBUTES.VAR#=#Evaluate('(Page - 1) * ATTRIBUTES.Limit')#';document.forms.nextprev.submit();">#Page#</a>
								<cfelse>
									<a href="#ATTRIBUTES.PageLink#?#URLParam#&#ATTRIBUTES.VAR#=#Evaluate('(Page - 1) * ATTRIBUTES.Limit')#">#Page#</a>
								</cfif>
							</cfif>
						<cfelseif (DotCount LT 3)>
							.
							<cfset DotCount = DotCount + 1>
						</cfif>
					</cfloop>
				</td>
				<td id="Next">
					<cfif ATTRIBUTES.Offset + ATTRIBUTES.RecordCount LT ATTRIBUTES.TotalCount>
						<cfif ATTRIBUTES.Post>
							<a href="javascript:document.forms.nextprev.action += '&#ATTRIBUTES.VAR#=#Evaluate('ATTRIBUTES.Offset + ATTRIBUTES.Limit')#';document.forms.nextprev.submit();">#ATTRIBUTES.StringNext#</a>
						<cfelse>
							<a href="#ATTRIBUTES.PageLink#?#URLParam#&#ATTRIBUTES.VAR#=#Evaluate('ATTRIBUTES.Offset + ATTRIBUTES.Limit')#">#ATTRIBUTES.StringNext#</a>
						</cfif>
					<cfelse>
						#ATTRIBUTES.StringNext#
					</cfif>
				</td>
			</tr>
		</table>
	</cfoutput>
</cfif>