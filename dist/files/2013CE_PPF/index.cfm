<cfsilent>
<!---2012/08/08 ATN: 
Email Blast Form Submittal
rl - Response Link
rs - Response Subject
rt - Response Email To - Internal Switch on _tmp_requestinfo.cfm since it's not good design to expose an actual email as a URL var
qsr - QSRResponse@hme.com - default address
pcd - PCDResponse@hme.com
wrl - WirelessInfo@hme.com
pcn - hme@advcomsystems.com
--->
<cfparam name="rl" default="#CGI.HTTP_HOST##CGI.PATH_INFO#"> 
<cfparam name="rs" default="BK 1/29/13"> 
<cfparam name="rt" default="qsr"> 
</cfsilent>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CE Repairs</title>
</head>
<style type="text/css" media="all">
@import "/css/master.css";
</style>
<style>
* {
	margin:0;
	padding:0;
}
body {
	margin:0;
	padding:0;
}
div.image {
	margin:0 auto;
	display:block;
	border:0;
	width:570px;
}
</style>

<script type="text/javascript" src="/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="/js/mod/jquery.colorbox.js"></script>
<script type="text/javascript" src="/js/mod/master.js"></script>

<script type="text/javascript">
$(document).ready(function(){
$(".cboxElement").colorbox({innerWidth:"510px", innerHeight:"530px", iframe:true, open:true});
}); 
</script>

<body>
<cfoutput>
<div  class="image">
<a href="http://www.cerepairs.com/CE-EMA">
<img src="./protection-plan-email.jpg" border="0" />
</a>
</div>
</cfoutput>
</body>
</html>