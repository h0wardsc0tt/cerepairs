<cfsilent>
<cfscript>
	QUERY = StructNew();
	QUERY.QueryName = "qry_getProducts";
	QUERY.Datasource = APPLICATION.Datasource;
	QUERY.OrderBy = "prod.Product_Short_Description";
	
	if(IsDefined("URL.pdt") AND ReFindNoCase("[a-z0-9]{32}", URL.pdt)) {
		QUERY.Product_UID = URL.pdt;
		QUERY.SelectSQL = "prod.*, type.ProductType AS thisCategory, type.ProductType_UID, manf.Manufacturer_Name ";
	}
</cfscript>
<cfinclude template="./_qry_select_cat_type_product.cfm">

<cfscript>
	trackProduct = CreateObject("component", "_cfcs.SessionMgmt");
	thisProduct = trackProduct.TrackView(Product_UID=qry_getProducts.Product_UID,Session_UID=SESSION.Session_UID,User_Cookie=qry_getUser.User_Cookie);
</cfscript>

<cfscript>
	QUERY = StructNew();
	QUERY.QueryName = "qry_getRelated";
	QUERY.Datasource = APPLICATION.Datasource;
	QUERY.SelectSQL = "prod.*, type.ProductType AS thisCategory";
	QUERY.ProductType_UID = ValueList(qry_getProducts.ProductType_UID);
	QUERY.Product_UID_NOT_IN = qry_getProducts.Product_UID;
	QUERY.OrderBy = "NEWID()";
</cfscript>
<cfinclude template="./_qry_select_cat_type_product.cfm">

<cfquery name="qry_getDistinct" dbtype="query" maxrows="5">
	SELECT DISTINCT Product_UID, Product_Image_Thum, Product_Short_Description, Repair_Price, Retail_Price, Refurbished_Price, Exchange_Price, Recondition_Price
	FROM qry_getRelated
</cfquery>

<!---Find Discounts--->
<cfscript>
	QUERY = StructNew();
	QUERY.QueryName = "qry_getDiscounts";
	QUERY.Datasource = APPLICATION.Datasource;
	
	QUERY.Offer_IsActive = 1;
	QUERY.Offer_ValidTo = Now();
	QUERY.Offer_ValidFrom = Now();
	if(IsDefined("URL.pdt") AND ReFindNoCase("[a-z0-9]{32}", URL.pdt)) {
		QUERY.Product_UID = URL.pdt;
	}
</cfscript>
<cfinclude template="./_qry_select_offer.cfm">
</cfsilent>
<cfsavecontent variable="AddToCart">
<!---Raw Javascript Obfuscate code before making live.--->
<script type="text/javascript"> 
	<cfoutput>
	function html_CartCount(data) {
		var ret_result = "";
		$.each(data, function(index, data) {
			if(data.carttotal != "fail") {
				var cartcount = document.getElementById("cartcount");
				cartcount.innerHTML = " (" + data.carttotal + ")";
				
				/*if($("html").hasClass("IE8")) {
					alert("Product Successfully Added!");
				} else {
					var $dialog = $('.show_cart').dialog({
							dialogClass:'ui-dialog',
							draggable:false,
							autoOpen:false,
							resizeable:false,
							title:'hello'
						});
					$(".ui-dialog").addClass("itemadded itemstyle");
					$(".close_cart").css("display","block");
					$dialog.load("/js/quick_cart_view.cfm").dialog("open");
				}*/
				ret_result = "pass";
				window.location.href = "https://#APPLICATION.rootURL#/?pg=Cart";
			} else {
				ret_result = "fail";
			}
		});
		
		return ret_result;
	}
	function closeAdd() {
		//$(".itemadded").dialog('close');
		$('.show_cart').dialog('close');
		$('.close_cart').css('display','none');
		
		return false;
	}
	function addCart(field_id,prc) {
		var jadd_act = "/js/addCart.cfm";
		$.ajax({
			url: jadd_act, 
			data: ({ sess: '#SESSION.Session_UID#', pdt: field_id, qty: 1, uco: '#COOKIE.User_Cookie#', prc:prc }),
			dataType: "json",
			success: function(data) {
				var set_msg = html_CartCount(data);
				if(set_msg == "fail") {
					alert("I'm sorry your current shopping session has expired.");
					window.location.href = "https://#APPLICATION.rootURL#";
				}
			}
		});
	}
	</cfoutput>
</script>
</cfsavecontent>
<cfhtmlhead text="#AddToCart#">	
<cfoutput>
<div class="detail clear">
	<cfif IsDefined("ERR.ErrorFound") AND ERR.ErrorFound>
	<div class="error">
		<h4>The following errors occured during your request</h4>
		<ul>
			<cfloop list="#ERR.ErrorMessage#" index="thisError"><li>#thisError#</li>
			</cfloop>
		</ul>
	</div>
	</cfif>
	<div class="prod pimage">
		<cfif FileExists("#APPLICATION.rootDir#\images\products\#qry_getProducts.Product_Image_Med#")>
		<img src="/images/products/#qry_getProducts.Product_Image_Med#" /><cfelse><img src="/images/products/med_noimage.png" /></cfif>
		<cfif FileExists("#APPLICATION.rootDir#\images\products\#qry_getProducts.Product_Image_Full#")>
			<h5 class="enlarge"><a href="/images/products/#qry_getProducts.Product_Image_Full#" class="lightbox">Enlarge Image</a></h5>
		</cfif>
	</div>
	<div class="prod_detail clear">
		<div class="description">
			<h2>#qry_getProducts.Product_Short_Description#</h2>
			<p><strong>Part Number:</strong> #qry_getProducts.Manufacturer_Part_Number#</p>
		</div>
		<div class="action clear">
			<cfif qry_getProducts.Repair_Price NEQ 0 AND qry_getProducts.Recondition_Price NEQ 0>
				<div class="prod_price clear">
					<div class="prod_type gray-text pro-btm-marg"><strong>REPAIR</strong> Standard/Recondition<span class="tooltip"><img src="/images/tooltip.png" width="10"/><div class="tooltip"><h5>Standard/Recondition</h5><p>Basic functional issues will incur a repair fee. Repairs needing new case parts, button, gaskets or headset cords will be charged a reconditioning fee based on technician's assessment. In either case, a reconditioned replacement item may be returned to you.</p></div></span></div>
					<div class="prod_action pro-btm-marg gray-text"><a href="./?pg=SendRepair&pdt=#qry_getProducts.Product_UID#" class="send-it-in-lrg gray-text">$#NumberFormat(qry_getProducts.Repair_Price)#/$#NumberFormat(qry_getProducts.Recondition_Price)# <span class="thin-text">Send it in</span></a></div>
				</div>
			</cfif>
			<cfif qry_getProducts.Repair_Price NEQ 0 AND qry_getProducts.Recondition_Price EQ 0>
				<div class="prod_price clear">
					<div class="prod_type gray-text pro-btm-marg"><strong>REPAIR</strong> Standard<span class="tooltip"><img src="/images/tooltip.png" width="10"/><div class="tooltip"><h5>Standard/Recondition</h5><p>Basic functional issues will incur a repair fee. Repairs needing new case parts, button, gaskets or headset cords will be charged a reconditioning fee based on technician's assessment. In either case, a reconditioned replacement item may be returned to you.</p></div></span></div>
					<div class="prod_action pro-btm-marg gray-text"><a href="./?pg=SendRepair&pdt=#qry_getProducts.Product_UID#" class="send-it-in-lrg gray-text">$#NumberFormat(qry_getProducts.Repair_Price)# <span class="thin-text">Send it in</span></a></div>
				</div>
			</cfif>
			<cfif qry_getProducts.Repair_Price EQ 0 AND qry_getProducts.Recondition_Price NEQ 0>
				<div class="prod_price clear">
					<div class="prod_type gray-text pro-btm-marg"><strong>REPAIR</strong> Recondition<span class="tooltip"><img src="/images/tooltip.png" width="10"/><div class="tooltip"><h5>Recondition</h5><p>Basic functional issues will incur a repair fee. Repairs needing new case parts, button, gaskets or headset cords will be charged a reconditioning fee based on technician's assessment. In either case, a reconditioned replacement item may be returned to you.</p></div></span></div>
					<div class="prod_action pro-btm-marg gray-text"><a href="./?pg=SendRepair&pdt=#qry_getProducts.Product_UID#" class="send-it-in-lrg gray-text">$#NumberFormat(qry_getProducts.Recondition_Price)# <span class="thin-text">Send it in</span></a></div>
				</div>
			</cfif>
            <cfif qry_getProducts.Exchange_Price NEQ 0>
			<div class="prod_price clear">
				<div class="prod_type gray-text pro-top-marg"><strong>REPAIR</strong> Advance Exchange<span class="tooltip"><img src="/images/tooltip.png" width="10"/><div class="tooltip"><h5>Advance Exchange</h5><p>If you can't wait for a repair, CE will overnight you a refurbished unit in exchange for your broken unit.  Once you receive the replacement unit, return your broken unit in the same box. <div style="padding:12pt 0 12pt 0;">A minimum $35 shipping fee will apply.</div><div>Additional charges may apply for expedited shipping needs.</div></p></div></span></div>
				<div class="prod_action pro-top-marg gray-text"><strong>$#NumberFormat(qry_getProducts.Exchange_Price)#</strong> Call 877-731-0334</div>
			</div>
			</cfif>
			<!---<cfif qry_getProducts.Recondition_Price NEQ 0>
			<div class="prod_price clear">
				<!---20140114 CG: Changed "Reconditioned" to "Recondition" per Ticket#48326 --->
				<div class="prod_type">Recondition<span class="tooltip"><img src="/images/tooltip.png" /><div class="tooltip"><h5>Reconditioning</h5><p>The next step in repairs. Send in your equipment for a traditional repair as well as new case parts, buttons, and gaskets.  Makes your equipment look and function like new!</p></div></span></div>
				<!---20140123 CG: Removed "add to cart" link per Ticket#48524 --->
				<div class="prod_action">#DollarFormat(qry_getProducts.Recondition_Price)#</div>
			</div>
			</cfif>--->
            <br />
			<cfif qry_getProducts.Retail_Price NEQ 0>
			<div class="prod_price clear">
				<cfif Left(qry_getProducts.Product_Short_Description,2) IS "3M"><!---Changed Verbage for 3M per ticket 52590--->
				<div class="prod_type pro-btm-marg"><a href="javascript:void(false);" onclick="return addCart('#qry_getProducts.Product_UID#', 'Retail');">BUY <span class="thin-text">Unused</span></a><span class="tooltip"><img src="/images/tooltip.png" width="10" /><div class="tooltip"><h5>Unused</h5><p>These products have never been used.  They do not come with a 3M warranty, but instead are covered by a CE warranty of the same duration as that of the manufacturer.</p></div></span></div>
				<cfelse>
				<div class="prod_type pro-btm-marg"><a href="javascript:void(false);" onclick="return addCart('#qry_getProducts.Product_UID#', 'Retail');">BUY <span class="thin-text">New</span></a><span class="tooltip"><img src="/images/tooltip.png" width="10" /><div class="tooltip"><h5>New</h5><p>These products are straight from the factory and carry the OEM warranty. CE is a fully authorized distributor of this equipment.</p></div></span></div>
				</cfif>
				<div class="prod_action"><a href="javascript:void(false);" onclick="return addCart('#qry_getProducts.Product_UID#', 'Retail');" class="add-to-cart-lrg">#DollarFormat(qry_getProducts.Retail_Price)#</a></div>
			</div>
			</cfif>
			<cfif qry_getProducts.Refurbished_Price NEQ 0>
			<div class="prod_price clear">
				<div class="prod_type pro-top-marg"><a href="javascript:void(false);" onclick="return addCart('#qry_getProducts.Product_UID#', 'Refurbished');">BUY <span class="thin-text">Refurbished</span></a><span class="tooltip"><img src="/images/tooltip.png" width="10"/><div class="tooltip"><h5>Refurbished</h5><p>This is a previously used product. Our technicians thoroughly check, repair, and upgrade to ensure like-new operation.  Your purchase of these products includes a 4-month warranty. Please note that all refurbished items are subject to equipment availability.</p></div></span></div>
				<div class="prod_action pro-top-marg"><a href="javascript:void(false);" onclick="return addCart('#qry_getProducts.Product_UID#', 'Refurbished');" class="add-to-cart-lrg">#DollarFormat(qry_getProducts.Refurbished_Price)#</a></div>
			</div>
			</cfif>
            <br /><br /><br /><br />
		</div>
		<div class="description">
			<h3>Details</h3>
			<p>#qry_getProducts.Product_Long_Description#</p>
		</div>
		<!---<cfif qry_getDiscounts.RecordCount NEQ 0>
		<div class="specialoffer">
			<h3>Special Offer</h3>
			<p>#qry_getDiscounts.Offer_Description#</p>
		</div>
		</cfif>--->
	</div>
	<div class="clear"></div>
	<div class="component clear">
		<h3>Related Components &amp; Accessories</h3>
		<cfloop query="qry_getDistinct">
		<div class="accessories">
			<a href="./?pg=Detail&pdt=#qry_getDistinct.Product_UID#">
			<cfif FileExists("#APPLICATION.rootDir#\images\products\#qry_getDistinct.Product_Image_Thum#")>
			<img src="/images/products/#qry_getDistinct.Product_Image_Thum#" /><cfelse><img src="/images/products/noimage.gif" /></cfif></a>
			<h4>#qry_getDistinct.Product_Short_Description#</h4>
			<!--- 20140910 CG: Added if list to display prices that arent $0.00 Ticket#55229 --->
			<cfif qry_getDistinct.Retail_Price NEQ "0.0000">
				<h4>#DollarFormat(qry_getDistinct.Retail_Price)#</h4>
			<cfelseif qry_getDistinct.Repair_Price NEQ "0.0000">
				<h4>#DollarFormat(qry_getDistinct.Repair_Price)#</h4>
			<cfelseif qry_getDistinct.Recondition_Price NEQ "0.0000">
				<h4>#DollarFormat(qry_getDistinct.Recondition_Price)#</h4>
			<cfelseif qry_getDistinct.Refurbished_Price NEQ "0.0000">
				<h4>#DollarFormat(qry_getDistinct.Refurbished_Price)#</h4>
			<cfelse>
				<h4>#DollarFormat(qry_getDistinct.Exchange_Price)#</h4>
			</cfif>
		</div>
		</cfloop>
	</div>
	<div class="clear"></div>
	<cfinclude template="./_tmp_product_recommended.cfm">
</div>
</cfoutput>