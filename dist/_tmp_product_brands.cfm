<cfsilent>
	<cfscript>
		QUERY = StructNew();
		QUERY.QueryName = "qry_getManufacturer";
		QUERY.Datasource = APPLICATION.Datasource;
		QUERY.SelectSQL = "DISTINCT manf.Manufacturer_UID, manf.Manufacturer_Name, manf.Manufacturer_Logo, manf.Manufacturer_ID, type.ProductType, type.ProductType_UID, type.ProductType_Order";
		QUERY.Manufacturer_IsActive = "1";
		QUERY.ProductType_IsActive = "1";
		QUERY.OrderBy = "manf.Manufacturer_ID, type.ProductType_Order, type.ProductType";
	</cfscript>
	<cfinclude template="./_qry_select_manf_type.cfm">
</cfsilent>
		
<div class="component clear">
	<!---20131206 CG: Changed Header Content Ticket#47002 --->
	<h3>REPAIRS, ACCESSORIES &amp; SALES BY BRAND</h3>
	<cfoutput query="qry_getManufacturer" group="Manufacturer_ID">
	<div class="manufacturers">
     <!---20161003 HRS Change to 3M logo to use text per Ticket_#77518 --->
    <cfif Manufacturer_UID eq 'XG822OZAORX6P4KPMU4I0OHWDJQASWU4'>
    	<div class="manu_logo"><a href="./?pg=Product&pmu=#Manufacturer_UID#"  style="text-decoration:none; text-align:center;"><div class="large_text_logo_3m">3M<sup>&reg;&nbsp;*</sup></div></a></div>
    <cfelse>
		<div class="manu_logo"><a href="./?pg=Product&pmu=#Manufacturer_UID#"><cfif FileExists("#APPLICATION.rootDir#\images\#Manufacturer_Logo#")><img src="/images/#Manufacturer_Logo#" /><cfelse><img src="/images/noimage.gif" /></cfif></a></div>
    </cfif>
	<!--- End Ticket_#77518 --->
		<ul>
		<cfoutput><li><a href="./?pg=Product&ptu=#ProductType_UID#">#ProductType#</a></li></cfoutput>
		</ul>
		</div>
	</cfoutput>
</div>