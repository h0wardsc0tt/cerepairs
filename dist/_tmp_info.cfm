<cfsilent>
	<cfparam name="Page_Title" default="">
	<cfparam name="Page_Content" default="">
	<cfparam name="Page_BackgroundImage" default="">
	<cfparam name="page_style" default="single">
	<cfif Page_BackgroundImage IS NOT "" AND FileExists("#APPLICATION.rootDir#\images\pages\#Page_BackgroundImage#")>
		<cfset page_style = "double">
	</cfif>
	<cfscript>
		function cleanOldURLs(content, rep_string) { //Make sure current URL is used in content
			oldURLs = "devcerepairs.hme.com,uatcerepairs.hme.com";
			for(x=1;x LTE ListLen(oldURLs); x=x+1) {
				cleanURL = ReplaceNoCase(content, "#ListGetAt(oldURLs, x)#", rep_string, "all");
			}
			return cleanURL;
		}
	</cfscript>
</cfsilent>
<cfoutput>
<div class="info">
	<h1 class="pagetitle">#Page_Title#</h1>
	<div class="#page_style# clear">
		#cleanOldURLs(Page_Content, APPLICATION.rootURL)#
	</div>
	<cfif page_style IS "double">
	<div class="x2 clear">
		<img src="/images/pages/#Page_BackgroundImage#" />
	</div>
	</cfif>
</div>
</cfoutput>