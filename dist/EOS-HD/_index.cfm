<cfsetting showdebugoutput="yes">
<cfparam name="URL.auto" default="">

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
<title>EOS HD&reg; |Drive-Thru Headset Systems | HME</title>
<meta name="keywords" content="drive-thru headset,drive-thru headsets,drive-thru headset system,drive-thru headset systems,drive-thru communication,drive-thru communications"/>
<meta name="description" content="HD Audio transforms the drive-thru conversation with better sound clarity and less noise.">
<!-- Stylesheet for website -->
<style type="text/css" media="all">@import "css/master.css";</style>

<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/jquery.wt-rotator.js"></script>
<script type="text/javascript">
$(document).ready(	
function() {
$("#page-container").wtRotator({
delay:5000,
transition:"diag.fade"
});
}
);
</script>
<script type="text/javascript" src="js/jquery.colorbox.js"></script>
<!--- 2015/01/14 CG: Updated EOS Demo Ticket#57982 --->

<script type="text/javascript" src="js/master.js"></script>
<!-- Javascript for flash videos -->
<script src="js/presentations.js" type="text/javascript"></script>
<script src="Scripts/swfobject_modified.js" type="text/javascript"></script>
</head>
<body onload="colorbox();">
<div id="page-container">
	<cfinclude template="cfincludes/hme_logo.cfm">
	<cfinclude template="cfincludes/hme_drop_down_menu.cfm">
	<h1 class="heading2">Drive-Thru Headset Systems</h1>

<!-- begin rotator -->
<div class="wt-rotator">
	<img id="bg-img" src="css/assets/spacer.png">
	<a href="#"><img id="main-img" src="css/assets/spacer.png"/></a>            
	<div class="desc"></div>
	<div class="preloader"><img src="css/assets/loader.gif"/></div>
	<div id="tooltip"></div>
	<div class="c-panel">
		<div class="buttons">
			<div class="prev-btn"></div>
			<div class="play-btn"></div>
			<div class="next-btn"></div>           
		</div>
		<div class="thumbnails">
			<ul>
				<!--- 20140403 Replaced ION banners with EOS banners Ticket#50235 --->
				<li>
					<img src="images/EOS_Web_Banner_01.jpg" alt="Drive-Thru Headset System EOS | HD">
					<a href="/qsr/drive-thru-headsets-EOS-HD/"></a>
					<!--- <a href="/qsr/audio/sound_comparison.html"  onclick="$(&quot;a[target='ion-video']&quot;).colorbox({iframe:true, innerWidth:650, innerHeight:380});" class="cboxElement" target="ion-video"></a>  --->
					<!---<a href="http://www.youtube.com/v/LJntIjWigbE?version=3?fs=0&amp;hl=en_US&amp;rel=0&amp;hd=0;autoplay=1"  onclick="$(&quot;a[target='ion-video']&quot;).colorbox({iframe:true, innerWidth:630, innerHeight:380});" class="cboxElement" target="ion-video"></a> --->                
				</li>
				<li delay="8000">
					<img src="images/EOS_Web_Banner_02.jpg" alt="EOS | HD Drive-Thru Headset System Features">
					<a href="/qsr/link/HME_EOS-HD_Brochure_Web.pdf" target="_blank"></a>
					<!---<a href="http://www.youtube.com/v/LJntIjWigbE?version=3?fs=0&amp;hl=en_US&amp;rel=0&amp;hd=0;autoplay=1"  onclick="$(&quot;a[target='ion-video']&quot;).colorbox({iframe:true, innerWidth:630, innerHeight:380});" class="cboxElement" target="ion-video"></a>---> 
					<!--- <a href="http://www.youtube.com/v/HyEJ_b-UTkw?fs=0&amp;hl=en_US&amp;rel=0&amp;hd=0;autoplay=1"  onclick="$(&quot;a[target='ion-video']&quot;).colorbox({iframe:true, innerWidth:630, innerHeight:380});" class="cboxElement"  target="ion-video"></a>--->                  
				</li>
				<li>
					<img src="images/EOS_Web_Banner_03.jpg" alt="EOS | HD Drive-Thru Headset System Features">
					<a href="/qsr/link/HME_EOS-HD_Brochure_Web.pdf" target="_blank"></a>
					<!---<a href="http://www.youtube.com/v/LJntIjWigbE?version=3?fs=0&amp;hl=en_US&amp;rel=0&amp;hd=0;autoplay=1"  onclick="$(&quot;a[target='ion-video']&quot;).colorbox({iframe:true, innerWidth:630, innerHeight:380});" class="cboxElement" target="ion-video"></a>--->
					<!---   <a href="http://www.youtube.com/v/HyEJ_b-UTkw?fs=0&amp;hl=en_US&amp;rel=0&amp;hd=0;autoplay=1"  onclick="$(&quot;a[target='ion-video']&quot;).colorbox({iframe:true, innerWidth:630, innerHeight:380});" class="cboxElement" target="ion-video"></a>--->                
				</li>
				<li>
					<img src="images/EOS_Web_Banner_04.jpg" alt="EOS | HD Drive-Thru Headset System Features">
					<a href="/qsr/link/HME_EOS-HD_Brochure_Web.pdf" target="_blank"></a>
					<!---<a href="/qsr/link/DurabilityHandout-WEB.pdf" target="_blank"></a>--->
				</li>
				<!---<cfinclude template="/cfincludes/banner-announcement.cfm">--->
			</ul>
		</div>
	</div>
</div>
<!-- end rotator -->
<div id="content">
	<div id="content1">
		<div id="sidebar">
			<div class="phone">Call 858.535.6000</div>
			<!---2012/10/15 ATN: Added LiveChat Template--->
			<cfinclude template="cfincludes/livechat.cfm">
				<div id="accordion">
					<ul class="acc" id="acc">
						<li id="noback acc">
							<h3>Learn More About EOS HD</h3>
							<div class="acc-section">
							<div class="acc-content">
							<ul class="linklist">
								<li><a href="/eossounddemo_nonav/" class="cboxElement" target="example61">EOS | HD Sound Demonstration</a></li>
								<li><a href="https://www.youtube.com/embed/OB1lilggbUg?rel=0&amp;autoplay=1" class="cboxElement prod-video" target="example61">EOS | HD Product Video</a></li>
								<li><a href="/qsr/link/HME_EOS-HD_Brochure_Web.pdf" target="_blank">EOS | HD Brochure</a></li>
                                <li><a href="/qsr/link/EOS-HD_Brochure_FR-CA_WEB.pdf" target="_blank">EOS | HD Brochure (French)</a></li>
                                <li><a href="/qsr/link/EOS-HD_Brochure_Spanish.pdf" target="_blank">EOS | HD Brochure (Spanish)</a></li>
								<li><a href="/qsr/link/EOS_Durability_web.pdf" target="_blank">EOS | HD Durability Brochure</a></li>
								<li><a href="/qsr/link/HSC_EOSHD_Infographic_V7.pdf" target="_blank">Drive-Thru Headset Infographic</a></li>
                                <li><a href="/qsr/link/CIB-Datasheet-EOS-HD-ION-IQ.pdf" target="_blank">CIB Datasheet</a></li>

								<li><a href="https://www.youtube.com/embed/YHPxI1IaCbw?rel=0&amp;autoplay=1" id="vid2" class="cboxElement" style="display:none;" target="example61">EOS | HD Product Video</a></li>


							</ul>
							</div>
							</div>
						</li>
						<li id="acc">
							<h3>Request Information</h3>
							<div class="acc-section">
							<div class="acc-content">
							<cfinclude template="cfincludes/getinfo-form.cfm">
							</div><!--end of acc-content-->
							</div><!--end of acc-section-->
						</li>
						<li id="acc">
							<h3>Receive Special Offers and Promotions</h3>
							<div class="acc-section">
							<div class="acc-content">
							<cfinclude template="cfincludes/newsletter-form.cfm" >
							</div><!--end of acc-content-->
							</div><!--end of acc-section-->
						</li>
					</ul>
				</div><!-- end of noback acc-->
			<br />
			<!---<blockquote>
				<p>With ION IQ, I&#39;ve noticed a dramatic improvement in 
				communication between <br>
				our customers and employees.
				But what&#39;s really exciting is how much our speed and order accuracy has improved.<br>
				It&#39;s a win-win.</p>
			</blockquote>
			<h6><b>SCOTT JOHNSON</b><br>Major Quick Service<br> Restaurant Owner</h6>--->
		</div>
		<div id="bottom-hide"></div>
	</div>
	<div class="col7">
		<div class="eos_demo">
			<a href="/eossounddemo_nonav/" class="cboxElement" target="example61"><img src="images/EOS_Demo.jpg" alt="EOS Sound Demo" style="margin-bottom:10px;" /></a>
			<!---<a href="/eossounddemo_nonav/" onclick="$(&quot;a[target='eos-video']&quot;).colorbox({iframe:true, innerWidth:900, innerHeight:695});" class="cboxElement" target="eos-video"><img src="images/EOS_Demo.jpg" alt="EOS Sound Demo" style="margin-bottom:10px;" /></a>--->
			<p class="eos_demo_label"><span id="eos_label">HEAR WHY HD IS BETTER</span><br />Sound quality so good you have to hear it to believe it!</p>
		</div>
		<h2 style="margin-top:8px;">EOS | HD<sup>&reg;</sup> DRIVE-THRU HEADSET SYSTEM: THE NEW STANDARD IN DRIVE-THRU SOUND QUALITY</h2>
		<p class="eos_para">HME revolutionizes sound quality in the drive-thru with the introduction of EOS | HD &ndash; the first drive-thru headset system with HD Audio. Combining advanced wideband technology with multiple noise reduction features, EOS | HD delivers the clearest, most intelligible sound ever heard in a drive-thru headset. Designed to create more understandable drive-thru conversations to improve speed of service and order accuracy, EOS | HD sets a new standard for rive-thru sound clarity.</p>
		<ul class="eos_list">
			<li><strong>Less Inbound Noise:</strong> EOS | HD's innovative noise reduction technology dramatically reduces external noise from the drive-thru, making it easier for crewmembers to hear customer orders.</li>
			<li><strong>Less Outbound Noise:</strong> EOS | HD significantly reduces noise from inside the kitchen, providing clearer communication to customers for faster drive-thru service and an enhanced customer experience.</li>
			<li><strong>Clearer, More Intelligible Sound:</strong> By expanding the audio range to capture high and low voice frequencies that regular digital headsets can&rsquo;t, EOS | HD&rsquo;s wideband technology increases sound intelligibility in the drive-thru by a full 20%. Clearer communications reduces order mistakes while creating happier drive-thru customers.</li>
		</ul>
        <div class="eos_demo">
            <img src="images/EOS_Headset_Product_Shot_01.jpg" alt="EOS drive-thru headset" width="250" class="floatright margin-lt" style="display:block;margin-right:0px;margin-bottom:15px;"/>
            <p style="display: block; clear: both;" class="eos_demo_label"><span id="eos_label"></span>EOS | HD&mdash;The only headset with high-definition audio</p>
		</div>
        
		<!---<ul style="float:left;width:290px;margin-bottom:15px"></ul>--->
		<div class="linebreak"></div> 
		<h3 class="head3">TWICE THE AUDIO BANDWIDTH OF STANDARD HEADSETS!</h3>
		<img src="images/EOS_Wideband_Graphic.jpg" alt="EOS Wideband Graphic"/>
		
		<table>
			<tr>
				<td style="padding-right:5px;">1.</td>
				<td style="width:285px;padding-right:5px;padding-bottom:10px;"><strong>2X The Audio Bandwidth</strong> &#8211; EOS | HD delivers unprecedented drive-thru sound clarity by capturing the sound you need to hear and eliminating the sound you don&rsquo;t. Reducing unwanted noise allows crewmembers to hear more of the conversation so they can get the order right the first time.</td>
				<td style="padding-right:5px;">2.</td>
				<td><strong>Improve Intelligibility and Profits</strong> &#8211; EOS | HD makes drive-thru orders 20% easier to understand for faster speed of service, fewer order mistakes, and enhanced profitability.</td>
			</tr>
			<tr>
				<td colspan="2"><img src="images/Noise_Reduction_Icons_01.jpg" width="250" style="margin-bottom: 10px;margin-left: 15px;margin-top: 20px;"></td>
				<td colspan="2"><img src="images/Noise_Reduction_Icons_02.jpg" width="250" style="margin-bottom: 10px;margin-left: 15px;margin-top: 20px;"></td>
			</tr>
			<tr>
				<td style="padding-right:5px;">3.</td>
				<td><strong>Lower Inbound Noise by 44%</strong> &#8211; Innovative noise reduction technology removes most kitchen noises, enabling customers to hear and understand crewmembers with remarkable clarity. </td>
				<td style="padding-right:5px;">4.</td>
				<td><strong>Lower Outbound Noise By 88%</strong> &#8211; Innovative noise reduction technology removes most kitchen noises, enabling customers to hear and understand crewmembers with remarkable clarity.</td>
			</tr>
		</table>
		<div style="margin-top:-4px;margin-bottom:6px;">
		<blockquote>
			<p>Using this headset, it's like the customer is standing right next to me! HD Audio makes a huge difference in the clarity of the orders.</p>
		</blockquote>
		<h6><b>JOE MORENO</b><br>Major Quick Service <br>Restaurant Owner</h6>
		</div>
		
		<!---<div class="bmh">
			<div>
				<div>1.</div>
				<div class="bmh_cell"><strong>2X The Audio Bandwidth</strong> &#8211; The greatest advancement in sound since digital drive-thru headsets, the expanded audio range of EOS | HD means you hear more of the conversation so you can get the order right the first time.</div>
			</div>
			<div id="bmh2">
				<div>2.</div>
				<div class="bmh_cell"><strong>20% Increase in Intelligibility</strong> &#8211; With wideband technology and advanced noise reduction, EOS | HD makes drive-thru orders 20% easier to understand. Understanding the customer better means faster speed of service and profitability for your drive-thru. </div>
			</div>
			<div>
				<div class="fnone"><img src="images/Noise_Reduction_Icons_01.jpg" width="250" style="margin-bottom: 10px;margin-left: 15px;margin-top: 20px;"></div>
				<div>3.</div>
				<div class="bmh_cell"><strong>44% Improvement in Inbound Noise Reduction</strong> &#8211; Removes engine and traffic sounds from the drive-thru to reduce employee fatigue. </div>
				<blockquote>
					<p>Using this headset, it's like the customer is standing right next to me! HD Audio makes a huge difference in the clarity of the orders.</p>
				</blockquote>
				<h6><b>JOE MORENO</b><br>Major Quick Service <br>Restaurant Owner</h6>
			</div>
			<div class="fnone"><img src="images/Noise_Reduction_Icons_02.jpg" width="250" style="margin-bottom: 10px;margin-left: 15px;margin-top: 20px;"></div>
			<div>
				<div>4.</div>
				<div class="bmh_cell"><strong>88% Improvement in Outbound Noise Reduction</strong> &#8211; EOS | HD's innovative outbound noise reduction removes kitchen noise, like blenders, so that customers can hear crew members better. Orders are more accurate and customers leave happier than ever before. </div>
			</div>
		</div>--->
		
		
		<br />
		<div class="linebreak"></div>
		<img src="images/EOS_Headset_Application_01.jpg" alt="EOS headset comfort" width="250" border="0" class="floatright margin-bt" style="margin-bottom: 40px;margin-left:40px;"/>
		<h3 class="head3" style="margin-top:-20px;"></br>COMFORTABLE. DURABLE. EASY TO WEAR.</h3>
		<ul>
			<li class="no-arrow">EOS | HD&rsquo;s lightweight, balanced design provides lasting comfort for employees &ndash; even on long, all-day shifts. The superior design and construction deliver reliable performance day in and day out while reducing or eliminating many common headset repair problems.</li>
		</ul>
		<div class="linebreak"></div>
	</div>
	<div class="col7">
		<span class="head3">PROGRAMMABLE MESSAGE CENTER</span>
		<p>
		<ul>
			<li class="no-arrow">The EOS | HD Message Center improves drive-thru efficiency and customer service with easy-to-program, customizable communications.</li>
		</ul>
		</p>
		<p>
			<ol class="b">
				<li><img src="images/ION-IQ-drive-thru-communication-greetings.jpg" alt="ION IQ drive-thru communication greetings" width="150" height="100" class="floatleft margin-rt margin-bt"><strong>1. Customer Greetings</strong> &#8211; Record up to 12 different customer greetings to ensure a timely welcome at the order point. Customize messages for specific dayparts, specials, or promotions to help crewmembers upsell and increase average check totals.</li>
				<li><img src="images/ION-IQ-drive-thru-communication-reminders.jpg" alt="ION IQ drive-thru communication reminders" width="150" height="100" class="floatleft margin-rt margin-bt"><strong>2. Employee Reminders</strong> &#8211; Automatically remind crewmembers of important tasks such as cleaning bathrooms, restocking supplies, and washing hands. Instantly send messages to multiple headsets using pre-recorded or customized reminders.</li>
				<li><img src="images/ION-IQ-drive-thru-communication-alerts.jpg" alt="ION IQ drive-thru communication alerts" width="150" height="100" class="floatleft clear margin-rt margin-bt"><strong>3. Food Safety Alerts</strong> &#8211; Keep your food and your customers safe by automatically notifying employees of food safety or security issues, such as "back door open." Alerts occur only when the system detects that certain event thresholds are met.</li>
				<div class="move-up margin-bt">
					<blockquote style="margin-top: 70px;">
						<p>The EOS | HD message center gives me the flexibility I’ve been waiting for to manage my restaurant. I can sequence greetings and promote the specials I want, whenever I want.</p>
					</blockquote>
					<h6 style="margin-bottom:23px;"><b>KAHN PHOMASY</b><br>Major Quick Service <br />Restaurant Owner</h6>
				</div>
			</ol>
		</p>
		<div class="linebreak"></div>
		<h2>MANAGE YOUR DRIVE THRU FROM AFAR</h2>
		<img  src="images/ION-IQ-drive-thru-network-capability.jpg" alt="ION IQ drive-thru network capability" width="268" height="187" style="clear:both;"/>
		<img src="images/EOS-HD-drive-thru-remote-access.jpg" alt="EOS HD drive-thru remote access" width="268" class="floatright"/>
		<p class="caption floatright clear" >Manage your drive-thru operation from the office or remotely with EOS | HD.</p>
		<p>
			<ol class="a" style="width:270px;margin-bottom:20px;">
				<li>Remotely connect to your EOS | HD system using your PC, tablet or smartphone.</li>
				<li>Easily troubleshoot problems or update settings at home or on the road.</li>
				<li>Program new greetings, reminders and alerts from any location.</li>
			</ol>
		</p>
		<div class="linebreak"></div>
		<h3 class="head3"></br>SAVE TIME, MONEY WITH OPTIONAL ACCESSORIES</h3>
		<ol>
			<li style="background:none"><img src="images/thm_TI6000_Telephone_Interface.jpg" alt="ION IQ Telephone Interface" style="clear:both;float: left;margin: 0 20px 20px;" border="0" /><Strong>1. Telephone Interface</Strong> &mdash; Lets you easily conduct business over the phone while attending to other restaurant business.</li><br clear="all" />
			<li style="background:none"><img src="images/thm_ION_IQ_Temp_Sensor.jpg" alt="ION IQ Temperature Sensor" style="clear:both;float: left;margin: 0 20px 20px;"  border="0" /><Strong>2. <a href="../link/ION_IQ_Temp_Sensor_Data_Sheet_web.pdf" target="_blank">Temperature Sensor</a></Strong> &mdash; Helps prevent food spoilage by sending instant alerts when refrigerator or freezer temperatures exceed pre-set limits.</li>
		</ol>
		<div class="linebreak"></div>
		<img src="images/EOS_BaseStation_Controls.jpg" alt="ION IQ drive-thru base station display" width="268" height="197" border="0" class="floatright margin-lt" />
		<p class="caption clear floatright" >Streamlined page design lets you quickly access and edit messages, system preferences, and more.</p>
		<p><span class="head3 margin-bt">INTUITIVE, USER-FRIENDLY CONTROLS</span></p>
		<p>EOS | HD makes it easier than ever to manage drive-thru data and performance.		</p>
		<ul style="line-height:1.6">
<li>Large display screen reduces eye fatigue</li>
<li>Soft-touch digital controls for fast, efficient data management</li>
<li>Simple, on-screen instructions for navigating menus and settings</li>
<li>Multi-level password options ensure data security</li>
			</ul>
		</p>
		<div class="linebreak"></div>
		<img  src="images/EOS_System.jpg" alt="ION IQ System" width="230"  class="base_img1"/><br />
		<p><span class="head3 margin-bt" >SCALABLE DRIVE-THRU COMMUNICATIONS</span></p>
		<p>The revolutionary EOS | HD Headset System can easily adapt to any size store.</p>
<p>        
<ul style="margin-left:254px">
<li>One system can handle up to 15 AIO headsets</li>
<li>Use as few or as many headsets as you need</li>
<li>Ideal for single or multiple store operations</li>
</ul>        
</p>        
        
        <div class="linebreak lastbreak"></div>
        <img  src="images/HME-CIB-D-2.jpg" alt="ION IQ System" width="230" class="base_img2"/>
		<p><span class="head3 margin-bt" >CONVENIENT CLOUD MANAGEMENT</span></p>

   		<p>EOS | HD seamlessly integrates with HME&rsquo;s CLOUD Interface Bridge (CIB) for remote management of your entire drive-thru headset system. Save time and money by connecting directly to the cloud to:</p>
        <p>&nbsp;</p>
        <ul style="margin-left:254px">
        <li>Instantly access multiple store data</li>
        <li>Simultaneously update reminders, greetings and alerts in each store</li>
        <li>Upload system updates</li>
        <li>Remotely resolve technical problems</li>
        </ul>   <br>
<p>Take multiple store performance and profits to the next level with EOS | HD and the HME CLOUD<strong><sup>&reg;</sup></strong> Interface Bridge!</p>        
	</div>
</div>
<div id="bread-crumbs">
	<ul id="crumbs">
		<li><a href="/"><div id="minilogo"><img src="images/HME-logo-small.png" width="40" height="21" alt="HME logo small" border="0"/></div></a></li>
		<li><a href="/qsr/drive-thru-headset-systems/">Drive-Thru Headset and Timer Systems</a></li>
		<li><a href="#">EOS | HD Drive-Thru Headset</a></li>
	</ul>
</div>
<cfinclude template="cfincludes/sitemap.cfm">
<cfinclude template="cfincludes/footer.cfm">
</div>
</div>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript">
var parentAccordion=new TINY.accordion.slider("parentAccordion");
parentAccordion.init("acc","h3",1,0,"acc-selected");
</script>

<cfoutput>
<script type="text/javascript">
	$(document).ready(function(){	
		var ms_ie = false;
		var ua = window.navigator.userAgent;
		var old_ie = ua.indexOf('MSIE ');
		var new_ie = ua.indexOf('Trident/');
		
		if ((old_ie > -1) || (new_ie > -1)) {
			ms_ie = true;
		}							   
		//Examples of how to assign the ColorBox event to elements
		$("a[rel='example2']").colorbox({transition:"fade"});
		$(".example6").colorbox({iframe:true, innerWidth:641, innerHeight:380});
		$(".example7").colorbox({width:"80%", height:"80%", iframe:true});
		$(".ion-video").colorbox({iframe:true, innerWidth:630, innerHeight:380});
		$("a[target='ion-video']").colorbox({iframe:true, innerWidth:630, innerHeight:380});
		$(".example61").colorbox({iframe:true, innerWidth:650, innerHeight:340});
        $(".prod-video").colorbox({iframe:true, innerWidth:1280, innerHeight:680});

		if(ms_ie){
			$("a[target='example61']").colorbox({iframe:true, innerWidth:1315, innerHeight:760, scrolling:false});	
		} else {
			$("a[target='example61']").colorbox({iframe:true, innerWidth:1280, innerHeight:760, scrolling:false});	
		}


		<cfif '#URL.auto#' IS 'video'>
			setTimeout(function() {
			   $(".prod-video").trigger('click');
			},10);
		<cfelseif '#URL.auto#' IS 'video2'>
			setTimeout(function() {
			   $("##vid2").trigger('click');
			},10);				
		<cfelseif '#URL.auto#' IS 'demo'>
			setTimeout(function() {
			   $("a[target='example61']").trigger('click');
			},10);		
		</cfif>
		
	});
</script>


</cfoutput>


</body>
</html>