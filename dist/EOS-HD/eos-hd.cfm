


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
<title>EOS HD&reg; |Drive-Thru Headset Systems | HME</title>
<meta name="keywords" content="drive-thru headset,drive-thru headsets,drive-thru headset system,drive-thru headset systems,drive-thru communication,drive-thru communications"/>
<meta name="description" content="HD Audio transforms the drive-thru conversation with better sound clarity and less noise.">
<!-- Stylesheet for website -->
<style type="text/css" media="all">@import "css/master.css";</style>
<style type="text/css">
	sup {font-size: 65%;vertical-align: super;}	
</style>
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>

</head>
<body>
<div id="page-container">

<script type="text/javascript">
	$(document).ready(function() {
		$(".c-panel").css("width", ($(".c-panel").width() + 5));
	});
</script>

<div id="content">

	<div class="col7">
		<div class="eos_demo">
<!--- 			<a href="http://www.hme.com/eossounddemo_nonav/" class="cboxElement" target="example61"><img src="images/EOS_Demo.jpg" alt="EOS Sound Demo" style="margin-bottom:10px;" /></a>		 --->	
<a href="http://www.hme.com/eossounddemo_nonav/" target="_blank"><img src="images/EOS_Demo.jpg" alt="EOS Sound Demo" style="margin-bottom:10px;" /></a>
			
			<p class="eos_demo_label"><span id="eos_label">HEAR WHY HD IS BETTER</span><br />Sound quality so good you have to hear it to believe it!</p>
		</div>
		<h2 style="margin-top:8px;">EOS | HD<sup>&reg;</sup> DRIVE-THRU HEADSET SYSTEM: THE NEW STANDARD IN DRIVE-THRU SOUND QUALITY</h2>
		<p class="eos_para">HME revolutionizes sound quality in the drive-thru with the introduction of EOS | HD &ndash; the first drive-thru headset system with HD Audio. Combining advanced wideband technology with multiple noise reduction features, EOS | HD delivers the clearest, most intelligible sound ever heard in a drive-thru headset. Designed to create more understandable drive-thru conversations to improve speed of service and order accuracy, EOS | HD sets a new standard for drive-thru sound clarity.</p>
		<ul class="eos_list">
			<li><strong>Less Inbound Noise:</strong> EOS | HD's innovative noise reduction technology dramatically reduces external noise from the drive-thru, making it easier for crewmembers to hear customer orders.</li>
			<li><strong>Less Outbound Noise:</strong> EOS | HD significantly reduces noise from inside the kitchen, providing clearer communication to customers for faster drive-thru service and an enhanced customer experience.</li>
			<li><strong>Clearer, More Intelligible Sound:</strong> By expanding the audio range to capture high and low voice frequencies that regular digital headsets can&rsquo;t, EOS | HD&rsquo;s wideband technology increases sound intelligibility in the drive-thru by a full 20%. Clearer communications reduces order mistakes while creating happier drive-thru customers.</li>
		</ul>
        <div class="eos_demo">
            <img src="images/EOS_Headset_Product_Shot_01.jpg" alt="EOS drive-thru headset" width="250" class="floatright margin-lt" style="display:block;margin-right:0px;margin-bottom:15px;"/>
            <p style="display: block; clear: both;" class="eos_demo_label"><span id="eos_label"></span>EOS | HD&mdash;The only headset with high-definition audio</p>
		</div>
        
		
		<div class="linebreak"></div> 
		<h3 class="head3">TWICE THE AUDIO BANDWIDTH OF STANDARD HEADSETS!</h3>
		<img src="images/EOS_Wideband_Graphic.jpg" alt="EOS Wideband Graphic"/>
		
		<table>
			<tr>
				<td style="padding-right:5px;">1.</td>
				<td style="width:285px;padding-right:5px;padding-bottom:10px;"><strong>2X The Audio Bandwidth</strong> &#8211; EOS | HD delivers unprecedented drive-thru sound clarity by capturing the sound you need to hear and eliminating the sound you don&rsquo;t. Reducing unwanted noise allows crewmembers to hear more of the conversation so they can get the order right the first time.</td>
				<td style="padding-right:5px;">2.</td>
				<td><strong>Improve Intelligibility and Profits</strong> &#8211; EOS | HD makes drive-thru orders 20% easier to understand for faster speed of service, fewer order mistakes, and enhanced profitability.</td>
			</tr>
			<tr>
				<td colspan="2"><img src="images/Noise_Reduction_Icons_01.jpg" width="250" style="margin-bottom: 10px;margin-left: 15px;margin-top: 20px;"></td>
				<td colspan="2"><img src="images/Noise_Reduction_Icons_02.jpg" width="250" style="margin-bottom: 10px;margin-left: 15px;margin-top: 20px;"></td>
			</tr>
			<tr>
				<td style="padding-right:5px;">3.</td>
				<td><strong>Lower Inbound Noise by 44%</strong> &#8211; Innovative noise reduction technology removes most kitchen noises, enabling customers to hear and understand crewmembers with remarkable clarity. </td>
				<td style="padding-right:5px;">4.</td>
				<td><strong>Lower Outbound Noise By 88%</strong> &#8211; Innovative noise reduction technology removes most kitchen noises, enabling customers to hear and understand crewmembers with remarkable clarity.</td>
			</tr>
		</table>
		<div style="margin-top:-4px;margin-bottom:6px;">
		<blockquote style="margin-top:20px;margin-bottom:6px;"">
			<p>Using this headset, it's like the customer is standing right next to me! HD Audio makes a huge difference in the clarity of the orders.</p>
		</blockquote>
		<h6><b>JOE MORENO</b><br>Major Quick Service <br>Restaurant Owner</h6>
		</div>
		
		
		
		
		<br />
		<div class="linebreak"></div>
		<img src="images/EOS_Headset_Application_01.jpg" alt="EOS headset comfort" width="250" border="0" class="floatright margin-bt" style="margin-bottom: 40px;margin-left:40px;"/>
		<h3 class="head3" style="margin-top:-20px;"></br>COMFORTABLE. DURABLE. EASY TO WEAR.</h3>
		<ul>
			<li class="no-arrow">EOS | HD&rsquo;s lightweight, balanced design provides lasting comfort for employees &ndash; even on long, all-day shifts. The superior design and construction deliver reliable performance day in and day out while reducing or eliminating many common headset repair problems.</li>
		</ul>
		<div class="linebreak"></div>
	</div>
	<div class="col7">
		<span class="head3">PROGRAMMABLE MESSAGE CENTER</span>
		<p>
		<ul>
			<li class="no-arrow">The EOS | HD Message Center improves drive-thru efficiency and customer service with easy-to-program, customizable communications.</li>
		</ul>
		</p>
		<p>
			<ol class="b">
				<li><img src="images/ION-IQ-drive-thru-communication-greetings.jpg" alt="ION IQ drive-thru communication greetings" width="150" height="100" class="floatleft margin-rt margin-bt"><strong>1. Customer Greetings</strong> &#8211; Record up to 12 different customer greetings to ensure a timely welcome at the order point. Customize messages for specific dayparts, specials, or promotions to help crewmembers upsell and increase average check totals.</li>
				<li><img src="images/ION-IQ-drive-thru-communication-reminders.jpg" alt="ION IQ drive-thru communication reminders" width="150" height="100" class="floatleft margin-rt margin-bt"><strong>2. Employee Reminders</strong> &#8211; Automatically remind crewmembers of important tasks such as cleaning bathrooms, restocking supplies, and washing hands. Instantly send messages to multiple headsets using pre-recorded or customized reminders.</li>
				<li><img src="images/ION-IQ-drive-thru-communication-alerts.jpg" alt="ION IQ drive-thru communication alerts" width="150" height="100" class="floatleft clear margin-rt margin-bt"><strong>3. Food Safety Alerts</strong> &#8211; Keep your food and your customers safe by automatically notifying employees of food safety or security issues, such as "back door open." Alerts occur only when the system detects that certain event thresholds are met.</li>
				<div class="move-up margin-bt">
					<blockquote style="margin-top: 70px;">
						<p>The EOS | HD message center gives me the flexibility I’ve been waiting for to manage my restaurant. I can sequence greetings and promote the specials I want, whenever I want.</p>
					</blockquote>
					<h6 style="margin-bottom:23px;"><b>KAHN PHOMASY</b><br>Major Quick Service <br />Restaurant Owner</h6>
				</div>
			</ol>
		</p>
		<div class="linebreak"></div>
		<h2>MANAGE YOUR DRIVE THRU FROM AFAR</h2>
		<img src="images/ION-IQ-drive-thru-network-capability.jpg" alt="ION IQ drive-thru network capability" width="268" height="187" style="clear:both;"/>
		<img src="images/EOS-HD-drive-thru-remote-access.jpg" alt="EOS HD drive-thru remote access" width="268" class="floatright"/>
		<p class="caption floatright clear" >Manage your drive-thru operation from the office or remotely with EOS | HD.</p>
		<p>
			<ol class="a" style="width:270px;margin-bottom:20px;">
				<li>Remotely connect to your EOS | HD system using your PC, tablet or smartphone.</li>
				<li>Easily troubleshoot problems or update settings at home or on the road.</li>
				<li>Program new greetings, reminders and alerts from any location.</li>
			</ol>
		</p>
		<div class="linebreak"></div>
		<h3 class="head3"></br>SAVE TIME, MONEY WITH OPTIONAL ACCESSORIES</h3>
		<ol>
			<li style="background:none"><img src="images/thm_TI6000_Telephone_Interface.jpg" alt="ION IQ Telephone Interface" style="clear:both;float: left;margin: 0 20px 20px;" border="0" /><Strong>1. Telephone Interface</Strong> &mdash; Lets you easily conduct business over the phone while attending to other restaurant business.</li><br clear="all" />
			<li style="background:none"><img src="images/thm_ION_IQ_Temp_Sensor.jpg" alt="ION IQ Temperature Sensor" style="clear:both;float: left;margin: 0 20px 20px;"  border="0" /><Strong>2. Temperature Sensor</Strong> &mdash; Helps prevent food spoilage by sending instant alerts when refrigerator or freezer temperatures exceed pre-set limits.</li>
		</ol>
		<div class="linebreak"></div>
		<img src="images/EOS_BaseStation_Controls.jpg" alt="ION IQ drive-thru base station display" width="268" height="197" border="0" class="floatright margin-lt" />
		<p class="caption clear floatright" >Streamlined page design lets you quickly access and edit messages, system preferences, and more.</p>
		<p><span class="head3 margin-bt">INTUITIVE, USER-FRIENDLY CONTROLS</span></p>
		<p>EOS | HD makes it easier than ever to manage drive-thru data and performance.		</p>
		<ul style="line-height:1.6">
<li>Large display screen reduces eye fatigue</li>
<li>Soft-touch digital controls for fast, efficient data management</li>
<li>Simple, on-screen instructions for navigating menus and settings</li>
<li>Multi-level password options ensure data security</li>
			</ul>
		</p>
		<div class="linebreak"></div>
		<img  src="images/EOS_System.jpg" alt="ION IQ System" width="230"  class="base_img1"/><br />
		<p><span class="head3 margin-bt" >SCALABLE DRIVE-THRU COMMUNICATIONS</span></p>
		<p>The revolutionary EOS | HD Headset System can easily adapt to any size store.</p>
<p>        
<ul style="margin-left:254px">
<li>One system can handle up to 15 AIO headsets</li>
<li>Use as few or as many headsets as you need</li>
<li>Ideal for single or multiple store operations</li>
</ul>        
</p>        
        
        <div class="linebreak lastbreak"></div>
        <img  src="images/HME-CIB-D-2.jpg" alt="ION IQ System" width="230" class="base_img2"/>
		<p><span class="head3 margin-bt" >CONVENIENT CLOUD MANAGEMENT</span></p>

   		<p>EOS | HD seamlessly integrates with HME&rsquo;s CLOUD Interface Bridge (CIB) for remote management of your entire drive-thru headset system. Save time and money by connecting directly to the cloud to:</p>
        <p>&nbsp;</p>
        <ul style="margin-left:254px">
        <li>Instantly access multiple store data</li>
        <li>Simultaneously update reminders, greetings and alerts in each store</li>
        <li>Upload system updates</li>
        <li>Remotely resolve technical problems</li>
        </ul>   <br>
<p>Take multiple store performance and profits to the next level with EOS | HD and the HME CLOUD<strong><sup>&reg;</sup></strong> Interface Bridge!</p>        
	</div>
</div>


  

</div>
</div>
<!--- <script type="text/javascript" src="js/script.js"></script> --->

<script type="text/javascript" src="js/jquery.colorbox.js"></script>
<script type="text/javascript" src="js/master.js"></script>

<script type="text/javascript">
	$(document).ready(function(){	
		var ms_ie = false;
		var ua = window.navigator.userAgent;
		var old_ie = ua.indexOf('MSIE ');
		var new_ie = ua.indexOf('Trident/');
		
		if ((old_ie > -1) || (new_ie > -1)) {
			ms_ie = true;
		}							   
		//Examples of how to assign the ColorBox event to elements
		$("a[rel='example2']").colorbox({transition:"fade"});
		$(".example6").colorbox({iframe:true, innerWidth:641, innerHeight:380});
		$(".example7").colorbox({width:"80%", height:"80%", iframe:true});
		$(".ion-video").colorbox({iframe:true, innerWidth:630, innerHeight:380});
		$("a[target='ion-video']").colorbox({iframe:true, innerWidth:630, innerHeight:380});
		$(".example61").colorbox({iframe:true, innerWidth:650, innerHeight:340});
        $(".prod-video").colorbox({iframe:true, innerWidth:1280, innerHeight:680});

		if(ms_ie){
			$("a[target='example61']").colorbox({iframe:true, innerWidth:1315, innerHeight:760, scrolling:false});	
		} else {
			$("a[target='example61']").colorbox({iframe:true, innerWidth:1280, innerHeight:760, scrolling:false});	
		}

	});
</script>





</body>
</html>