<cfoutput>
<div class="info">
	<h1 class="pagetitle">CURRENT CUSTOMER<br>DRIVE-THRU REPAIR SPECIALS</h1>
	<div class="single clear">
		<div class="plug">
			<div class="right_textplug">
				<h2><a href="https://#APPLICATION.rootURL#/files/protection-plan-flyer.pdf" target="_blank">First Month Free on a CE Drive-Thru Protection Plan</a></h2>
				<p>Stop worrying about your next potential repair and get peace of mind with a CE Drive-Thru Protection Plan. Protection Plans offer unlimited repairs at one low monthly payment, and if you sign up now, your first month is on us! Protection Plans cover all models and brands. Call 877-731-0334 for details.</p>
			</div>
			<div class="right_imgplug">
				<img src="/images/pages/ceprotection.jpg" />
			</div>
			<div class="plug_border">
				<img src="/images/pages/Special_GreyFadedRule.jpg" />
			</div>
		</div>
		<div class="plug">
			<div class="right_textplug">
				<h2><a href="https://#APPLICATION.rootURL#/bonusbucks">CE Drive-Thru Repairs Bonus Bucks</a></h2>
				<p>We want to reward you for your commitment to us. For every $100 you spend on a repair with CE, we'll give you $5 in Bonus Bucks to use on future repairs. It's just like cash, only better! Use them on your next repair or save them up for a larger discount later.</p>
			</div>
			<div class="right_imgplug">
				<img src="/images/pages/Special_BonusBucks.jpg" />
			</div>
			<div class="plug_border">
				<img src="/images/pages/Special_GreyFadedRule.jpg" />
			</div>
		</div>
		<!--- 20140514 CG: Removed referal program section per Ticket#51757  --->
		<!---<div class="plug">
			<div class="right_textplug">
				<h2><a href="https://#APPLICATION.rootURL#/referralprogram">CE Drive-Thru Repairs Referral Program</a></h2>
				<p>Know a store that could use a good repair company? For every new customer you refer to CE, we'll give you a $25 Amazon.com gift card. Plus, the person you refer will get to take 50% off a drive-thru headset repair or drive-thru belt-pac repair.</p>
			</div>
			<div class="right_imgplug">
				<img src="/images/pages/Special_ReferralProgram.jpg" />
			</div>
			<div class="plug_border">
				<img src="/images/pages/Special_GreyFadedRule.jpg" />
			</div>
		</div>--->
		<div class="plug">
			<div class="right_textplug">
				<h2><a href="https://#APPLICATION.rootURL#/multistorediscount">CE Drive-Thru Repairs Multi-Store Discount</a></h2>
				<p>Have multiple stores? Save more with the multi-store discount program! Save up to 25% off select products and services. Just give us a call at 877-731-0334 for more details.</p>
			</div>
			<div class="right_imgplug">
				<img src="/images/pages/Special_MultiStore.jpg" />
			</div>
			<div class="plug_border">
				<img src="/images/pages/Special_GreyFadedRule.jpg" />
			</div>
		</div>
		<!---20140114 CG: Removed Headset special per Ticket#48303 --->
		<!---<div class="plug">
			<div class="right_textplug">
				<h2><a href="https://#APPLICATION.rootURL#/freeheadset">Free Drive-Thru Headset Special</a></h2>
				<p>Time to buy new headsets? Purchase 2 CE branded headsets for any HME, Panasonic, or 3M system and get a 3<sup>rd</sup> headset free. Offer valid on CE branded headsets only. Free headset must be of equal or lesser value.</p>
			</div>
			<div class="right_imgplug">
				<img src="/images/pages/Special_FreeHeadset.jpg" />
			</div>
			<div class="plug_border">
				<img src="/images/pages/Special_GreyFadedRule.jpg" />
			</div>
		</div>--->
		<div class="plug">
			<div class="right_textplug">
				<h2><a href="https://#APPLICATION.rootURL#/freebattery">Free Drive-Thru Battery Special</a></h2>
				<p>Need batteries? Get a free battery from CE! We know that batteries are a necessity for your drive-thru, and it's our job to keep you stocked. Purchase 3 CE branded batteries for any HME, Panasonic, or 3M system and get your 4<sup>th</sup> battery free. Offer valid on CE branded batteries only. Free battery must be of equal or lesser value.</p>
			</div>
			<div class="right_imgplug">
				<img src="/images/pages/Special_FreeBattery.jpg" />
			</div>
			<div class="plug_border">
				<img src="/images/pages/Special_GreyFadedRule.jpg" />
			</div>
		</div>
	</div>
</div>
</cfoutput>