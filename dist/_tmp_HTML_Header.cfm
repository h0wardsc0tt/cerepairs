<cfsilent>
<cfparam name="GreetUser" default="">
<cfparam name="Page_Title" default="Drive-Thru Repair, Drive-Thru Headset Repair, Drive-Thru Repairs | CE">
<cfparam name="Page_MetaTitle" default="Drive-Thru Repair, Drive-Thru Headset Repair, Drive-Thru Repairs | CE">
<cfparam name="Page_MetaKeywords" default="HME, Panasonic, Drive-thru, communications, electronics">
<cfparam name="Page_MetaDescription" default="The leader in drive-thru repair for over 40 years. Trained technicians specialize in drive-thru headset repair and timer repair for HME and Panasonic.">

<cfif StructKeyExists(SESSION, "Session_UID") AND StructKeyExists(COOKIE, "User_Cookie")>
	<cfquery name="qry_getUser" datasource="#APPLICATION.Datasource#" maxrows="1">
		SELECT sess.*
		FROM 
			dtbl_User_Session sess 
		WHERE sess.User_Session_UID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#SESSION.Session_UID#">
		AND sess.User_Cookie = <cfqueryparam cfsqltype="cf_sql_varchar" value="#COOKIE.User_Cookie#">
	</cfquery>
	
	<cfif qry_getUser.RecordCount NEQ 0>
		<cfscript>
			QUERY = StructNew();
			QUERY.QueryName = "qry_getCart";
			QUERY.Datasource = APPLICATION.Datasource;
			QUERY.Cart_Session_UID = SESSION.Session_UID;
			QUERY.Cart_User_Cookie = qry_getUser.User_Cookie;
			QUERY.Cart_IsCompleted = 0;
			QUERY.OrderBy = "cart.Cart_DTS";
		</cfscript>
		<cfinclude template="/_qry_select_cart_product.cfm">
	</cfif>
</cfif>
</cfsilent>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><cfoutput>
<title>#Page_MetaTitle#</title>
<meta http-equiv="X-UA-Compatible" content="IE=9" />
<meta name="keywords" content="#Page_MetaKeywords#" />
<meta name="description" content="#Page_MetaDescription#" /></cfoutput>
<meta name="google-site-verification" content="qNustrU4FhOPARRyL8qWpr9qX61aX0PHpUA43vkvagM" />
<link rel="shortcut icon" href="./favicon.ico" type="image/x-icon" />
<!--<link rel="stylesheet" type="text/css" href="/css/ui-custom/jquery-ui-1.8.22.custom.css" />-->
<link rel="stylesheet" type="text/css" href="/css/ui-custom/jquery-ui.css" />
<script type="text/javascript" src="/js/jquery-1.8.3.min.js"></script>
<!--<script src="/js/jquery-ui-1.8.21.custom.min.js" language="JavaScript" type="text/javascript"></script>-->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script type="text/javascript" src="/js/jquery.lightbox-0.5.min.js"></script>
<script type="text/javascript" src="/js/mod/jquery.colorbox.js"></script>
<script type="text/javascript" src="/js/mod/master.js"></script>
<link rel="stylesheet" type="text/css" href="/css/style.cer.css" />
<link rel="stylesheet" type="text/css" href="/css/style.ddm.css" />
<link rel="stylesheet" type="text/css" href="/css/style.box.css" media="screen" />
<script type="text/javascript">
	$(function() {
		$('a.lightbox').lightBox();
	});
	$(document).ready(function(){
		$(".cboxElement").colorbox({innerWidth:"510px", innerHeight:"530px", iframe:true, open:false});
	});
	$(document).ready(function() {
		if(navigator.platform == "iPad") {
			$(".drop1").each(function() {
				var onClick;
				var firstClick = function() {
					onClick = secondClick;
					$(".ddm2").hide();
					return false;
				};
				var secondClick = function() {
					$(".ddm2").show();
					onClick = firstClick;
					return true;
				};
				onClick = firstClick;
				$(this).click(function() {
					return onClick();
				});
			});
		}
	});
</script>
<script type="text/javascript" src="/js/jquery.wt-rotator.js"></script>
<script type="text/javascript">
	$(document).ready(	
		function() {
			$("#Page").wtRotator({
				delay:3500,
				width: 800,
				height: 376,
				cpanel_align: "BR",
				tooltip_type: "none",
				transition:"diag.fade"
			});
		}
	);
</script> 
<!--- <script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-38043531-1']);
	_gaq.push(['_setDomainName', '.cerepairs.com']);
	_gaq.push(['_trackPageview']);
	
	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
</script> --->

<!---20140731 CG: Replace Google Analytics with new one per Ticket#53842 --->
<script>
	(function(i,s,o,g,r,a,m){
		i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)
		},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	ga('create', 'UA-20995670-1', 'auto');
	ga('send', 'pageview');
</script>

<style>
	#livechat-compact-container {
		display:none;	
	}
</style>

<noscript><meta http-equiv="refresh" content="0;url=./?pg=NoAuth"></noscript>

<script>
(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"4030928"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");
</script>
<noscript><img src="//bat.bing.com/action/0?ti=4030928&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>

</head>

<body>
<div id="Page">
	<div id="Header" class="clear">
		<div class="leftLogo">
			<a href="./"><img src="/images/celogo.png" /></a>
		</div>
		<div class="rightLogo">
			<h4>Call Us: 877-731-0334</h4>
			<div class="account clear">
				<ul>
					<!---<li><a href="./?pg=Account">Account</a></li>
					<li><a href="./?pg=Wishlist">Wishlist</a><span id="wishqty"></span></li>--->
					<li>
					<form name="form1" method="post" action="./?pg=Search" />
						<div class="searchFrame">
						<input name="criteria" type="text" class="searchBox" id="criteria" size="8" value="Search" onFocus="this.value=''" />
						<input type="image" src="/images/search.png" class="searchBox_submit" value="" />
						</div>
					</form>
					</li>
					<li class="div_pipe"> | </li>
					<li class="cart">
						<a href="./?pg=Cart">Cart</a><span id="cartcount"><cfif ((IsDefined("qry_getCart") AND IsQuery(qry_getCart)) AND qry_getCart.RecordCount NEQ 0)><cfoutput> (<strong>#qry_getCart.RecordCount#</strong>)</cfoutput></cfif></span>
						<div class="close_cart">
							<a href="javascript:void(0);" onclick="return closeAdd();" role="button"><span>CLOSE</span></a>
						</div>
						<div class="show_cart"></div>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<cfinclude template="./_tmp_HTML_Navigation.cfm">