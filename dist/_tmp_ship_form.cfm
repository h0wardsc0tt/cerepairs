<cfsilent>
<cfsavecontent variable="JS_Shipping">
<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"4030928"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script><noscript><img src="//bat.bing.com/action/0?ti=4030928&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>
<script type="text/javascript">
<!--
var preloadFlag = false;
function preloadImages() {
	if (document.images) {

		preloadFlag = true;
	}
}
// -->
</script>
<script type="text/javascript" language="javascript">
<!--
	var eastObj;
	var westObj;
	var shipForm;
	var elSel;
	var eastStateArr = new Array("AK","AL","AR","AZ","CA","CO","CT","DC","DE","FL","GA","HI","IL","IN","IA","ID","KS","KY","LA","MA","ME","MD","MI","MN","MS","MO","NE","NH","NJ","NM","NV","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY");

	function initThisPage(){
		eastObj = document.getElementById('eastCoast');
		westObj = document.getElementById('westCoast');
		shipForm = document.getElementById('shippingForm');
		elSel = document.getElementById('state');
		
	}
	function showRollover(param){
		hideRollover();
		if(param == "east"){
			eastObj.style.display = "block";	
		} else {
			westObj.style.display = "block";
		}
		
	}
	function hideRollover(){
		westObj.style.display = "none";	
		eastObj.style.display = "none";	
	}
	function showForm(param){
		hideRollover();
		elSel.options.length = 0;
		shipForm.style.display = "block";
		if(param == "east"){
			document.shipForm.centerSelect.value="east";
		} else {
			document.shipForm.centerSelect.value="west";
		}
		
		for(i = 0; i < eastStateArr.length; i++ ){
			elSel.options[i] = new Option(eastStateArr[i],eastStateArr[i],false,false); // standards compliant; doesn't work in IE
		}			
	}
	function hideForm(){
		shipForm.style.display = "none";
	}

	function validateEntries(){
		var errors = "";
		var numericPat = /^[0-9]{1,}(\.[0-9]{1,3})?$/i;
		var zipPat = /^[0-9]{5}(-[0-9]{4})?$/i;

		if(document.shipForm.firstName.value == "") errors += "Enter your first name\n";
		if(document.shipForm.lastName.value == "") errors += "Enter your last name\n";
		if(document.shipForm.company.value == "") errors += "Enter your retuarant/Store name\n";
		if(document.shipForm.phone.value == "") errors += "Enter your phone\n";
		if(document.shipForm.address.value == "") errors += "Enter your address\n";
		if(document.shipForm.city.value == "") errors += "Enter your city\n";
		if(document.shipForm.state.value == "") errors += "Select your state\n";
		if(!document.shipForm.zip.value.match(zipPat)) errors += "Enter a valid zip code\n";
		if(!document.shipForm.length.value.match(numericPat)) errors += "Package length must be numeric\n";
		if(!document.shipForm.width.value.match(numericPat)) errors += "Package width must be numeric\n";
		if(!document.shipForm.height.value.match(numericPat)) errors += "Package height must be numeric\n";
		if(!document.shipForm.weight.value.match(numericPat)) errors += "Package weight must be numeric\n";
		if(document.shipForm.centerSelect.value == "") errors += "Region not set. Press cancel and try again\n";
		if(errors!=""){
			alert(errors);
		} else {
			var labelURL = "/ars.cfm?region="+document.shipForm.centerSelect.value;
			labelURL += "&name="+document.shipForm.firstName.value+" "+document.shipForm.lastName.value;
			labelURL += "&company="+document.shipForm.company.value;
			labelURL += "&address="+document.shipForm.address.value;
			labelURL += "&city="+document.shipForm.city.value;
			labelURL += "&state="+document.shipForm.state.value;
			labelURL += "&zip="+document.shipForm.zip.value;
			labelURL += "&length="+document.shipForm.length.value;
			labelURL += "&width="+document.shipForm.width.value;
			labelURL += "&height="+document.shipForm.height.value;
			labelURL += "&weight="+document.shipForm.weight.value;
			labelURL += "&phone="+document.shipForm.phone.value;
			labelURL += "&fax="+document.shipForm.fax.value;

			
			window.open(labelURL);
			shipForm.style.display = "none";
		
		}
	
	}
	$(document).ready(function() {
		$('#t_form').submit(function(){
			var foundError = 0;
		
			$('input[type=checkbox]').each(function () {
				var itemToCheck = $(this).attr('rel').split('::');
				if(itemToCheck[0] && this.checked){
					
					if(itemToCheck[2]){						
						if($('input[name='+itemToCheck[2]+']').val() < 1){
							var extraText = '';
							if($('input[name='+itemToCheck[0]+']').val() == ''){
								extraText = "Enter text for " + itemToCheck[1];
							}
							alert("Enter an amount for " + itemToCheck[3] + ". " + extraText);
							foundError = 1;
							return false;
						}
						
					} else {
						if($('input[name='+itemToCheck[0]+']').val() < 1){
						
							alert("Enter an amount for " + itemToCheck[1]);
							foundError = 1;
							return false;
						}
					}
					
				}
			});
			
			if(foundError){
				return false;
			} else {
				return true;
			}
			
			
		//console.log (sList);
			
		});
	
	});		
-->
</script>
<script type="text/javascript">
	$(function(){
		//initially hide the textbox
		$("#Store_Name_other").hide();
		$('#Store_Name').change(function() {
		  if($(this).find('option:selected').val() == "Other"){
			$("#Store_Name_other").show();
		  }else{
			$("#Store_Name_other").hide();
		  }
		});
		$("#Store_Name_other").keyup(function(ev){
			var othersOption = $('#Store_Name').find('option:selected');
			if(othersOption.val() == "Other")
			{
				ev.preventDefault();
				//change the selected drop down text
				$(othersOption).html($("#Store_Name_other").val()); 
			} 
		});
		$('#t_form').submit(function() {
			var othersOption = $('#Store_Name').find('option:selected');
			if(othersOption.val() == "Other")
			{
				// replace select value with text field value
				othersOption.val($("#Store_Name_other").val());
			}
		});
	});
</script>
</cfsavecontent>
<cfhtmlhead text="#JS_Shipping#">

<cfif StructKeyExists(URL, "pdt") AND Len(URL.pdt) EQ 32>
    <cfquery name="qry_getProductName" datasource="#APPLICATION.Datasource#" maxrows="1">
        SELECT Product_Short_Description
        FROM tbl_Products
        WHERE Product_UID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#URL.pdt#">
    </cfquery>
</cfif>

<cfscript>
	qry_getChains = QueryNew("Chain,FieldValue", "VarChar,VarChar");
	newRow = QueryAddRow(qry_getChains, 19);
	temp = QuerySetCell(qry_getChains, "Chain", "Arby's", 1);
	temp = QuerySetCell(qry_getChains, "FieldValue", "Arby's", 1);
	
	temp = QuerySetCell(qry_getChains, "Chain", "Burger King", 2);
	temp = QuerySetCell(qry_getChains, "FieldValue", "Burger King", 2);
	
	temp = QuerySetCell(qry_getChains, "Chain", "Carl's Jr.", 3);
	temp = QuerySetCell(qry_getChains, "FieldValue", "Carl's Jr.", 3);
	
	temp = QuerySetCell(qry_getChains, "Chain", "Checker's/Rally's", 4);
	temp = QuerySetCell(qry_getChains, "FieldValue", "Checker's/Rally's", 4);
	
	temp = QuerySetCell(qry_getChains, "Chain", "Chic-fil-A", 5);
	temp = QuerySetCell(qry_getChains, "FieldValue", "Chic-fil-A", 5);

	temp = QuerySetCell(qry_getChains, "Chain", "Chicken Express", 6);
	temp = QuerySetCell(qry_getChains, "FieldValue", "Chicken Express", 6);

	temp = QuerySetCell(qry_getChains, "Chain", "Dairy Queen", 7);
	temp = QuerySetCell(qry_getChains, "FieldValue", "Dairy Queen", 7);
	
	temp = QuerySetCell(qry_getChains, "Chain", "Dunkin' Donuts", 8);
	temp = QuerySetCell(qry_getChains, "FieldValue", "Dunkin' Donuts", 8);
	
	temp = QuerySetCell(qry_getChains, "Chain", "Hardee's", 9);
	temp = QuerySetCell(qry_getChains, "FieldValue", "Hardee's", 9);
	
	temp = QuerySetCell(qry_getChains, "Chain", "Jack in the Box", 10);
	temp = QuerySetCell(qry_getChains, "FieldValue", "Jack in the Box", 10);
	
	temp = QuerySetCell(qry_getChains, "Chain", "KFC", 11);
	temp = QuerySetCell(qry_getChains, "FieldValue", "KFC", 11);
	
	temp = QuerySetCell(qry_getChains, "Chain", "Long John Silver's", 12);
	temp = QuerySetCell(qry_getChains, "FieldValue", "Long John Silver's", 12);
	
	temp = QuerySetCell(qry_getChains, "Chain", "McDonald's", 13);
	temp = QuerySetCell(qry_getChains, "FieldValue", "McDonald's", 13);
	
	temp = QuerySetCell(qry_getChains, "Chain", "Popeyes", 14);
	temp = QuerySetCell(qry_getChains, "FieldValue", "Popeyes", 14);
	
	temp = QuerySetCell(qry_getChains, "Chain", "Taco Bell", 15);
	temp = QuerySetCell(qry_getChains, "FieldValue", "Taco Bell", 15);
	
	temp = QuerySetCell(qry_getChains, "Chain", "Tim Hortons", 16);
	temp = QuerySetCell(qry_getChains, "FieldValue", "Tim Hortons", 16);
	
	temp = QuerySetCell(qry_getChains, "Chain", "Wendy's", 17);
	temp = QuerySetCell(qry_getChains, "FieldValue", "Wendy's", 17);
	
	temp = QuerySetCell(qry_getChains, "Chain", "Zaxby's", 18);
	temp = QuerySetCell(qry_getChains, "FieldValue", "Zaxby's", 18);
	
	temp = QuerySetCell(qry_getChains, "Chain", "Other (Enter Chain)", 19);
	temp = QuerySetCell(qry_getChains, "FieldValue", "Other", 19);	


</cfscript>

<cfparam name="FORM.Store_Name" default="">
<cfparam name="FORM.RMA" default="">
<cfparam name="FORM.Company_Name" default="">
<cfparam name="FORM.First_Name" default="">
<cfparam name="FORM.Last_Name" default="">
<cfparam name="FORM.Store_Name" default="">
<cfparam name="FORM.Store_Name_other" default="">
<cfparam name="FORM.Store_Number" default="">
<cfparam name="FORM.Email" default="">
<cfparam name="FORM.Phone" default="">
<cfparam name="FORM.Address1" default="">
<cfparam name="FORM.Address2" default="">
<cfparam name="FORM.City" default="">
<cfparam name="FORM.State" default="">
<cfparam name="FORM.ZipCode" default="">
<cfparam name="FORM.State" default="">
<cfparam name="FORM.Product" default="">
<cfparam name="FORM.State" default="">
<cfparam name="FORM.RMA" default="">
<cfparam name="FORM.item10" default="0">

<cfset VARIABLES.StateList = "AK,AL,AR,AZ,CA,CO,CT,DC,DE,FL,GA,HI,IA,ID,IL,IN,KS,KY,LA,MA,MD,ME,MI,MN,MO,MS,MT,NC,ND,NE,NH,NJ,NM,NV,NY,OH,OK,OR,PA,RI,SC,SD,TN,TX,UT,VA,VT,WA,WI,WV,WY,AA,AE,AP,AS,FM,GU,MH,MP,PR,PW,VI">
</cfsilent>
<cfoutput>
<div class="ship_repair">
	<cfform action="./?pg=SendRepair&st=GetLabel" method="post" id="t_form">
		<table>
			<cfif ERR.ErrorFound>
			<tr>
				<td colspan="2">
					<div class="error">
						<h4>The following errors occured during your request:</h4>
						<ul>
							<li>#ERR.ErrorMessage#</li>
						</ul>
					</div>
				</td>
			</tr>
			</cfif>
			<tr>
				<td valign="top">
					<table>
						<tr>
							<td>*First Name:</td>
							<td><input type="text" name="First_Name" value="#FORM.First_Name#" message="Your First Name is Required!" validateat="onSubmit"  required="yes"></td>
						</tr>
						<tr>
							<td>*Last Name:</td>
							<td><input type="text" name="Last_Name" value="#FORM.Last_Name#" message="Your Last Name is Required!" validateat="onSubmit"  required="yes"></td>
						</tr>
						<tr>
							<td>*Chain:</td>
							<td>
								<select id="Store_Name" name="Store_Name" value="#FORM.Store_Name#" required="yes" message="Your Store Name is Required!">
										<option value=""></option>
									<cfloop query="qry_getChains">
										<option <cfif FORM.Store_Name IS FieldValue>selected="selected"</cfif> value="#FieldValue#">#Chain#</option>
									</cfloop>
								</select>
								<input id="Store_Name_other" type="Text" name="Store_Name_other" value="#FORM.Store_Name_other#">
							</td>
						</tr>
						<tr>
							<td>*Store Number:</td>
							<td><input type="text" name="Store_Number" value="#FORM.Store_Number#" message="Your Store Number/ID is Required!" required="yes"></td>
						</tr>
						<tr>
							<td>*Email Address:</td>
							<td><input type="text" name="Email" value="#FORM.Email#" message="Not a valid Email address!" validateat="onSubmit"  validate="email" required="yes"></td>
						</tr>
						<tr>
							<td>*Phone Number:</td>
							<td><input type="text" name="Phone" value="#FORM.Phone#" message="The Telephone Number is not valid!" validateat="onSubmit" validate="telephone" required="yes"></td>
						</tr>
						<tr>
							<td nowrap="nowrap">*Address Line 1:</td>
							<td><input type="text" name="Address1" value="#FORM.Address1#" message="Address line 1 is required!"validateat="onSubmit"  required="yes"></td>
						</tr>
						<tr>
							<td>Address Line 2:</td>
							<td> <input type="text" name="Address2" value="#FORM.Address2#"></td>
						</tr>
						<tr>
							<td>*City:</td>
							<td><input type="text" name="City" value="#FORM.City#" message="City is required!" validateat="onSubmit"  required="yes"></td>
						</tr>
						<tr>
							<td>*State:</td>
							<td>
								<select name="State"><option value="" selected="selected">Select a State</option><cfloop list="#VARIABLES.StateList#" index="thisState">
									<option value="#thisState#" <CFIF FORM.State IS thisState> selected="selected"</cfif>>#thisState#</option></cfloop>
								</select>
							</td>
						</tr>
						<tr>
							<td>*Zip Code:</td>
							<td><input type="text" name="ZipCode" value="#FORM.ZipCode#" message="Zipcode is not valid!" validateat="onSubmit" validate="zipcode" required="yes"></td>
						</tr>
						<tr>
							<td colspan="2">*=Required Fields</td>
						</tr>
					</table>
				</td>
				<td valign="top">
					<table>
						<tr>
							<td>
								<table>
                                	<cfif StructKeyExists(URL, "pdt") AND Len(URL.pdt) EQ 32>
                                        <tr>
                                            <th colspan="2" nowrap="nowrap">Product you wish to send in for repair</th>
                                            <th nowrap="nowrap">Quantity</th>
                                        </tr>
                                        <tr>
                                        	<td colspan="2" class="send-it-in-text">#qry_getProductName.Product_Short_Description#</td>
                                            <td><cfinput type="text" name="item0" message="Not a valid product amount!" validateat="onBlur" validate="integer" value="1" size="2" maxlength="2">&nbsp;&nbsp;<a href="./?pg=SendRepair" class="send-it-in-remove">(Remove)</a></td>
                                        </tr>
                                        <tr>
                                            <th colspan="2" nowrap="nowrap">Other Products</th>
                                            <th nowrap="nowrap"><input type="hidden" name="Product_Name" id="Product_Name" value="#qry_getProductName.Product_Short_Description#" /></th>
                                        </tr>
                                    <cfelse>
                                        <tr>
                                            <th colspan="2" nowrap="nowrap">Check the Products you wish to return</th>
                                            <th nowrap="nowrap">Quantity</th>
                                        </tr>
                                    </cfif>
									<tr>
										<td><cfinput type="checkbox" name="Product" message="At least one Product must be checked!" required="yes" rel="item1::Headset" value=""></td>
										<td>Headset:</td>
										<td><cfinput type="text" name="item1" message="Not a valid product amount!" validateat="onBlur" validate="integer" value="0" size="2" maxlength="2"></td>
									</tr>
									<tr>
										<td><cfinput type="checkbox" name="Product" message="At least one Product must be checked!" required="yes" rel="item2::All-in-one headset" value=""></td>
										<td>All-in-one headset:</td>
										<td><cfinput type="text" name="item2" validateat="onBlur" validate="integer" size="2" maxlength="2" value="0" message="Not a valid product amount!"></td>
									</tr>
									<tr>
										<td><cfinput type="checkbox" name="Product" message="At least one Product must be checked!" required="yes"  rel="item3::Communicator"  value=""></td>
										<td>Communicator:</td>
										<td><cfinput type="text" name="item3" validateat="onBlur" validate="integer" size="2" maxlength="2" value="0" message="Not a valid product amount!"></td>
									</tr>
									<tr>
										<td><cfinput type="checkbox" name="Product" message="At least one Product must be checked!" required="yes"  rel="item4::Timer SYS30"  value=""></td>
										<td>Timer SYS30:</td>
										<td><cfinput type="text" name="item4" validateat="onBlur" validate="integer" size="2" maxlength="2" value="0" message="Not a valid product amount!"></td>
									</tr>
									<tr>
										<td><cfinput type="checkbox" name="Product" message="At least one Product must be checked!" required="yes"  rel="item5::Timer DASH"  value=""></td>
										<td>Timer DASH (control unit):</td>
										<td><cfinput type="text" name="item5" validateat="onBlur" validate="integer" size="2" maxlength="2" value="0" message="Not a valid product amount!"></td>
									</tr>
									<tr>
										<td><cfinput type="checkbox" name="Product" message="At least one Product must be checked!" required="yes"  rel="item6::Timer Zoom"  value=""></td>
										<td>Timer Zoom (control unit):</td>
										<td><cfinput type="text" name="item6"  validateat="onBlur" validate="integer" size="2" maxlength="2" value="0" message="Not a valid product amount!"></td>
									</tr>
									<tr>
										<td><cfinput type="checkbox" name="Product" message="At least one Product must be checked!" required="yes"  rel="item7::Zoom TSP"  value=""></td>
										<td>Zoom TSP:</td>
										<td><cfinput type="text" name="item7" validateat="onBlur" validate="integer" size="2" maxlength="2" value="0"></td>
									</tr>
									
									<tr>
										<td><cfinput type="checkbox" name="Product" message="At least one Product must be checked!" required="yes"  rel="item8::Battery Charger" value=""></td>
										<td>Battery Charger:</td>
										<td><cfinput type="text" name="item8" validateat="onBlur" validate="integer" size="2" maxlength="2" value="0" message="Not a valid product amount!"></td>
									</tr>
									<tr>
										<td><cfinput type="checkbox" name="Product" message="At least one Product must be checked!" required="yes" value="" rel="item9a::Other::item9Wgt::weight"></td>
										<td>Other: <cfinput type="text" name="item9a" message="Other Item Description must be less the 30 letters long." validateat="onBlur" validate="maxlength" maxlength="30">(open 30 characters)</td>
										<td>Weight:	<cfinput type="text" name="item9Wgt" message="Not a valid weight! it must be a whole number." validate="integer" value="0"  size="2" maxlength="2"> LBS</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td align="right" nowrap="nowrap"><span class="style1">Your <a href="./Online_Privacy_Policy.pdf" target="_blank">privacy</a> is assured</span>
								<input type="hidden" name="item10" value="0">
								<input type="hidden" name="RMA" value="">
								<input type="hidden" name="COMPANY_NAME" value="" />
								<input name="submit" type="submit" value="Submit"  id="sbt_btn">
                                
                                <script>
									window.uetq = window.uetq || []; 
									window.uetq.push({ 'ec':'Button', 'ea':'Click', 'el':'Submit Button', 'ev':'1' }); 
								</script>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</cfform>
</div>
</cfoutput>