<cfif CGI.REMOTE_ADDR CONTAINS "10.10.14">
<cfelse>
<cfabort>
</cfif>
<!---2014/10/23 ATN: This Page Reprints a User Invoice--->
<!---2014/10/23 ATN: This only takes a single var get_Session which is the User's Unique Session_UID--->
<cfif NOT StructKeyExists(URL, "get_Session")>
	<cfabort>
</cfif>

<cfscript>
	QUERY = StructNew();
	QUERY.QueryName = "qry_getInvoice";
	QUERY.Datasource = APPLICATION.Datasource;
	QUERY.Session_UID = get_Session;
	QUERY.SelectSQL = "DISTINCT invc.Invoice_Session_UID, invc.Invoice_User_Cookie, invc.Invoice_Product_UID, invc.Invoice_Manufacturer_Part_Number, invc.Invoice_Product_Description, invc.Invoice_Product_Unit_Price, invc.Invoice_Product_Price, invc.Invoice_Product_Discount, invc.Invoice_Product_Qty, invc.Invoice_UOM, invc.Invoice_UNSPSC, invc.Invoice_Order, invc.Invoice_Created_DTS, invc.Invoice_Product_Price_Type, invc.Invoice_Transaction_ID, Invoice_Correlation_ID";
	QUERY.OrderBy = "invc.Invoice_Session_UID, invc.Invoice_Order, invc.Invoice_Created_DTS";
</cfscript>
<cfinclude template="./_qry_select_user_invoice.cfm">

<cfscript>
	QUERY = StructNew();
	QUERY.QueryName = "qry_getUserInfo";
	QUERY.Datasource = APPLICATION.Datasource;
	QUERY.User_Cookie = qry_getInvoice.Invoice_User_Cookie;
	//QUERY.User_Created_IP = CGI.REMOTE_ADDR;
	QUERY.Invoice_Transaction_ID = qry_getInvoice.Invoice_Transaction_ID;
	QUERY.OrderBy = "usrs.User_Created_DTS DESC";
</cfscript>

<cfquery name="#QUERY.QueryName#" datasource="#QUERY.Datasource#" maxrows="1">
	SELECT usrs.*, invc.*
	FROM 
		tbl_Users usrs 
		INNER JOIN stbl_User_Invoice invc ON usrs.User_Cookie = invc.Invoice_User_Cookie
	WHERE 0=0
	AND usrs.User_Cookie = <cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.User_Cookie#">
	AND invc.Invoice_Transaction_ID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.Invoice_Transaction_ID#">
	ORDER BY #QUERY.OrderBy#
</cfquery>
<cfif StructClear(QUERY)></cfif>
		
<cfset pdf_InvoiceName = "Invoice_#qry_getInvoice.Invoice_Transaction_ID#">

<!---Create PDF--->
<cfdocument 
	name="pdf_Content" 
	format="pdf" 
	pagetype="letter" 
	backgroundvisible="yes" 
	fontembed="no" 
	mimetype="text/html">
	<?xml version="1.0" encoding="UTF-8" ?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<style type="text/css" media="screen">@import"/css/style.cer.css";</style>
	</head>
	<body>
	<div id="Payments" class="products clear">
		<div><img src="/images/celogo.png" /></div>
		<cfinclude template="./_tmp_cart_invoice.cfm">
	</div>
	<cfdocumentitem type="footer">
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td align="center"><cfoutput>#CFDOCUMENT.currentpagenumber# of #CFDOCUMENT.totalpagecount#</cfoutput></td>
		</tr>
	</table>
	</cfdocumentitem>
	</body>
	</html>
</cfdocument>

<!---<cfset SendInvoice_To = "CStockton@ceinstl.com,mkapler@ceinstl.com,rturner@ceinstl.com,tjablonski@ceinstl.com">--->
<!---cc="abaer@hme.com,CHeger@ceinstl.com"--->
<cfset SendInvoice_To = "chrisg@hme.com">
<!---Mail it to CE--->
<cfmail 
	to="#SendInvoice_To#" 
	
	bcc="chrisg@hme.com"
	from="no-reply@cerepairs.com" 
	subject="Invoice of Order - #qry_getInvoice.Invoice_Transaction_ID#" 
	type="html">
	<html>
	<style>
		@charset "utf-8";
		/* CSS Document */
		* {
			border:none;
			margin:0;
			padding:0;
			font-size:12px;
			text-decoration:none;
			font-weight:normal;
			font-family:Arial, Helvetica, sans-serif;
		}
		html {
		}
		body {
			background:none;
		}
		strong {
			font-size:inherit;
			font-weight:bolder;
		}
		em {
			font-size:inherit;
			font-style:italic;
		}
		a:link, a:visited, a:hover, a:active {
			color:##679cd5;
			font-weight:bold;
		}
		a:hover {
			text-decoration:underline;
		}
		.products h5 {
			border-bottom: 1px solid ##CCCCCC;
			color: ##AF292E;
			font-size: 15px;
			font-weight: bold;
			margin: 0 0 10px;
			padding: 0 0 5px;
			display:block;
		}
		.products table {
			width:100%;
			padding:5px;
		}
		.products th {
			color:##999;
			background-color:##ebebeb;
			font-weight:bold;
			vertical-align:top;
			padding:5px;
		}
		.products table.full_cart th {
			color:##999;
			background-color:##ebebeb;
			font-weight:bold;
			vertical-align:top;
			padding:5px;
		}
		.products table.full_cart td.p_tot div {
			text-align:right;
		}
		.products td {
			padding:3px;
			text-align:left;
			vertical-align:text-top;
		}
		.products td.c_qty {
			text-align:right;
		} 
		.products table.full_cart td p {
			font-weight:bold;
			text-align:left;
		}
		.products td h3 {
			font-weight:bold;
			font-size:14px;
			text-align:center;
			padding:10px;
		}
		.products th.thm {
			width:100px;
		}
		.products tr.shade {
			background-color:##ebebeb;
		}
		.products tr.noshade {
			background-color:##fff;
		}
		.strike_disc {
			text-decoration:line-through;
		}
		.discount_amount {
			color:##af292e;
		}
		.clear:after {
			clear:both;
			content:".";
			display:block;
			height:0;
			visibility:hidden;
			font-size:0;
		}
	</style>
	<body>
	<div id="Payments" class="products clear">
	<h2>The following order was placed through the website:</h2>
	<cfinclude template="./_tmp_cart_invoice.cfm">
	</div>
	</body>
	</html>
	<cfmailparam
		file="#pdf_InvoiceName#.pdf"
		type="application/pdf"
		content="#pdf_Content#"
		/>
</cfmail>

Done!