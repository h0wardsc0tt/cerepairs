<cfparam name="owth" default="360">
<cfparam name="ohth" default="219">
<cfoutput>
<div class="info">
	<h1 class="pagetitle">CE Drive-Thru Maintenance Videos</h1>
	<div class="single clear">
		<h2>ION IQ Videos</h2>
		<div class="plug">
			<div class="vidplug">
				<h3>How to register an ION IQ AIO headset to an ION IQ base station</h3>
                <object width="#owth#" height="#ohth#">
                    <param 
                        name="movie" 
                        value="https://www.youtube.com/v/HYy4NU4ifZk?hl=en_US&amp;version=3&amp;rel=0"></param>
                    <param 
                        name="allowFullScreen" 
                        value="true"></param>
                    <param name="allowscriptaccess" value="always"></param>
                    <embed src="https://www.youtube.com/v/HYy4NU4ifZk?hl=en_US&amp;version=3&amp;rel=0" type="application/x-shockwave-flash" width="#owth#" height="#ohth#" allowscriptaccess="always" allowfullscreen="true"></embed>
                </object>
			</div>
			<div class="vidplug">
				<h3>How to register a ION IQ belt-pac to an ION IQ base station</h3>
                <object width="#owth#" height="#ohth#">
                    <param 
                        name="movie" 
                        value="https://www.youtube.com/v/YL8HBJYke-c?hl=en_US&amp;version=3&amp;rel=0"></param>
                    <param 
                        name="allowFullScreen" 
                        value="true"></param>
                    <param name="allowscriptaccess" value="always"></param>
                    <embed src="https://www.youtube.com/v/YL8HBJYke-c?hl=en_US&amp;version=3&amp;rel=0" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="#owth#" height="#ohth#"></embed>
                </object>
			</div>
		</div>
		<div class="plug">
			<div class="vidplug">
				<h3>How to clear-all registration from the base station</h3>
                <object width="#owth#" height="#ohth#">
                    <param 
                        name="movie" 
                        value="https://www.youtube.com/v/F91PbBZQWSs?hl=en_US&amp;version=3&amp;rel=0"></param>
                    <param 
                        name="allowFullScreen" 
                        value="true"></param>
                    <param name="allowscriptaccess" value="always"></param>
                    <embed src="https://www.youtube.com/v/F91PbBZQWSs?hl=en_US&amp;version=3&amp;rel=0" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="#owth#" height="#ohth#"></embed>
                </object>
			</div>
			<div class="vidplug">
				<h3>How to clear-inactive registration from the base station</h3>
                <object width="#owth#" height="#ohth#">
                    <param 
                        name="movie" 
                        value="https://www.youtube.com/v/bBgDEpUdA7U?hl=en_US&amp;version=3&amp;rel=0"></param>
                    <param 
                        name="allowFullScreen" 
                        value="true"></param>
                    <param name="allowscriptaccess" value="always"></param>
                    <embed src="https://www.youtube.com/v/bBgDEpUdA7U?hl=en_US&amp;version=3&amp;rel=0" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="#owth#" height="#ohth#"></embed>
                </object>
			</div>
		</div>
        <div class="plug">
			<div class="vidplug">
				<h3>How to change the language on an AIO headset</h3>
                <object width="#owth#" height="#ohth#">
                    <param 
                        name="movie" 
                        value="https://www.youtube.com/v/awBpsbnUHjw?hl=en_US&amp;version=3&amp;rel=0"></param>
                    <param 
                        name="allowFullScreen" 
                        value="true"></param>
                    <param name="allowscriptaccess" value="always"></param>
                    <embed src="https://www.youtube.com/v/awBpsbnUHjw?hl=en_US&amp;version=3&amp;rel=0" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="#owth#" height="#ohth#"></embed>
                </object>
			</div>
			<div class="vidplug">
				<h3>How to change the language on a belt-pac</h3>
                <object width="#owth#" height="#ohth#">
                    <param 
                        name="movie" 
                        value="https://www.youtube.com/v/GH45v1fV0aI?hl=en_US&amp;version=3&amp;rel=0"></param>
                    <param 
                        name="allowFullScreen" 
                        value="true"></param>
                    <param name="allowscriptaccess" value="always"></param>
                    <embed src="https://www.youtube.com/v/GH45v1fV0aI?hl=en_US&amp;version=3&amp;rel=0" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="#owth#" height="#ohth#"></embed>
                </object>
			</div>
		</div>
        <div class="plug">
			<div class="vidplug">
				<h3>How to set up reminders</h3>
                <object width="#owth#" height="#ohth#">
                    <param 
                        name="movie" 
                        value="https://www.youtube.com/v/y4fV4coK-go?hl=en_US&amp;version=3&amp;rel=0"></param>
                    <param 
                        name="allowFullScreen" 
                        value="true"></param>
                    <param name="allowscriptaccess" value="always"></param>
                    <embed src="https://www.youtube.com/v/y4fV4coK-go?hl=en_US&amp;version=3&amp;rel=0" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="#owth#" height="#ohth#"></embed>
                </object>
			</div>
		</div>
	</div>
</div>
</cfoutput>

