<cfsilent>
	<!---Validate Session and insert/create invoice--->
	<cfparam name="get_Session" default="">
	<cfinclude template="./_dat_cart_prep.cfm">
	
	<cfscript>
		get_Session = SESSION.Session_UID;
		get_Cookie = COOKIE.User_Cookie;
	</cfscript>
	
	<!---User Entry Validation--->
	<cfscript>
		if(Len(FORM.firstName) EQ 0) {
			ERR.ErrorFound = true;
			ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Enter the first name as it appears on your credit card");
		}
		if(Len(FORM.lastName) EQ 0) {
			ERR.ErrorFound = true;
			ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Enter the last name as it appears on your credit card");
		}
		if(Len(FORM.email) EQ 0) {
			ERR.ErrorFound = true;
			ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Please enter your email address");
		} else {
			if(ReFindNoCase("^([a-zA-Z0-9_\-\.])+@(([0-2]?[0-5]?[0-5]\.[0-2]?[0-5]?[0-5]\.[0-2]?[0-5]?[0-5]\.[0-2]?[0-5]?[0-5])|((([a-zA-Z0-9\-])+\.)+([a-zA-Z\-])+))$", FORM.email) EQ 0) {
				ERR.ErrorFound = true;
				ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Please enter a valid email address");
			}
		}
		if(Len(FORM.phone) EQ 0) {
			ERR.ErrorFound = true;
			ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Please enter your phone number");
		} else {
				FORM.phone = ReReplaceNoCase(FORM.phone, "[^0-9]", "", "all");//Strip All Non-Numeric Chars
			if(NOT IsNumeric(FORM.phone)) {
				ERR.ErrorFound = true;
				ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Please enter a valid phone number");
			} else {
				if(Len(FORM.phone) EQ 10) {
					prenum = Left(FORM.phone, 3);
					midnum = Mid(FORM.phone, 4, 3);
					//midnum = Mid(FORM.phone, 3, 3); !20131206 CG: Altered start position from 3 to 4 to correct output - Ticket#46609!
					posnum = Right(FORM.phone, 4);
					FORM.phone = "(#prenum#) #midnum#-#posnum#";
				}
			}
		}
		if(Len(FORM.creditCardType) EQ 0) {
			ERR.ErrorFound = true;
			ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Please enter a credit card type");
		}
		if(Len(FORM.creditCardNumber) EQ 0) {
			ERR.ErrorFound = true;
			ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Please enter your credit card");
		}
		if(Len(FORM.expDateMonth) EQ 0) {
			ERR.ErrorFound = true;
			ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Please enter the expiration date on credit card");
		}
		if(Len(FORM.expDateYear) EQ 0) {
			ERR.ErrorFound = true;
			ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Please enter the expiration date on credit card");
		}
		if(Len(FORM.expDateMonth) NEQ 0 AND Len(FORM.expDateYear) NEQ 0) {
			checkDate = "#FORM.expDateMonth#/1/#FORM.expDateYear#";
			currDate = CreateDate(Year(Now()), Month(Now()), 1);
			if(NOT IsDate(checkDate) OR (checkDate LT currDate)) {
				ERR.ErrorFound = true;
				ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "The expiration date you entered does not appear to be valid");
			}
		}
		if(Len(FORM.cvv2Number) EQ 0 OR NOT IsNumeric(FORM.cvv2Number)) {
			ERR.ErrorFound = true;
			ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Please enter the CVN on the back of your credit card");
		}
		if(IsDefined("FORM.usesameadd") AND FORM.usesameadd EQ 1) {
			if(Len(FORM.address1) EQ 0) { //Copy Over All Shipping To Billing. Javascript Error Catch
				FORM.address1 = FORM.Ship_address1;
				FORM.address2 = FORM.Ship_address2;
				FORM.city = FORM.Ship_city;
				FORM.state = FORM.Ship_state;
				FORM.zip = FORM.Ship_zip;
			}
		}
		if(Len(FORM.address1) EQ 0) {
			ERR.ErrorFound = true;
			ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Please enter your billing address");
		}
		if(Len(FORM.city) EQ 0) {
			ERR.ErrorFound = true;
			ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Please enter your billing city");
		}
		if(Len(FORM.state) EQ 0) {
			ERR.ErrorFound = true;
			ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Please enter your billing state");
		}
		if(Len(FORM.zip) EQ 0) {
			ERR.ErrorFound = true;
			ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Please enter your billing zip");
		}
	</cfscript>
</cfsilent>
	
	<!---On Error Send User Back--->
	<cfif ERR.ErrorFound>
		<cfset ERR.Billing_Error = true>
		<cfinclude template="./_tmp_cart_bil.cfm">
		<cfexit method="exittemplate">
	</cfif>
	
	<!---Capture Info and Submit Payment--->
	<cfobject component="_cfcs.CallerService" name="caller">
	<cftry>
		<cfscript>
			expDate =  FORM.expDateMonth & FORM.expDateYear;
			requestData = StructNew();
			requestData.METHOD = "DoDirectPayment";
			requestData.PAYMENTACTION = "#FORM.PAYMENTACTION#";
			requestData.USER = "#APPLICATION.PayPal_USER#";
			requestData.PWD = "#APPLICATION.PayPal_PASS#";
			requestData.SIGNATURE = "#APPLICATION.PayPal_SIGN#";
			requestData.SUBJECT = "";//No Subject Needed for 3 token: User, PWD, Signature
			requestData.VERSION = "#APPLICATION.PayPal_APIV#";
			requestData.FIRSTNAME = "#FORM.firstName#";
			requestData.LASTNAME = "#FORM.lastName#";
			requestData.AMT = "#FORM.amount#";
			requestData.STREET = "#FORM.address1#";
			requestData.CURRENCYCODE = "USD";
			requestData.ZIP = "#FORM.zip#";
			requestData.CVV2 = "#FORM.cvv2Number#";
			requestData.CREDITCARDTYPE = "#FORM.creditCardType#";
			requestData.EXPDATE = "#expDate#";
			requestData.STATE = "#FORM.state#";
			requestData.COUNTRYCODE = "US";
			requestData.ACCT = "#FORM.creditCardNumber#";
			requestData.CITY = "#FORM.city#";
		</cfscript>
		<!--- Calling doHttppost for API call --->
		<cfinvoke component="_cfcs.CallerService" method="doHttppost" returnvariable="response">
			<cfinvokeargument name="requestData" value="#requestData#">
			<cfinvokeargument name="serverURL" value="#APPLICATION.PayPal_URL#">
			<cfinvokeargument name="proxyName" value="#APPLICATION.PayPal_proxyName#">
			<cfinvokeargument name="proxyPort" value="#APPLICATION.PayPal_proxyPort#">
			<cfinvokeargument name="useProxy" value="#APPLICATION.PayPal_useProxy#">
		</cfinvoke>
       	<cfoutput>
        <cfdump var="#APPLICATION.PayPal_URL#">
        <cfdump var="#APPLICATION.PayPal_proxyName#">
        <cfdump var="#APPLICATION.PayPal_proxyPort#">
        <cfdump var="#APPLICATION.PayPal_useProxy#">
        <cfdump var="#requestData#">
        <cfdump var="#response#">
		<cfabort>
		</cfoutput>	
	
	<cfset responseStruct = caller.getNVPResponse(#URLDecode(response)#)>
	<cfset messages = ArrayNew(1)>
	<cfif responseStruct.Ack IS "Success" OR responseStruct.Ack IS "SuccessWithWarning">
		<cfquery name="qry_InsUser" datasource="#APPLICATION.Datasource#">
			INSERT INTO tbl_Users
				(User_Cookie,
				Contact_FirstName,
				Contact_LastName,
				Contact_Email,
				Contact_Phone,
				Bill_Address1,
				Bill_Address2,
				Bill_City,
				Bill_State,
				Bill_Zip,
				Bill_Country,
				Ship_Company,
				Ship_Address1,
				Ship_Address2,
				Ship_City,
				Ship_State,
				Ship_Zip,
				Ship_Country,
				User_Created_DTS,
				User_Created_IP,
				User_Created_Agent)
			VALUES
				(<cfqueryparam value="#get_Cookie#" cfsqltype="cf_sql_varchar">,
				<cfqueryparam value="#FORM.firstName#" cfsqltype="cf_sql_varchar">,
				<cfqueryparam value="#FORM.lastName#" cfsqltype="cf_sql_varchar">,
				<cfqueryparam value="#FORM.email#" cfsqltype="cf_sql_varchar">,
				<cfqueryparam value="#FORM.phone#" cfsqltype="cf_sql_varchar">,
				<cfqueryparam value="#FORM.address1#" cfsqltype="cf_sql_varchar">,
				<cfqueryparam value="#FORM.address2#" cfsqltype="cf_sql_varchar">,
				<cfqueryparam value="#FORM.city#" cfsqltype="cf_sql_varchar">,
				<cfqueryparam value="#FORM.state#" cfsqltype="cf_sql_varchar">,
				<cfqueryparam value="#FORM.zip#" cfsqltype="cf_sql_varchar">,
				<cfqueryparam value="USA" cfsqltype="cf_sql_varchar">,
				<cfqueryparam value="#FORM.Ship_Company#" cfsqltype="cf_sql_varchar">,
				<cfqueryparam value="#FORM.Ship_address1#" cfsqltype="cf_sql_varchar">,
				<cfqueryparam value="#FORM.Ship_address2#" cfsqltype="cf_sql_varchar">,
				<cfqueryparam value="#FORM.Ship_city#" cfsqltype="cf_sql_varchar">,
				<cfqueryparam value="#FORM.Ship_state#" cfsqltype="cf_sql_varchar">,
				<cfqueryparam value="#FORM.Ship_zip#" cfsqltype="cf_sql_varchar">,
				<cfqueryparam value="USA" cfsqltype="cf_sql_varchar">,
				<cfqueryparam value="#CreateODBCDateTime(Now())#" cfsqltype="cf_sql_date">,
				<cfqueryparam value="#CGI.REMOTE_ADDR#" cfsqltype="cf_sql_varchar">,
				<cfqueryparam value="#CGI.HTTP_USER_AGENT#" cfsqltype="cf_sql_varchar">)
		</cfquery>
		
		<cfset Transaction_ID = responseStruct.TRANSACTIONID>
		<cfset Correlation_ID = responseStruct.CORRELATIONID>
		
		<cfquery name="qry_updInvoice" datasource="#APPLICATION.Datasource#">
			UPDATE stbl_User_Invoice
			SET 
				Invoice_Transaction_ID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Transaction_ID#">,
				Invoice_Correlation_ID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Correlation_ID#">
			WHERE 
				Invoice_Session_UID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#get_Session#">
			AND
				Invoice_User_Cookie = <cfqueryparam cfsqltype="cf_sql_varchar" value="#get_Cookie#">
		</cfquery>
		
		<cfscript>
			QUERY = StructNew();
			QUERY.QueryName = "qry_getInvoice";
			QUERY.Datasource = APPLICATION.Datasource;
			QUERY.Session_UID = get_Session;
			QUERY.SelectSQL = "DISTINCT invc.Invoice_Session_UID, invc.Invoice_User_Cookie, invc.Invoice_Product_UID, invc.Invoice_Manufacturer_Part_Number, invc.Invoice_Product_Description, invc.Invoice_Product_Unit_Price, invc.Invoice_Product_Price, invc.Invoice_Product_Discount, invc.Invoice_Product_Qty, invc.Invoice_UOM, invc.Invoice_UNSPSC, invc.Invoice_Order, invc.Invoice_Created_DTS, invc.Invoice_Product_Price_Type, invc.Invoice_Transaction_ID, Invoice_Correlation_ID";
			QUERY.OrderBy = "invc.Invoice_Session_UID, invc.Invoice_Order, invc.Invoice_Created_DTS";
		</cfscript>
		<cfinclude template="./_qry_select_user_invoice.cfm">
		
		<cfscript>
			QUERY = StructNew();
			QUERY.QueryName = "qry_getUserInfo";
			QUERY.Datasource = APPLICATION.Datasource;
			QUERY.User_Cookie = get_Cookie;
			QUERY.User_Created_IP = CGI.REMOTE_ADDR;
			QUERY.Invoice_Transaction_ID = Transaction_ID;
			QUERY.OrderBy = "usrs.User_Created_DTS DESC";
		</cfscript>
		
		<cfquery name="#QUERY.QueryName#" datasource="#QUERY.Datasource#" maxrows="1">
			SELECT usrs.*, invc.*
			FROM 
				tbl_Users usrs 
				INNER JOIN stbl_User_Invoice invc ON usrs.User_Cookie = invc.Invoice_User_Cookie
			WHERE 0=0
			AND usrs.User_Cookie = <cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.User_Cookie#">
			AND usrs.User_Created_IP = <cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.User_Created_IP#">
			AND invc.Invoice_Transaction_ID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.Invoice_Transaction_ID#">
			ORDER BY #QUERY.OrderBy#
		</cfquery>
		<cfif StructClear(QUERY)></cfif>
		
<cfset pdf_InvoiceName = "Invoice_#qry_getInvoice.Invoice_Transaction_ID#">

<!---Create PDF--->
<cfdocument 
	name="pdf_Content" 
	format="pdf" 
	pagetype="letter" 
	backgroundvisible="yes" 
	fontembed="no" 
	mimetype="text/html">
	<?xml version="1.0" encoding="UTF-8" ?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<style type="text/css" media="screen">@import"/css/style.cer.css";</style>
	</head>
	<body>
	<div id="Payments" class="products clear">
		<div><img src="/images/celogo.png" /></div>
		<cfinclude template="./_tmp_cart_invoice.cfm">
	</div>
	<cfdocumentitem type="footer">
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td align="center"><cfoutput>#CFDOCUMENT.currentpagenumber# of #CFDOCUMENT.totalpagecount#</cfoutput></td>
		</tr>
	</table>
	</cfdocumentitem>
	</body>
	</html>
</cfdocument>

<!---<cfset SendInvoice_To = "CStockton@ceinstl.com,mkapler@ceinstl.com,rturner@ceinstl.com,tjablonski@ceinstl.com,BGrijalva@ceinstl.com,ARathbun@ceinstl.com,JBellin@ceinstl.com">--->
<cfset SendInvoice_To = "CEORDERS@hme.com">

<!---Mail it to CE--->
<cfmail 
	to="CEORDERS@hme.com" 
	cc="abaer@hme.com"
	bcc="Web_Support@hme.com"
	from="no-reply@cerepairs.com" 
	subject="Invoice of Order - #qry_getInvoice.Invoice_Transaction_ID#" 
	type="html">
	<html>
	<style>
		@charset "utf-8";
		/* CSS Document */
		* {
			border:none;
			margin:0;
			padding:0;
			font-size:12px;
			text-decoration:none;
			font-weight:normal;
			font-family:Arial, Helvetica, sans-serif;
		}
		html {
		}
		body {
			background:none;
		}
		strong {
			font-size:inherit;
			font-weight:bolder;
		}
		em {
			font-size:inherit;
			font-style:italic;
		}
		a:link, a:visited, a:hover, a:active {
			color:##679cd5;
			font-weight:bold;
		}
		a:hover {
			text-decoration:underline;
		}
		.products h5 {
			border-bottom: 1px solid ##CCCCCC;
			color: ##AF292E;
			font-size: 15px;
			font-weight: bold;
			margin: 0 0 10px;
			padding: 0 0 5px;
			display:block;
		}
		.products table {
			width:100%;
			padding:5px;
		}
		.products th {
			color:##999;
			background-color:##ebebeb;
			font-weight:bold;
			vertical-align:top;
			padding:5px;
		}
		.products table.full_cart th {
			color:##999;
			background-color:##ebebeb;
			font-weight:bold;
			vertical-align:top;
			padding:5px;
		}
		.products table.full_cart td.p_tot div {
			text-align:right;
		}
		.products td {
			padding:3px;
			text-align:left;
			vertical-align:text-top;
		}
		.products td.c_qty {
			text-align:right;
		} 
		.products table.full_cart td p {
			font-weight:bold;
			text-align:left;
		}
		.products td h3 {
			font-weight:bold;
			font-size:14px;
			text-align:center;
			padding:10px;
		}
		.products th.thm {
			width:100px;
		}
		.products tr.shade {
			background-color:##ebebeb;
		}
		.products tr.noshade {
			background-color:##fff;
		}
		.strike_disc {
			text-decoration:line-through;
		}
		.discount_amount {
			color:##af292e;
		}
		.clear:after {
			clear:both;
			content:".";
			display:block;
			height:0;
			visibility:hidden;
			font-size:0;
		}
	</style>
	<body>
	<div id="Payments" class="products clear">
	<h2>The following order was placed through the website:</h2>
	<cfinclude template="./_tmp_cart_invoice.cfm">
	</div>
	</body>
	</html>
	<cfmailparam
		file="#pdf_InvoiceName#.pdf"
		type="application/pdf"
		content="#pdf_Content#"
		/>
</cfmail>
<!---Mail it to Customer--->
<cfmail 
	to="#FORM.email#" 
	bcc="atnelson@hme.com"
	from="no-reply@cerepairs.com" 
	subject="Invoice of Order - #qry_getInvoice.Invoice_Transaction_ID#" 
	type="html">
	<html>
	<style>
		@charset "utf-8";
		/* CSS Document */
		* {
			border:none;
			margin:0;
			padding:0;
			font-size:12px;
			text-decoration:none;
			font-weight:normal;
			font-family:Arial, Helvetica, sans-serif;
		}
		html {
		}
		body {
			background:none;
		}
		strong {
			font-size:inherit;
			font-weight:bolder;
		}
		em {
			font-size:inherit;
			font-style:italic;
		}
		a:link, a:visited, a:hover, a:active {
			color:##679cd5;
			font-weight:bold;
		}
		a:hover {
			text-decoration:underline;
		}
		.products h5 {
			border-bottom: 1px solid ##CCCCCC;
			color: ##AF292E;
			font-size: 15px;
			font-weight: bold;
			margin: 0 0 10px;
			padding: 0 0 5px;
			display:block;
		}
		.products table {
			width:100%;
			padding:5px;
		}
		.products th {
			color:##999;
			background-color:##ebebeb;
			font-weight:bold;
			vertical-align:top;
			padding:5px;
		}
		.products table.full_cart th {
			color:##999;
			background-color:##ebebeb;
			font-weight:bold;
			vertical-align:top;
			padding:5px;
		}
		.products table.full_cart td.p_tot div {
			text-align:right;
		}
		.products td {
			padding:3px;
			text-align:left;
			vertical-align:text-top;
		}
		.products td.c_qty {
			text-align:right;
		} 
		.products table.full_cart td p {
			font-weight:bold;
			text-align:left;
		}
		.products td h3 {
			font-weight:bold;
			font-size:14px;
			text-align:center;
			padding:10px;
		}
		.products th.thm {
			width:100px;
		}
		.products tr.shade {
			background-color:##ebebeb;
		}
		.products tr.noshade {
			background-color:##fff;
		}
		.strike_disc {
			text-decoration:line-through;
		}
		.discount_amount {
			color:##af292e;
		}
		.clear:after {
			clear:both;
			content:".";
			display:block;
			height:0;
			visibility:hidden;
			font-size:0;
		}
	</style>
	<body>
	<div id="Payments" class="products clear">
	<h2>Your Order</h2>
	<cfinclude template="./_tmp_cart_invoice.cfm">
	</div>
	</body>
	</html>
	<cfmailparam
		file="#pdf_InvoiceName#.pdf"
		type="application/pdf"
		content="#pdf_Content#"
		/>
</cfmail>
		<div id="Payments" class="products clear">			
			<h2>Thank you. Your payment was processed successfully!<br />An invoice was sent to the email address you provided.</h2>
			<cfinclude template="./_tmp_cart_invoice.cfm">
		</div>
		
		<cfsavecontent variable = "JS_Checkout">
			<!--- 20140806 CG: Added google conversion script per Ticket#54036 --->
			<!-- Google Code for Accessory Sale Conversion Page -->
			<script type="text/javascript">
			/* <![CDATA[ */
			var google_conversion_id = 1000449060;
			var google_conversion_language = "en";
			var google_conversion_format = "3";
			var google_conversion_color = "ffffff";
			var google_conversion_label = "D-6ICNzctggQpMiG3QM";
			var google_remarketing_only = false;
			/* ]]> */
			</script>
			<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
			</script>
			<noscript>
			<div style="display:inline;">
			<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1000449060/?label=D-6ICNzctggQpMiG3QM&amp;guid=ON&amp;script=0"/>
			</div>
			</noscript>
		</cfsavecontent>
		<cfhtmlhead text="#JS_Checkout#">
	</cfif>
		  
	<cfif responseStruct.Ack is "Failure">
		<cfmail to="atnelson@hme.com, chrisg@hme.com" from="no-reply@hme.com" subject="Payment Failure">
			#responseStruct#
			#get_Cookie#
		</cfmail>
		<cfif StructKeyExists(responseStruct, "L_LONGMESSAGE0")>
			<cfset ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "We're Sorry. An Error Occured While Processing Your Payment. #responseStruct.L_LONGMESSAGE0# Please try again.")>
		<cfelse>
			<cfset ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "We're Sorry. An Error Occured While Processing Your Payment. Please try again.")>
		</cfif>
		<cfinclude template="./_tmp_cart_bil.cfm">
		<cfexit method="exittemplate">
	</cfif>
	
		<cfcatch><!---If any CF errors are thrown in the payment process occur, send them back to the form.--->
			<cfset ERR.ErrorFound = true>
			<cfif StructKeyExists(responseStruct, "L_LONGMESSAGE0")>
				<cfset ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "We're Sorry. An Error Occured While Processing Your Payment. #responseStruct.L_LONGMESSAGE0# Please try again.")>
			<cfelse>
				<cfset ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "We're Sorry. An Error Occured While Processing Your Payment. Please try again.")>
			</cfif>
			<cfinclude template="./_tmp_cart_bil.cfm">
			<cfexit method="exittemplate">
		</cfcatch>
	</cftry>

<!---Complete Cart--->
<cfquery name="qry_FinalizeCart" datasource="#APPLICATION.Datasource#">
	UPDATE stbl_User_Cart_Products
	SET Cart_IsCompleted = 1
	WHERE 
		Cart_Session_UID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#get_Session#">
	AND Cart_User_Cookie = <cfqueryparam cfsqltype="cf_sql_varchar" value="#get_Cookie#">
</cfquery>

<cfscript>
	//re-init session
	clearAll = StructClear(SESSION);
	CreateSession = CreateObject("component", "_cfcs.SessionMgmt");
	Session_UID = CreateSession.initSession(User_Cookie=qry_getInvoice.Invoice_User_Cookie);
</cfscript>

<cfsavecontent variable="JS_CartCount">
<script type="text/javascript">
	$(window).load(function(e) {
		var cartcount = document.getElementById("cartcount");
		cartcount.innerHTML = "";
	});
</script>
</cfsavecontent>
<cfhtmlhead text="#JS_CartCount#">