<cfsilent>
	<cfinclude template="./_dat_cart_prep.cfm">
	
	<cfscript>
		if(ReFindNoCase("[a-z0-9]{32}", URL.suid) NEQ 0) { //Check URL
			get_Session = URL.suid;
		}
		if(ListLen(FORM.suid) GT 0 AND ReFindNoCase("[a-z0-9]{32}", FORM.suid) NEQ 0) { //Check Form
			get_Session = FORM.suid; 
		} 
		if(Len(get_Session) EQ 0) { //Assume no URL or FORM
			get_Session = SESSION.Session_UID;
		}
	</cfscript>
</cfsilent>

<cfoutput>
<div id="Payments" class="products clear">
	<h2>Submit Shipping Information</h2>
	<!-- PayPal Logo --><table border="0" cellpadding="10" cellspacing="0" align="center"><tr><td align="center"></td></tr><tr><td align="center"><a href="##" title="How PayPal Works" onclick="javascript:window.open('https://www.paypal.com/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700');"><img src="https://www.paypalobjects.com/webstatic/mktg/logo/AM_SbyPP_mc_vs_dc_ae.jpg" border="0" alt="PayPal Acceptance Mark"></a></td></tr></table><!-- PayPal Logo -->
	<cfif NOT IsQuery(qry_getCart) OR (IsQuery(qry_getCart) AND qry_getCart.RecordCount EQ 0)><!---No Records In Cart Throw Error--->
		<cfset ERR.ErrorFound = true>
		<cfset ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "You have no items in your cart")>
		<table class="shipto">
				<cfif ERR.ErrorFound>
				<div class="error">
					<h4>The following errors occured during your request:</h4>
					<ul>
						<cfloop list="#ERR.ErrorMessage#" index="thisError"><li>#thisError#</li></cfloop>
					</ul>
				</div>
				</cfif>
		</table>
	<cfelse>
		<form action="https://#APPLICATION.rootURL#/?pg=Cart&st=Billing" id="ce_cart" name="frm_cart" method="post" class="sscart">
			<table class="shipto">
				<cfif ERR.ErrorFound>
				<div class="error">
					<h4>The following errors occured during your request:</h4>
					<ul>
						<cfloop list="#ERR.ErrorMessage#" index="thisError"><li>#thisError#</li></cfloop>
					</ul>
				</div>
				</cfif>
				<cfif IsDefined("FORM.vSubTotal") AND IsNumeric(FORM.vSubTotal)>
				<cfif FORM.vSubTotal GT 1500>
				<tr>
					<td colspan="2"><em>If your order includes items requiring installation, please note that installation cost is NOT included in this pricing.  Please <a href="/contact-ce-drive-thru-repairs" target="_blank">contact CE</a> to discuss installation options.</em></td>
				</tr>
				</cfif>
				</cfif>
				<tr>
					<th colspan="2" class="PayHeader">Shipping Information</th>
				</tr>
				<tr>
					<th class="req">Company Name:</th>
					<td><input type="text" size="25" maxlength="100" name="Ship_Company" value="#FORM.Ship_Company#" /></td>
				</tr>
				<tr>
					<th class="req">Address 1:</th>
					<td><input type="text" size="25" maxlength="100" name="Ship_address1" value="#FORM.Ship_address1#" /></td>
				</tr>
				<tr>
					<th>Address 2:</th>
					<td><input type="text" size="25" maxlength="100" name="Ship_address2" value="#FORM.Ship_address2#" />(optional)</td>
				</tr>
				<tr>
					<th class="req">City:</th>
					<td><input type="text" size="25" maxlength="40" name="Ship_city" value="#FORM.Ship_city#" /></td>
				</tr>
				<tr>
					<th class="req">State:</th>
					<td>
						<select name="Ship_state"><cfloop list="#VARIABLES.StateList#" index="thisState">
							<option value="#thisState#"<cfif FORM.Ship_state IS thisState> selected="selected"</cfif>>#thisState#</option></cfloop>
						</select>
					</td>
				</tr>
				<tr>
					<th class="req">ZIP Code:</th>
					<td><input type="text" size="10" maxlength="10" name="Ship_zip" value="#FORM.Ship_zip#" />(5 or 9 digits)</td>
				</tr>
				<tr>
					<th>Country:</th>
					<td>United States</td>
				</tr>
				<tr>
					<td><input type="image" src="/images/cart_proceed.png"/><sup> | <a href="https://#APPLICATION.rootURL#/?pg=Search">Continue Shopping</a></sup></td>
					<td></td>
				</tr>
			</table>
		</form>
	</cfif>
	<div class="clear"></div>
</div>
</cfoutput>

