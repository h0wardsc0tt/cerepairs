<cfparam name="FORM.promo_code" default="#SESSION.Promo_Code#">

<cfsilent>
	<cfparam name="VARIABLES.subTotal" default="0">
	<cfscript>
		QUERY = StructNew();
		QUERY.QueryName = "qry_getCart";
		QUERY.Datasource = APPLICATION.Datasource;
		QUERY.Cart_Session_UID = SESSION.Session_UID;
		QUERY.Cart_User_Cookie = qry_getUser.User_Cookie;
		QUERY.Cart_IsCompleted = 0;
		QUERY.OrderBy = "cart.Cart_DTS";
	</cfscript>
	<cfinclude template="/_qry_select_cart_product.cfm">
</cfsilent>

<cfsavecontent variable="UpdateQuantity">
	<script type="text/javascript"> 
	function actionQuantity(field_id) {
		var upd_field = document.getElementById(field_id + "_upd");
		upd_field.style.visibility = "visible";
		
		var sav_field = document.getElementById(field_id + "_sav");
		sav_field.style.visibility = "hidden";
	}
	
	function htmlQuantity(data,field_id,qty,base_price) {
		var ret_result = "";
		$.each(data, function(index, data) {
			if(data.subtotal != "fail") {
				var sub_field = document.getElementById("subprice");
				sub_field.innerHTML = data.subtotal;
				
				var tot_field = document.getElementById(field_id + "_tot");
				field_total = qty * base_price;
				field_total = "$" + field_total.toFixed(2);//Simple Dollar format
				tot_field.innerHTML = field_total;
				
				var sav_field = document.getElementById(field_id + "_sav");
				sav_field.style.visibility = "visible";
				
				var upd_field = document.getElementById(field_id + "_upd");
				upd_field.style.visibility = "hidden";
				ret_result = "pass";
			} else {
				ret_result = "fail";
			}
		});
		
		return ret_result;
	}
	
	function updateQuantity(field_id,qty,base_price) {
		var jqty_act = "/js/qtyCart.cfm";
		$.ajax({
			url: jqty_act, 
			data: ({ sess: '<cfoutput>#SESSION.Session_UID#</cfoutput>', pdt: field_id, qty: qty }),
			dataType: "json",
			success: function(data) {
				var set_msg = htmlQuantity(data,field_id,qty,base_price);
				if(set_msg == "fail") {
					alert("Error occured");
					<cfoutput>
					window.location.href = "https://#APPLICATION.rootURL#";
					</cfoutput>
				}
			}
		});
	}
	
	function confirmClear() {
		var $con_window = $('<div class="itemadded"></div>')
		.html('This will remove all of the items in your shopping cart. Are you sure?')
		.dialog({
			title: 'Confirm',
			modal:false,
			autoOpen: true,
			buttons: {
				"Remove All Items": function() {
					window.location = "./?pg=Cart&st=Clear";
				},
				Cancel: function() {
					$(this).dialog('close');
				}
			}
		});
	}
	
	function validatePromo(sessionUID, promo, callback){
		jQuery.ajax({ 
			async: false,
			type: 'POST', 
			url: '/_cfcs/CartMgmt.cfc?method=promoValidate', 
			data: {Session_UID: sessionUID, PromoCode: promo}, 
			cache: false, 
			dataType: 'text', 
			success: function(rtnData,rtnStatus,rtnXhr) {
				if (rtnData == 'OK'){
					callback(true);
				}else{
					$('#discount').html(rtnData);
					$('#discount').show();
					setTimeout("hideStatus('discount');", 5000);
					callback(false);
				}
			} 
		});
	}
	
	function applyPromo(sessionUID){
		var promocode = $("#promo_code").val();
		var isValid = false;
		
		validatePromo(sessionUID, promocode, function(rtnStatus){
			isValid = rtnStatus;
		});

		// display discount amount if the coupon code meets the criteria
		if (isValid){
			jQuery.ajax({ 
				async: false,
				type: 'POST', 
				url: '/_cfcs/CartMgmt.cfc?method=getPromoDiscount', 
				data: {Session_UID: sessionUID, PromoCode: promocode}, 
				cache: false, 
				dataType: 'text', 
				success: function(rtnData,rtnStatus,rtnXhr) {
					if (rtnStatus != "success"){
						$('#discount').html('An error occurred. Please try again.');
						$('#discount').show();
						setTimeout("hideStatus('discount');", 5000);
					}else{
						objData = JSON.parse(rtnData);
						
						$('#tot_qty').html('Promotion Quantity: ' + objData.DATA[0][0]);
						$('#tot_qty').show();
						$('#discount').html('Discount: $ -' + objData.DATA[0][1]);
						$('#discount').show();
					}
				} 
			});
		}
		
		return true;
	}
	
	function hideStatus(obj) {
		$('#' + obj).hide();
	}
	</script>
</cfsavecontent>
<cfhtmlhead text="#UpdateQuantity#">

<cfoutput>
<div class="products clear">
	<h2>Shopping Cart</h2>
    
	<cfif qry_getCart.RecordCount NEQ 0>
        <form action="https://#APPLICATION.rootURL#/?pg=Cart&st=Checkout" name="frm_cart" method="post" class="sscart">
        <table class="full_cart">
            <tr>
                <th style="width:5%"></th>
                <th style="width:50%" align="left">Product</th>
                <th style="width:15%" align="right">Unit Price</th>
                <th style="width:15%" align="right">Extended Price</th>
                <th style="width:10%" align="right">Qty</th>
                <th style="width:5%"></th>
            </tr>
            <cfloop query="qry_getCart">
                <cfset p_price = 0>
                <cfset VARIABLES.subTotal = VARIABLES.subTotal + (Cart_Product_QTY * Cart_Product_Price_SubTotal)>
                <tr class="#IIF(CurrentRow MOD 2, DE('noshade'), DE('shade'))#">
                    <td>#Cart_Product_Price_Type#</td>
                    <td valign="top"><p><cfif FileExists("#APPLICATION.rootDir#\images\products\#Product_Image_Thum#")><img src="/images/products/#Product_Image_Thum#" class="p_thumb_qv" /><cfelse><img src="/images/products/noimage.png" class="p_thumb_qv" /></cfif>
                        #Product_Short_Description#</p>
                        <p>Part ##: #Manufacturer_Part_Number#</p>
                    </td>
                    <td valign="top" class="p_tot"><div><cfset p_price = Evaluate(Cart_Product_Price_Type & '_Price')>#DollarFormat(p_price)#</div></td>
                    <td valign="top" class="p_tot"><div id="#Product_UID#_tot">#DollarFormat(Evaluate(p_price*Cart_Product_QTY))#</div></td>
                    <td class="qty">
                        <input onkeyup="return actionQuantity('#Product_UID#');" onchange="return actionQuantity('#Product_UID#');" type="text" name="cqty_#Product_UID#" value="#Cart_Product_QTY#">
                        <div id="#Product_UID#_upd" class="status" style="visibility:hidden;"><a onclick="return updateQuantity('#Product_UID#',frm_cart.cqty_#Product_UID#.value,#p_price#);" href="javascript:void(false);">Update</a></div>
                        <div id="#Product_UID#_sav" class="status ok" style="visibility:hidden;">Saved</div>
                    </td>
                    <td class="remp" style="top:50%"><a href="./?pg=Cart&st=Del&pdt=#Product_UID#">Remove</a></td>
                </tr>
            </cfloop>
            <tr>
                <td colspan="6" class="cleartot">
                    <div class="clearcart"><a href="javascript:void(false);" onclick="return confirmClear();">Clear Shopping Cart</a></div>
                    <div class="subtotal">
                        <label for="promo_code"><strong>Promo Code:</strong></label>
                        <input type="text" name="promo_code" id="promo_code" value="#SESSION.Promo_Code#"/> <a onClick="return applyPromo('#SESSION.Session_UID#')" href="javascript:void(0)">Apply</a>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="6">
                	<div id="tot_qty" class="subtotal" style="display:none;">Promotion Quantity:</div>
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <div id="discount" class="subtotal" style="display:none; color:red;">Discount:</div>
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <input type="hidden" name="vSubTotal" value="#VARIABLES.subTotal#" /><input type="image" src="/images/cart_checkout.png" /><sup> | <a href="https://#APPLICATION.rootURL#/?pg=Search">Continue Shopping</a></sup>
                    <div class="subtotal">
                        Sub-Total:&nbsp;<span id="subprice">#DollarFormat(VARIABLES.subTotal)#</span>
                    </div>
                </td>
            </tr>
        </table></form>
    <cfelse>
        <table>
            <tr>
                <th style="width:5%"></th>
                <th style="width:50%" align="left">Product</th>
                <th style="width:15%" align="right">Unit Price</th>
                <th style="width:15%" align="right">Extended Price</th>
                <th style="width:10%" align="right">Qty</th>
                <th style="width:5%"></th>
            </tr>
            <tr>
                <td colspan="6"><h3>Your cart is empty</h3></td>
            </tr>
        </table>
    </cfif>
	
    <div class="clear"></div>
	<cfinclude template="./_tmp_product_recommended.cfm">
</div>
</cfoutput>