<cfsilent>
<cfparam name="URL.tr" default="">
<cfset cut_off = DateAdd("h", -24, Now())>
<style>
.cust_addr {height:180px;font-size:28px;padding-left:20px;}
</style>
<!---20131206 CG: Added Mike Kapler to the distribution list per Ticket#47580 --->
<cfset emailDist = "CStockton@ceinstl.com,RTurner@ceinstl.com,mkapler@ceinstl.com,abaer@hme.com,RKarsten@hme.com,BGrijalva@ceinstl.com">
	<cfif Len(URL.tr) GT 0>
		<cfquery name="qry_getPackaging" datasource="HMEUPS" maxrows="1">
			SELECT *
			FROM UPS_CAPTURE
			WHERE 
				tracking_number = <cfqueryparam cfsqltype="cf_sql_varchar" value="#URL.tr#">
			AND Created_DTS >= <cfqueryparam cfsqltype="cf_sql_date" value="#CreateODBCDateTime(cut_off)#">
			AND 
				(
					Packing_Label <> '' 
				AND 
					Packing_Label IS NOT NULL
				)
		</cfquery>
		
		<cfif FileExists("#getDirectoryFromPath(getCurrentTemplatePath())#\images\ups\#qry_getPackaging.Packing_Label#")>
			<cfmail to="#qry_getPackaging.email#" from="no-reply@hme.com" bcc="atnelson@hme.com" subject="UPS Electronic Return Label" type="html">
			<!---<cfmail to="atnelson@hme.com" from="no-reply@hme.com" subject="UPS Electronic Return Label" type="html">--->
				<table border="0" cellpadding="0" cellspacing="0" width="600">
					<tr>
						<td height="210" align="left" valign="top">
						<B class="large_text">UPS Electronic Return Label: View/Print Label</B>&nbsp;<br>
						<ol class="small_text">
							<li><b>Ensure that there are no other tracking labels attached to your package.</b><br><br></li>
							<li><b>Tear this sheet of paper at the dotted line.</b> Place the top half of this sheet of paper inside of the shipping box. Place the bottom half of this sheet of paper containing the UPS label in a UPS Shipping Pouch. If you do not have a pouch, affix the label using clear plastic shipping tape over the entire label.  Take care not to cover any seams or closures.<br><br></li>
							<li><b>Collection and Drop-off<br></b>
								<ul>
									<li>Take this package to any location of The UPS Store &reg;, UPS Drop Box, UPS Customer Center, UPS Alliances (Office Depot&reg; or Staples&reg;) or Authorized Shipping Outlet near you. Items sent via UPS ReturnsSM Services(including via UPS Ground) are accepted at Drop Boxes, to find your closest UPS location visit  <a href="http://www.ups.com/content/us/en/index.jsx" target="_blank">www.ups.com/content/us/en/index.jsx</a> and select Drop Off. Click to locate Dropoff location: <a href="http://www.ups.com/dropoff?autosubmit=1&loc=en_US&appid=XOLT&country=US&Postal=<cfoutput>#qry_getPackaging.Zip_Code#</cfoutput>&trans_mode=002" target="_blank">Drop Off Locator</a></li>
									<li>Daily Collection customers: Have your shipment(s) ready for the driver as usual.</li>
								</ul>
							</li>
						</ol>
						</td>
					</tr>
				</table>
				<div class="cust_addr" style="height:180px;font-size:28px;padding-left:20px;margin-bottom:20px;">
					<cfoutput>
						#qry_getPackaging.contact_name#<br />
						#qry_getPackaging.store_name# : #qry_getPackaging.store_number#<br />
						#qry_getPackaging.address_1#<br />
						<cfif qry_getPackaging.address_2 NEQ ''>
							#qry_getPackaging.address_2#<br />
						</cfif>
						#qry_getPackaging.city#, #qry_getPackaging.state# #qry_getPackaging.zip_code#<br />
					</cfoutput>
				</div>
				<table border="0" cellpadding="0" cellspacing="0" width="600">
					<tr>
						<td class="small_text" align="left" valign="top">&nbsp;&nbsp;&nbsp;<a name="foldHere">TEAR HERE</a></td>
					</tr>
					<tr>
						<td align="left" valign="top"><hr style="border-top: dotted 1px;"></td>
					</tr>
				</table>
				<table>
					<tr>
						<td height="10">&nbsp;</td>
					</tr>
				</table>
				<table border="0" cellpadding="0" cellspacing="0" height="392" width="651">
					<tr>
						<td align="left" valign="top" height="392" width="651"><cfoutput><img src="https://#APPLICATION.rootURL#/images/ups/#qry_getPackaging.Packing_Label#" /></cfoutput></td>
					</tr>
				</table>
			</cfmail>
		</cfif>
	</cfif>
</cfsilent>
<div class="ship_repair">
<table width="804" height="16" border="0" align="center" cellpadding="0" cellspacing="0">
<tr align="right" valign="top">
<td width="335"><img src="/images/ceshipping_instructions.png" width="290" height="490"></td>
<td width="469" align="left">
<table>
<tr><td>Thank you. Your label has been sent.</td></tr>
</table>
</td>
</tr>
</table>
</div>