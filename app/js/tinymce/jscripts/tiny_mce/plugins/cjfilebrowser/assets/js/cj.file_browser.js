﻿/*globals window,document,alert,jQuery,escape,tinyMCE,tinyMCEPopup,dateFormat*/
/* ***********************************************************************************
 
	CJ File Browser Javascript Engine
	
	Copyright (c) 2007-2010, Doug Jones. All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions
	are met:
	
	a) Redistributions of source code must retain the above copyright
	notice, this list of conditions, the following disclaimer and any
	website links included within this document.
	
	b) Redistributions in binary form must reproduce the above copyright
	notice, this list of conditions, the following disclaimer and any
	website links included within this document in the documentation 
	and/or other materials provided with the distribution. 
	
	c) Neither the name of the Creative Juices, Bo. Co. nor the names of its
	contributors may be used to endorse or promote products derived from
	this software without specific prior written permission.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
	"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
	LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
	A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
	OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
	LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
	THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	For further information, visit the Creative Juices website: www.cjboco.com.
	
	Version History
	
	1.0			(2007-06-26) - Initial release.
	1.0.1		(2007-08-17) - Fixed a small bug in IE and FireFox (Windows)
								that was causing an error that prevented you
								from selecting files.
	1.0.3		(2007-08-22) - Fixed the path delimeter to work on both Windows
								AND Unix. Fixed path information in the FileBrowser.cfm
								file (leftover code). Also fixed the window reference
								error on the callback to TinyMCE.
	1.0.4		(2007-12-11) - Cleaned the code some more. Added the ability to pass
								the width and height back to the tiny_mce dialog box.
								Added a few more comments and tried to make the instructions
								a little easier. I also added an Application.cfm file to
								the cf_ibrowse directory.
	1.0.4		(2008-05-13) - I made a special edition for all of the Pre-ColdFusion 5
								users. I stripped out some of the tags that were introduced
								after CF5. Since I no longer have access to a CF5 server,
								you guys are going to have to let me know if it works or not.
								Also, I merged the _setup.cfm into the Application.cfm file.
								(Something I plan on doing in the next release anyway.)
	2.0			(2009-01-02) - First attempt at a tinyMCE plug-in version.
	3.0			(2010-03-25) - A complete rewrite. Better tinyMCE plugin support. Tried to
								improve the interface. Uses jQuery and jQuery UI.
	3.0.1		(2010-03-29) - Bug Fixes.
	3.1			(2010-04-11) - Split functionality between jQuery and Handler Engine System. This
								will allow for other server technologies besides ColdFusion.
								Major interface overhaul. Added more functionality.
								Had to re-write upload module, in order to do this.
								Added more theming options and overhauled the look.
								Added fix for IE AJAX caching issue
	3.1.1		(2010-04-25) - Fixed dblClick on FILE not passing file object bug.
								Added further typeof testing for "console" is defined.
	3.1.2		(2010-04-26) - Added proper date format check in JS and handler.
	3.1.3		(2010-07-14) - Added cookie support to remember last directory.
								Trying to fix the new CF9 Ajax Debugging bug by
								using "&_cf_nodebug=true" in ajax calls.
									(Thank MeT and WebDuckie!)
								Fixed a filename bug where using "escape" was not
									escaping a "+" sign. (Thank tarekac!)
								Fixed an issue when attempting to use both the
									tinyMCE version along side the stand-alone.
 
 *********************************************************************************** */
/*
 jQuery.isJson - Not sure the author, but thanks!!
 http://osdir.com/ml/jQuery/2009-07/msg00672.html
*/
if (typeof jQuery.isJson !== "function") {
	jQuery.isJson = function (str) {
		if (typeof str === "string") {
			if (jQuery.trim(str) === "") {
				return false;
			} else {
				str = str.replace(/\\./g, '@').replace(/"[^"\\\n\r]*"/g, "");
				return (/^[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]*$/).test(str);
			}
		} else {
			return true;
		}
	};
}

/*
 Authors: Jeff Walden, Andrea Giammarchi, John Resig
 http://ejohn.org/blog/javascript-array-remove/#postcomment
*/
Array.prototype.remove = function(from, to) {
  this.splice(from, (to || from || 1) + (from < 0 ? this.length : 0));
  return this.length;
};

/*
  Cookie plugin
 
  Copyright (c) 2006 Klaus Hartl (stilbuero.de)
  Dual licensed under the MIT and GPL licenses:
  http://www.opensource.org/licenses/mit-license.php
  http://www.gnu.org/licenses/gpl.html
*/
if (typeof jQuery.cookie !== "function") {
	jQuery.cookie = function (name, value, options) {
		if (typeof value != 'undefined') {
			options = options || {};
			if (value === null) {
				value = '';
				options.expires = -1;
			}
			var expires = '';
			if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
				var date;
				if (typeof options.expires == 'number') {
					date = new Date();
					date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
				} else {
					date = options.expires;
				}
				expires = '; expires=' + date.toUTCString();
			}
			var path = options.path ? '; path=' + (options.path) : '';
			var domain = options.domain ? '; domain=' + (options.domain) : '';
			var secure = options.secure ? '; secure' : '';document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
		} else {
			var cookieValue = null;
			if (document.cookie && document.cookie != '') {
				var cookies = document.cookie.split(';');
				for (var i = 0; i < cookies.length; i++) {
					var cookie = jQuery.trim(cookies[i]);
					if (cookie.substring(0, name.length + 1) == (name + '=')) {
						cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
						break;
					}
				}
			}
			return cookieValue;
		}
	};
}

/*
 CJ File Browser Engine 3.1.3
*/
if (typeof cjFileBrowser !== "function") {
	var cjFileBrowser = {
	
		settings: {
			actions: [],
			baseUrl: [],
			fileExts: "",
			maxSize: 0,
			maxWidth: 0,
			maxHeight: 0,
			showImgPreview: true,
			engine: "",
			handler: "",
			timeOut: 0,
			callBack: null
		},
		
		locvars: {
			version: "3.1.3",
			timer: null,
			autoCenter: null,
			currentUrl: 0,
			dirContents: [],
			debug: false,
			nocache: function() {
				return new Date().getTime();
			}
		},
	
		// simple internal function to determine if the RESULT is JSON string, object or invalid
		doEvalJSON: function (r) {
			if (typeof r === "string") {
				return JSON.parse(jQuery.trim(r));
			} else if (typeof r === "object") {
				return r;
			} else {
				return {
					"ERROR": true,
					"ERROR_MSG": "Error parsing JSON object."
				};
			}
		},
	
		// adds "..." in the middle of long filenames
		doShortentFileName: function (str, len) {
			if (typeof str === "string" && str.length > len) {
				var h = parseInt(len / 2, 10);
				return str.substring(0, h) + "..." + str.substring(str.length - h, str.length);
			} else {
				return str;
			}
		},
		
		clearTimer: function() {
			window.clearTimeout(cjFileBrowser.locvars.timer);
			cjFileBrowser.locvars.timer = null;
		},
	
		// displays our dialog boxes
		displayDialog: function (opts) {
			// opts: type, state, label, content, cbOk, cbCancel
			if(typeof opts === "object" && typeof opts.type === "string" && typeof opts.state === "string") {
				if (typeof jQuery("#CJModalDialog").get(0) !== "undefined") {
					if (cjFileBrowser.locvars.autoCenter !== null) {
						// clear any autocenter intervals
						window.clearInterval(cjFileBrowser.locvars.autoCenter);
						cjFileBrowser.locvars.autoCenter = null;
					}
					jQuery("#CJModalDialog").remove();
				} 
				switch(opts.type) {
					case "progress":
						jQuery("body").append(
							'<div id="CJModalDialog">' + 
								'<div class="innerBox">' + 
									'<div class="margins">' +
										'<div class="label"><\/div>' +
										'<div class="indicator" style="background-position: 0px 0px"><\/div>' + 
									'<\/div>' +
								'<\/div>' +
							'<\/div>'
						);
						// setup our animated progress indicator
						jQuery("#CJModalDialog .indicator").get(0).hpos = 0;
						cjFileBrowser.locvars.autoCenter = window.setInterval(function () {
							var bar = jQuery("#CJModalDialog .indicator");
							if (typeof bar.get(0).hpos === "undefined") {
								bar.get(0).hpos = 0;
							} else {
								bar.get(0).hpos = (bar.get(0).hpos < -32) ? 0 : bar.get(0).hpos - 2;
							}
							bar.css({
								backgroundPosition: bar.get(0).hpos + "px 0px"
							});
						}, 50);
						if (typeof opts.label !== "string") {
							opts.label = "Loading data...";
						}
						break;
					case "confirm":
						jQuery("body").append(
							'<div id="CJModalDialog">' +
								'<div class="innerBox">' +
									'<div class="margins">' +
										'<div class="label"><\/div>' +
										'<div class="content"><\/div>' +
										'<div class="buttons">' +
											'<button type="button" name="buttonCANCEL" id="buttonCANCEL" class="input_button">Cancel<\/button>' +
											'<button type="button" name="buttonOK" id="buttonOK" class="input_button">OK<\/button>' +
										'<\/div>' +
									'<\/div>' +
								'<\/div>' +
							'<\/div>'
						);
						jQuery("#CJModalDialog #buttonCANCEL").mouseup(function () {
							cjFileBrowser.displayDialog({
								type: "confirm",
								state: "hide"
							});
						});
						if (typeof opts.label !== "string") {
							opts.label = "Please confirm...";
						}
						break;
					case "alert":
						jQuery("body").append(
							'<div id="CJModalDialog">' +
								'<div class="innerBox">' +
									'<div class="margins">' +
										'<div class="label"><\/div>' +
										'<div class="content"><\/div>' +
										'<div class="buttons">' +
											'<button type="button" name="buttonOK" id="buttonOK" class="input_button">OK<\/button>' +
										'<\/div>' +
									'<\/div>' +
								'<\/div>' +
							'<\/div>'
						);
						jQuery("#CJModalDialog #buttonOK").mouseup(function () {
							cjFileBrowser.displayDialog({
								type: "alert",
								state: "hide"
							});
						});
						if (typeof opts.label !== "string") {
							opts.label = "The system encountered an alert...";
						}
						break;
					default:
						break;
				}
				if (opts.state === "show") {
					// display the background overlay
					jQuery("#CJModalBGround").css({
						display: "block",
						opacity: 0.65
					});
					jQuery("#CJModalDialog .label").html(opts.label);
					if(typeof opts.content === "string") {
						if (opts.type === "alert") {
							jQuery("#CJModalDialog .content").html('<div class="clearfix"><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"><\/span>' + opts.content + '<\/div><\/div>');
						} else {
							jQuery("#CJModalDialog .content").html(opts.content);
						}
					} else if (typeof opts.content === "object") {
						var err_idx, err_list = "";
						for (err_idx in opts.content) {
							if (typeof err_idx === "string" && !(opts.content[err_idx] instanceof Function)) {
								err_list += "<p>" + opts.content[err_idx] + "<\/p>";
							}
						}
						if (err_list !== "" && opts.type === "alert") {
							jQuery("#CJModalDialog .content").html('<div class="clearfix"><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"><\/span>' + err_list + '<\/div><\/div>');
						} else if (err_list !== "") {
							jQuery("#CJModalDialog .content").html(err_list);
						} else {
							jQuery("#CJModalDialog .content").html('<div class="clearfix"><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"><\/span>An unknown error occurred. (Error message could not be determined)<\/div><\/div>');
						}
					}
					if (typeof opts.cbOk === "function") {
						jQuery("#CJModalDialog #buttonOK").mouseup(opts.cbOk);
					}
					if (typeof opts.cbCancel === "function") {
						jQuery("#CJModalDialog #buttonCANCEL").mouseup(opts.cbCancel);
					}
					jQuery("#CJModalDialog").css({
						top: (parseInt((jQuery(window).height() / 2), 10) - parseInt(jQuery("#CJModalDialog").height() / 2, 10)) + "px",
						left: (parseInt((jQuery(window).width() / 2), 10) - parseInt(jQuery("#CJModalDialog").width() / 2, 10)) + "px"
					}).fadeIn();
				} else if (opts.state === "hide") {
					if (cjFileBrowser.locvars.autoCenter !== null) {
						// clear any autocenter intervals
						window.clearInterval(cjFileBrowser.locvars.autoCenter);
						cjFileBrowser.locvars.autoCenter = null;
					}
					// remove any click action that may have been attached.
					jQuery("#CJModalDialog #buttonOK").unbind();
					jQuery("#CJModalDialog #buttonCANCEL").unbind();
					// remove the dialog
					jQuery("#CJModalDialog").remove();
					jQuery("#CJModalBGround").css({
						display: "none"
					});
				}
			}
		},
		
		// disable all options (used before performing various options)
		resetAllOptions: function() {
			jQuery("#directoryOut,#directoryIn,#directories,#newfolder,#fileUpload,#fileDelete,#fileSelect").attr("disabled", true);
			jQuery("#directories option").remove();
			cjFileBrowser.updateDisplayStats("",0,0);
		},
		
		// converts bytes to a nicer display...
		displayFileSize: function (filesize) {
			if (filesize >= 1073741824) {
				filesize = parseFloat(filesize / 1073741824).toFixed(2) + ' Gb';
			} else {
				if (filesize >= 1048576) {
					filesize = parseFloat(filesize / 1048576).toFixed(2) + ' Mb';
				} else {
					if (filesize >= 1024) {
						filesize = parseFloat(filesize / 1024).toFixed(2) + ' Kb';
					} else {
						filesize = parseInt(filesize, 10) + ' bytes';
					}
				}
			}
			return filesize;
		},
	
		// updates the main browser window's stats
		updateDisplayStats: function (path, cnt, size) {
			var stats = "";
			stats += '<div class="margins">';
			stats += '<div class="item">Path: <strong>' + path + '<\/strong><\/div>';
			stats += '<div class="item">Size: <strong>' + (typeof size === 'number' ? cjFileBrowser.displayFileSize(parseInt(size, 10)) : 'NaN') + '<\/strong><\/div>';
			stats += '<div class="item">Files: <strong>' + (typeof cnt === 'number' ? cnt : 'NaN') + '<\/strong><\/div>';
			stats += '<\/div>';
			jQuery("#footer .stats").html(stats);
		},
		
		// populates the directory SELECT menu
		updateDirOptions: function() {
			jQuery("#directories option").remove();
			jQuery("#directories optgroup").remove();
			jQuery("#directories").append(
				'<optgroup label="Current Directory Path" title="Current Directory Path">'
			);
			jQuery(cjFileBrowser.settings.baseUrl).each( function (intIndex, objValue) {
				jQuery("#directories").append(
					'<option value="' + objValue + '"' + (intIndex === cjFileBrowser.locvars.currentUrl ? ' selected="selected"' : '') + '>' + objValue + '<\/option>'
				).attr("disabled", false);
			});
			// need to see if we have more than one directory and change the directory NAVIGATION accordingly
			if (cjFileBrowser.settings.baseUrl.length > 1 && cjFileBrowser.locvars.currentUrl > 0) {
				jQuery("#directoryOut").attr("disabled", false);
			} else {
				jQuery("#directoryOut").attr("disabled", true);
			}
		},
		
		doLoadDirectoryImage: function(elem, idx, currentDir) {
			if (typeof elem === "object" && jQuery(elem).length > 0 && currentDir === cjFileBrowser.locvars.currentUrl) {
				var curElem = (typeof idx === "number" && idx > -1 && idx < elem.length) ? elem[idx] : null,
					fname = escape(jQuery(curElem).find(".diritem").attr("rel"));
				// fix for files with "+" in the name
				fname = fname.replace("+","%2B");
				jQuery.getJSON("assets/engines/" + cjFileBrowser.settings.engine + "/" + cjFileBrowser.settings.handler + "?method=getImageThumb&returnFormat=JSON&_cf_nodebug=true&timeOut=" + parseInt(cjFileBrowser.settings.timeOut,10) + "&baseUrl=" + escape(cjFileBrowser.settings.baseUrl[cjFileBrowser.locvars.currentUrl]) + "&elemID=" + jQuery(curElem).attr("id") + "&fileName=" + fname, { "noCache": cjFileBrowser.locvars.nocache }, function (result, status) {
					if (status === "success") {
						var r = cjFileBrowser.doEvalJSON(result);
						if (typeof r === "object" && typeof r.ERROR === "boolean" && r.ERROR === false) {
							jQuery("#" + r.ELEMID + " .icon").css("background","#fff");
							jQuery("#" + r.ELEMID + " .icon .imgHolder").html(r.IMGSTR);
							jQuery("#" + r.ELEMID + " .icon .imgHolder img").load(function () {
								jQuery("#" + r.ELEMID + " .icon .imgHolder").css("display","block");
							});
							if (idx < elem.length - 1) {
								cjFileBrowser.doLoadDirectoryImage(elem, idx + 1, currentDir);
							}
						} else if (typeof r !== "object" || (typeof r.ERROR === "boolean" && r.ERROR === true)) {
							cjFileBrowser.displayDialog({
								type: "alert",
								state: "show",
								label: "Oops! There was a problem",
								content: r.ERROR_MSG
							});
						} else {
							cjFileBrowser.displayDialog({
								type: "alert",
								state: "show",
								label: "Oops! There was a problem",
								content: "Problems communicating with the CFC. Unexpected results returned when loading thumbnails."
							});
						}
					}
				});
			}
		},
		
		// grabs an image preview for image files
		getDirectoryImages: function() {
			// show image previews?
			if (cjFileBrowser.settings.showImgPreview) {
				cjFileBrowser.doLoadDirectoryImage(jQuery("#browser ul li.GIF, #browser ul li.JPG, #browser ul li.PNG"), 0, cjFileBrowser.locvars.currentUrl);
			}
		},
	
		// displays the directory listing
		displayDirListing: function () {
			var dir = cjFileBrowser.locvars.dirContents,
				total_size = 0, infoStr;
			
			// show out progress
			cjFileBrowser.displayDialog({
				type: "progress",
				state: "show",
				label: "Displaying directory contents..."
			});
			
			if (dir.length > 0) {
	
				// init the sidebar list element
				jQuery("#sidebar").append('<ul><\/ul>');
				jQuery("#browser").append('<ul><\/ul>');
	
				// cycle through the json array and create our directory list items from each entry
				jQuery.each(dir, function (intIndex, objValue) {
					var lis, lib, info;
	
					// create our SIDEBAR list items
					lis = jQuery("<li>").attr({
						"id": "sidebar_ID" + intIndex,
						"rel": objValue.TYPE === "FILE" ? "File" : "Directory"
					}).addClass(objValue.EXTENSION).html('<span rel="' + objValue.NAME + '">' + cjFileBrowser.doShortentFileName(objValue.NAME, 20) + '<\/span>');
	
					// create our BROWSER list items
					lib = jQuery("<li>").attr({
						"id": "browser_ID" + intIndex,
						"rel": objValue.TYPE === "FILE" ? "File" : "Directory"
					}).addClass(objValue.EXTENSION);
					
					// add a special class to the LI if it's a viewable image 
					if (cjFileBrowser.settings.showImgPreview && (objValue.EXTENSION === "GIF" || objValue.EXTENSION === "JPG" || objValue.EXTENSION === "PNG")) {
						lib.addClass("viewable");
					}
	
					// create our info box
					info = jQuery("<div>").addClass("diritem").addClass(objValue.EXTENSION).attr("rel",objValue.NAME).append(
						'<div class="icon" rel="' + objValue.NAME + '">' + (typeof objValue.WIDTH === "number" && typeof objValue.HEIGHT === "number" ? '<div class="imgHolder"><\/div><div class="mask"><\/div>' : '') + '<\/div>' + 
						'<div class="name">' + cjFileBrowser.doShortentFileName(objValue.NAME, 20) + '<\/div>' + 
						'<div class="namefull">' + objValue.NAME + '<\/div>' + 
						(objValue.TYPE === "FILE" ? '<div class="size">' + cjFileBrowser.displayFileSize(parseInt(objValue.SIZE, 10)) + '<\/div>' : '') + 
						(typeof objValue.WIDTH === "number" && typeof objValue.HEIGHT === "number" ? '<div class="dimensions"><span class="width">' + objValue.WIDTH + '<\/span> x <span class="height">' + objValue.HEIGHT + '<\/span> pixels<\/div>' : '') + 
						(objValue.DATELASTMODIFIED.length > 0 ? '<div class="modified">' + dateFormat(objValue.DATELASTMODIFIED, 'mmm dS, yyyy, h:MM:ss TT') + '<\/div>' : '') + 
						'<div class="mimeType">' + objValue.MIME + '<\/div>'
					);
	
					// add the file size to the directories total
					total_size += parseInt(objValue.SIZE, 10);
	
					// append out info string to the list items
					infoStr = objValue.NAME + " \n" + (objValue.TYPE === "FILE" ? cjFileBrowser.displayFileSize(parseInt(objValue.SIZE, 10)) + " \n" : "") + (typeof objValue.WIDTH === "number" && typeof objValue.HEIGHT === "number" ? objValue.WIDTH + " x " + objValue.HEIGHT + " pixels \n" : "") + dateFormat(objValue.DATELASTMODIFIED, 'mmm dS, yyyy, h:MM:ss TT');
					jQuery(lis).attr("title",infoStr);
					jQuery(lib).attr("title",infoStr).append(info);
	
					// add the direct list to the BROWSER and SIDEBAR windows
					jQuery("#sidebar ul").append(lis);
					jQuery("#browser ul").append(lib);
				});
	
				// set up our hover and click actions for the SIDEBAR window
				jQuery("#sidebar ul li").hover(function () {
					jQuery(this).addClass("hovering");
				}, function () {
					jQuery(this).removeClass("hovering");
				}).click(function (e) {
					var twin = jQuery("#browser #" + (jQuery(this).attr("id")).replace("sidebar_ID", "browser_ID"));
					if (jQuery(this).attr("rel") === "File") {
						jQuery("#directoryIn").attr("disabled", true).unbind();
						if (jQuery(this).hasClass("selected")) {
							jQuery(this).removeClass("selected");
							jQuery(twin).removeClass("selected");
							jQuery("#fileSelect,#fileDelete").attr("disabled", true);
						} else {
							jQuery("#sidebar ul li,#browser ul li").removeClass("selected");
							jQuery(this).addClass("selected");
							jQuery(twin).addClass("selected");
							jQuery("#fileSelect").attr("disabled", false);
							if (jQuery.inArray("fileDelete",cjFileBrowser.settings.actions) > -1) {
								jQuery("#fileDelete").attr("disabled", false);
							} else {
								jQuery("#fileDelete").attr("disabled", true);
							}
							jQuery("#browser").animate({
								scrollTop: jQuery("#browser").scrollTop() + twin.offset().top - twin.height()
							}, "fast");
						}
					} else if (jQuery(this).attr("rel") === "Directory") {
						if (jQuery(this).hasClass("selected")) {
							jQuery(this).removeClass("selected");
							jQuery(twin).removeClass("selected");
							jQuery("#fileSelect").attr("disabled", true);
							jQuery("#directoryIn").attr("disabled", true).unbind();
						} else {
							jQuery("#sidebar ul li,#browser ul li").removeClass("selected");
							jQuery(this).addClass("selected");
							jQuery(twin).addClass("selected");
							jQuery("#fileSelect").attr("disabled", true);
							if (jQuery.inArray("deleteDirectory",cjFileBrowser.settings.actions) > -1) {
								jQuery("#fileDelete").attr("disabled", false);
							} else {
								jQuery("#fileDelete").attr("disabled", true);
							}
							jQuery("#browser").animate({
								scrollTop: jQuery("#browser").scrollTop() + twin.offset().top - twin.height()
							}, "fast");
							jQuery("#directoryIn").attr("disabled", false).click(function (e) {
								if (jQuery(this).attr("disabled") !== "disabled" && jQuery(this).attr("disabled") !== true) {
									var path = jQuery("#sidebar ul li.selected").find("span").attr("rel");
									cjFileBrowser.settings.baseUrl.push(cjFileBrowser.settings.baseUrl[cjFileBrowser.locvars.currentUrl] + path + "/");
									cjFileBrowser.locvars.currentUrl += 1;
									cjFileBrowser.updateDirOptions();
									cjFileBrowser.doDirListing();
								}
							});
						}
					}
					e.stopImmediatePropagation();
				}).dblclick(function (e) {
					// double-click action for directories, might allow this for files eventually.
					var path = jQuery(this).find("span").attr("rel");
					if (jQuery(this).attr("rel") === "File") {
						var fileObj = cjFileBrowser.locvars.dirContents[parseInt((jQuery(this).attr("ID")).replace("sidebar_ID",""),10)];
						cjFileBrowser.doFileSelect(fileObj);
					} else if (jQuery(this).attr("rel") === "Directory") {
						cjFileBrowser.settings.baseUrl.push(cjFileBrowser.settings.baseUrl[cjFileBrowser.locvars.currentUrl] + path + "/");
						cjFileBrowser.locvars.currentUrl += 1;
						cjFileBrowser.updateDirOptions();
						cjFileBrowser.doDirListing();
					}
					e.stopImmediatePropagation();
				});
	
				// set up our hover and click actions for the BROWSER window
				jQuery("#browser ul li").hover(function () {
					jQuery(this).addClass("hovering");
				}, function () {
					jQuery(this).removeClass("hovering");
				}).click(function (e) {
					var twin = jQuery("#sidebar #" + (jQuery(this).attr("id")).replace("browser_ID", "sidebar_ID"));
					if (jQuery(this).attr("rel") === "File") {
						jQuery("#directoryIn").attr("disabled", true).unbind();
						if (jQuery(this).hasClass("selected")) {
							jQuery(this).removeClass("selected");
							jQuery(twin).removeClass("selected");
							jQuery("#fileSelect,#fileDelete").attr("disabled", true);
						} else {
							jQuery("#sidebar ul li,#browser ul li").removeClass("selected");
							jQuery(this).addClass("selected");
							jQuery(twin).addClass("selected");
							jQuery("#fileSelect").attr("disabled", false);
							if (jQuery.inArray("fileDelete",cjFileBrowser.settings.actions) > -1) {
								jQuery("#fileDelete").attr("disabled", false);
							} else {
								jQuery("#fileDelete").attr("disabled", true);
							}
							jQuery("#sidebar").animate({
								scrollTop: jQuery("#sidebar").scrollTop() + twin.offset().top - 43
							}, "fast"); // #sidebar css position top = 43px
						}
					} else if (jQuery(this).attr("rel") === "Directory") {
						if (jQuery(this).hasClass("selected")) {
							jQuery(this).removeClass("selected");
							jQuery(twin).removeClass("selected");
							jQuery("#fileSelect").attr("disabled", true);
							jQuery("#directoryIn").attr("disabled", true).unbind();
						} else {
							jQuery("#sidebar ul li,#browser ul li").removeClass("selected");
							jQuery(this).addClass("selected");
							jQuery(twin).addClass("selected");
							jQuery("#fileSelect").attr("disabled", true);
							if (jQuery.inArray("deleteDirectory",cjFileBrowser.settings.actions) > -1) {
								jQuery("#fileDelete").attr("disabled", false);
							} else {
								jQuery("#fileDelete").attr("disabled", true);
							}
							jQuery("#sidebar").animate({
								scrollTop: jQuery("#sidebar").scrollTop() + twin.offset().top - 43
							}, "fast"); // not sure why 43 is working here?!?
							jQuery("#directoryIn").attr("disabled", false).click(function (e) {
								if (jQuery(this).attr("disabled") !== "disabled" && jQuery(this).attr("disabled") !== true) {
									var path = jQuery("#browser ul li.selected").find(".diritem").attr("rel");
									cjFileBrowser.settings.baseUrl.push(cjFileBrowser.settings.baseUrl[cjFileBrowser.locvars.currentUrl] + path + "/");
									cjFileBrowser.locvars.currentUrl += 1;
									cjFileBrowser.updateDirOptions();
									cjFileBrowser.doDirListing();
								}
							});
						}
					}
					e.stopImmediatePropagation();
				}).dblclick(function (e) {
					// double-click action for directories, might allow this for files eventually.
					var path = jQuery(this).find(".diritem").attr("rel");
					if (jQuery(this).attr("rel") === "File") {
						var fileObj = cjFileBrowser.locvars.dirContents[parseInt((jQuery(this).attr("ID")).replace("browser_ID",""),10)];
						cjFileBrowser.doFileSelect(fileObj);
					} else if (jQuery(this).attr("rel") === "Directory") {
						cjFileBrowser.settings.baseUrl.push(cjFileBrowser.settings.baseUrl[cjFileBrowser.locvars.currentUrl] + path + "/");
						cjFileBrowser.locvars.currentUrl += 1;
						cjFileBrowser.updateDirOptions();
						cjFileBrowser.doDirListing();
					}
					e.stopImmediatePropagation();
				});
				
				cjFileBrowser.getDirectoryImages();
	
			}
			
			// hide our progress bar
			cjFileBrowser.displayDialog({
				type: "progress",
				state: "hide"
			});
	
			// update display stats info
			cjFileBrowser.updateDisplayStats(cjFileBrowser.settings.baseUrl[cjFileBrowser.locvars.currentUrl], dir.length, total_size);
		},
	
		// grabs the directory listing
		doDirListing: function () {
	
			// do the progress bar
			cjFileBrowser.displayDialog({
				type: "progress",
				state: "show",
				label: "Reading directory contents..."
			});
	
			// erase any old data
			jQuery("#sidebar ul").remove();
			jQuery("#browser ul").remove();
			cjFileBrowser.resetAllOptions();
	
			// grab our directory listing
			jQuery.getJSON("assets/engines/" + cjFileBrowser.settings.engine + "/" + cjFileBrowser.settings.handler + "?method=getDirectoryList&returnFormat=JSON&_cf_nodebug=true&timeOut=" + parseInt(cjFileBrowser.settings.timeOut,10) + "&baseUrl=" + escape(cjFileBrowser.settings.baseUrl[cjFileBrowser.locvars.currentUrl]) + "&fileExts=" + escape(cjFileBrowser.settings.fileExts), { "noCache": cjFileBrowser.locvars.nocache }, function (result, status) {
				var r = cjFileBrowser.doEvalJSON(result);
				if (typeof r === "object" && typeof r.ERROR === "boolean" && r.ERROR === false && typeof r.DIRLISTING !== "undefined") {
	
					// everything good.
					if (r.DIRLISTING.length >= 0) {
						cjFileBrowser.locvars.dirContents = r.DIRLISTING;
						cjFileBrowser.displayDialog({
							type: "progress",
							state: "hide"
						});
						if (typeof cjFileBrowser.locvars.dirContents === "object" && cjFileBrowser.locvars.dirContents.length > 0) {
							jQuery.cookie('cjFileBrowser_dir', cjFileBrowser.settings.baseUrl[cjFileBrowser.locvars.currentUrl], { 
								expires: 1, 
								path: '/'
							});
							cjFileBrowser.displayDirListing();
						}
					}
	
				} else if (typeof r !== "object" || (typeof r.ERROR === "boolean" && r.ERROR === true)) {
	
					// must have been a problem in the CFC
					cjFileBrowser.locvars.dirContents = [];
					cjFileBrowser.displayDialog({
						type: "alert",
						state: "show",
						label: "Oops! There was a problem",
						content: r.ERROR_MSG
					});
	
				} else {
	
					// problem with the processing, server might be down.
					cjFileBrowser.locvars.dirContents = [];
					cjFileBrowser.displayDialog({
						type: "alert",
						state: "show",
						label: "Oops! There was a problem",
						content: "Problems communicating with the CFC. Unexpected results returned for doDirListing."
					});
	
				}
			});
			
			// enable and re-initialize some options and buttons
			cjFileBrowser.updateDirOptions();
			jQuery("#newfolder, #fileUpload").attr({
				disabled: false
			});
		},
		
		// pass the file to tinyMCE or do stand-alone selection
		doFileSelect: function(fileObj) {
			
			/*
			The fileObj is a JSON object of the file selection.
			{
				"ATTRIBUTES" : "",
				"DATELASTMODIFIED" : "April, 16 2010 21:15:49",
				"DIRECTORY" : "/myFolder/",
				"EXTENSION" : "JPG",
				"FULLPATH" : "/Users/myname/myserver/myFolder/myimage.jpg",
				"HEIGHT" : 435,
				"MIME" : "image/jpeg",
				"NAME" : "myimage.jpg",
				"SIZE" : 10785,
				"TYPE" : "FILE",
				"WIDTH" : 315
			}
			*/
			
			if (
				!(typeof fileObj.ATTRIBUTES === "string" ||
				typeof fileObj.DATELASTMODIFIED === "string" ||
				typeof fileObj.DIRECTORY === "string" ||
				typeof fileObj.EXTENSION === "string" ||
				typeof fileObj.FULLPATH === "string" ||
				typeof fileObj.MIME === "string" ||
				typeof fileObj.HEIGHT === "string" || typeof fileObj.HEIGHT === "number" ||
				typeof fileObj.NAME !== "string" ||
				typeof fileObj.SIZE !== "number" ||
				typeof fileObj.TYPE !== "string" ||
				typeof fileObj.WIDTH === "string" || typeof fileObj.WIDTH === "number")
			) {
			
				// debug the file selection settings (if debug is turned on and console is available)
				if (cjFileBrowser.locvars.debug && typeof console !== "undefined" && typeof console.debug !== "undefined") {
					console.debug("ATTRIBUTES: " + typeof fileObj.ATTRIBUTES + " value: " + fileObj.ATTRIBUTES);
					console.debug("DATELASTMODIFIED: " + typeof fileObj.DATELASTMODIFIED + " value: " + fileObj.DATELASTMODIFIED);
					console.debug("DIRECTORY: " + typeof fileObj.DIRECTORY + " value: " + fileObj.DIRECTORY);
					console.debug("EXTENSION: " + typeof fileObj.EXTENSION + " value: " + fileObj.EXTENSION);
					console.debug("FULLPATH: " + typeof fileObj.FULLPATH + " value: " + fileObj.FULLPATH);
					console.debug("HEIGHT: " + typeof fileObj.HEIGHT + " value: " + fileObj.HEIGHT);
					console.debug("MIME: " + typeof fileObj.MIME + " value: " + fileObj.MIME);
					console.debug("NAME: " + typeof fileObj.NAME + " value: " + fileObj.NAME);
					console.debug("SIZE: " + typeof fileObj.SIZE + " value: " + fileObj.SIZE);
					console.debug("TYPE: " + typeof fileObj.TYPE + " value: " + fileObj.TYPE);
					console.debug("WIDTH: " + typeof fileObj.WIDTH + " value: " + fileObj.WIDTH);
				}
				
				cjFileBrowser.displayDialog({
					type: "alert",
					state: "show",
					label: "Oops! There was a problem",
					content: "There was a problem reading file selection information."
				});
			
			} else if (typeof tinyMCE !== "undefined" && typeof tinyMCEPopup !== "undefined") {
				var win = tinyMCEPopup.getWindowArg("window"),
					relPath = fileObj.DIRECTORY + fileObj.NAME;
					
				// insert information into the tinyMCE window
				win.document.getElementById(tinyMCEPopup.getWindowArg("input")).value = relPath;
				
				// for image browsers: update image dimensions
				if (win.ImageDialog) {
					if (win.ImageDialog.getImageData) {
						win.ImageDialog.getImageData();
					}
					if (win.ImageDialog.showPreviewImage) {
						win.ImageDialog.showPreviewImage(relPath);
					}
				}
	
				// close popup window
				tinyMCEPopup.close();
	
			} else {
				
				// We are a standalone mode. What to do, is up to you...
				if (cjFileBrowser.settings.callBack !== null) {
				
					// user passed a callback function
					cjFileBrowser.settings.callBack(fileObj);
				
				} else {
				
					// just do a simple alert
					cjFileBrowser.displayDialog({
						type: "alert",
						state: "show",
						label: "Standalone Mode Message",
						content: "You selected" + (fileObj.DIRECTORY + fileObj.NAME) + "<br \/>" + fileObj.FULLPATH
					});
				}
	
			}
		},
		
		// evaluates the from post results
		doPostEval: function (result) {
			var r = cjFileBrowser.doEvalJSON(result);
			if (jQuery.isJson(r)) {
				if (typeof r === "object" && typeof r.ERROR === "boolean" && r.ERROR === false) {
					cjFileBrowser.doDirListing();
				} else {
					cjFileBrowser.displayDialog({
						type: "alert",
						state: "show",
						label: "Oops! There was a problem",
						content: r.ERROR_MSG
					});
				}
			}
		},
		
		setup: function() {
			
			// create a modal box background (make sure click throughs don't happen)
				jQuery("body").append(
					'<div id="CJModalBGround"><\/div>'
				).click(function(e) {
					e.stopImmediatePropagation();
				});
				
				// start our progress bar and do init stuff 
				cjFileBrowser.displayDialog({
					type: "progress",
					state: "show",
					label: "Initializing..."
				});
				
				// do some basic button setup (header buttons)
				jQuery("#header button").hover(function() {
					jQuery(this).css({
						"background-position": "0px -32px"
					});
				}, function() {
					jQuery(this).css({
						"background-position": "0px 0px"
					});
				}).mousedown(function() {
					jQuery(this).css({
						"background-position": "0px -96px"
					});
				}).mouseup(function() {
					jQuery(this).css({
						"background-position": "0px 0px"
					});
				});
				
				// set up deselect if the user clicks the browser window
				jQuery("#sidebar,#browser").click(function(e) {
					jQuery("#sidebar li").removeClass("selected");
					jQuery("#browser li").removeClass("selected");
					jQuery("#fileSelect,#fileDelete").attr("disabled", true);
					jQuery("#directoryIn").attr("disabled", true).unbind();
					e.stopImmediatePropagation();
				});
				
				// determine which buttons to show based on user passed actions
				if (jQuery.inArray("navigateDirectory",cjFileBrowser.settings.actions) === -1) {
					jQuery("#directoryOptions").remove();
				} else {
					// set up directory NAVIGATION buttons
					jQuery("#directoryOut").mouseup(function () {
						if (jQuery(this).attr("disabled") !== "disabled" && jQuery(this).attr("disabled") !== true) {
							cjFileBrowser.locvars.currentUrl = cjFileBrowser.locvars.currentUrl - 1 >= 0 ? cjFileBrowser.locvars.currentUrl - 1 : 0;
							cjFileBrowser.settings.baseUrl.remove(cjFileBrowser.locvars.currentUrl + 1, cjFileBrowser.settings.baseUrl.length - cjFileBrowser.locvars.currentUrl + 1);
							cjFileBrowser.doDirListing();
						}
					});
					
					// set up the directories SELECT menu
					jQuery("#directories").change(function () {
						if (jQuery(this).get(0).selectedIndex < cjFileBrowser.locvars.currentUrl) {
							cjFileBrowser.settings.baseUrl.remove(jQuery(this).get(0).selectedIndex + 1, cjFileBrowser.settings.baseUrl.length - jQuery(this).get(0).selectedIndex + 1);
							cjFileBrowser.locvars.currentUrl = jQuery(this).get(0).selectedIndex;
							cjFileBrowser.doDirListing();
						}
					});
				}
				
				if (jQuery.inArray("createDirectory",cjFileBrowser.settings.actions) === -1) {
					jQuery("#fileOptions #newfolder").remove();
				} else {
					// handle the NEW FOLDER action
					jQuery("#newfolder").mouseup(function () {
						jQuery(this).css({
							"background-position": "0px 0px"
						}).attr("disabled", true);
						cjFileBrowser.displayDialog({
							type: "confirm",
							state: "show",
							label: "Create a new directory...",
							content: (
								'<form name="CJNewDirectoryForm" id="CJNewDirectoryForm" action="javascript:void(0);" method="post">' + 
									'<div class="fields">' +
										'<input type="text" name="directoryName" id="directoryName" class="input_text" \/>' +
									'<\/div>' +
								'<\/form>'
							),
							cbOk: function(str) {
								var dirName = jQuery("#CJNewDirectoryForm #directoryName").val();
								if (typeof dirName === "string" && dirName.length > 0) {
									cjFileBrowser.displayDialog({
										type: "progress",
										state: "show",
										label: "Creating new directory..."
									});
									jQuery("#fileSelect").attr("disabled", true);
									jQuery("#fileDelete").attr("disabled", true);
									// set a nice timer to ensure that the CFC does not timeout
									cjFileBrowser.locvars.timer = window.setTimeout(function(){
										cjFileBrowser.displayDialog({
											type: "alert",
											state: "show",
											label: "Oops! There was a problem",
											content: "The system timed out attempting to create a new directory."
										});
										jQuery("#newfolder").attr("disabled", false);
									}, parseInt(cjFileBrowser.settings.timeOut,10) * 1000);
									// call out handler
									jQuery.getJSON("assets/engines/" + cjFileBrowser.settings.engine + "/" + cjFileBrowser.settings.handler + "?method=doCreateNewDirectory&returnFormat=JSON&_cf_nodebug=true&timeOut=" + parseInt(cjFileBrowser.settings.timeOut,10) + "&baseUrl=" + escape(cjFileBrowser.settings.baseUrl[cjFileBrowser.locvars.currentUrl]) + "&dirName=" + escape(dirName), { "noCache": cjFileBrowser.locvars.nocache }, function (result, status) {
										var r = cjFileBrowser.doEvalJSON(result);
										if (typeof r === "object" && typeof r.ERROR === "boolean" && r.ERROR === false) {
				
											// everything good.
											cjFileBrowser.clearTimer();
											jQuery("#newfolder").attr("disabled", false);
											cjFileBrowser.doDirListing();
				
										} else if (typeof r !== "object" || (typeof r.ERROR === "boolean" && r.ERROR === true && typeof r.ERROR_MSG === "string" && r.ERROR_MSG.length > 0)) {
				
											// something happened in the CFC
											cjFileBrowser.clearTimer();
											cjFileBrowser.displayDialog({
												type: "alert",
												state: "show",
												label: "Oops! There was a problem",
												content: r.ERROR_MSG
											});
											jQuery("#newfolder").attr("disabled", false);
				
										} else {
				
											// problem with the processing, server might be down.
											cjFileBrowser.clearTimer();
											cjFileBrowser.displayDialog({
												type: "alert",
												state: "show",
												label: "Oops! There was a problem",
												content: "Problems communicating with the CFC. Unexpected results returned for newfolder (click)."
											});
											jQuery("#newfolder").attr("disabled", false);
										}
									});
								}
							},
							cbCancel: function() {
								jQuery("#newfolder").attr("disabled", false);
							}
						});
					});
				}
				
				if (jQuery.inArray("deleteDirectory",cjFileBrowser.settings.actions) === -1 && jQuery.inArray("fileDelete",cjFileBrowser.settings.actions) === -1) {
					jQuery("#fileOptions #fileDelete").remove();
				} else {
					// setup our DELETE action
					jQuery("#fileDelete").mouseup(function () {
						var type = (jQuery("#browser ul li.selected").attr("rel")).toLowerCase();
						if ((type === "directory" && jQuery.inArray("deleteDirectory",cjFileBrowser.settings.actions) > -1) || (type === "file" && jQuery.inArray("fileDelete",cjFileBrowser.settings.actions) > -1)) {
							jQuery(this).css({
								"background-position": "0px 0px"
							}).attr("disabled", true);
							cjFileBrowser.displayDialog({
								type: "confirm",
								state: "show",
								label: "Delete file...",
								content: "This " + (jQuery("#browser ul li.selected").attr("rel")).toLowerCase() + " will be deleted and cannot be recovered. Are you sure you want to perform this action?",
								cbOk: function () {
									var curFile = jQuery("#browser ul li.selected .icon").attr("rel"),
										curType = jQuery("#browser ul li.selected").attr("rel"),
										method, param;
									if (typeof curFile === "string" && curFile.length > 0) {
										cjFileBrowser.displayDialog({
											type: "progress",
											state: "show",
											label: "Deleting file..."
										});
										jQuery("#fileSelect").attr("disabled", true);
										jQuery("#fileDelete").attr("disabled", true);
										// set a nice timer to ensure that the CFC does not timeout
										cjFileBrowser.locvars.timer = window.setTimeout(function(){
											cjFileBrowser.displayDialog({
												type: "alert",
												state: "show",
												label: "Oops! There was a problem",
												content: "The system timed out attempting to delete a file."
											});
											jQuery("#newfolder").attr("disabled", false);
										}, parseInt(cjFileBrowser.settings.timeOut,10) * 1000);
										
										// call out handler to DELETE A FILE
										if (curType === "File") {
											method = "doDeleteFile";
											param = "fileName";
										} else if (curType === "Directory") {
											method = "doDeleteDirectory";
											param = "dirName";
										}
										if (typeof method === "string" && typeof param === "string") {
											
											// delete the file or directory
											jQuery.getJSON("assets/engines/" + cjFileBrowser.settings.engine + "/" + cjFileBrowser.settings.handler + "?method=" + method + "&returnFormat=JSON&_cf_nodebug=true&timeOut=" + parseInt(cjFileBrowser.settings.timeOut,10) + "&baseUrl=" + escape(cjFileBrowser.settings.baseUrl[cjFileBrowser.locvars.currentUrl]) + "&" + param + "=" + escape(curFile), { "noCache": cjFileBrowser.locvars.nocache }, function (result, status) {
												var r = cjFileBrowser.doEvalJSON(result);
												if (typeof r === "object" && typeof r.ERROR === "boolean" && r.ERROR === false) {
						
													// everything good.
													cjFileBrowser.clearTimer();
													jQuery("#fileDelete").attr("disabled", false);
													cjFileBrowser.doDirListing();
						
												} else if (typeof r !== "object" || (typeof r.ERROR === "boolean" && r.ERROR === true)) {
						
													// something happened in the CFC
													cjFileBrowser.clearTimer();
													cjFileBrowser.displayDialog({
														type: "alert",
														state: "show",
														label: "Oops! There was a problem",
														content: (typeof r.ERROR_MSG === "string" && r.ERROR_MSG !== "") ? r.ERROR_MSG : "Problems communicating with the CFC when attempting to delete a " + curType.toLowerCase() + "."
													});
													jQuery("#fileDelete").attr("disabled", false);
						
												} else {
						
													// problem with the processing, server might be down.
													cjFileBrowser.clearTimer();
													cjFileBrowser.displayDialog({
														type: "alert",
														state: "show",
														label: "Oops! There was a problem",
														content: "Problems communicating with the CFC. Unexpected results returned for fileDelete (click)."
													});
													jQuery("#fileDelete").attr("disabled", false);
						
												}
											});
											
										} else {
										
											// problem determining the file parameters?
											cjFileBrowser.clearTimer();
											jQuery("#fileDelete").attr("disabled", false);
											cjFileBrowser.displayDialog({
												type: "alert",
												state: "show",
												label: "Oops! There was a problem",
												content: "Problems encountered attempting to delete your selection."
											});
											
										}
									}
								},
								cbCancel: function() {
									jQuery("#fileDelete").attr("disabled", false);
								}
							});
						}
					});
				}
				
				// handle the UPLOAD action
				if (jQuery.inArray("fileUpload",cjFileBrowser.settings.actions) === -1) {
					jQuery("#fileOptions #fileUpload").remove();
				} else {
					jQuery("#fileUpload").mouseup(function () {
						jQuery(this).css({
							"background-position": "0px 0px"
						}).attr("disabled", true);
						cjFileBrowser.displayDialog({
							type: "confirm",
							state: "show",
							label: "Upload a file...",
							content: (
								'<form name="CJUploadForm" id="CJUploadForm" action="javascript:void(0);" method="post">' + 
									'<div class="fields">' +
										'<input type="file" name="fileUploadField" id="fileUploadField" \/>' +
									'<\/div>' +
								'<\/form>'
							),
							cbOk: function() {
								if (jQuery("#CJUploadForm #fileUploadField").val() !== "") {
									jQuery("#CJFileBrowser #CJFileBrowserForm").append(
										'<div class="hider">' +
											'<input type="hidden" name="baseUrl" value="' + escape(cjFileBrowser.settings.baseUrl[cjFileBrowser.locvars.currentUrl]) + '" \/>' + 
											'<input type="hidden" name="fileExts" value="' + escape(cjFileBrowser.settings.fileExts) + '" \/>' + 
											'<input type="hidden" name="maxSize" value="' + escape(cjFileBrowser.settings.maxSize) + '" \/>' + 
											'<input type="hidden" name="maxWidth" value="' + escape(cjFileBrowser.settings.maxWidth) + '" \/>' + 
											'<input type="hidden" name="maxHeight" value="' + escape(cjFileBrowser.settings.maxHeight) + '" \/>' +
											'<input type="hidden" name="timeOut" value="' + parseInt(cjFileBrowser.settings.timeOut,10) + '" \/>' + 
										'<\/div>'
									);
									jQuery("#CJUploadForm #fileUploadField").appendTo(jQuery("#CJFileBrowser #CJFileBrowserForm .hider"));
									cjFileBrowser.displayDialog({
										type: "confirm",
										state: "hide"
									});
									jQuery("#fileUpload").attr("disabled", false);
									jQuery("#CJFileBrowserForm").submit();
								}
							},
							cbCancel: function() {
								jQuery("#fileUpload").attr("disabled", false);
							}
						});
						// fix the file upload input
						jQuery("#CJUploadForm #fileUploadField").css({
							opacity: 0.0
						}).change(function() {
							jQuery("#CJUploadForm #uploadFakeFile").val(jQuery(this).val());
						});
						jQuery("#CJUploadForm .fields").append(
						jQuery("<div></div>").addClass("fakefile").append(
						jQuery("<input>").attr({
							type: "text",
							id: "uploadFakeFile"
						}), jQuery("<img>").attr({
							src: "assets/images/bg_fileselect.png"
						}).css("z-index", -1))).addClass("clearfix");
					});
					
					// handle the FORM submit
					jQuery("#CJFileBrowserForm").submit(function () {
						var formElem = jQuery(this),
							jFrame, ifrmName = ("uploader" + (new Date()).getTime());
						
						// display the progress bar...
						cjFileBrowser.displayDialog({
							type: "progress",
							state: "show",
							label: "Uploading file..."
						});
						
						// do some fancy form stuff... (posting into an iframe)
						jFrame = jQuery("<iframe name=\"" + ifrmName + "\" id=\"" + ifrmName + "\" src=\"about:blank\" \/>");
						jFrame.css({
								position: "absolute",
								top: "-999px",
								left: "-999px",
								display: "none",
								width: "1px",
								height: "1px"
							});
						jFrame.load(function (objEvent) {
							var objUploadBody = jQuery(this).contents().find("body").html();
							var fnc = function () {
								jFrame.remove();
							};
							if (jQuery.trim(objUploadBody) !== "") {
								formElem.attr({
									action: function() {
										return false;
									},
									method: "post",
									enctype: "multipart/form-data",
									encoding: "multipart/form-data",
									target: ""
								});
								cjFileBrowser.clearTimer();
								jQuery("#CJFileBrowserForm").find(".hider").remove();
								cjFileBrowser.doPostEval(jQuery.trim(objUploadBody));
								cjFileBrowser.timer = window.setTimeout(fnc, 100);
							}
						});
						jQuery("body").append(jFrame);
						
						// set a nice timer to ensure that the CFC does not timeout
						cjFileBrowser.locvars.timer = window.setTimeout(function(){
							cjFileBrowser.displayDialog({
								type: "alert",
								state: "show",
								label: "Oops! There was a problem",
								content: "The system timed out attempting to upload a file."
							});
							jFrame.remove();
							jQuery("#CJFileBrowserForm").find(".hider").remove();
							jQuery("#fileUpload").attr("disabled", false);
						}, parseInt(cjFileBrowser.settings.timeOut,10) * 1000);
		
						// We don't know where we are at. So we need to determine the path to do the form submit.
						// not sure if this is elegant or a mess, but it works.
						var base_path = (document.location.href.split("?")[0]).replace(/(\/|\\)cjfilebrowser\.html/g, "");
						formElem.attr({
							action: base_path + "/assets/engines/" + cjFileBrowser.settings.engine + "/" + cjFileBrowser.settings.handler + "?method=doFileUpload",
							method: "post",
							enctype: "multipart/form-data",
							encoding: "multipart/form-data",
							target: ifrmName
						});
						return true;
					});
				}
				
				// handle the file SELECT action
				if (jQuery.inArray("fileSelect",cjFileBrowser.settings.actions) === -1) {
					jQuery("#fileOptions #fileSelect").remove();
				} else {
					jQuery("#fileSelect").mouseup(function () {
						var fileID = parseInt(jQuery("#browser ul li.selected").attr("ID").replace("browser_ID",""),10);
						if (typeof fileID === "number" && typeof cjFileBrowser.locvars.dirContents[fileID] === "object") {
							cjFileBrowser.doFileSelect(cjFileBrowser.locvars.dirContents[fileID]);
						} else {
							cjFileBrowser.displayDialog({
								type: "alert",
								state: "show",
								label: "Oops! There was a problem",
								content: "Problems encountered attempting to determine file selection."
							});
						}
					});
				}
				
				// make sure all dialog auto centers
				jQuery(window).resize(function() {
					var dlg = jQuery("#CJModalDialog");
					dlg.css({
						top: (parseInt((jQuery(window).height() / 2), 10) - parseInt(dlg.height() / 2, 10)) + "px",
						left: (parseInt((jQuery(window).width() / 2), 10) - parseInt(dlg.width() / 2, 10)) + "px"
					});
				});
				
				// do we have a cookie with out last directory?
				var c = jQuery.cookie("cjFileBrowser_dir"),
					t = "/";
				if (c !== null && typeof c === "string" && c.length > 1) {
					c = c.substring(1,c.length-1);
					c = c.split("/");
					cjFileBrowser.settings.baseUrl = ["/"];
					jQuery.each(c, function(idx, val) {
						if(val.length > 0) {
							t += val + "/";
							cjFileBrowser.settings.baseUrl.push(t);
						}
					});
					cjFileBrowser.locvars.currentUrl = c.length;
				}
				
				// get directory listing
				cjFileBrowser.doDirListing();
				
		},
		
		// initial test to see if our handler exists
		getHandler: function() {
			try {
				jQuery.getJSON("assets/engines/" + cjFileBrowser.settings.engine + "/" + cjFileBrowser.settings.handler + "?method=isHandlerReady&returnFormat=JSON&_cf_nodebug=true&timeOut=" + parseInt(cjFileBrowser.settings.timeOut,10) + "&version=" + cjFileBrowser.locvars.version, { "noCache": cjFileBrowser.locvars.nocache }, function (result, status) {
					var r = cjFileBrowser.doEvalJSON(result);
					if (typeof r === "object" && typeof r.ERROR === "boolean" && r.ERROR === false) {
					
						// no errors, continue with setup
						cjFileBrowser.setup();
						
					} else {
						
						// oops, we had an error or it didn't respond properly
						alert("Oops! There was a problem\n\nThe handler engine did not respond properly during initialization.");
						
					}
				});
			} catch (err) {
			
				// we had an error or it didn't respond properly
				alert("Oops! There was a problem\n\nThe handler engine did not respond during initialization. ");
				
			}
		},
		
		init: function (options) {
			
			// reset directory path
			cjFileBrowser.settings.baseUrl = [];
			cjFileBrowser.locvars.currentUrl = 0;
			
			// setup the footer
			jQuery("#footer").append('<div class="callout"><div class="margins">CJ File Browser <strong id="CJVersion">v' + cjFileBrowser.locvars.version + '<\/strong>. Created by <a href="http://www.cjboco.com/" target="_blank" title="Creative Juice Bo. Co.">Creative Juice Bo. Co.<\/a><\/div><\/div>');
			jQuery("#footer").append('<div class="stats"><\/div>');
			
			// if we are a tinyMCE plug-in then we need to grab the user settings
			if (typeof tinyMCE !== "undefined" && typeof tinyMCE.settings.plugin_cjfilebrowser_actions !== "undefined") {
				cjFileBrowser.settings.actions = typeof tinyMCE.settings.plugin_cjfilebrowser_actions === "string" ? tinyMCE.settings.plugin_cjfilebrowser_actions.split(",") : "";
				cjFileBrowser.settings.baseUrl.push(typeof tinyMCE.settings.plugin_cjfilebrowser_assetsUrl === "string" ? tinyMCE.settings.plugin_cjfilebrowser_assetsUrl : "");
				cjFileBrowser.settings.fileExts = typeof tinyMCE.settings.plugin_cjfilebrowser_fileExts === "string" ? tinyMCE.settings.plugin_cjfilebrowser_fileExts : "";
				cjFileBrowser.settings.maxSize = typeof tinyMCE.settings.plugin_cjfilebrowser_maxSize === "number" ? tinyMCE.settings.plugin_cjfilebrowser_maxSize : 0;
				cjFileBrowser.settings.maxWidth = typeof tinyMCE.settings.plugin_cjfilebrowser_maxWidth === "number" ? tinyMCE.settings.plugin_cjfilebrowser_maxWidth : 0;
				cjFileBrowser.settings.maxHeight = typeof tinyMCE.settings.plugin_cjfilebrowser_maxHeight === "number" ? tinyMCE.settings.plugin_cjfilebrowser_maxHeight : 0;
				cjFileBrowser.settings.showImgPreview = typeof tinyMCE.settings.plugin_cjfilebrowser_showImgPreview === "boolean" ? tinyMCE.settings.plugin_cjfilebrowser_showImgPreview : true;
				cjFileBrowser.settings.engine = typeof tinyMCE.settings.plugin_cjfilebrowser_engine === "string" ? tinyMCE.settings.plugin_cjfilebrowser_engine : "";
				cjFileBrowser.settings.handler = typeof tinyMCE.settings.plugin_cjfilebrowser_handler === "string" ? tinyMCE.settings.plugin_cjfilebrowser_handler : "";
				cjFileBrowser.settings.timeOut = typeof tinyMCE.settings.plugin_cjfilebrowser_timeOut === "number" ? tinyMCE.settings.plugin_cjfilebrowser_timeOut : 900; // 15 minutes
				cjFileBrowser.settings.callBack = null;
			} else if (options) {
				// we are in stand-alone mode...
				jQuery.extend(cjFileBrowser.settings, options);
			}
			
			// debug the init settings (if debug is turned on and console is available)
			if (cjFileBrowser.locvars.debug && typeof console !== "undefined" && typeof console.debug !== "undefined") {
				console.debug("actions: " + typeof cjFileBrowser.settings.actions + " value: " + cjFileBrowser.settings.actions.length);
				console.debug("baseUrl: " + typeof cjFileBrowser.settings.baseUrl + " value: " + cjFileBrowser.settings.baseUrl.length);
				console.debug("fileExts: " + typeof cjFileBrowser.settings.fileExts + " value: " + cjFileBrowser.settings.fileExts);
				console.debug("maxSize: " + typeof cjFileBrowser.settings.maxSize + " value: " + cjFileBrowser.settings.maxSize);
				console.debug("maxWidth: " + typeof cjFileBrowser.settings.maxWidth + " value: " + cjFileBrowser.settings.maxWidth);
				console.debug("maxHeight: " + typeof cjFileBrowser.settings.maxHeight + " value: " + cjFileBrowser.settings.maxHeight);
				console.debug("showImgPreview: " + typeof cjFileBrowser.settings.showImgPreview + " value: " + cjFileBrowser.settings.showImgPreview);
				console.debug("engine: " + typeof cjFileBrowser.settings.engine + " value: " + cjFileBrowser.settings.engine);
				console.debug("handler: " + typeof cjFileBrowser.settings.handler + " value: " + cjFileBrowser.settings.handler);
				console.debug("timeOut: " + typeof cjFileBrowser.settings.timeOut + " value: " + cjFileBrowser.settings.timeOut);
				console.debug("callBack: " + typeof cjFileBrowser.settings.callBack + " value: " + cjFileBrowser.settings.callBack);
				console.debug("currentUrl: " + typeof cjFileBrowser.locvars.currentUrl + " value: " + cjFileBrowser.locvars.currentUrl);
			}
				
			// need to verify that we have valid setting values...
			if (
				(typeof cjFileBrowser.settings.actions !== "object" || cjFileBrowser.settings.actions.length === 0) ||
				(typeof cjFileBrowser.settings.baseUrl !== "object" || cjFileBrowser.settings.baseUrl.length === 0) ||
				(typeof cjFileBrowser.settings.fileExts !== "string" || cjFileBrowser.settings.fileExts === "") ||
				(typeof cjFileBrowser.settings.maxSize !== "number" || cjFileBrowser.settings.maxSize === 0) ||
				(typeof cjFileBrowser.settings.maxWidth !== "number" || cjFileBrowser.settings.maxWidth === 0) ||
				(typeof cjFileBrowser.settings.maxHeight !== "number" || cjFileBrowser.settings.maxHeight === 0) || 
				(typeof cjFileBrowser.settings.showImgPreview !== "boolean") ||
				(typeof cjFileBrowser.settings.engine !== "string" || cjFileBrowser.settings.engine === "") ||
				(typeof cjFileBrowser.settings.handler !== "string" || cjFileBrowser.settings.handler === "") ||
				(typeof cjFileBrowser.settings.timeOut !== "number" || cjFileBrowser.settings.timeOut < 0) ||
				(typeof cjFileBrowser.settings.callBack !== "function" && cjFileBrowser.settings.callBack !== null) ||
				(typeof cjFileBrowser.locvars.currentUrl !== "number" || cjFileBrowser.locvars.currentUrl < 0)
			) {
				
				// problem initializing the script
				alert("Oops! There was a problem\n\nVariables were not initialized properly or missing values.");
			
			} else {
			
				// test to make sure our handler exists
				cjFileBrowser.getHandler();
			}
		}
	};
}