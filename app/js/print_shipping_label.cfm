<cfsetting showdebugoutput="no">
<!---20140708 WW: Added print shipping label popup per Ticket#51800 --->
<cfparam name="URL.tr" default="">
<cfset cut_off = DateAdd("h", -24, Now())>

<cfif Len(URL.tr) GT 0>
	<cfquery name="qry_getPackaging" datasource="HMEUPS" maxrows="1">
		SELECT	Zip_Code, Packing_Label, store_name, store_number, contact_name, address_1, address_2, city, state, zip_code
		FROM	dbo.UPS_CAPTURE
		WHERE 	tracking_number = <cfqueryparam cfsqltype="cf_sql_varchar" value="#URL.tr#">
		AND 	Created_DTS >= <cfqueryparam cfsqltype="cf_sql_date" value="#CreateODBCDateTime(cut_off)#">
		AND 	(Packing_Label <> '' AND Packing_Label IS NOT NULL)
	</cfquery>
<cfelse>
	An error occurred. <cfabort />
</cfif>

<!DOCTYPE html>
<html>
<head>
<style type="text/css" media="all">
.fold_line{
	/*margin-top: 130px;*/
	margin-top: -30px;
	margin-bottom: 35px;
	border-bottom: dotted gray;
}
.line_break_dash {border-top:2px dashed #000000;padding-bottom:15px;}
.cust_addr {height:180px;font-size:28px;padding-left:20px;}
</style>
</head>
<body>

<div style="width:720px;">
<div style="color:gray; font-size:1.17em; font-weight:bold; padding:6px 6px 25px 0px; top:-5px;">UPS Electronic Return Label: View/Print Label</div>

<cfif FileExists(ExpandPath("/images/ups/#qry_getPackaging.Packing_Label#"))>
	<script type="text/javascript"> 
		window.onload=function(){self.print();} 
	</script>
    
    <div>
    	<ol>
            <li><b>Ensure that there are no other tracking labels attached to your package.</b><br><br></li>
			<li><b>Tear this sheet of paper at the dotted line.</b> Place the top half of this sheet of paper inside of the shipping box. Place the bottom half of this sheet of paper containing the UPS label in a UPS Shipping Pouch. If you do not have a pouch, affix the label using clear plastic shipping tape over the entire label.  Take care not to cover any seams or closures.<br><br></li>
            <li><b>Collection and Drop-off<br></b>
                <ul>
                    <li>Take this package to any location of The UPS Store &reg;, UPS Drop Box, UPS Customer Center, UPS Alliances (Office Depot&reg; or Staples&reg;) or Authorized Shipping Outlet near you. Items sent via UPS ReturnsSM Services(including via UPS Ground) are accepted at Drop Boxes, to find your closest UPS location visit  <a href="http://www.ups.com/content/us/en/index.jsx" target="_blank">www.ups.com/content/us/en/index.jsx</a> and select Drop Off. Click to locate Dropoff location: <a href="http://www.ups.com/dropoff?autosubmit=1&loc=en_US&appid=XOLT&country=US&Postal=<cfoutput>#qry_getPackaging.Zip_Code#</cfoutput>&trans_mode=002" target="_blank">Drop Off Locator</a></li>
                    <li>Daily Collection customers: Have your shipment(s) ready for the driver as usual.</li>
                </ul>
            </li>
        </ol>
    </div>
	
	<div class="cust_addr">
		<cfoutput>
			#qry_getPackaging.contact_name#<br />
			#qry_getPackaging.store_name# : #qry_getPackaging.store_number#<br />
			#qry_getPackaging.address_1#<br />
			<cfif qry_getPackaging.address_2 NEQ ''>
				#qry_getPackaging.address_2#<br />
			</cfif>
			#qry_getPackaging.city#, #qry_getPackaging.state# #qry_getPackaging.zip_code#<br />
		</cfoutput>
	</div>
    
    <div class="fold_line">
    	TEAR HERE
    </div>
    
    <div>
    	<cfoutput><img src="https://#APPLICATION.rootURL#/images/ups/#qry_getPackaging.Packing_Label#" /></cfoutput>
    </div>
<cfelse>
	An error occurred. The return label was not found.
</cfif>
</div>

</body>
</html>