<cfsilent>
<cfparam name="URL.suid" default="">
<cfparam name="FORM.suid" default="">
<cfparam name="get_Session" default="">
<cfparam name="VARIABLES.subTotal" default="0">
<!---Billing Info--->
<cfparam name="FORM.firstName" default="">
<cfparam name="FORM.lastName" default="">
<cfparam name="FORM.email" default="">
<cfparam name="FORM.phone" default="">
<cfparam name="FORM.creditCardType" default="">
<cfparam name="FORM.creditCardNumber" default="">
<cfparam name="FORM.expDateMonth" default="#Month(Now())#">
<cfparam name="FORM.expDateYear" default="#Year(Now())#">
<cfparam name="FORM.cvv2Number" default="">
<cfparam name="FORM.address1" default="">
<cfparam name="FORM.address2" default="">
<cfparam name="FORM.city" default="">
<cfparam name="FORM.state" default="">
<cfparam name="FORM.zip" default="">
<!---Shipping Info--->
<cfparam name="FORM.Ship_Company" default="">
<cfparam name="FORM.Ship_address1" default="">
<cfparam name="FORM.Ship_address2" default="">
<cfparam name="FORM.Ship_city" default="">
<cfparam name="FORM.Ship_state" default="">
<cfparam name="FORM.Ship_zip" default="">
<cfparam name="FORM.amount" default="">
<cfparam name="FORM.PAYMENTACTION" default="sale">

<cfparam name="ERR.Billing_Error" default="false">

<cfset VARIABLES.StateList = "AK,AL,AR,AZ,CA,CO,CT,DC,DE,FL,GA,HI,IA,ID,IL,IN,KS,KY,LA,MA,MD,ME,MI,MN,MO,MS,MT,NC,ND,NE,NH,NJ,NM,NV,NY,OH,OK,OR,PA,RI,SC,SD,TN,TX,UT,VA,VT,WA,WI,WV,WY,AA,AE,AP,AS,FM,GU,MH,MP,PR,PW,VI">
</cfsilent>