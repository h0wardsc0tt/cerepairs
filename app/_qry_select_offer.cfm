<cfparam name="QUERY" default="#StructNew()#">
<cfparam name="QUERY.QueryName" default="qry_getOffers">
<cfparam name="QUERY.Datasrouce" default="#APPLICATION.Datasource#">
<cfparam name="QUERY.SelectSQL" default="offr.*">
<cfparam name="QUERY.Offer_IsActive" default="1">
<cfparam name="QUERY.OrderBy" default="offr.Offer_ValidTo DESC">

<cfquery name="#QUERY.QueryName#" datasource="#QUERY.Datasource#">
	SELECT #QUERY.SelectSQL#
	FROM 
		tbl_Products prod
		INNER JOIN itbl_Offer_Product iofp ON prod.Product_ID = iofp.Product_ID
		INNER JOIN tbl_Offers offr ON iofp.Offer_ID = offr.Offer_ID
	WHERE 0=0
	<cfif StructKeyExists(QUERY, "Offer_UID")>
	AND offr.Offer_UID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.Offer_UID#" list="yes">)
	</cfif>
	<cfif StructKeyExists(QUERY, "Offer_IsActive")>
	AND offr.Offer_IsActive IN (<cfqueryparam cfsqltype="cf_sql_tinyint" value="#QUERY.Offer_IsActive#" list="yes">)
	</cfif>
	<cfif StructKeyExists(QUERY, "Offer_ValidFrom")>
	AND offr.Offer_ValidFrom <= <cfqueryparam cfsqltype="cf_sql_date" value="#CreateODBCDate(QUERY.Offer_ValidFrom)#">
	</cfif>
	<cfif StructKeyExists(QUERY, "Offer_ValidTo")>
	AND offr.Offer_ValidTo >= <cfqueryparam cfsqltype="cf_sql_date" value="#CreateODBCDate(QUERY.Offer_ValidTo)#">
	</cfif>
	<cfif StructKeyExists(QUERY, "Product_UID")>
	AND prod.Product_UID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.Product_UID#" list="yes">)
	</cfif>
	<cfif StructKeyExists(QUERY, "Offer_Qty")>
	AND offr.Offer_Qty <= <cfqueryparam cfsqltype="cf_sql_integer" value="#QUERY.Offer_Qty#">
	</cfif>
	ORDER BY #QUERY.OrderBy#
</cfquery>

<cfquery name="#QUERY.QueryName#_Total" dbtype="query">
	SELECT Count(*) AS Records
	FROM #QUERY.QueryName#
</cfquery>
<cfif StructClear(QUERY)></cfif>