<cfsilent>
<cfparam name="FORM.criteria" default="">
<cfparam name="URL.o" default="0">
<cfparam name="URL.lm" default="10">
<cfif NOT IsNumeric(URL.o) OR NOT IsNumeric(URL.lm)>
	<cfmail to="atnelson@hme.com" from="no-reply@cerepairs.com" subject="Unexpected Value Passed">
	Bad Request found on #CGI.SCRIPT_NAME#
	#CGI.QUERY_STRING#
	</cfmail>
	<cflocation url="https://#APPLICATION.rootURL#" addtoken="no">
	<cfabort>
</cfif>

<cfscript>
	QUERY = StructNew();
	QUERY.QueryName = "qry_getProducts";
	QUERY.Datasource = APPLICATION.Datasource;
	QUERY.SelectSQL = "DISTINCT prod.Product_UID, prod.Product_Short_Description, prod.Supplier_Part_Number, prod.Manufacturer_Part_Number, prod.UOM, prod.Product_Image_Thum, prod.Retail_Price, prod.Exchange_Price, prod.Repair_Price, prod.Refurbished_Price, prod.Recondition_Price, prod.Product_IsActive";
	QUERY.Product_IsActive = 1;
	if(ReFindNoCase("[a-z0-9 -]", FORM.criteria) GT 0 AND IsNumeric(URL.o)) {
		QUERY.Product_SEARCH = ReplaceNoCase(FORM.criteria, " ", "|", "all");
	}
	QUERY.OrderBy = "prod.Product_Short_Description";
</cfscript>
<cfinclude template="./_qry_select_cat_type_product.cfm">

<cfscript>
	Attribs = StructNew();
	Attribs.Str_Row = URL.o;
	Attribs.Tot_Row = URL.lm;
	Attribs.Totals = qry_getProducts_Total.Records;
	
	recStr = Evaluate(URL.o+1);
	recEnd = Evaluate(URL.lm+URL.o);
	if(recEnd GT qry_getProducts.RecordCount) {
		recEnd = qry_getProducts.RecordCount;
	}
</cfscript>
</cfsilent>
<cfoutput>
<div class="products clear">
	<h2>Showing #recStr# - #recEnd# of (#qry_getProducts.RecordCount#) Results</h2>
	<cfinclude template="./_tmp_product_list.cfm">
</div>
</cfoutput>