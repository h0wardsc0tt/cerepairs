<div class="shipinfo clear">
	<div class="shipplug">
		<!---20140422 CG: Replaced Banner Ticket#50958 --->
		<!---20141007 CG: Removed Banner Ticket#56083 --->
		<!---<div><a href="./2014_CE_Prospect_Coupon.pdf" target="_blank"><img src="/images/teasers/2014-CE-Prospect-Banner.jpg" /></a></div>--->
		<h1>Send in a Drive-Thru Repair</h1>
		<h3>Drive-Thru Repairs - Easy Online Form</h3>
		<p>Fill out a quick form and we'll provide free inbound shipping labels - it couldn't be easier.</p>
		<h3>Send Us Your Drive-Thru Repairs in 3 Easy Steps.</h3>
		<ol>
			<!--- 20141009 CG: Updated text per Ticket#56104 --->
			<li>Place your equipment in a box (include packaging around your equipment for protection during shipment)</li>
			<li>Fill out the form below to print a FREE UPS return service label and place on the box, keeping a copy of your UPS tracking number.</li>
			<!---20131213 CG: Fixed typo, changed "any" to "at" Ticket#47739 --->
			<li>Drop the box off at any authorized UPS service location or give it to your UPS driver.</li>
		</ol>
	</div>
	<cfinclude template="./_tmp_ship_form.cfm">
	<div class="shipplug">
		<h3>Call for Drive-Thru Repair</h3>
		<p>Call 1-877-731-0334 for details on our convenient nationwide service and installation. Our referral network of qualified onsite service providers is waiting to serve you.</p>
		<h3>Free Shipping On Your Drive-Thru Repair</h3>
		<p>With CE, all inbound shipping for drive-thru repairs is absolutely free. Just fill out the easy online form and we'll provide the free inbound shipping labels.</p>
	</div>
</div>