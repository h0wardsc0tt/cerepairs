<cfparam name="QUERY" default="#StructNew()#">
<cfparam name="QUERY.QueryName" default="qry_getTypes">
<cfparam name="QUERY.Datasource" default="#APPLICATION.Datasource#">
<cfparam name="QUERY.SelectSQL" default="cats.*, type.*">
<cfparam name="QUERY.OrderBy" default="cats.ProductCategory, type.ProductType">
<cfparam name="QUERY.ProductCategory_IsActive" default="1">

<cfquery name="#QUERY.QueryName#" datasource="#QUERY.Datasource#">
	SELECT #QUERY.SelectSQL#
	FROM
		ltbl_Product_Category cats 
		LEFT JOIN ltbl_Product_Type type ON cats.ProductCategory_ID = type.ProductCategory_ID
		LEFT JOIN ltbl_Manufacturer manf ON manf.Manufacturer_ID = type.ProductManufacturer_ID
	WHERE 0=0
	<cfif StructKeyExists(QUERY, "ProductType_IsActive")>
	AND type.ProductType_IsActive IN (<cfqueryparam cfsqltype="cf_sql_tinyint" value="#QUERY.ProductType_IsActive#" list="yes">)
	</cfif>
	<cfif StructKeyExists(QUERY, "ProductCategory_IsActive")>
	AND cats.ProductCategory_IsActive IN (<cfqueryparam cfsqltype="cf_sql_tinyint" value="#QUERY.ProductCategory_IsActive#" list="yes">)
	</cfif>
	<cfif StructKeyExists(QUERY, "ProductCategory_ID")>
	AND cats.ProductCategory_ID IN (<cfqueryparam cfsqltype="cf_sql_tinyint" value="#QUERY.ProductCategory_ID#" list="yes">)
	</cfif>
	<cfif StructKeyExists(QUERY, "ProductCategory_ID_NOT_IN")>
	AND cats.ProductCategory_ID NOT IN (<cfqueryparam cfsqltype="cf_sql_tinyint" value="#QUERY.ProductCategory_ID_NOT_IN#" list="yes">)
	</cfif>
	<cfif StructKeyExists(QUERY, "ProductType_ID")>
	AND type.ProductType_ID IN (<cfqueryparam cfsqltype="cf_sql_tinyint" value="#QUERY.ProductType_ID#" list="yes">)
	</cfif>
	<cfif StructKeyExists(QUERY, "ProductType_ID_NOT_IN")>
	AND type.ProductType_ID_NOT_IN IN (<cfqueryparam cfsqltype="cf_sql_tinyint" value="#QUERY.ProductType_ID_NOT_IN#" list="yes">)
	</cfif>
	ORDER BY #QUERY.OrderBy#
</cfquery>

<cfquery name="#QUERY.QueryName#_Total" dbtype="query">
	SELECT Count(*) AS Records
	FROM #QUERY.QueryName#
</cfquery>

<cfif StructClear(QUERY)></cfif>