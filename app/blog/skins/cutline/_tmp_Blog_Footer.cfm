<cfimport prefix="mango" taglib="../../tags/mango">
<cfimport prefix="mangox" taglib="../../tags/mangoextras">
<cfimport prefix="template" taglib=".">
			</div>
		</div>
	</div>
	<div id="sidebar" class="col2">
		<ul class="sidebar_list">
			<mangox:PodGroup locationId="sidebar" template="index">
				<mangox:TemplatePod id="blog-description" title="CE Repairs">
				<p><mango:Blog description descriptionParagraphFormat /></p>
				</mangox:TemplatePod>
				<template:sidebar />
			</mangox:PodGroup>	
		</ul>
	</div>
</div>
	<div id="footer">
		<mango:Event name="afterFooterStart" />
			<p>&#169; <cfoutput>#Year(Now())#</cfoutput> HM Electronics, Inc.</p>
		<mango:Event name="beforeFooterEnd" />
	</div>
	<mango:Event name="beforeHtmlBodyEnd" />
</div>
</body>
</html>
<br clear="all" />