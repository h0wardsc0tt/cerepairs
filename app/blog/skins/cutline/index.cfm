<cfinclude template="./_tmp_Blog_Header.cfm">
<cfimport prefix="mango" taglib="../../tags/mango">
<cfimport prefix="mangox" taglib="../../tags/mangoextras">
<cfimport prefix="template" taglib=".">
				<mango:Event name="beforeHtmlBodyStart" />
				<div id="content_box">
					<div id="content" class="posts">
						<div class="intro">
							<h1>Welcome to CE's Drive-Thru Repair Blog</h1>
						</div>
						<h3>Latest News</h3>
						<mango:Posts count="10">
						<mango:Post>	
							<h2><a href="<mango:PostProperty link />" rel="bookmark" title="Permanent Link to <mango:PostProperty title />"><mango:PostProperty title /></a></h2>
							<h4><mango:PostProperty date dateformat="mmmm dd, yyyy" /> &middot; By <mango:PostProperty author /> &middot; <mango:PostProperty ifcommentsallowed><a href="<mango:PostProperty link />#respond" title="Comment on <mango:PostProperty title />"><mango:PostProperty ifCommentCountGT="0"><mango:PostProperty commentCount /> Comment<mango:PostProperty ifCommentCountGT="1">s</mango:PostProperty></mango:PostProperty><mango:PostProperty ifCommentCountLT="1">No Comments</mango:PostProperty></a></mango:PostProperty></h4>
							<div class="entry">
								<mango:PostProperty ifhasExcerpt excerpt>
									<p><a href="<mango:PostProperty link />" title="Read the rest of this entry">[Read more &rarr;]</a></p>
								</mango:PostProperty>
								<mango:PostProperty ifNotHasExcerpt body />
							</div>
							<div class="entry-footer entry">
								<mango:Event name="beforePostContentEnd" template="index" mode="excerpt" />
							</div>
							<p class="tagged">
								<span class="add_comment">
									<mango:PostProperty ifcommentsallowed>&rarr; <a href="<mango:PostProperty link />#respond" title="Comment on <mango:PostProperty title />"><mango:PostProperty ifCommentCountGT="0"><mango:PostProperty commentCount /> Comment<mango:PostProperty ifCommentCountGT="1">s</mango:PostProperty></mango:PostProperty><mango:PostProperty ifCommentCountLT="1">No Comments</mango:PostProperty></a></mango:PostProperty>
								</span>
								<strong>Tags:</strong> 
								<mango:Categories><mango:Category><a href="<mango:CategoryProperty link />" title="View all posts in  <mango:CategoryProperty title />" rel="category tag"><mango:CategoryProperty title /></a>
									<mango:Category ifCurrentIsNotLast>&middot; </mango:Category></mango:Category>
								</mango:Categories>
							</p>
							<div class="clear"></div>
						</mango:Post>
						</mango:Posts>
						
						<mango:Archive pageSize="10">
						<div class="navigation">
							<div class="previous"><mango:ArchiveProperty ifHasNextPage><a class="previous" href="<mango:ArchiveProperty link pageDifference="1" />">&larr; Previous Entries</a></mango:ArchiveProperty></div>
							<div class="next"></div>
						</div>
						</mango:Archive>
						<div class="clear flat"></div>
					</div>
				</div>
<cfinclude template="./_tmp_Blog_Footer.cfm">