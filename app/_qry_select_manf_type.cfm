<cfparam name="QUERY" default="#StructNew()#">
<cfparam name="QUERY.QueryName" default="qry_getManufacturer">
<cfparam name="QUERY.Datasource" default="#APPLICATION.Datasource#">
<cfparam name="QUERY.SelectSQL" default="manf.*, type.*">
<cfparam name="QUERY.Manufacturer_IsActive" default="1">
<cfparam name="QUERY.OrderBy" default="manf.Manufacturer_ID">

<cfquery name="#QUERY.QueryName#" datasource="#QUERY.Datasource#">
	SELECT #QUERY.SelectSQL#
	FROM 
		ltbl_Manufacturer manf 
		INNER JOIN ltbl_Product_Type type ON manf.Manufacturer_ID = type.ProductManufacturer_ID
	WHERE 0=0
	<cfif StructKeyExists(QUERY, "Manufacturer_IsActive")>
	AND manf.Manufacturer_IsActive IN (<cfqueryparam cfsqltype="cf_sql_tinyint" value="#QUERY.Manufacturer_IsActive#" list="yes">) 
	</cfif>
	<cfif StructKeyExists(QUERY, "ProductType_IsActive")>
	AND type.ProductType_IsActive IN (<cfqueryparam cfsqltype="cf_sql_tinyint" value="#QUERY.Manufacturer_IsActive#" list="yes">) 
	</cfif>
	<cfif StructKeyExists(QUERY, "Manufacturer_ID")>
	AND manf.Manufacturer_ID IN (<cfqueryparam cfsqltype="cf_sql_integer" value="#QUERY.Manufacturer_ID#" list="yes">) 
	</cfif>
	<cfif StructKeyExists(QUERY, "Manufacturer_UID")>
	AND manf.Manufacturer_UID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.Manufacturer_UID#" list="yes">)
	</cfif>
	ORDER BY #QUERY.OrderBy#
</cfquery>

<cfquery name="#QUERY.QueryName#_Total" dbtype="query">
	SELECT Count(*) AS Records
	FROM #QUERY.QueryName#
</cfquery>
<cfif StructClear(QUERY)></cfif>