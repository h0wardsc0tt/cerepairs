<cfsilent>
	<cfscript>
		QUERY = StructNew();
		QUERY.QueryName = "qry_getManufacturers";
		QUERY.Datasource = APPLICATION.Datasource;
		QUERY.SelectSQL = "DISTINCT manf.Manufacturer_UID, manf.Manufacturer_Name, manf.Manufacturer_Logo, manf.Manufacturer_ID, type.ProductType, type.ProductType_UID, type.ProductType_Order";
		QUERY.Manufacturer_IsActive = "1";
		QUERY.ProductType_IsActive = "1";
		QUERY.OrderBy = "manf.Manufacturer_ID, type.ProductType_Order, type.ProductType";
	</cfscript>
	<cfinclude template="./_qry_select_manf_type.cfm">
</cfsilent>
	<div id="Navbar" class="clear">
		<div class="main-nav">
			<ul id="nav" class="dropdown dropdown-horizontal">
				<li class="ddm1">
					<h4 class="small">Drive-Thru Repairs Accessories &amp; Sales</h4>
					<ul class="ddm2 clear">
						<li class="subhead2"><h5>Drive-Thru Repairs</h5>
							<ul>
								<li><a href="./?pg=SendRepair"><img src="/images/sendrepair.png" /></a></li>
							</ul>
						</li>
						<li class="subhead3"><h5>Drive-Thru Accessories &amp; Sales</h5>
							<ul class="ddm3"><cfoutput query="qry_getManufacturers" group="Manufacturer_ID">
								<li class="manufact"><a href="./?pg=Product&pmu=#Manufacturer_UID#">#Manufacturer_Name#</a>
									<ul>
									<cfoutput><li><a href="./?pg=Product&ptu=#ProductType_UID#">#ProductType#</a></li></cfoutput>
									</ul>
								</li></cfoutput>
							</ul>
						</li>
					</ul>
				</li>
				<!---Begin HC--->
				<li class="ddm1">
					<a href="./?pg=SendRepair">
						<h4>Send In A Repair</h4>
						<h5>Free Shipping</h5>
					</a>
				</li>
				<li class="ddm1">
					<h4>Why Choose CE?</h4>
					<h5>Drive-Thru Repairs</h5>
					<ul class="ddm2 clear">
						<li class="subhead nolight"><a href="/about-ce">About CE</a></li>
						<li class="subhead nolight"><a href="/about-our-repairs">About Our Repairs</a>
							<ul>
								<li><a href="/Factory-Authorized-Repairs">Factory-Authorized Repairs</a></li>
								<li><a href="/same-day-service">Same-Day Service</a></li>
								<li><a href="/nationwide-installation">Nationwide Installation</a></li>
								<li><a href="/send-in-a-repair-free-shipping">Free Inbound Shipping</a></li>
								<li><a href="/tech-support">Drive-Thru Repair Technical Support</a></li>
							</ul>
						</li>
						<li class="subhead nolight"><a href="/drive-thru-warranty-and-emas">Repair Warranty &amp; Maintenance Programs</a>
							<ul>
								<li><a href="/4-month-warranty">4-Month Repair Warranty</a></li>
								<li><a href="/CE-EMA">Drive-Thru Protection Plan</a></li>
								<li><a href="/advanceexchange">Advance Exchange</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li class="ddm1">
					<a href="special-offers">
                        <h4>Special Offers</h4>
                        <h5>Repairs &amp; Accessories</h5>
					</a>
					<ul class="ddm2 clear">
						<li class="subhead"><a href="/new-customer-specials">If You've Never Used Us</a>
							<ul>
								<!---20140114 CG: Removed Headset special per Ticket#48303 --->
								<!---<li><a href="/freeheadset">Free Headset Special</a></li>--->
								<li><a href="/freebattery">Free Battery Special</a></li>
								<li><a href="/CE-EMA">First Month Free on a CE Drive-Thru Protection Plan</a></li>
							</ul>
						</li>
						<li class="subhead"><a href="/current-customer-specials">If You're Currently Using Us</a>
							<ul>
								<li><a href="/bonusbucks">Bonus Bucks</a></li>
								<!--- 20140514 CG: Removed referal program section per Ticket#51757  --->
								<li><a href="/pricematching">Price Matching Special</a></li>
								<li><a href="/multistorediscount">Multi-Store Discount</a></li>
								<!---<li><a href="/freeheadset">Free Headset Special</a></li>--->
								<li><a href="/freebattery">Free Battery Special</a></li>
							</ul>
						</li>
						<li class="subhead"><a href="/half-off-aio-repairs">If You Haven't Used Us In Over A Year</a>
							<ul>
								<li><a href="/half-off-aio-repairs">$75 OFF Headset or Belt-Pac Repair</a></li>
							</ul>                        
					</ul>
				</li>
				<li class="ddm1">
					<h4>Help Center</h4>
					<h5>Drive-Thru Repairs</h5>
					<ul class="ddm2 clear">
						<li class="subhead nolight"><a href="http://hs1.hme.com/qsr/drive-thru-user-manuals/" target="_blank">User Manuals</a>
                        	<ul>
								<li><a href="./files/warranty.pdf" target="_blank">Warranty</a></li>
							</ul>
                        </li>
						<li class="subhead nolight"><a href="/tech-support">Technical Support</a></li>
						<li class="subhead nolight"><a href="/drive-thru-repair-help-center">Help Center</a>
							<ul>
								<li><a href="/drive-thru-faqs">FAQs</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li class="ddm1">
					<a href="./blog">
						<h4>CE Blog</h4>
						<h5>Drive-Thru Repairs</h5>
					</a>
				</li>
				<li class="ddm1" style="border:none">
					<a href="/contact-ce-drive-thru-repairs">
						<h4>Contact CE</h4>
						<h5>Drive-Thru Repairs</h5>
					</a>
				</li>
				<!---End HC--->
			</ul>
		</div>
	</div>