<cfsilent>
<cfparam name="URL.o" default="0">
<cfparam name="URL.lm" default="10">
<cfparam name="URL.ptl" default="">
<cfparam name="URL.ptu" default="">
<cfparam name="URL.pcu" default="">
<cfparam name="VARIABLES.cleanURL_String" default="#CGI.QUERY_STRING#">
<cfif NOT IsNumeric(URL.o) OR NOT IsNumeric(URL.lm)>
	<cfmail to="atnelson@hme.com, chrisg@hme.com" from="no-reply@cerepairs.com" subject="Unexpected Value Passed">
	Bad Request found on #CGI.SCRIPT_NAME#
	#CGI.QUERY_STRING#
	</cfmail>
	<cflocation url="https://#APPLICATION.rootURL#" addtoken="no">
	<cfabort>
</cfif>
</cfsilent>

<cfif IsDefined("URL.pmu") AND ReFindNoCase("[a-z0-9]{32}", URL.pmu)> <!---ProductManufacturer Page--->
	<cfsilent>
	<cfscript>
		QUERY = StructNew();
		QUERY.QueryName = "qry_getManufacturerProdType";
		QUERY.Datasource = APPLICATION.Datasource;
		QUERY.Manufacturer_UID = URL.pmu;
		QUERY.ProductType_IsActive = 1;
		QUERY.OrderBy = "type.ProductType_Order";
	</cfscript>
	
	<cfinclude template="./_qry_select_manf_type.cfm">
	</cfsilent>

	<cfif qry_getManufacturerProdType.RecordCount NEQ 0>
		<cfinclude template="./_tmp_manf_prod_type.cfm">
    <cfelse>
    	<cflocation url="./" addtoken="no">
	</cfif>
	<cfexit method="exittemplate">
<cfelseif IsDefined("URL.pmu") AND (URL.pmu IS "" OR NOT ReFindNoCase("[a-z0-9]{32}", URL.pmu))>
	<cflocation url="./" addtoken="no">
</cfif>

<cfsilent>
<cfscript>
	QUERY = StructNew();
	QUERY.QueryName = "qry_getAllClasses";
	QUERY.Datasource = APPLICATION.Datasource;
	
	QUERY.OrderBy = "clas.ProductClass";
</cfscript>

<cfquery name="#QUERY.QueryName#" datasource="#QUERY.Datasource#">
	SELECT clas.*
	FROM ltbl_Product_Class clas
	WHERE 0=0
	<cfif StructKeyExists(QUERY, "ProductClass_ID")>
	AND clas.ProductClass_ID IN (<cfqueryparam cfsqltype="cf_sql_integer" value="#QUERY.ProductClass_ID#" list="yes">)
	</cfif>
	ORDER BY #QUERY.OrderBy#
</cfquery>
<cfif StructClear(QUERY)></cfif>

<cfscript>
	QUERY = StructNew();
	QUERY.QueryName = "qry_getProductType";
	QUERY.Datasource = APPLICATION.Datasource;
	QUERY.OrderBy = "prod.Product_Short_Description";
	QUERY.ProductType_UID = '';
	
	if(IsDefined("URL.ptu") AND ReFindNoCase("[a-z0-9]{32}", URL.ptu)) { //ProductType
		QUERY.ProductType_UID = URL.ptu;		
	}
	
	QUERY.SelectSQL = "DISTINCT clas.ProductClass, manf.Manufacturer_UID, manf.Manufacturer_Name, manf.Manufacturer_Logo, prod.Product_UID, prod.Product_Short_Description, prod.Supplier_Part_Number, prod.Manufacturer_Part_Number, prod.UOM, prod.Product_Image_Thum, prod.Retail_Price, prod.Exchange_Price, prod.Repair_Price, prod.Refurbished_Price, prod.Product_IsActive, type.ProductType_UID, type.ProductType AS thisCategory";
</cfscript>

<cfinclude template="./_qry_select_cat_type_product.cfm">

<!---20131206 CG: Added Repair filter per Ticket#47002 --->
<cfscript>
	QUERY = StructNew();
	QUERY.QueryName = "qry_getProducts";
	QUERY.Datasource = APPLICATION.Datasource;
	QUERY.OrderBy = "prod.Product_Short_Description";
	
	if(IsDefined("URL.pcu") AND ReFindNoCase("[a-z0-9]{32}", URL.pcu)) { //ProductCategory
		QUERY.ProductCategory_UID = URL.pcu;
		QUERY.SelectSQL = "DISTINCT clas.ProductClass, manf.Manufacturer_UID, manf.Manufacturer_Name, manf.Manufacturer_Logo, prod.Product_UID, prod.Product_Short_Description, prod.Supplier_Part_Number, prod.Manufacturer_Part_Number, prod.UOM, prod.Product_Image_Thum, prod.Retail_Price, prod.Exchange_Price, prod.Repair_Price, prod.Refurbished_Price, prod.Recondition_Price, prod.Product_IsActive, cats.ProductCategory AS thisCategory";
	}
	if(IsDefined("URL.ptu") AND ReFindNoCase("[a-z0-9]{32}", URL.ptu)) { //ProductType
		if(URL.ptl EQ 99){
			//QUERY.ProductClass_ID = URL.ptl;
			QUERY.Repair = 99;
		}else{
			QUERY.ProductType_UID = URL.ptu;
		}
		QUERY.SelectSQL = "DISTINCT clas.ProductClass, manf.Manufacturer_UID, manf.Manufacturer_Name, manf.Manufacturer_Logo, prod.Product_UID, prod.Product_Short_Description, prod.Supplier_Part_Number, prod.Manufacturer_Part_Number, prod.UOM, prod.Product_Image_Thum, prod.Retail_Price, prod.Exchange_Price, prod.Repair_Price, prod.Refurbished_Price, prod.Recondition_Price, prod.Product_IsActive, type.ProductType_UID, type.ProductType AS thisCategory";
	}
	if(IsDefined("URL.ptl") AND IsNumeric(URL.ptl)) { //ProductClass
		if(URL.ptl EQ 99){
			//QUERY.ProductClass_ID = URL.ptl;
			QUERY.Repair = 99;
		}else{
			QUERY.ProductClass_ID = URL.ptl;	
		}
		//if URL.ptl exists, strip it out of Query String
		VARIABLES.cleanURL_String = ReplaceNoCase(VARIABLES.cleanURL_String, "&ptl=#URL.ptl#", "", "all");
	}
	if(IsDefined("URL.pdt") AND ReFindNoCase("[a-z0-9]{32}", URL.pdt)) { //Product Detail
		QUERY.Product_UID = URL.pdt;
		QUERY.SelectSQL = "DISTINCT clas.ProductClass, manf.Manufacturer_UID, manf.Manufacturer_Name, manf.Manufacturer_Logo, prod.Product_UID, prod.Product_Short_Description, prod.Supplier_Part_Number, prod.Manufacturer_Part_Number, prod.UOM, prod.Product_Image_Thum, prod.Retail_Price, prod.Exchange_Price, prod.Repair_Price, prod.Refurbished_Price, prod.Recondition_Price, prod.Product_IsActive, type.ProductType AS thisCategory";
	}
</cfscript>

<cfinclude template="./_qry_select_cat_type_product.cfm">

<cfif IsDefined("URL.ptl") AND IsNumeric(URL.ptl)>
	<cfscript>
		QUERY = StructNew();
		QUERY.QueryName = "qry_getThisClasses";
		QUERY.Datasource = APPLICATION.Datasource;
		
		QUERY.ProductClass_ID = URL.ptl;
		QUERY.OrderBy = "clas.ProductClass";
	</cfscript>
	
	<cfquery name="#QUERY.QueryName#" datasource="#QUERY.Datasource#">
		SELECT clas.*
		FROM ltbl_Product_Class clas
		WHERE 0=0
		<cfif StructKeyExists(QUERY, "ProductClass_ID")>
		AND clas.ProductClass_ID IN (<cfqueryparam cfsqltype="cf_sql_integer" value="#QUERY.ProductClass_ID#" list="yes">)
		</cfif>
		ORDER BY #QUERY.OrderBy#
	</cfquery>
	<cfif StructClear(QUERY)></cfif>
</cfif>

<cfscript>
	Attribs = StructNew();
	Attribs.Str_Row = URL.o;
	Attribs.Tot_Row = URL.lm;
	Attribs.Totals = qry_getProducts_Total.Records;
	
	recStr = Evaluate(URL.o+1);
	recEnd = Evaluate(URL.lm+URL.o);
	if(recEnd GT qry_getProducts.RecordCount) {
		recEnd = qry_getProducts.RecordCount;
	}
</cfscript>

<cfset numCols = 5>
<cfset currRow = 1>
</cfsilent>

<cfoutput>
<div class="products clear">
	<h2 class="ctrail">
         <!---20161003 HRS Change to 3M logo to use text per Ticket_#77518 --->
    	<cfif qry_getProductType.Manufacturer_UID eq 'XG822OZAORX6P4KPMU4I0OHWDJQASWU4'>
        	<a href="./?pg=Product&pmu=#qry_getProductType.Manufacturer_UID#" class="small_text_logo_3m">3M<sup>&reg;&nbsp;*</sup></a>
    	<cfelse>
		<a href="./?pg=Product&pmu=#qry_getProductType.Manufacturer_UID#"><cfif FileExists("#APPLICATION.rootDir#\images\#qry_getProductType.Manufacturer_Logo#")><img src="/images/#qry_getProductType.Manufacturer_Logo#" style="height:15px;width:auto;" /><cfelse><img src="/images/noimage.gif" /></cfif></a>
		</cfif>
        <!--- End Ticket_#77518 --->    
		<cfif ((Len(URL.ptu) GT 0) AND (qry_getProductType.thisCategory IS NOT ""))><span><a href="./?pg=Product&pmu=#qry_getProductType.Manufacturer_UID#" class="close_filter">#qry_getProductType.thisCategory#</a></span></cfif>
		<cfif ((Len(URL.ptl) GT 0) AND (qry_getThisClasses.ProductClass IS NOT ""))><span>&gt; <a href="./?#VARIABLES.cleanURL_String#" class="close_filter">#qry_getThisClasses.ProductClass#</a></span></cfif>
	</h2>
	<div class="prod_filter clear">
		<h3>Filter By Product Type</h3>
		<table>
			<tr><cfset VARIABLES.cleanURL_String = ReReplaceNoCase(VARIABLES.cleanURL_String, "(&o=[0-9]{1,})", "", "all")>
				<cfloop query="qry_getAllClasses"><td><a href="./?#VARIABLES.cleanURL_String#&ptl=#ProductClass_ID#">#ProductClass#</a></td><cfif currRow EQ Evaluate(qry_getAllClasses.CurrentRow/numCols)><cfset currRow = currRow+1></tr><tr></tr></cfif></cfloop>
			
			<tr><!---20131206 CG: Added Repair filter per Ticket#47002 --->
				<td><a href="./?#VARIABLES.cleanURL_String#&ptl=99">Repairs</a></td>
			</tr>
		</table>
	</div>
    
	<cfinclude template="./_tmp_product_list.cfm">
    
	<cfif IsDefined("URL.ptu") AND ReFindNoCase("[a-z0-9]{32}", URL.ptu)>
		<cfsilent>
		<cfscript>
			QUERY = StructNew();
			QUERY.QueryName = "qry_getProductTypeDesc";
			QUERY.Datasource = APPLICATION.Datasource;
			QUERY.ProductType_UID = URL.ptu;
			QUERY.OrderBy = "prod.Product_Short_Description";
		</cfscript>
		<cfinclude template="./_qry_select_cat_type_product.cfm">
		</cfsilent>
		<cfif qry_getProductTypeDesc.RecordCount NEQ 0>
		<div class="info homefront clear">
			<div>
				#qry_getProductTypeDesc.ProductType_Description#
			</div>
		</div>
		</cfif>
	</cfif>
</div>
</cfoutput>