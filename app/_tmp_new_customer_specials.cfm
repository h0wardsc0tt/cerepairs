<cfoutput>
<div class="info">
	<h1 class="pagetitle">NEW CUSTOMER<br>DRIVE-THRU REPAIR SPECIALS</h1>
	<div class="single clear">
		<div class="plug">
			<div class="right_textplug">
				<h2><a href="https://#APPLICATION.rootURL#/files/protection-plan-flyer.pdf" target="_blank">First Month Free on a CE Drive-Thru Protection Plan</a></h2>
				<p>Stop worrying about your next potential repair and get peace of mind with a CE Drive-Thru Protection Plan. Protection Plans offer unlimited repairs at one low monthly payment, and if you sign up now, your first month is on us! Protection Plans cover all models and brands. Call 877-731-0334 for details.</p>
			</div>
			<div class="right_imgplug">
				<img src="/images/pages/ceprotection.jpg" />
			</div>
			<div class="plug_border">
				<img src="/images/pages/Special_GreyFadedRule.jpg" />
			</div>
		</div>
		<div class="plug">
			<div class="right_textplug">
				<h2><a href="https://#APPLICATION.rootURL#/half-off-aio-repairs">Drive-Thru Headset and Belt-Pac Repair Special</a></h2>
				<p>Try us out! As a first time customer with CE, you'll receive up to 50% off any all-in-one headset or belt-pac repair for any HME, Panasonic, or 3M drive-thru headset system. Take advantage of our quality repairs with same-day service and a 4-month warranty. Some restrictions and conditions apply. Call 877-731-0334 for details.</p>
			</div>
			<div class="right_imgplug">
				<img src="/images/pages/Special_RepairSpecial.jpg" />
			</div>
			<div class="plug_border">
				<img src="/images/pages/Special_GreyFadedRule.jpg" />
			</div>
		</div>
		<!---20140114 CG: Removed Headset special per Ticket#48303 --->
		<!---<div class="plug">
			<div class="right_textplug">
				<h2><a href="https://#APPLICATION.rootURL#/freeheadset">Free Drive-Thru Headset Special</a></h2>
				<p>Time to buy new headsets? Purchase 2 CE branded headsets for any HME, Panasonic, or 3M system and get a 3<sup>rd</sup> headset free. Offer valid on CE branded headsets only. Free headset must be of equal or lesser value.</p>
			</div>
			<div class="right_imgplug">
				<img src="/images/pages/Special_FreeHeadset.jpg" />
			</div>
			<div class="plug_border">
				<img src="/images/pages/Special_GreyFadedRule.jpg" />
			</div>
		</div>--->
		<div class="plug">
			<div class="right_textplug">
				<h1><a href="https://#APPLICATION.rootURL#/freebattery">Free Drive-Thru Battery Special</a></h1>
				<p>Need batteries? Get a free battery from CE! We know that batteries are a necessity for your drive-thru, and it's our job to keep you stocked. Purchase 3 CE branded batteries for any HME, Panasonic, or 3M system and get your 4<sup>th</sup> battery free. Offer valid on CE branded batteries only. Free battery must be of equal or lesser value.</p>
			</div>
			<div class="right_imgplug">
				<img src="/images/pages/Special_FreeBattery.jpg" />
			</div>
			<div class="plug_border">
				<img src="/images/pages/Special_GreyFadedRule.jpg" />
			</div>
		</div>
	</div>
</div>
</cfoutput>
