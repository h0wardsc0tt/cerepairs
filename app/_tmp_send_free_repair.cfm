<cfoutput>
<div class="info">
	<h1 class="pagetitle">DRIVE-THRU REPAIRS<br>FREE INBOUND SHIPPING</h1>
	<div class="single clear">
		<div class="plug">
			<h2>Drive-Thru Repair Form - Quick and Easy!</h2>
			<p>Fill out the form below, and we'll provide free inbound shipping labels - it couldn't be easier.</p>
		</div>
		<cfinclude template="./_tmp_ship_form.cfm">
		<div class="plug">
			<h2>Drive-Thru Headset Repair and Equipment Repair - 100% Guaranteed</h2>
			<p>Some repair companies will claim they can't fix something, just to sell you new equipment. Not at CE. If we can't fix it, you'll receive a $100 credit toward the purchase of a new unit, or $65 toward the purchase of a refurbished unit. We fix what others can't, or won't.</p>
		</div>
		<div class="plug">
			<h2><a href="https://#APPLICATION.rootURL#/same-day-service">Same-Day Service on Drive-Thru Repairs or It's FREE</a></h2>
			<p>We know that you have a business to run, and you don't have time for broken equipment. That's why in most cases we'll repair your product within 24 hours of receiving it or the repair is FREE. Some limitations and exclusions apply.</p>
		</div>
	</div>
</div>
</cfoutput>

