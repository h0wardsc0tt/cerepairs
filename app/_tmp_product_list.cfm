<cfsilent>
<cfparam name="PageIsWishlist" default="false">
<cfscript>
	if(PageIsWishlist) {
		numCols = 6;
		noResults = "You have no items on your wishlist";
	} else {
		numCols = 5;
		noResults = "Your search returned no results";
	}
</cfscript>

</cfsilent>
<cfsavecontent variable="AddToCart">
<!---Raw Javascript Obfuscate code before making live.--->
<script type="text/javascript"> 
	<cfoutput>
	function html_CartCount(data) {
		var ret_result = "";
		$.each(data, function(index, data) {
			if(data.carttotal != "fail") {
				var cartcount = document.getElementById("cartcount");
				cartcount.innerHTML = " (" + data.carttotal + ")";
				
				/*if($("html").hasClass("IE8")) {
					alert("Product Successfully Added!");
				} else {
					var $dialog = $('.show_cart').dialog({
							dialogClass:'ui-dialog',
							draggable:false,
							autoOpen:false,
							resizeable:false,
							title:'hello'
						});
					$(".ui-dialog").addClass("itemadded itemstyle");
					$(".close_cart").css("display","block");
					$dialog.load("/js/quick_cart_view.cfm").dialog("open");
				}*/
				ret_result = "pass";
				window.location.href = "https://#APPLICATION.rootURL#/?pg=Cart";
			} else {
				ret_result = "fail";
			}
		});
		
		return ret_result;
	}
	function closeAdd() {
		//$(".itemadded").dialog('close');
		$('.show_cart').dialog('close');
		$('.close_cart').css('display','none');
		
		return false;
	}
	function addCart(field_id,prc) {
		var jadd_act = "/js/addCart.cfm";
		$.ajax({
			url: jadd_act, 
			data: ({ sess: '#SESSION.Session_UID#', pdt: field_id, qty: 1, uco: '#COOKIE.User_Cookie#', prc:prc }),
			dataType: "json",
			success: function(data) {
				var set_msg = html_CartCount(data);
				if(set_msg == "fail") {
					alert("I'm sorry your current shopping session has expired.");
					window.location.href = "https://#APPLICATION.rootURL#";
				}
			}
		});
	}
	</cfoutput>
</script>
</cfsavecontent>
<cfhtmlhead text="#AddToCart#">	
<cfoutput>
<table>
	<tr>
		<th class="thm"></th>
		<th class="thp">Product Name</th>
		<th class="thm">Part Number</th>
		<!---20131206 CG: Changed Header Content per Ticket#47002 --->
		<th>Repair/Product Pricing</th>
		<cfif PageIsWishlist><th></th></cfif>
	</tr>
	<cfif qry_getProducts.RecordCount NEQ 0><cfloop query="qry_getProducts" startrow="#recStr#" endrow="#recEnd#"><cfif Product_IsActive EQ 1>
	<tr class="#IIF(CurrentRow MOD 2, DE('noshade'), DE('shade'))#">
		<td><a href="./?pg=Detail&pdt=#Product_UID#"><cfif FileExists("#APPLICATION.rootDir#\images\products\#Product_Image_Thum#")><img src="/images/products/#Product_Image_Thum#" class="p_thumb" /><cfelse><img src="/images/products/noimage.png" class="p_thumb" /></cfif></a></td>
		<td class="prod_short_desc prod_list_desc"><a href="./?pg=Detail&pdt=#Product_UID#">#Product_Short_Description#</a></td>
		<td class="manf_part"><strong>#Manufacturer_Part_Number#</strong></td>
		<td class="prod_price_list">
			<!---<cfif Repair_Price NEQ 0><strong>Repair: #DollarFormat(Repair_Price)#</strong><br /></cfif>
			<!---20140114 CG: Changed "Reconditioned" to "Recondition" per Ticket#48326 --->
			<cfif Recondition_Price NEQ 0><strong>Recondition: #DollarFormat(Recondition_Price)#</strong><br /></cfif>
			<cfif Retail_Price NEQ 0>
				<strong><!---20140617 ATN: Unused instead of New just for 3M per ticket#52590--->
					<cfif Left(qry_getProducts.Product_Short_Description,2) IS "3M">Unused:<cfelse>New:</cfif>#DollarFormat(Retail_Price)#
				</strong><br />
			</cfif>
			<cfif Refurbished_Price NEQ 0><strong>Refurbished: #DollarFormat(Refurbished_Price)#</strong><br /></cfif>
			<cfif Exchange_Price NEQ 0><strong>Advance Exchange: #DollarFormat(Exchange_Price)#</strong><br /></cfif>--->
            
            <table class="price_list_inner">
				<!---2016-05-09 ATN: Changed logic to OR instead of AND 
				<cfif Repair_Price NEQ 0 AND Recondition_Price NEQ 0>--->
            	<cfif Repair_Price NEQ 0 AND Recondition_Price NEQ 0>
                    <tr>
                        <td class="ta-left"><strong>REPAIR</strong></td>
                    </tr>
                    <tr>
                        <td class="ta-left">
                            Standard/Recondition
                            <span class="tooltip" style="padding: 0px;">
                                <img src="/images/tooltip.png" width="10">
                                <div class="tooltip">
                                	<h5>Standard/Recondition</h5>
                                    <p>Basic functional issues will incur a repair fee. Repairs needing new case parts, button, gaskets or headset cords will be charged a reconditioning fee based on technician's assessment. In either case, a reconditioned replacement item may be returned to you.</p>
                                </div>
                            </span>
                        </td>
                        <td class="ta-right"><a class="send-it-in gray-text" href="./?pg=SendRepair&pdt=#Product_UID#">$#NumberFormat(Repair_Price)#/$#NumberFormat(Recondition_Price)# <span class="thin-text">Send it in</span></a></td>
                    </tr>
                    <cfif Exchange_Price EQ 0>
                    	<tr><td>&nbsp;</td></tr>
                    </cfif>
                </cfif>
             	<cfif Repair_Price NEQ 0 AND Recondition_Price EQ 0>
                    <tr>
                        <td class="ta-left"><strong>REPAIR</strong></td>
                    </tr>
                    <tr>
                        <td class="ta-left">
                            Standard
                            <span class="tooltip" style="padding: 0px;">
                                <img src="/images/tooltip.png" width="10">
                                <div class="tooltip">
                                	<h5>Standard</h5>
                                    <p>Basic functional issues will incur a repair fee. Repairs needing new case parts, button, gaskets or headset cords will be charged a reconditioning fee based on technician's assessment. In either case, a reconditioned replacement item may be returned to you.</p>
                                </div>
                            </span>
                        </td>
                        <td class="ta-right"><a class="send-it-in gray-text" href="./?pg=SendRepair&pdt=#Product_UID#">$#NumberFormat(Repair_Price)# <span class="thin-text">Send it in</span></a></td>
                    </tr>
                    <cfif Exchange_Price EQ 0>
                    	<tr><td>&nbsp;</td></tr>
                    </cfif>
                </cfif>
            	<cfif Repair_Price EQ 0 AND Recondition_Price NEQ 0>
                    <tr>
                        <td class="ta-left"><strong>REPAIR</strong></td>
                    </tr>
                    <tr>
                        <td class="ta-left">
                            Recondition
                            <span class="tooltip" style="padding: 0px;">
                                <img src="/images/tooltip.png" width="10">
                                <div class="tooltip">
                                	<h5>Recondition</h5>
                                    <p>Basic functional issues will incur a repair fee. Repairs needing new case parts, button, gaskets or headset cords will be charged a reconditioning fee based on technician's assessment. In either case, a reconditioned replacement item may be returned to you.</p>
                                </div>
                            </span>
                        </td>
                        <td class="ta-right"><a class="send-it-in gray-text" href="./?pg=SendRepair&pdt=#Product_UID#">$$#NumberFormat(Recondition_Price)# <span class="thin-text">Send it in</span></a></td>
                    </tr>
                    <cfif Exchange_Price EQ 0>
                    	<tr><td>&nbsp;</td></tr>
                    </cfif>
                </cfif>
               <cfif Exchange_Price NEQ 0>
                	<tr>
                    	<td class="ta-left">
                            Adv. Exchange
                            <span class="tooltip" style="padding: 0px;">
                                <img src="/images/tooltip.png" width="10">
                                <div class="tooltip">
                                    <h5>Advance Exchange</h5>
                                    <p>If you can't wait for a repair, CE will overnight you a refurbished unit in exchange for your broken unit.  Once you receive the replacement unit, return your broken unit in the same box. <div style="padding:12pt 0 12pt 0;">A minimum $35 shipping fee will apply.</div><div>Additional charges may apply for expedited shipping needs.</div></p>
                                </div>
                            </span>
                        </td>
                        <td class="ta-right gray-text"><strong>$#NumberFormat(Exchange_Price)# </strong><span class="thin-text">Call 877-731-0334</span></td>
                    </tr>
                    <tr><td>&nbsp;</td></tr>
                </cfif>
                <cfif Retail_Price NEQ 0>
                    <tr>
                        <td class="ta-left"><a href="javascript:void(false);" onclick="return addCart('#Product_UID#', 'Retail');">BUY <cfif Left(qry_getProducts.Product_Short_Description,2) IS "3M">Unused<cfelse>New</cfif></a></td>
                        <td class="ta-right"><a class="add-to-cart" href="javascript:void(false);" onclick="return addCart('#Product_UID#', 'Retail');">#DollarFormat(Retail_Price)#</a></td>
                    </tr>
                </cfif>
                <cfif Refurbished_Price NEQ 0>
                	<tr>
                    	<td class="ta-left"><a href="javascript:void(false);" onclick="return addCart('#Product_UID#', 'Refurbished');">BUY Refurbished</a></td>
                        <td class="ta-right"><a class="add-to-cart" href="javascript:void(false);" onclick="return addCart('#Product_UID#', 'Refurbished');">#DollarFormat(Refurbished_Price)#</a></td>
                    </tr>
                </cfif>
            </table>
		</td>
		<cfif PageIsWishlist><td><a href="./?pg=Wishlist&st=Del&pdt=#Product_UID#">Delete</a></td></cfif>
	</tr></cfif></cfloop>
	<tr>
		<td colspan="#numCols#">
			<cfmodule template="./_mod_next_prev.cfm" OFFSET="#URL.o#" LIMIT="#URL.lm#" RECORDCOUNT="#Evaluate((URL.lm+URL.o))#" TOTALCOUNT="#qry_getProducts_Total.Records#" POST="true">
		</td>
	</tr><cfelse>
	<tr>
		<td colspan="#numCols#"><h3>#noResults#</h3></td>
	</tr></cfif>
</table>
</cfoutput>
<div class="clear"></div>
<!---<cfinclude template="./_tmp_product_recommended.cfm">--->